package chat.hola.com.howdoo.post;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.widget.ShareDialog;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.gson.Gson;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AACTrackImpl;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import com.howdoo.dubly.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Database.PostDb;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Dialog.ChannelPicker;
import chat.hola.com.howdoo.Utilities.App_permission_23;
import chat.hola.com.howdoo.Utilities.ConnectivityReceiver;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.SocialShare;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.category.CategoryActivity;
import chat.hola.com.howdoo.club.clubs.ClubActivity;
import chat.hola.com.howdoo.hastag.AutoCompleteTextView;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.location.Address_list_holder;
import chat.hola.com.howdoo.location.Location_Search_Activity;
import chat.hola.com.howdoo.location.Location_service;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.post.model.AddressAdapter;
import chat.hola.com.howdoo.post.model.CategoryData;
import chat.hola.com.howdoo.post.model.ChannelData;
import chat.hola.com.howdoo.post.model.Post;
import chat.hola.com.howdoo.post.model.PostData;
import dagger.android.support.DaggerAppCompatActivity;


/**
 * <h2>PostActivity</h2>
 * <p>It uploads media to cloudinary server and creates a post</p>
 *
 * @author 3Embed
 * @since 2/26/2018
 */

public class PostActivity extends DaggerAppCompatActivity implements App_permission_23.Permission_Callback,
        Location_service.GetLocationListener, PostContract.View, ChannelPicker.ChannelSelectCallback, AddressAdapter.ClickListner,
        CompoundButton.OnCheckedChangeListener, ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = "PostActivity";
    private CallbackManager callbackManager;

    @Inject
    AlertDialog.Builder reportDialog;
    @Inject
    PostPresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    ChannelPicker channelPicker;
    @Inject
    SocialShare socialShare;

    @BindView(R.id.ivPreview)
    ImageView ivPreview;
    @BindView(R.id.video_icon)
    ImageView video_icon;
    @BindView(R.id.vidViewPreview)
    VideoView vidViewPreview;
    @BindView(R.id.sdPreview)
    SimpleDraweeView sdPreview;
    @BindView(R.id.etPostTitle)
    AutoCompleteTextView etPostTitle;
    @BindView(R.id.actionBarRl)
    android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.tvAddLocation)
    TextView tvAddLocation;
    @BindView(R.id.tvCategory)
    TextView tvCategory;
    @BindView(R.id.tvClub)
    TextView tvClub;
    @BindView(R.id.tvAddToMyChannel)
    TextView tvAddToMyChannel;
    @BindView(R.id.recyclerChannel)
    RecyclerView recyclerChannel;
    @BindView(R.id.tvShare)
    TextView tvShare;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.btnPublish)
    FloatingActionButton btnPublish;
    @BindView(R.id.switchAddToMyChannel)
    SwitchCompat switchAddToMyChannel;
    @BindView(R.id.rvRecentAddress)
    RecyclerView rvRecentAddress;
    @BindView(R.id.ibClose)
    ImageButton ibClose;
    @BindView(R.id.switchFacebook)
    SwitchCompat switchFacebook;
    @BindView(R.id.switchInsta)
    SwitchCompat switchInsta;
    @BindView(R.id.switchTwitter)
    SwitchCompat switchTwitter;
    @BindView(R.id.llChannel)
    RelativeLayout llChannel;

    @BindView(R.id.tvAddCategory)
    TextView tvAddCategory;
    @BindView(R.id.llShare)
    LinearLayout llShare;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.switchHome)
    SwitchCompat switchHome;

    private Unbinder unbinder;
    private String path;
    private String type;
    private AddressAdapter addressAdapter;
    private Address_list_holder addresslist;
    private boolean first;
    private String categoryId = "";
    //    private String channelId = "";
    private String musicId = "";
    private String clubId;
    private int access;
    private boolean publishAsHome;
    PostData postData = new PostData();
    Post post = new Post();
    PostDb db = new PostDb(this);
    @Inject
    SessionManager sessionManager;
    private InputMethodManager imm;
    @Inject
    BlockDialog dialog;
    private static final String[] ACCESS_OPTIONS = {"Public", "Private", "Exclusive"};

    @Override
    public void userBlocked() {
        dialog.show();
    }

    Data data = null;
    boolean isEdit = false;
    Map<String, String> map = new HashMap<>();

    ProgressDialog progressDialog;
    ArrayList<String> files;
    String audioFile;

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        unbinder = ButterKnife.bind(this);

        files = getIntent().getStringArrayListExtra("videoArray");
        audioFile = getIntent().getStringExtra("audio");
        post.setFiles(files);
        post.setAudioFile(audioFile);


        progressDialog = new ProgressDialog(this);
        //init the facebook callback manager
        callbackManager = CallbackManager.Factory.create();

        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        etPostTitle.requestFocus();
        etPostTitle.setOnItemClickListener((adapterView, view, i, l) -> {
            etPostTitle.setText(etPostTitle.getText().toString().replace("##", "#"));
            etPostTitle.setSelection(etPostTitle.getText().length());
        });
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        }

        data = (Data) getIntent().getSerializableExtra("data");
        isEdit = getIntent().getStringExtra("call") != null;

        // llShare.setVisibility(isEdit ? View.GONE : View.VISIBLE);

        type = getIntent().getStringExtra(Constants.Post.TYPE);
        if (!isEdit) {
            path = getIntent().getStringExtra(Constants.Post.PATH).replace(Constants.Post.PATH_FILE, Constants.EMPTY);
            musicId = getIntent().getStringExtra("musicId");
            first = true;

            presenter.init(path, type);
            post.setPathForCloudinary(path);
            post.setTypeForCloudinary(type);
            post.setMusicId(musicId);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ACCESS_OPTIONS);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    clubId = null;
                    tvClub.setText("Select club");
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            path = data.getImageUrl1();
            spinner.setEnabled(false);
            ivPreview.setVisibility(View.GONE);
            vidViewPreview.setVisibility(View.GONE);
            sdPreview.setVisibility(View.VISIBLE);

            tvLocation.setVisibility(View.VISIBLE);
            tvAddress.setVisibility(View.VISIBLE);
            video_icon.setVisibility(type.equals(Constants.Post.IMAGE) ? View.GONE : View.VISIBLE);

            sdPreview.setImageURI(Uri.parse(data.getImageUrl1().replace("mp4", "jpg")));
            etPostTitle.setText(data.getTitle());
            tvLocation.setText(data.getPlace());
            tvAddress.setText(data.getCity());
            tvCategory.setText(data.getCategoryName());
            categoryId = data.getCategoryId();
            clubId = data.getClubId();
            if (data.getClubName() != null && !data.getClubName().isEmpty())
                tvClub.setText(data.getClubName());
            etPostTitle.setSelection(etPostTitle.getText().length());
            switchAddToMyChannel.setChecked(true);
        }

        setAddressList();
        presenter.getChannels();
        updateUi();

        switchAddToMyChannel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (switchAddToMyChannel.isChecked()) {
                    post.setStory(true);
                } else {
                    post.setStory(false);
                }
            }
        });

        switchInsta.setOnCheckedChangeListener(this);
        switchFacebook.setOnCheckedChangeListener(this);
        switchTwitter.setOnCheckedChangeListener(this);
        switchAddToMyChannel.setOnCheckedChangeListener(this);
        showKeyboard();
        recyclerChannel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
            }
        });
        presenter.getCategories();


    }


    private void showKeyboard() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                etPostTitle.requestFocus();
                if (imm != null) {
                    imm.showSoftInput(etPostTitle, InputMethodManager.SHOW_FORCED);
                }
            }
        }, 200);
    }

    private void hideKeyBoard() {
        if (imm != null)
            imm.hideSoftInputFromWindow(etPostTitle.getWindowToken(), 0);
    }

    @OnClick(R.id.ibClose)
    public void close() {
        ibClose.setVisibility(View.GONE);
        tvLocation.setVisibility(View.GONE);
        tvAddress.setVisibility(View.GONE);
        rvRecentAddress.setVisibility(View.VISIBLE);
    }

    //@OnCheckedChanged(R.id.switchAddToMyChannel)
    public void clickSwitchAddToMyChannel(boolean isChecked) {
        if (switchAddToMyChannel.isChecked())
            recyclerChannel.setVisibility(View.VISIBLE);
        else
            recyclerChannel.setVisibility(View.GONE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setAddressList() {
        SessionManager sessionManager = new SessionManager(this);
        addresslist = new Gson().fromJson(sessionManager.getAdresses(), Address_list_holder.class);

        if (addresslist != null && addresslist.getList_of_address().size() > 0) {
            if (first) {
                rvRecentAddress.setVisibility(View.VISIBLE);
                // tvAddLocation.setVisibility(View.VISIBLE);
            }
            addressAdapter = new AddressAdapter(addresslist.getList_of_address());
            addressAdapter.setListener(this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            rvRecentAddress.setLayoutManager(mLayoutManager);
            rvRecentAddress.setItemAnimator(new DefaultItemAnimator());
            rvRecentAddress.setAdapter(addressAdapter);
        } else {
            rvRecentAddress.setVisibility(View.GONE);
            // tvAddLocation.setVisibility(View.GONE);
        }
    }

    private void updateUi() {
        llChannel.setVisibility(View.GONE);
        etPostTitle.setListener(new AutoCompleteTextView.AutoTxtCallback() {
            @Override
            public void onHashTag(String tag) {
                Log.d("erd1", "" + tag);
                presenter.searchHashTag(tag);
            }

            @Override
            public void onUserSearch(String tag) {
                presenter.searchUserTag(tag);
            }

            @Override
            public void onClear() {

            }
        });


    }

    @Override
    public void setUser(Hash_tag_people_pojo tag) {
        etPostTitle.updateUserSearch(tag);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.disposeObservable();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @OnClick(R.id.tvAddLocation)
    public void location() {
        hideKeyBoard();
        Intent intent = new Intent(this, Location_Search_Activity.class);
        startActivityForResult(intent, 1);
    }

    @OnClick(R.id.btnPublish)
    public void publish() {
        if (type.equals(Constants.Post.VIDEO))
            post.setDub(true);
        postIt();

    }

    private void facebookShare() {
        if (!sessionManager.isFacebookLogin() || !sessionManager.isFacebookPublishPermission()) {
            //login with facebook
            facebookLogin();
        } else {
            //share to facebook
            File imgFile = new File(path);

            ShareContent shareContent = new ShareMediaContent.Builder()
                    .addMedium(type.equals(Constants.Post.VIDEO) ?
                            new ShareVideo.Builder().setLocalUrl(Uri.fromFile(imgFile)).build() :
                            new SharePhoto.Builder().setBitmap(getBitmapImage(imgFile.getAbsolutePath())).build())
                    .build();

            ShareDialog shareDialog = new ShareDialog(this);
            shareDialog.registerCallback(callbackManager, shareResultFacebookCallback);
            shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
        }

    }

    //converts path to bitmap image
    private Bitmap getBitmapImage(String path) {
        return BitmapFactory.decodeFile(path);
    }


    @Override
    public void applyFont() {
        etPostTitle.setTypeface(typefaceManager.getSemiboldFont());

        tvAddLocation.setTypeface(typefaceManager.getMediumFont());
        tvAddCategory.setTypeface(typefaceManager.getMediumFont());
        tvCategory.setTypeface(typefaceManager.getMediumFont());
        tvAddToMyChannel.setTypeface(typefaceManager.getMediumFont());
        tvShare.setTypeface(typefaceManager.getMediumFont());

        presenter.getCategories();
    }

    @Override
    public void onBackPressed() {
        hideKeyBoard();
        super.onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 123 && resultCode == RESULT_OK) {
            tvClub.setText(data.getStringExtra("club"));
            post.setClubId(data.getStringExtra("clubId"));
            post.setClubName(data.getStringExtra("club"));
            clubId = post.getClubId();
        } else if (requestCode == 222 && resultCode == RESULT_OK) {
            tvCategory.setText(data.getStringExtra("category"));
            post.setCategoryId(data.getStringExtra("category_id"));
            categoryId = post.getCategoryId();
        } else if (requestCode == 1 && resultCode == RESULT_OK) {

            post.setLatitude(data.getStringExtra("latitude"));
            post.setLongitude(data.getStringExtra("longitude"));
            post.setLocation(data.getStringExtra("locationName"));

            map.put("longitude", data.getStringExtra("latitude"));
            map.put("latitude", data.getStringExtra("longitude"));
            map.put("place", data.getStringExtra("locationName"));

            setAddress(data.getStringExtra("locationName"), data.getStringExtra("locationDetails"));
            first = false;
            setAddressList();
            if (data.getStringExtra("latitude") != null && data.getStringExtra("longitude") != null)
                getCityAndCountry(Double.parseDouble(data.getStringExtra("latitude")), Double.parseDouble(data.getStringExtra("longitude")));
        }
    }

    void getCityAndCountry(double lat, double lng) {
        try {
            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = gcd.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0) {
                post.setCountrySname(addresses.get(0).getCountryCode());
                post.setCity(addresses.get(0).getAddressLine(0));
                map.put("countrySname", addresses.get(0).getCountryCode());
                map.put("city", addresses.get(0).getAddressLine(0));
            }
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    private void setAddress(String locationName, String locationDetails) {
        rvRecentAddress.setVisibility(View.GONE);
        //  tvAddLocation.setVisibility(View.GONE);
        ibClose.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(locationName)) {
            tvLocation.setVisibility(View.VISIBLE);
            tvLocation.setText(locationName);
        }

        if (!TextUtils.isEmpty(locationDetails)) {
            tvAddress.setVisibility(View.VISIBLE);
            tvAddress.setText(locationDetails);
        }
    }

    @Override
    public void displayMedia() {
        btnPublish.setEnabled(true);
        if (type.equals(Constants.Post.IMAGE)) {
            ivPreview.setVisibility(View.VISIBLE);
            vidViewPreview.setVisibility(View.GONE);
            video_icon.setVisibility(View.GONE);
            File imgFile = new File(path);
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ivPreview.setImageBitmap(myBitmap);
        } else {
            ivPreview.setVisibility(View.GONE);
            vidViewPreview.setVisibility(View.VISIBLE);
            video_icon.setVisibility(View.VISIBLE);
            MediaController mediaController = new MediaController(PostActivity.this);
            mediaController.setVisibility(View.GONE);
            vidViewPreview.setMediaController(mediaController);
            vidViewPreview.setVideoPath(files.get(0));

            vidViewPreview.setOnPreparedListener(mp -> {
                mp.setLooping(true);
                // videoView.setZOrderOnTop(true);
                vidViewPreview.start();

            });
        }
    }


    @Override
    public void onBackPress() {

    }

    @Override
    public void setTag(Hash_tag_people_pojo response) {
        etPostTitle.updateHashTagDetails(response);
    }

    @Override
    public void attacheCategory(List<CategoryData> data) {
    }

    @Override
    public void attacheChannels(List<ChannelData> data) {
        if (data.size() > 0) {
            llChannel.setVisibility(View.VISIBLE);
            LinearLayoutManager llm = new LinearLayoutManager(this);
            recyclerChannel.setLayoutManager(llm);
            recyclerChannel.setHasFixedSize(true);
            recyclerChannel.setNestedScrollingEnabled(false);
//            recyclerChannel.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            channelPicker.refreshData(this, data, clubId);
            recyclerChannel.setAdapter(channelPicker);
            channelPicker.setChannelSelector(this, clubId);
        } else {
            llChannel.setVisibility(View.GONE);
        }
    }


    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(this, getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        super.onResumeFragments();
    }

    @OnClick({R.id.llCategory, R.id.tvAddCategory, R.id.tvCategory})
    public void addCategory() {
        hideKeyBoard();
        startActivityForResult(new Intent(PostActivity.this, CategoryActivity.class).putExtra("categoryId", categoryId), 222);
    }

    @OnClick({R.id.llClubs, R.id.tvAddClubs, R.id.tvClub})
    public void addClub() {
        hideKeyBoard();
        startActivityForResult(new Intent(PostActivity.this, ClubActivity.class).putExtra("access", spinner.getSelectedItemPosition()).putExtra("forSelect", true), 123);
    }

    @Override
    public void onItemClick(int position) {
        setAddress(addresslist.getList_of_address().get(position).getAddress_title(), addresslist.getList_of_address().get(position).getSub_Address());
        post.setLatitude(addresslist.getList_of_address().get(position).getLatitude());
        post.setLongitude(addresslist.getList_of_address().get(position).getLogitude());
        post.setLocation(addresslist.getList_of_address().get(position).getAddress_title());

        if (addresslist.getList_of_address().get(position).getLatitude() != null && addresslist.getList_of_address().get(position).getLogitude() != null)
            getCityAndCountry(Double.parseDouble(addresslist.getList_of_address().get(position).getLatitude()), Double.parseDouble(addresslist.getList_of_address().get(position).getLogitude()));
    }

    @Override
    public void onclick(String channelId, String categoryId, String categoryName) {
//        this.channelId = channelId;
        this.categoryId = categoryId;
        tvCategory.setText(categoryName);
        post.setChannelId(channelId);
    }

    @Override
    public void updateLocation(Location location) {

    }

    @Override
    public void location_Error(String error) {

    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {

    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {

    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {

    }

    @Override
    public void onPermissionPermanent_Denied(String tag) {

    }

    //share result callback
    FacebookCallback<Sharer.Result> shareResultFacebookCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            //shared successfully
            Log.i("Facebook-Share", "onSuccess: ");
            Toast.makeText(PostActivity.this, "Post shared successfully to Facebook", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel() {
            switchFacebook.setChecked(false);
            Log.i("Facebook-Share", "onCancel: ");
        }

        @Override
        public void onError(FacebookException error) {
            switchFacebook.setChecked(false);
            Log.e("Facebook-Share", "onError: ", error);
            Toast.makeText(PostActivity.this, "Post is not shared successfully to Facebook", Toast.LENGTH_SHORT).show();
        }
    };

    //login result callback
    FacebookCallback<LoginResult> loginResultFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            //facebook login success
            Log.i("Facebook-Login", "onSuccess: ");
            sessionManager.setFacebookAccessToken(loginResult.getAccessToken());
        }

        @Override
        public void onCancel() {
            Log.i("Facebook-Login", "onCancel: ");
            switchFacebook.setChecked(false);
        }

        @Override
        public void onError(FacebookException error) {
            Log.e("Facebook-Login", "onError: ", error);
            switchFacebook.setChecked(false);
            Toast.makeText(PostActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked && buttonView.getId() == R.id.switchFacebook) {
//            facebookShare();
            //share to facebook
            File imgFile = new File(path);

            ShareContent shareContent = new ShareMediaContent.Builder()
                    .addMedium(type.equals("video") ?
                            new ShareVideo.Builder().setLocalUrl(Uri.fromFile(imgFile)).build() :
                            new SharePhoto.Builder().setBitmap(getBitmapImage(imgFile.getAbsolutePath())).build())
                    .build();

            ShareDialog shareDialog = new ShareDialog(this);
            shareDialog.registerCallback(callbackManager, shareResultFacebookCallback);
            shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
        }


        if (isChecked && buttonView.getId() == R.id.switchTwitter && appInstalledOrNot("com.twitter.android", "Twitter")) {
            String type = this.type.equals("video") ? "video/*" : "image/*";

            // Create the new Intent using the 'Send' action.
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setPackage("com.twitter.android");
            // Set the MIME type
            share.setType(type);

            // Create the URI from the media
            File media = new File(path);
            Uri uri = Uri.fromFile(media);

            // Add the URI to the Intent.
            share.putExtra(Intent.EXTRA_STREAM, uri);

            // Broadcast the Intent.
            startActivity(Intent.createChooser(share, etPostTitle.getText().toString()));
        }

        if (isChecked && buttonView.getId() == R.id.switchInsta && appInstalledOrNot("com.instagram.android", "Instagram")) {
            String type = this.type.equals("video") ? "video/*" : "image/*";

            // Create the new Intent using the 'Send' action.
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setPackage("com.instagram.android");
            // Set the MIME type
            share.setType(type);

            // Create the URI from the media
            File media = new File(path);
            Uri uri = Uri.fromFile(media);

            // Add the URI to the Intent.
            share.putExtra(Intent.EXTRA_STREAM, uri);

            // Broadcast the Intent.
            startActivity(Intent.createChooser(share, etPostTitle.getText().toString()));
        }


        if (buttonView.getId() == R.id.switchAddToMyChannel)
            clickSwitchAddToMyChannel(isChecked);

    }

    private boolean appInstalledOrNot(String uri, String name) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "Please install " + name + " app", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    private void facebookLogin() {
        LoginManager.getInstance().registerCallback(callbackManager, loginResultFacebookCallback);
        LoginManager.getInstance().logInWithPublishPermissions(this, Collections.singletonList("publish_actions"));
    }

    @Override
    public void showCategory(List<CategoryData> data) {
//        this.categoryId = data.get(0).getId();
//        tvCategory.setText(data.get(0).getId());
    }

    @Override
    public void updated() {
        startActivity(new Intent(this, LandingActivity.class));
        finish();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Toast.makeText(this, isConnected ? "Internet connected" : "No Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void reload() {

    }

    private void postIt() {
        presenter.init(path, type);
        post.setPathForCloudinary(path);
        post.setTypeForCloudinary(type);

        if (!switchHome.isChecked() && TextUtils.isEmpty(clubId)) {
            Toast.makeText(this, "Please select club or Post as a home...", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(etPostTitle.getText().toString())) {
            Toast.makeText(this, "Please enter caption...", Toast.LENGTH_SHORT).show();
        } else if (etPostTitle.getText().toString().contains("#") && TextUtils.isEmpty(categoryId)) {
            Toast.makeText(this, "Please select category...", Toast.LENGTH_SHORT).show();
        } else {
            String text = etPostTitle.getText().toString();
            String regexPattern = "(#\\w+)";

            Pattern p = Pattern.compile(regexPattern);
            Matcher m = p.matcher(text);
            StringBuilder hashtag = new StringBuilder();
            while (m.find()) {
                hashtag.append(",").append(m.group(1));
            }

            post.setHashTags(hashtag.toString());
            post.setTitle(etPostTitle.getText().toString());
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            post.setId(ts);

            access = spinner.getSelectedItemPosition();
            access++;
            post.setAccess(access);
            post.setClubId(clubId);
            post.setClubName(tvClub.getText().toString());
            post.setPublishAsHome(switchHome.isChecked());

            postData.setId(ts);
            postData.setUserId(AppController.getInstance().getUserId());
            postData.setData(new Gson().toJson(post));
            postData.setStatus(0); //Notstarted

            postData.setFbShare(switchFacebook.isChecked());
            postData.setTwitterShare(switchTwitter.isChecked());

            if (!isEdit) {

                postData.setFbShare(switchFacebook.isChecked());
                postData.setInstaShare(switchTwitter.isChecked());
                postData.setTwitterShare(switchInsta.isChecked());

                db.addData(postData);
                Intent i1 = new Intent(getApplicationContext(), LandingActivity.class);
                i1.setAction(Intent.ACTION_MAIN);
                i1.addCategory(Intent.CATEGORY_HOME);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i1.putExtra("caller", "PostActivity");
                startActivity(i1);
            } else {
                map.put("postId", data.getId());
                map.put("title", etPostTitle.getText().toString());
                map.put("categoryId", categoryId);
                map.put("hashTags", hashtag.toString());
                map.put("musicId", musicId);

                presenter.updatePost(map);
            }
            finish();
        }
//         else if (!tvClub.getText().equals("Select club") && clubId.isEmpty()) {
//            reportDialog.setMessage(R.string.select_channel_confirm);
//            reportDialog.setPositiveButton(R.string.ok, (dialog, which) -> dialog.dismiss());
//            reportDialog.setNegativeButton(R.string.dont_want, (dialog, which) -> switchAddToMyChannel.setChecked(false));
//            reportDialog.create().show();
//        }
    }
}
