package chat.hola.com.howdoo.club.create;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.policy.TimeWindow;
import com.howdoo.dubly.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.ApiOnServer;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.UriUtil;
import chat.hola.com.howdoo.Utilities.Utilities;
import chat.hola.com.howdoo.manager.session.SessionManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ankit on 22/2/18.
 */

public class CreateClubPresenter implements CreateClubContract.Presenter, UploadCallback {

    private final String TAG = CreateClubPresenter.class.getSimpleName();

    @Inject
    CreateClubContract.View view;
    @Inject
    HowdooService service;
    @Inject
    SessionManager sessionManager;
    @Inject
    Context context;

    private String requestId;
    private String picturePath;
    private Uri imageUri;
    private Bitmap bitmapToUpload, bitmap;
    private boolean isProfilePic = true;

    private String profilePic = "";
    private String coverPic = "";
    private String subject;
    private String about;
    private int access;
    private String status;


    @Inject
    public CreateClubPresenter() {
    }


    @Override
    public void init() {
        view.applyFont();
    }


    public void createClubValidate(String subject, String about, int access, String status) {

        if (profilePic == null) {
            view.showSnackMsg(R.string.enter_profile_pic);
        } else if (coverPic == null) {
            view.showSnackMsg(R.string.enter_cover_pic);
        } else {
            this.status = status;
            this.subject = subject;
            this.about = about;
            this.access = access;
            if (access == 2)
                view.addMembers(profilePic, coverPic, subject, status, about, access);
            else
                createClub();
        }
    }

    private void createClub() {
        Map<String, Object> params = new HashMap<>();
        params.put("subject", subject);
        params.put("image", profilePic);
        params.put("coverImage", coverPic);
        params.put("about", about);
        params.put("access", access);
        params.put("status", status);

        service.createClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                view.clubCreated();
                                break;
                            case 401:
                                view.sessionExpired();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    private void uploadPicture(String picturePath) {
        view.showProgress(true);
        try {
            requestId = MediaManager.get().upload(picturePath)
                    .option("folder", "club")
                    .callback(this)
                    .constrain(TimeWindow.immediate())
                    .dispatch();
        } catch (Exception ignored) {
        }
    }

    @Override
    public void launchCamera(PackageManager packageManager) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(packageManager) != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            } else {
                List<ResolveInfo> resInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    context.grantUriPermission(packageName, imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            view.launchCamera(intent);
        } else {
            view.showSnackMsg(R.string.string_61);
        }
    }

    @Override
    public void launchImagePicker() {
        Intent intent = null;
        intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setType("image/*");

        view.launchImagePicker(intent);
    }

    @Override
    public void parseSelectedImage(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                Uri uri = data.getData();
                if (uri == null)
                    return;
                picturePath = UriUtil.getPath(context, uri);
                if (picturePath != null) {
                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(picturePath, options);

                    if (options.outWidth > 0 && options.outHeight > 0) {

                        //launch crop image
                        view.launchCropImage(data.getData());

                    } else {
                        //image can't be selected try another
                        view.showSnackMsg(R.string.string_31);
                    }
                } else {
                    //image can't be selected try another
                    view.showSnackMsg(R.string.string_31);
                }
            } catch (OutOfMemoryError e) {
                //out of mem try again
                view.showSnackMsg(R.string.string_15);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            //image selection canceled.
            view.showSnackMsg(R.string.string_16);
        } else {
            //failed to select image.
            view.showSnackMsg(R.string.string_113);
        }

    }

    @Override
    public void parseCapturedImage(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(picturePath, options);
                if (options.outWidth > 0 && options.outHeight > 0) {
                    //launch crop image
                    view.launchCropImage(imageUri);
                } else {
                    //failed to capture image.
                    picturePath = null;
                    view.showSnackMsg(R.string.string_17);
                }
            } catch (OutOfMemoryError e) {
                //out of mem try again
                picturePath = null;
                view.showSnackMsg(R.string.string_15);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            picturePath = null;
            //img capture canceled.
            view.showSnackMsg(R.string.string_18);
        } else {
            //sorry failed to capture
            picturePath = null;
            view.showSnackMsg(R.string.string_17);
        }
    }

    @Override
    public void parseCropedImage(int requestCode, int resultCode, Intent data) {
        try {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                picturePath = UriUtil.getPath(context, result.getUri());
                if (picturePath != null) {
                    bitmapToUpload = BitmapFactory.decodeFile(picturePath);
                    bitmap = getCircleBitmap(bitmapToUpload);
                    if (bitmap != null && bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
                        uploadPicture(picturePath);
                        if (isProfilePic)
                            view.setProfileImage(bitmap);
                        else
                            view.setCoverPic(bitmap);
                    } else {
                        //sorry failed to capture
                        picturePath = null;
                        view.showSnackMsg(R.string.string_19);
                    }
                } else {
                    //sorry failed to capture
                    picturePath = null;
                    view.showSnackMsg(R.string.string_19);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                //sorry failed to capture
                picturePath = null;
                view.showSnackMsg(R.string.string_19);
            }

        } catch (OutOfMemoryError e) {
            //out of mem try again
            picturePath = null;
            view.showSnackMsg(R.string.string_15);
        }
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {

        try {

            final Bitmap circuleBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(circuleBitmap);

            final int color = Color.GRAY;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
            final RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawOval(rectF, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);


            return circuleBitmap;
        } catch (Exception e) {
            return null;
        }
    }

    private Uri setImageUri() {
        String name = Utilities.tsInGmt();
        name = new Utilities().gmtToEpoch(name);
        File folder = new File(Environment.getExternalStorageDirectory().getPath() + ApiOnServer.IMAGE_CAPTURE_URI);
        if (!folder.exists() && !folder.isDirectory()) {
            folder.mkdirs();
        }
        File file = new File(Environment.getExternalStorageDirectory().getPath() + ApiOnServer.IMAGE_CAPTURE_URI, name + ".jpg");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Uri imgUri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
        this.imageUri = imgUri;
        this.picturePath = file.getAbsolutePath();
        name = null;
        folder = null;
        file = null;
        return imgUri;
    }


    //cloudinary
    @Override
    public void onStart(String requestId) {

    }

    @Override
    public void onProgress(String requestId, long bytes, long totalBytes) {

    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        if (isProfilePic) {
            profilePic = (String) resultData.get(Constants.Post.URL);
        } else {
            coverPic = (String) resultData.get(Constants.Post.URL);
        }
        Log.i(TAG + " Profile pic", "onSuccess: " + profilePic);
        Log.i(TAG + " Cover pic", "onSuccess: " + profilePic);
    }

    @Override
    public void onError(String requestId, ErrorInfo error) {

    }

    @Override
    public void onReschedule(String requestId, ErrorInfo error) {

    }

    public void setPictype(boolean pictype) {
        this.isProfilePic = pictype;
    }

    public void updateClubValidate(String clubId, String name, String about, int access, String status) {
        if (profilePic == null) {
            view.showSnackMsg(R.string.enter_profile_pic);
        } else if (coverPic == null) {
            view.showSnackMsg(R.string.enter_cover_pic);
        } else {
            this.status = status;
            this.subject = subject;
            this.about = about;
            this.access = access;
            updateClub(clubId, name, about, access, status, profilePic, coverPic);
        }
    }

    private void updateClub(String clubId, String name, String about, int access, String status, String profilePic, String coverPic) {
        Map<String, Object> params = new HashMap<>();
        params.put("subject", name);
        params.put("image", profilePic);
        params.put("coverImage", coverPic);
        params.put("about", about);
        params.put("access", access);
        params.put("status", status);
        params.put("clubId", clubId);
        service.updateClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                view.clubUpdated();
                                break;
                            case 401:
                                view.sessionExpired();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
