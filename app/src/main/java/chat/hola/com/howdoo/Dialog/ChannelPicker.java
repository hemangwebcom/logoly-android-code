package chat.hola.com.howdoo.Dialog;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.howdoo.dubly.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.post.model.ChannelData;

/**
 * Created by ankit on 23/2/18.
 */

public class ChannelPicker extends RecyclerView.Adapter<ChannelPicker.ViewHolder> {

    private List<ChannelData> data;
    private int selectedPos = -1;
    private TypefaceManager typefaceManager;
    private Context context;
    private String channelId;
    private ChannelSelectCallback callback;

    public void refreshData(Context context, List<ChannelData> data, String channelId) {
        this.data = data;
        this.context = context;
        if (channelId != null)
            this.channelId = channelId;
        notifyDataSetChanged();
    }

    public ChannelPicker(TypefaceManager typefaceManager) {
        this.typefaceManager = typefaceManager;
    }


    public void setChannelSelector(ChannelSelectCallback channelSelector, String channelId) {
        this.callback = channelSelector;
        this.channelId = channelId;
    }

    public interface ChannelSelectCallback {
        void onclick(String channelId, String categoryId, String categoryName);
    }

    @Override
    public ChannelPicker.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_row, parent, false);
        return new ChannelPicker.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChannelPicker.ViewHolder holder, int position) {
        if (getItemCount() != -1) {
            ChannelData channel = data.get(position);
            Glide.with(context).load(channel.getChannelImageUrl().replace("upload/", Constants.PROFILE_PIC_SHAPE)).asBitmap().centerCrop().placeholder(R.mipmap.hola_ic_launcher).into(new BitmapImageViewTarget(holder.ivRow));
            holder.tvRow.setText(channel.getChannelName());

            if (channelId != null && channel.getId().equals(channelId))
                selectedPos = position;
            holder.cbRow.setChecked(selectedPos == position);
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : -1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ivRow)
        ImageView ivRow;
        @BindView(R.id.tvRow)
        TextView tvRow;
        @BindView(R.id.cbRow)
        CheckBox cbRow;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cbRow.setOnClickListener(this);
            tvRow.setTypeface(typefaceManager.getMediumFont());
        }

        @Override
        public void onClick(View v) {

            if (v.getId() == R.id.cbRow) {
                if (((CheckBox) v).isChecked()) {
                    selectedPos = getAdapterPosition();
                    callback.onclick(data.get(selectedPos).getId(), data.get(selectedPos).getCategoryId(), data.get(selectedPos).getCategoryName());
                    for (int i = 0; i <= getItemCount(); i++)
                        notifyItemChanged(i);
                }
            }
        }

    }
}
