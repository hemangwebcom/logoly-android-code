package chat.hola.com.howdoo.blockUser;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.blockUser.model.ClickListner;
import chat.hola.com.howdoo.blockUser.model.BlockUserResponse;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;
import chat.hola.com.howdoo.models.NetworkConnector;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>BlockUserPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class BlockUserPresenter implements BlockUserContract.Presenter, ClickListner {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;
    @Inject
    HowdooService service;
    @Inject
    BlockUserContract.View view;
    @Inject
    BlockUserModel model;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    BlockUserPresenter() {
    }


    @Override
    public void getBlockedUsers(int offset, int limit) {
        isLoading = true;
        service.getBlockedUser(AppController.getInstance().getApiToken(), Constants.LANGUAGE, offset, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<chat.hola.com.howdoo.blockUser.model.Response>>() {
                    @Override
                    public void onNext(Response<chat.hola.com.howdoo.blockUser.model.Response> response) {
                        try {
                            if (response.code() == 200) {
                                isLastPage = response.body().getData().size() < PAGE_SIZE;
                                if (offset == 0)
                                    model.clearList();
                                model.setData(response.body().getData());
                            } else if (response.code() == 401) {
                                view.sessionExpired();
                            }
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }
                });
    }

    @Override
    public void callApiOnScroll(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                getBlockedUsers(PAGE_SIZE * page, PAGE_SIZE);
            }
        }
    }

    @Override
    public ClickListner getPresenter() {
        return this;
    }


    @Override
    public void onUserClick(int position) {
        view.openProfile(model.getUserId(position));
    }

    @Override
    public void itemSelect(int position, boolean isSelected) {
//        model.selectItem(position, isSelected);
    }

    @Override
    public void btnUnblock(int position) {
        Map<String, String> map = new HashMap<>();
        map.put("reason", "");
        service.block(AppController.getInstance().getApiToken(), Constants.LANGUAGE, model.getUserId(position), "unblock", map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        try {
                            if (response.code() == 200) {
                                model.updateData(position);
                            } else if (response.code() == 401) {
                                view.sessionExpired();
                            }
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }
                });
    }
}
