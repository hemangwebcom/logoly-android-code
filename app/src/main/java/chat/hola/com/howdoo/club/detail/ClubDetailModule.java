package chat.hola.com.howdoo.club.detail;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 22/2/18.
 */

@ActivityScoped
@Module
public interface ClubDetailModule {

    @ActivityScoped
    @Binds
    ClubDetailContract.Presenter presenter(ClubDetailPresenter presenter);

    @ActivityScoped
    @Binds
    ClubDetailContract.View view(ClubDetailActivity clubDetailActivity);

}
