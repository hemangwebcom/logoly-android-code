package chat.hola.com.howdoo.profileScreen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.policy.TimeWindow;
import com.howdoo.dubly.R;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.ApiOnServer;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.UriUtil;
import chat.hola.com.howdoo.Utilities.Utilities;
import chat.hola.com.howdoo.cameraActivities.manager.CameraOutputModel;
import chat.hola.com.howdoo.club.create.CreateClubPresenter;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.post.ReportReason;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.FollowResponse;
import chat.hola.com.howdoo.profileScreen.model.Profile;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ankit on 22/2/18.
 */

public class ProfilePresenter implements ProfileContract.Presenter {

    @Inject
    ProfileContract.View view;

    @Inject
    HowdooService service;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    Context context;

    private String picturePath;
    private Bitmap bitmapToUpload;
    private Uri imageUri;
    private Bitmap bitmap;

    @Inject
    public ProfilePresenter() {
    }

    @Override
    public void init() {
        view.applyFont();
        view.setupViewPager();
    }

    @Override
    public void loadProfileData() {

        view.isLoading(true);
        service.getUserProfile(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Profile>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<Profile> response) {
                        Log.i("RESPONSE", response.toString());
                        if (response.code() == 200 && response.body() != null) {
                            if (view != null)
                                view.showProfileData(response.body());
                        } else if (response.code() == 401)
                            if (view != null)
                                view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.isInternetAvailable(networkConnector.isConnected());
                            view.isLoading(false);
                            view.showMessage(e.getMessage(), 0);
                        }
                    }

                    @Override
                    public void onComplete() {
                        view.isLoading(false);
                    }
                });

    }

    @Override
    public void loadMemberData(String userId) {

        view.isLoading(true);
        service.getMember(AppController.getInstance().getApiToken(), Constants.LANGUAGE, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Profile>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<Profile> response) {
                        Log.i("RESPONSE", response.toString());
                        if (response.code() == 200 && response.body() != null)
                            if (view != null)
                                view.showProfileData(response.body());
                            else if (response.code() == 401)
                                if (view != null)
                                    view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.isInternetAvailable(networkConnector.isConnected());
                            view.isLoading(false);
                            view.showMessage(e.getMessage(), 0);
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (view != null)
                            view.isLoading(false);
                    }
                });
    }

    @Override
    public void follow(String followingId) {

        Map<String, Object> params = new HashMap<>();
        params.put("followingId", followingId);
        service.follow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<FollowResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<FollowResponse> response) {
                        if (response.code() == 200)
                            view.isFollowing(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showMessage(e.getMessage(), 0);
                        view.isFollowing(false);
                    }

                    @Override
                    public void onComplete() {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }
                });

    }

    @Override
    public void unfollow(String followingId) {

        Map<String, String> map = new HashMap<>();
        map.put("followerId", AppController.getInstance().getUserId());
        service.unfollow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, followingId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<FollowResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<FollowResponse> response) {
                        Log.i("RESPONSE", response.toString());
                        view.isFollowing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        //   view.showMessage(e.getMessage(),0);
                        view.isFollowing(true);
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }


    public void getReportReasons() {
        service.userReportReasons(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ReportReason>>() {
                    @Override
                    public void onNext(Response<ReportReason> response) {
                        if (response.code() == 200) {
                            view.addToReportList(response.body().getData());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void reportUser(String userId, String reason, String message) {
        Map<String, String> map = new HashMap<>();
        map.put("targetUserId", userId);
        map.put("reason", reason);
        map.put("message", message);
        service.reportUser(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {

                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.showMessage(null, R.string.reported);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void getBlockReasons() {
        service.getBlockReason(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ReportReason>>() {
                    @Override
                    public void onNext(Response<ReportReason> response) {
                        if (response.code() == 200) {
                            view.addToBlockList(response.body().getData());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void block(String userId, String block, String reason) {
        Map<String, String> map = new HashMap<>();
        map.put("reason", reason);
        service.block(AppController.getInstance().getApiToken(), Constants.LANGUAGE, userId, block, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            view.block(block.equals("block"));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    /**
     * Update status, profile pic and cover pic
     *
     * @param string  : either status, profile pic url or cover pic url
     * @param forWhat : 0=status, 1= profile pic, 2= cover pic
     */
    public void update(String string, int forWhat) {
        Map<String, Object> map = new HashMap<>();
        switch (forWhat) {
            case 0:
                //status
                map.put("status", string);
                break;
            case 1:
                //profile pic
                map.put("image", string);
                break;
            case 2:
                //cover pic
                map.put("coverImage", string);
                break;

        }

        service.updateProfile(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void launchCamera(PackageManager packageManager) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(packageManager) != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            } else {
                List<ResolveInfo> resInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    context.grantUriPermission(packageName, imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            view.launchCamera(intent);
        } else {
            view.showSnackMsg(R.string.string_61);
        }
    }

    public void launchImagePicker() {
        Intent intent = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setType("image/*");

        } else {
            intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }
        view.launchImagePicker(intent);
    }

    public void parseSelectedImage(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                Uri uri = data.getData();
                //TODO: it will prevent further crash ( getting uri as null on android 7.0 Mi phone).
                if (uri == null)
                    return;
                picturePath = UriUtil.getPath(context, uri);
                if (picturePath != null) {
                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(picturePath, options);

                    if (options.outWidth > 0 && options.outHeight > 0) {

                        //launch crop image
                        view.launchCropImage(data.getData());

                    } else {
                        //image can't be selected try another
                        view.showSnackMsg(R.string.string_31);
                    }
                } else {
                    //image can't be selected try another
                    view.showSnackMsg(R.string.string_31);
                }
            } catch (OutOfMemoryError e) {
                //out of mem try again
                view.showSnackMsg(R.string.string_15);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            //image selection canceled.
            view.showSnackMsg(R.string.string_16);
        } else {
            //failed to select image.
            view.showSnackMsg(R.string.string_113);
        }

    }

    public void parseCapturedImage(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(picturePath, options);
                if (options.outWidth > 0 && options.outHeight > 0) {
                    //launch crop image
                    view.launchCropImage(imageUri);

                } else {
                    //failed to capture image.
                    picturePath = null;
                    view.showSnackMsg(R.string.string_17);
                }
            } catch (OutOfMemoryError e) {
                //out of mem try again
                picturePath = null;
                view.showSnackMsg(R.string.string_15);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            picturePath = null;
            //img capture canceled.
            view.showSnackMsg(R.string.string_18);
        } else {
            //sorry failed to capture
            picturePath = null;
            view.showSnackMsg(R.string.string_17);
        }
    }

    public void parseCropedImage(int requestCode, int resultCode, Intent data) {
        try {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                picturePath = UriUtil.getPath(context, result.getUri());
                if (picturePath != null) {
                    bitmapToUpload = BitmapFactory.decodeFile(picturePath);
                    bitmap = getCircleBitmap(bitmapToUpload);
                    if (bitmap != null && bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
                        view.setImage(bitmap);
                    } else {
                        //sorry failed to capture
                        picturePath = null;
                        view.showSnackMsg(R.string.string_19);
                    }
                } else {
                    //sorry failed to capture
                    picturePath = null;
                    view.showSnackMsg(R.string.string_19);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                //sorry failed to capture
                picturePath = null;
                view.showSnackMsg(R.string.string_19);
            }

        } catch (OutOfMemoryError e) {
            //out of mem try again
            picturePath = null;
            view.showSnackMsg(R.string.string_15);
        }
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {

        try {

            final Bitmap circuleBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(circuleBitmap);

            final int color = Color.GRAY;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
            final RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawOval(rectF, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);


            return circuleBitmap;
        } catch (Exception e) {
            return null;
        }
    }

    private Uri setImageUri() {
        String name = Utilities.tsInGmt();
        name = new Utilities().gmtToEpoch(name);
        File folder = new File(Environment.getExternalStorageDirectory().getPath() + ApiOnServer.IMAGE_CAPTURE_URI);
        if (!folder.exists() && !folder.isDirectory()) {
            folder.mkdirs();
        }
        File file = new File(Environment.getExternalStorageDirectory().getPath() + ApiOnServer.IMAGE_CAPTURE_URI, name + ".jpg");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Uri imgUri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
        this.imageUri = imgUri;
        this.picturePath = file.getAbsolutePath();
        return imgUri;
    }

    public void uploadPhoto(int picFor) {
        try {
            MediaManager.get().upload(picturePath)
                    .option("folder", "profile")
                    .callback(new UploadCallback() {
                        @Override
                        public void onStart(String requestId) {

                        }

                        @Override
                        public void onProgress(String requestId, long bytes, long totalBytes) {

                        }

                        @Override
                        public void onSuccess(String requestId, Map resultData) {
                            update((String) resultData.get("url"), picFor);
                        }

                        @Override
                        public void onError(String requestId, ErrorInfo error) {

                        }

                        @Override
                        public void onReschedule(String requestId, ErrorInfo error) {

                        }
                    }).constrain(TimeWindow.immediate()).dispatch();
        } catch (Exception ignored) {
        }
    }

    public void addToContact(String userId) {
        Map<String, Object> params = new HashMap<>();
        params.put("contactId", userId);
        service.addToContact(AppController.getInstance().getApiToken(), "en", params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.showMessage("Added to the contact", -1);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteContact(String id) {
        service.deleteContact(AppController.getInstance().getApiToken(), Constants.LANGUAGE, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            view.showMessage("Contact delete", -1);
                            view.reload();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
