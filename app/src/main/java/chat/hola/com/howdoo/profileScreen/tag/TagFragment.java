package chat.hola.com.howdoo.profileScreen.tag;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.howdoo.dubly.R;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.story.model.StoryData;
import dagger.android.support.DaggerFragment;

/**
 * Created by ankit on 17/2/18.
 */

public class TagFragment extends DaggerFragment implements TagContract.View {

    @Inject
    TagPresenter presenter;

    @BindView(R.id.recyclerProfileTab)
    RecyclerView recyclerViewProfileTab;

    //TrendingDtlAdapter adapter;

    private Unbinder unbinder;
    @Inject
    SessionManager sessionManager;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }
    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Inject
    public TagFragment() {
    }

    public static TagFragment newInstance() {
        return new TagFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_story, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        presenter.init();
        return rootView;
    }

    @Override
    public void setupRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerViewProfileTab.setLayoutManager(llm);
        recyclerViewProfileTab.setHasFixedSize(true);
        recyclerViewProfileTab.setNestedScrollingEnabled(true);
//        adapter = new TrendingDtlAdapter(getContext());
//        recyclerViewProfileTab.setAdapter(adapter);
        presenter.loadTagImages();
    }

    @Override
    public void showTrendingImages(List<StoryData> trendingImages) {
        //  adapter.setData(trendingImages);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void reload() {

    }
}
