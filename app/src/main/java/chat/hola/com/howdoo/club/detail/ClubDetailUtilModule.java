package chat.hola.com.howdoo.club.detail;

import chat.hola.com.howdoo.Dialog.CustomProgressDialog;
import chat.hola.com.howdoo.Dialog.ImageSourcePicker;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.club.create.CreateClubActivity;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ankit on 27/2/18.
 */

@ActivityScoped
@Module
public class ClubDetailUtilModule {

    @ActivityScoped
    @Provides
    CustomProgressDialog customProgressDialog(CreateClubActivity activity, TypefaceManager typefaceManager) {
        return new CustomProgressDialog("Your club being created...", activity, typefaceManager);
    }

}
