package chat.hola.com.howdoo.home.activity.followingTab;

import android.support.annotation.Nullable;
import android.util.Log;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.home.activity.followingTab.model.ClickListner;
import chat.hola.com.howdoo.home.activity.followingTab.model.FollowingModel;
import chat.hola.com.howdoo.home.activity.followingTab.model.FollowingResponse;
import chat.hola.com.howdoo.models.NetworkConnector;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * @author 3Embed.
 * @since 22/2/18.
 */

public class FollowingPresenter implements FollowingContract.Presenter, ClickListner {

    @Nullable
    FollowingContract.View view;
    @Inject
    HowdooService service;
    @Inject
    FollowingModel model;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    public FollowingPresenter() {

    }

    @Override
    public void attachView(FollowingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void init() {
        if (view != null)
            view.initPostRecycler();
    }

    @Override
    public void loadFollowing() {
        service.followingActivities(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowingResponse>>() {
                    @Override
                    public void onNext(Response<FollowingResponse> response) {
                        if (view != null) {
                            if (response.code() == 200) {
                                view.showFollowings(response.body().getData());
                            } else if (response.code() == 401) {
                                view.sessionExpired();
                            }
                            view.isInternetAvailable(true);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.isInternetAvailable(networkConnector.isConnected());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void onUserClicked(String userId) {
        if (view != null) {
            view.onUserClicked(userId);
        }
    }

    @Override
    public void onMediaClick(int position, android.view.View v) {
        if (view != null) {
            view.onMediaClick(position, v);
        }
    }
}
