package chat.hola.com.howdoo.profileScreen.editProfile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Dialog.DatePickerFragment;
import chat.hola.com.howdoo.Dialog.ImageSourcePicker;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Profile.UpdateStatus;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.editProfile.model.EditProfileBody;
import chat.hola.com.howdoo.profileScreen.model.Data;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h>EditProfileActivity</h>
 * <p>
 * This fragment allow user to edit their profile detail like username, status etc.
 *
 * @author 3Embed
 * @since 19/2/18.
 */
public class EditProfileActivity extends DaggerAppCompatActivity implements EditProfileContract.View, View.OnFocusChangeListener {

    private static final String TAG = EditProfileActivity.class.getSimpleName();
    //constants
    private int EDIT_EMAIL_REQ_CODE = 333;
    private int STATUS_UPDATE_REQ_CODE = 555;
    private int CAMERA_REQ_CODE = 24;
    private int READ_STORAGE_REQ_CODE = 26;
    private int WRITE_STORAGE_REQ_CODE = 27;

    private static final int RESULT_CAPTURE_IMAGE = 0;
    private static final int RESULT_LOAD_IMAGE = 1;

    @Inject
    EditProfilePresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    DatePickerFragment datePickerFragment;
    @Inject
    ImageSourcePicker imageSourcePicker;
    @Inject
    SessionManager sessionManager;

    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.tvEditProfile)
    TextView tvEditProfile;
    @BindView(R.id.btnChangePhoto)
    Button btnChangePhoto;
    @BindView(R.id.flAddProfile)
    FrameLayout flAddProfile;
    @BindView(R.id.ivProfile)
    ImageView ivProfile;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.titleContactNumber)
    TextInputLayout tvContactNumberTitle;
    @BindView(R.id.etContactNumber)
    TextInputEditText etContactNumber;
    @BindView(R.id.tvContactDetail)
    TextView tvContactDeatil;
    @BindView(R.id.titleStatus)
    TextInputLayout titleStatus;
    @BindView(R.id.etStatus)
    TextInputEditText etStatus;
    @BindView(R.id.titleEmail)
    TextInputLayout titleEmail;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.tilName)
    TextInputLayout tilName;
    @BindView(R.id.tilUserName)
    TextInputLayout tilUserName;


    private Unbinder unbinder;
    private Data profileData;
    private ProgressDialog dialog;
    private String call = "";
    @Inject
    BlockDialog dialog1;

    @Override
    public void userBlocked() {
        dialog1.show();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        unbinder = ButterKnife.bind(this);

        Bundle bundle = getIntent().getBundleExtra("bundle");
        profileData = (Data) bundle.getSerializable("profile_data");
        call = getIntent().getStringExtra("call");
        setProfileData();
        presenter.init();
        progressBarSetup();
        etStatus.setOnTouchListener((v, event) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (etStatus.getRight() - etStatus.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    Intent intent = new Intent(EditProfileActivity.this, UpdateStatus.class);
                    intent.putExtra("status", etStatus.getText().toString());
                    startActivityForResult(intent, STATUS_UPDATE_REQ_CODE);

                    return true;
                }
            }
            return false;
        });
    }

    private void progressBarSetup() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.editProfileProgressMsg));
        dialog.setCancelable(false);
    }

    private void setProfileData() {
        if (profileData != null) {
            etUsername.setText(profileData.getUserName());
            String firstName = profileData.getFirstName();
            String lastName = profileData.getLastName();
            String name = ((firstName == null) ? "" : firstName) + ((lastName == null) ? "" : " " + lastName);
            etName.setText(name);
            etStatus.setText(profileData.getStatus());
            etEmail.setText(profileData.getEmail().getId());
            etContactNumber.setText(profileData.getNumber());
//            cbPrivate.setChecked(profileData.getPrivate().equals("1"));

            Glide.with(getBaseContext()).load(profileData.getProfilePic()).asBitmap().centerCrop().into(ivProfile);
        } else {
            Log.e(TAG, "profileData can not be null");
        }

        if (call != null)
            etName.setSelection(etName.getText().toString().trim().length());
    }

    @Override
    public void applyFont() {
        btnCancel.setTypeface(typefaceManager.getSemiboldFont());
        btnSave.setTypeface(typefaceManager.getSemiboldFont());
        tvEditProfile.setTypeface(typefaceManager.getSemiboldFont());
        btnChangePhoto.setTypeface(typefaceManager.getMediumFont());
        tvContactDeatil.setTypeface(typefaceManager.getMediumFont());
        tvContactNumberTitle.setTypeface(typefaceManager.getRegularFont());
        tilName.setTypeface(typefaceManager.getMediumFont());
        tilUserName.setTypeface(typefaceManager.getMediumFont());
        etName.setTypeface(typefaceManager.getMediumFont());
        etName.setOnFocusChangeListener(this);
        etUsername.setTypeface(typefaceManager.getMediumFont());
        etUsername.setOnFocusChangeListener(this);
        titleStatus.setTypeface(typefaceManager.getMediumFont());
        etStatus.setTypeface(typefaceManager.getMediumFont());
        etStatus.setOnFocusChangeListener(this);
        titleEmail.setTypeface(typefaceManager.getMediumFont());
        etEmail.setTypeface(typefaceManager.getMediumFont());
        etEmail.setOnFocusChangeListener(this);
        etContactNumber.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void finishActivity(boolean success) {
        if (success)
            setResult(RESULT_OK);
        else
            setResult(0);
        finish();
    }

    @Override
    public void showProgress(boolean show) {
        try {
            if (show && dialog != null && !dialog.isShowing()) {
                dialog.show();
            } else if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        } catch (IllegalArgumentException ignored) {
        }

    }

    @Override
    public void setEmail(String email) {
        etEmail.setText(email);
        titleEmail.setVisibility(email != null && TextUtils.isEmpty(email) ? View.VISIBLE : View.GONE);
    }

    @Override
    public void launchCamera(Intent intent) {
        startActivityForResult(intent, RESULT_CAPTURE_IMAGE);
    }


    @Override
    public void launchImagePicker(Intent intent) {
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @OnClick({R.id.ivProfile, R.id.ivAdd, R.id.btnChangePhoto})
    void addProfile() {
        //launch camera or gallery.
        imageSourcePicker.setOnSelectImageSource(callback);
        imageSourcePicker.show();
    }

    ImageSourcePicker.OnSelectImageSource callback = new ImageSourcePicker.OnSelectImageSource() {
        @Override
        public void onCamera() {
            checkCameraPermissionImage();
        }

        @Override
        public void onGallary() {
            checkReadImage();
        }

        @Override
        public void onCancel() {
            //nothing to do.
        }
    };


    @OnClick(R.id.btnCancel)
    public void cancel() {
        onBackPressed();
    }

    @OnClick(R.id.btnSave)
    public void save() {
        //need to call some api method
        EditProfileBody editProfileBody = new EditProfileBody();
        String[] name = etName.getText().toString().split("\\s+");
        editProfileBody.setFirstName((name[0] == null) ? "" : name[0]);
        editProfileBody.setLastName(name.length > 1 ? (name[1] == null) ? "" : name[1] : "");
        editProfileBody.setUserName(etUsername.getText().toString());
        String status = etStatus.getText().toString();
        editProfileBody.setStatus((status.isEmpty()) ? getResources().getString(R.string.default_status) : status);
        editProfileBody.setEmail(etEmail.getText().toString());
//        editProfileBody.set_private(cbPrivate.isChecked() ? 1 : 0);
        presenter.initUpdateProfile(editProfileBody, profileData);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(this, getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showSnackMsg(int msgId) {
        String msg = getResources().getString(msgId);
        Snackbar snackbar = Snackbar.make(root, "" + msg,
                Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                .setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void launchCropImage(Uri data) {
        CropImage.activity(data)
                .start(this);
    }

    @Override
    public void setProfileImage(Bitmap bitmap) {
        ivProfile.setImageBitmap(bitmap);
    }


    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        if (!flag)
            Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
    }


    private void checkCameraPermissionImage() {
        if (ActivityCompat.checkSelfPermission(EditProfileActivity
                .this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                presenter.launchCamera(getPackageManager());
            } else {
                /*
                 *permission required to save the image captured
                 */
                requestReadImagePermission(0);
            }
        } else {
            requestCameraPermissionImage();
        }
    }

    private void requestCameraPermissionImage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                Manifest.permission.CAMERA)) {

            Snackbar snackbar = Snackbar.make(root, R.string.string_221,
                    Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA},
                            CAMERA_REQ_CODE);
                }
            });
            snackbar.show();
            View view = snackbar.getView();
            ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                    .setGravity(Gravity.CENTER_HORIZONTAL);
        } else {
            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA},
                    CAMERA_REQ_CODE);
        }
    }

    private void checkReadImage() {
        if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            presenter.launchImagePicker();
        } else {
            requestReadImagePermission(1);
        }
    }


    private void requestReadImagePermission(int k) {
        if (k == 1) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar snackbar = Snackbar.make(root, R.string.string_222,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                READ_STORAGE_REQ_CODE);
                    }
                });
                snackbar.show();
                View view = snackbar.getView();
                ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                        .setGravity(Gravity.CENTER_HORIZONTAL);
            } else {
                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        READ_STORAGE_REQ_CODE);
            }
        } else if (k == 0) {
            /*
             * For capturing the image permission
             */
            if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar snackbar = Snackbar.make(root, R.string.string_1218,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                WRITE_STORAGE_REQ_CODE);
                    }
                });

                snackbar.show();
                View view = snackbar.getView();
                ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                        .setGravity(Gravity.CENTER_HORIZONTAL);
            } else {
                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_STORAGE_REQ_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_EMAIL_REQ_CODE && resultCode == RESULT_OK) {
            String newEmail = data.getStringExtra("email");
            if (newEmail != null && !newEmail.isEmpty())
                etEmail.setText(newEmail);
            else
                etEmail.setText(getResources().getString(R.string.enterEmail));

        } else if (requestCode == STATUS_UPDATE_REQ_CODE && resultCode == RESULT_OK) {
            String newStatus = data.getStringExtra("updatedValue");
            if (newStatus != null && !newStatus.isEmpty())
                etStatus.setText(newStatus);
            else
                etStatus.setText(getResources().getString(R.string.default_status));

        } else if (requestCode == RESULT_LOAD_IMAGE) {

            presenter.parseSelectedImage(requestCode, resultCode, data);

        } else if (requestCode == RESULT_CAPTURE_IMAGE) {
            presenter.parseCapturedImage(requestCode, resultCode, data);

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            presenter.parseCropedImage(requestCode, resultCode, data);
        }
    }


    /**
     * Result of the permission request
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_REQ_CODE) {

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        presenter.launchCamera(getPackageManager());
                    } else {
                        requestReadImagePermission(0);
                    }
                } else {
                    //camera permission denied msg
                    showSnackMsg(R.string.string_62);
                }
            } else {
                //camera permission denied msg
                showSnackMsg(R.string.string_62);
            }

        } else if (requestCode == READ_STORAGE_REQ_CODE) {

            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    presenter.launchImagePicker();

                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            } else {
                //Access storage permission denied
                showSnackMsg(R.string.string_1006);
            }
        } else if (requestCode == WRITE_STORAGE_REQ_CODE) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {

                    presenter.launchCamera(getPackageManager());
                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }


    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.etName:
                etName.setSelection(etName.getText().toString().trim().length());
                break;
            case R.id.etUsername:
                etUsername.setSelection(etUsername.getText().toString().trim().length());
                break;
            case R.id.etStatus:
                etStatus.setSelection(etStatus.getText().toString().trim().length());
                break;
            case R.id.etEmail:
                etStatus.setSelection(etEmail.getText().toString().trim().length());
                break;
            case R.id.etContactNumber:
                etContactNumber.setSelection(etContactNumber.getText().toString().trim().length());
                break;
        }
    }

    @Override
    public void reload() {

    }
}
