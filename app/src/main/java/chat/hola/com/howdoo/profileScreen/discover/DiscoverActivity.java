package chat.hola.com.howdoo.profileScreen.discover;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.activity.followingTab.FollowingFrag;
import chat.hola.com.howdoo.home.activity.youTab.YouFrag;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import chat.hola.com.howdoo.profileScreen.discover.contact.ContactFrag;
import chat.hola.com.howdoo.profileScreen.discover.facebook.FacebookFrag;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h>DiscoverActivity.class</h>
 * <p>
 * This fragment has two tabs {@link FollowingFrag} and {@link YouFrag}
 * which is handle by {@link DiscoverPageAdapter}.
 *
 * @author 3Embed
 * @since 02/03/18.
 */

public class DiscoverActivity extends DaggerAppCompatActivity implements DiscoverContract.View {

    private static final String TAG = DiscoverActivity.class.getSimpleName();

    @Inject
    DiscoverPresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    FacebookFrag facebookFrag;
    @Inject
    SessionManager sessionManager;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.btnSkip)
    Button btnSkip;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;
    @BindView(R.id.viewPagerActivities)
    ViewPager mViewPager;
    @BindView(R.id.tabLayoutActivities)
    TabLayout mTabLayout;

    private DiscoverPageAdapter discoverPageAdapter;
    private Unbinder unbinder;
    private String caller = "";
    private boolean isContact = true;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }
    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void triggerSearch(String query, @Nullable Bundle appSearchData) {
        super.triggerSearch(query, appSearchData);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        isContact = getIntent().getBooleanExtra("is_contact", true);
        caller = getIntent().getStringExtra("caller");
        callerHandle(caller);
        presenter.init();
        llNetworkError.setErrorListner(this);
    }

    private void callerHandle(String caller) {
        switch (caller) {
            case "ProfileActivity":
            case "SettingsActivity":
                ivBack.setVisibility(View.VISIBLE);
                btnSkip.setVisibility(View.GONE);
                break;
            case "SaveProfile":
                ivBack.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void applyFont() {
        tvTitle.setTypeface(typefaceManager.getSemiboldFont());
        btnSkip.setTypeface(typefaceManager.getSemiboldFont());
    }

    @Override
    public void changeSkipText(String text) {
        btnSkip.setText(text);
    }

    @Override
    public void viewPagerSetup() {
        //use inject for creating object
        discoverPageAdapter = new DiscoverPageAdapter(getSupportFragmentManager());
        discoverPageAdapter.addFragment(new ContactFrag(), "CONTACTS");
        discoverPageAdapter.addFragment(facebookFrag, "FACEBOOK");
        mTabLayout.setupWithViewPager(mViewPager, true);
        mViewPager.setAdapter(discoverPageAdapter);
        if (isContact) {
            TabLayout.Tab tab = mTabLayout.getTabAt(0);
            if (tab != null)
                tab.select();
        } else {
            TabLayout.Tab tab = mTabLayout.getTabAt(1);
            if (tab != null)
                tab.select();
        }
    }

    @OnClick({R.id.ivBack, R.id.btnSkip})
    public void back() {
        if (caller.equals("ProfileActivity") || caller.equals("SettingsActivity"))
            onBackPressed();
        else {
            Intent i = new Intent(this, LandingActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i);
            supportFinishAfterTransition();
        }
    }

    @Override
    public void onBackPressed() {
        if (caller.equals("ProfileActivity") || caller.equals("SettingsActivity"))
            super.onBackPressed();
    }

    @Override
    public void tabSetup() {
        //their exist some easier way to setfont to tab
        //need to replace.
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_textview, null);
        tabOne.setTypeface(typefaceManager.getSemiboldFont());
        tabOne.setText("CONTACTS");
        mTabLayout.getTabAt(0).setCustomView(tabOne);
        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_textview, null);
        tabTwo.setTypeface(typefaceManager.getSemiboldFont());
        tabTwo.setText("FACEBOOK");
        mTabLayout.getTabAt(1).setCustomView(tabTwo);
    }


    @Override
    public void onResume() {
        super.onResume();
        // landingPageActivity.visibleActionBar();
        // AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(this, getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK)
            facebookFrag.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void reload() {
        presenter.init();
    }
}
