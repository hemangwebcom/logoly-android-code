package chat.hola.com.howdoo.GroupChat.ViewHolders;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.howdoo.dubly.R;

import chat.hola.com.howdoo.AppController;

/**
 * Created by moda on 19/09/17.
 */

public class ViewHolderSelectedMember extends RecyclerView.ViewHolder {


    public TextView contactName;

    public ImageView contactImage, removeContact;


    public ViewHolderSelectedMember(View view) {
        super(view);


        contactName = (TextView) view.findViewById(R.id.contactName);

        contactImage = (ImageView) view.findViewById(R.id.storeImage2);
        removeContact = (ImageView) view.findViewById(R.id.contactSelected);

        Typeface tf = AppController.getInstance().getRegularFont();

        contactName.setTypeface(tf, Typeface.NORMAL);


    }
}
