package chat.hola.com.howdoo.Activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.howdoo.dubly.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.welcomeScreen.WelcomeActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.dubly", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppController.getInstance().getSignedIn() && AppController.getInstance().profileSaved()) {
                    Intent i2 = new Intent(MainActivity.this, LandingActivity.class);
                    i2.putExtra("userId", AppController.getInstance().getUserId());
                    i2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(i2);
                    supportFinishAfterTransition();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else {
                    Intent intent2 = new Intent(MainActivity.this, WelcomeActivity.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent2);
                    supportFinishAfterTransition();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                }
            }
        }, 1500);
    }
}
