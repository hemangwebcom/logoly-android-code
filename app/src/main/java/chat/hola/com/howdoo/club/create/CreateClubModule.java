package chat.hola.com.howdoo.club.create;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 22/2/18.
 */

@ActivityScoped
@Module
public interface CreateClubModule {

    @ActivityScoped
    @Binds
    CreateClubContract.Presenter presenter(CreateClubPresenter presenter);

    @ActivityScoped
    @Binds
    CreateClubContract.View view(CreateClubActivity createClubActivity);

}
