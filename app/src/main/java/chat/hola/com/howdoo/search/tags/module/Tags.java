package chat.hola.com.howdoo.search.tags.module;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DELL on 4/17/2018.
 */

public class Tags implements Serializable {
    @SerializedName("hashTags")
    @Expose
    private String hashTags;
    @SerializedName("totalPublicPosts")
    @Expose
    private Integer totalPublicPosts;

    public String getHashTags() {
        return hashTags;
    }

    public void setHashTags(String hashTags) {
        this.hashTags = hashTags;
    }

    public Integer getTotalPublicPosts() {
        return totalPublicPosts;
    }

    public void setTotalPublicPosts(Integer totalPublicPosts) {
        this.totalPublicPosts = totalPublicPosts;
    }

}
