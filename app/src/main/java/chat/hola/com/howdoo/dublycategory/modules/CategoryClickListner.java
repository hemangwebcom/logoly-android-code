package chat.hola.com.howdoo.dublycategory.modules;

/**
 * <h1>ClickListner</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public interface CategoryClickListner {
    void onItemClick(int position);
}
