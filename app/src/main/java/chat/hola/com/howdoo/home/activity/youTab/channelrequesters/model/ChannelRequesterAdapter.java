package chat.hola.com.howdoo.home.activity.youTab.channelrequesters.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.TypefaceManager;

/**
 * <h1>ChannelRequesterAdapter</h1>
 *
 * @author 3embed
 * @since 4/9/2018
 */

public class ChannelRequesterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DataList> list;
    private TypefaceManager typefaceManager;
    private ClickListner clickListner;
private Context context;
    @Override
    public int getItemViewType(int position) {
        return list.get(position).isParent() ? 0 : 1;
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    @Inject
    public ChannelRequesterAdapter(Context context, List<DataList> list, TypefaceManager typefaceManager) {
        this.context=context;
        this.list = list;
        this.typefaceManager = typefaceManager;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder2(LayoutInflater.from(parent.getContext()).inflate(R.layout.channel_request_item, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder2 holder2 = (ViewHolder2) holder;
        Glide.with(context).load(list.get(position).getProfilePic().replace("upload/", Constants.PROFILE_PIC_SHAPE)).asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(context.getResources().getDrawable(R.drawable.profile_one))
                .into(holder2.ivProfilePic);
        holder2.tvUserName.setText(list.get(position).getUserName());
        holder2.tvTime.setText(list.get(position).getChannelName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder2 extends RecyclerView.ViewHolder {
        @BindView(R.id.ivProfilePic)
        ImageView ivProfilePic;
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.btnAccept)
        Button btnAccept;
        @BindView(R.id.btnReject)
        Button btnReject;

        public ViewHolder2(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvTime.setTypeface(typefaceManager.getRegularFont());
            tvUserName.setTypeface(typefaceManager.getSemiboldFont());
            btnAccept.setTypeface(typefaceManager.getSemiboldFont());
            btnReject.setTypeface(typefaceManager.getSemiboldFont());
        }

        @OnClick(R.id.ivProfilePic)
        public void profile() {
            clickListner.onUserClicked(getAdapterPosition());
        }

        @OnClick(R.id.btnAccept)
        public void accept() {
            clickListner.onRequestAction(getAdapterPosition(), true);
        }

        @OnClick(R.id.btnReject)
        public void reject() {
            clickListner.onRequestAction(getAdapterPosition(), false);
        }
    }
}
