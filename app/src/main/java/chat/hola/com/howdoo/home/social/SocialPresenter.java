package chat.hola.com.howdoo.home.social;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Networking.HowdooServiceTrending;
import chat.hola.com.howdoo.Networking.connection.NetworkStateHolder;
import chat.hola.com.howdoo.Networking.observer.NetworkObserver;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.home.model.Posts;
import chat.hola.com.howdoo.home.social.model.ClickListner;
import chat.hola.com.howdoo.home.social.model.SocialModel;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.models.PostObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h1>ClubPostFragment</h1>
 *
 * @author 3Embed
 * @since 4/5/2018.
 */

public class SocialPresenter implements SocialContract.Presenter, ClickListner, SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = "ClubPostPresenter";
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;
    private NativeAd nativeAd;

    @Nullable
    SocialContract.View view;
    @Inject
    SocialModel model;
    @Inject
    HowdooService service;
    @Inject
    HowdooServiceTrending serviceTrending;
    @Inject
    NetworkObserver networkObserver;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    PostObserver postObserver;
    @Inject
    NetworkConnector networkConnector;
    private Context context;
    public List<Data> arrayList = new ArrayList<>();
    private String screen;

    @Inject
    SocialPresenter(Context context) {
        this.context = context;
    }

    public void setScreen(String screen) {
        this.screen = screen;
        Log.i(TAG, "setScreen: " + screen);
    }

    @Override
    public void callSocialApi(int offset, int limit, boolean load) {
        isLoading = true;
        if (view != null) {
            view.isLoading(load);
        }
        Log.i(TAG, "callSocialApi: " + screen);
        if (screen.equals("1")) {
            service.getPosts(AppController.getInstance().getApiToken(), Constants.LANGUAGE, offset, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<Response<Posts>>() {
                        @Override
                        public void onNext(Response<Posts> response) {
                            switch (response.code()) {
                                case 200:
                                    isLastPage = response.body().getData().size() < PAGE_SIZE;
                                    if (view != null)
                                        view.clearRecyclerView();
                                    model.setData(response.body().getData(), offset == 0);
//                                for (int i = 0; i < response.body().getData().size(); i++) {
//                                    if (i % 10 == 0 && i != 0)
//                                        loadNativeAd(i);
//                                }
                                    isLoading = false;
                                    if (view != null)
                                        view.isLoading(false);
                                    break;
                                case 401:
                                    if (view != null)
                                        view.sessionExpired();
                                    break;
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            isLoading = false;
                            if (view != null) {
                                view.isInternetAvailable(networkConnector.isConnected());
                                view.isLoading(false);
                            }
                        }

                        @Override
                        public void onComplete() {
                            isLoading = false;
                            if (view != null) {
                                view.isLoading(false);
                                view.showEmptyUi(model.arrayList.isEmpty());
                            }
                        }
                    });
        } else {
            service.getClubPosts(AppController.getInstance().getApiToken(), Constants.LANGUAGE, offset, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<Response<Posts>>() {
                        @Override
                        public void onNext(Response<Posts> response) {
                            switch (response.code()) {
                                case 200:
                                    isLastPage = response.body().getData().size() < PAGE_SIZE;
                                    model.setData(response.body().getData(), offset == 0);
                                    isLoading = false;
                                    if (view != null)
                                        view.isLoading(false);
                                    break;
                                case 401:
                                    if (view != null)
                                        view.sessionExpired();
                                    break;
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            isLoading = false;
                            if (view != null) {
                                view.isInternetAvailable(networkConnector.isConnected());
                                view.isLoading(false);
                            }
                        }

                        @Override
                        public void onComplete() {
                            isLoading = false;
                            if (view != null) {
                                view.isLoading(false);
                                view.showEmptyUi(model.arrayList.isEmpty());
                            }
                        }
                    });
        }

    }

    @Override
    public void attachView(SocialContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void onItemClick(int position, View view) {
        if (SocialPresenter.this.view != null && position >= 0)
            SocialPresenter.this.view.onItemClick(view, model.getData(position), position);
    }

    @Override
    public void onUserClick(int position) {
        assert view != null;
        view.onUserClick(model.getUserId(position));
    }

    @Override
    public void onChannelClick(int position) {
        view.onChannelClick(model.getChannelId(position));
    }

    @Override
    public void prefetchImage(int position) {

    }

    @Override
    public SocialPresenter getPresenter() {
        return this;
    }

    @Override
    public void callApiOnScroll(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                callSocialApi(PAGE_SIZE * page, PAGE_SIZE, false);
            }
        }
    }

    @Override
    public void onRefresh() {
        if (networkConnector.isConnected()) {
            model.arrayList.clear();
            page = 0;
            callSocialApi(PAGE_SIZE * page, PAGE_SIZE, true);
        }
    }

    @Override
    public void postObserver() {
        postObserver.getObservable().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean flag) {
                        onRefresh();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public List<Data> getDataList() {
        return model.getAllData();
    }


    public void filter(String page, String type) {
        isLoading = true;
        if (view != null) {
            view.isLoading(true);
        }
        serviceTrending.filter(AppController.getInstance().getApiToken(), Constants.LANGUAGE, type, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Posts>>() {
                    @Override
                    public void onNext(Response<Posts> response) {
                        switch (response.code()) {
                            case 200:
                                isLastPage = response.body().getData().size() < PAGE_SIZE;
                                if (view != null)
                                    view.clearRecyclerView();
                                model.setData(response.body().getData(), true);
                                isLoading = false;
                                if (view != null)
                                    view.isLoading(false);
                                break;
                            case 401:
                                if (view != null)
                                    view.sessionExpired();
                            case 204:
                                view.showMessage("No post found for this filter", -1);
                                view.showEmptyUi(true);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showMessage("No post found for this filter", -1);
                        isLoading = false;
                        if (view != null) {
                            view.isInternetAvailable(networkConnector.isConnected());
                            view.isLoading(false);
                        }
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                        if (view != null) {
                            view.isLoading(false);
                            view.showEmptyUi(model.arrayList.isEmpty());
                        }
                    }
                });
    }
}
