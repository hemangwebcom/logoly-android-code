package chat.hola.com.howdoo;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.PersistableBundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cloudinary.android.MediaManager;
import com.couchbase.lite.android.AndroidContext;
import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.howdoo.dubly.R;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.kochava.base.Tracker;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import com.squareup.picasso.Picasso;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import chat.hola.com.howdoo.Activities.ChatMessageScreen;
import chat.hola.com.howdoo.AppStateChange.AppStateListener;
import chat.hola.com.howdoo.AppStateChange.AppStateMonitor;
import chat.hola.com.howdoo.AppStateChange.RxAppStateMonitor;
import chat.hola.com.howdoo.Calls.CallingApis;
import chat.hola.com.howdoo.ContentObserver.ContactAddedOrUpdatedObserver;
import chat.hola.com.howdoo.ContentObserver.ContactDeletedObserver;
import chat.hola.com.howdoo.Database.CouchDbController;
import chat.hola.com.howdoo.DownloadFile.FileDownloadService;
import chat.hola.com.howdoo.DownloadFile.FileUploadService;
import chat.hola.com.howdoo.DownloadFile.FileUtils;
import chat.hola.com.howdoo.DownloadFile.ServiceGenerator;
import chat.hola.com.howdoo.GroupChat.Activities.GroupChatMessageScreen;
import chat.hola.com.howdoo.MQtt.CustomMQtt.IMqttActionListener;
import chat.hola.com.howdoo.MQtt.CustomMQtt.IMqttDeliveryToken;
import chat.hola.com.howdoo.MQtt.CustomMQtt.IMqttToken;
import chat.hola.com.howdoo.MQtt.CustomMQtt.MqttCallback;
import chat.hola.com.howdoo.MQtt.CustomMQtt.MqttConnectOptions;
import chat.hola.com.howdoo.MQtt.CustomMQtt.MqttMessage;
import chat.hola.com.howdoo.MQtt.MqttAndroidClient;
import chat.hola.com.howdoo.Networking.connection.NetworkCheckerService;
import chat.hola.com.howdoo.Notifications.RegistrationIntentService;
import chat.hola.com.howdoo.SecretChat.SecretChatMessageScreen;
import chat.hola.com.howdoo.Service.AppKilled;
import chat.hola.com.howdoo.Service.MQTT_constants;
import chat.hola.com.howdoo.Service.OreoJobService;
import chat.hola.com.howdoo.Utilities.ApiOnServer;
import chat.hola.com.howdoo.Utilities.ConnectivityReceiver;
import chat.hola.com.howdoo.Utilities.DeviceUuidFactory;
import chat.hola.com.howdoo.Utilities.MqttEvents;
import chat.hola.com.howdoo.Utilities.Utilities;
import chat.hola.com.howdoo.Utilities.twitterManager.TwitterManager;
import chat.hola.com.howdoo.dagger.AppComponent;
import chat.hola.com.howdoo.dagger.AppUtilModule;
import chat.hola.com.howdoo.dagger.DaggerAppComponent;
import chat.hola.com.howdoo.dagger.NetworkModule;
import chat.hola.com.howdoo.models.ConnectionObserver;
import chat.hola.com.howdoo.models.NetworkConnector;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static chat.hola.com.howdoo.Calls.CallingApis.OpenIncomingCallScreen;

/**
 * Created by moda on 29/06/17.
 */
public class AppController extends DaggerApplication implements Application.ActivityLifecycleCallbacks {
    private AppComponent appComponent;
    public static final String TAG = AppController.class.getSimpleName();
    public static Bus bus = new Bus(ThreadEnforcer.ANY);

    private static AppController mInstance;
    @Inject
    ConnectionObserver connectionObserver;
    @Inject
    NetworkConnector networkConnector;
    /**
     * Arrays for the secret chats dTag message received
     */
    final String[] dTimeForDB = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "15", "30", "60", "3600", "86400", "604800"};//getResources().getStringArray(R.array.dTimeForDB);
    final String[] dTimeOptions = {"off", "1 second", "2 seconds", "3 seconds", "4 seconds", "5 seconds", "6 seconds", "7 seconds",
            "8 seconds", "9 seconds", "10 seconds", "15 seconds", "30 seconds", "1 minute", "1 hour", "1 day", "1 week"};//getResources().getStringArray(R.array.dTimeOptions);
    ContactAddedOrUpdatedObserver contactUpdateObserver = new ContactAddedOrUpdatedObserver(new Handler());
    ContactDeletedObserver contactDeletedObserver = new ContactDeletedObserver(new Handler());
    private SharedPreferences sharedPref;
    private boolean foreground;
    private String groupChatsDocId, chatDocId, unsentMessageDocId, mqttTokenDocId, notificationDocId, statusDocId,

    mutedDocId, blockedDocId,

    callsDocId, contactsDocId, allContactsDocId, /*activeTimersDocId,*/
            userName, apiToken, userId, userImageUrl, userIdentifier;

    /**
     * logintype-0-email
     * <p>
     * 1-phone  number
     */

    private int signInType;
    private String indexDocId, pushToken;
    private CouchDbController db;

    private boolean signStatusChanged = false;

    private boolean signedIn = false;
    /*
     * Have put this value intentionally
     */
    private String activeReceiverId = "";
    private RequestQueue mRequestQueue;
    private ArrayList<String> colors;


    private int activeActivitiesCount;


    /*
     * For the unread chats count
     */
//    private int unreadChatCount = 0;
    private MqttAndroidClient mqttAndroidClient;
    private MqttConnectOptions mqttConnectOptions;
    private IMqttActionListener listener;
    private boolean flag = true;
    private ArrayList<HashMap<String, Object>> tokenMapping = new ArrayList<>();
    private HashSet<IMqttDeliveryToken> set = new HashSet<>();

    private boolean applicationKilled;
    private Typeface tfRegularFont, tfMediumFont, tfSemiboldFont, tfRegularRoboto;
    private long timeDelta = 0;


    private String defaultProfilePicUrl = "http://truesample.com/wp-content/themes/truesample/img/icon-Respondent020716v2.png";
    private boolean newSignupOrLogin = false;
    private boolean activeOnACall = false;


    private String activeCallId, activeCallerId;
    private String activeSecretId = "";
    private boolean profileSaved = false;
    /**
     * Initialize for callback in the chatlist or chatmessage screen activity
     */

    private boolean callMinimized = false;
    private boolean firstTimeAfterCallMinimized = false;
    private String deviceId;


    /*
     * Utilities for the resending of the unsent messages
     */
    private Intent notificationService;
    private boolean contactSynced = false;


    private boolean registeredObserver = false;

    public static Bus getBus() {
        return bus;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    private ArrayList<Map<String, Object>> dirtyContacts = new ArrayList<>();


    private ArrayList<Map<String, Object>> newContacts = new ArrayList<>();
    private ArrayList<Map<String, Object>> inactiveContacts = new ArrayList<>();


    private int pendingContactUpdateRequests = 0;
    private String userStatus;
    private boolean chatSynced;


    /*
     *
     * For clubbing of the notifications
     */

    private static final int NOTIFICATION_SIZE = 5;
    private ArrayList<Map<String, Object>> notifications = new ArrayList<>();


    private boolean activeOnVideoCall = false;

    /*
     *For auto download of the media messages
     *
     */

    // private boolean autoDownloadAllowed;
    private WifiManager wifiMgr;
    private WifiInfo wifiInfo;

    //changed here
    private boolean serviceAlreadyScheduled = false;

    /*
     *For message auto download
     */
    private String removedMessageString = "The message has been removed";

    /*
     *Starting the network service for the internet checking. */
    private void initNetworkService() {
        startService(new Intent(this, NetworkCheckerService.class));
    }

    @Override
    public void onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate();

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);

        //kochava
        Tracker.configure(new Tracker.Configuration(getApplicationContext())
                .setAppGuid("_YOUR_KOCHAVA_APP_GUID_")
                .setLogLevel(Tracker.LOG_LEVEL_INFO));

        try {
            MediaManager.init(this);
        } catch (IllegalStateException e) {
            e.fillInStackTrace();
        }

        Fresco.initialize(this);
        Fabric.with(this, new Crashlytics());

        TwitterManager.twitterInit(this, getResources().getString(R.string.TWITTER_CONSUMER_KEY), getResources().getString(R.string.TWITTER_CONSUMER_SECRET));
        mInstance = this;

        //initNetworkService();

        AppStateMonitor appStateMonitor = RxAppStateMonitor.create(this);
        appStateMonitor.addListener(new AppStateListener() {
            @Override
            public void onAppDidEnterForeground() {
                updatePresence(1, false);
                foreground = true;
            }

            @Override
            public void onAppDidEnterBackground() {


                updatePresence(0, false);
                foreground = false;

                updateTokenMapping();
            }
        });
        appStateMonitor.start();
        setBackgroundColorArray();


        sharedPref = this.getSharedPreferences("defaultPreferences", Context.MODE_PRIVATE);


        applicationKilled = sharedPref.getBoolean("applicationKilled", false);
        indexDocId = sharedPref.getString("indexDoc", null);
        db = new CouchDbController(new AndroidContext(this));

        if (indexDocId == null) {


            indexDocId = db.createIndexDocument();


            sharedPref.edit().putString("indexDoc", indexDocId).apply();


        }


        //autoDownloadAllowed = sharedPref.getBoolean("autoDownloadAllowed", true);
        pushToken = sharedPref.getString("pushToken", null);
        if (pushToken == null) {


            if (checkPlayServices()) {
                notificationService = new Intent(this, RegistrationIntentService.class);
//                try {
                startService(notificationService);
//                }catch (Exception e){
//                    e.fillInStackTrace();
//                }

            }

        }
        contactSynced = sharedPref.getBoolean("contactSynced", false);
        chatSynced = sharedPref.getBoolean("chatSynced", false);
        listener = new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {

                Log.d("log22", "connect");
                if (!contactSynced) {
                    JSONObject obj = new JSONObject();

                    try {
                        obj.put("eventName", "SyncContacts");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                //    Log.d("log53", "connectionsuccess");
                if (newSignupOrLogin) {

                    newSignupOrLogin = false;
                    try {
                        JSONObject tempObj = new JSONObject();
                        tempObj.put("status", 1);


                        /*
                         * Have to retain the message of currently being available for call
                         */


                        AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + userId, tempObj, 0, true);
                        AppController.getInstance().setActiveOnACall(false, true);


                    } catch (JSONException e) {

                    }
                } else {

                    /*
                     * Have put just for testing
                     */

                    /*TESTING*/

                    if (signedIn) {
                        try {
                            JSONObject obj = new JSONObject();
                            obj.put("status", 1);
                            publish(MqttEvents.CallsAvailability.value + "/" + userId, obj, 0, true);
                            AppController.getInstance().setActiveOnACall(false, true);


                        } catch (JSONException e) {

                        }
                    }


                }

                if (!applicationKilled) {
                    updatePresence(1, false);
                } else {

                    updatePresence(0, true);

                }

                /*
                 *
                 * Also have to make user available for call
                 *
                 */


                try {


                    JSONObject obj = new JSONObject();
                    obj.put("eventName", MqttEvents.Connect.value);
                    networkConnector.setConnected(true);
                    connectionObserver.isConnected(networkConnector);

                    bus.post(obj);


                } catch (Exception e) {

                }





                /*
                 * To stop the internet checking service
                 */


                if (flag) {

                    flag = false;
                    subscribeToTopic(MqttEvents.Message.value + "/" + userId, 1);
                    subscribeToTopic(MqttEvents.Acknowledgement.value + "/" + userId, 2);
                    subscribeToTopic(MqttEvents.Calls.value + "/" + userId, 0);

                    subscribeToTopic(MqttEvents.UserUpdates.value + "/" + userId, 1);


                    subscribeToTopic(MqttEvents.GroupChats.value + "/" + userId, 1);


                    /*
                     *
                     * Will have to control their subscription to be requirement based
                     */

                    subscribeToTopic(MqttEvents.FetchMessages.value + "/" + userId, 1);

                }


                resendUnsentMessages();


            }

            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {

                //    Log.d("log54", "connectionfailed" + exception.toString());
            }
        };


        Map<String, Object> signInDetails = db.isSignedIn(indexDocId);


        signedIn = (boolean) signInDetails.get("isSignedIn");


        if (signedIn) {
            signInType = (int) signInDetails.get("signInType");


            userId = (String) signInDetails.get("signedUserId");

            profileSaved = (boolean) signInDetails.get("profileSaved");


            getUserDocIdsFromDb(userId);


            createMQttConnection(userId, true);



            /*
             * First need to create the android client
             */

        }

        registerActivityLifecycleCallbacks(mInstance);


        tfRegularFont = Typeface.createFromAsset(mInstance.getAssets(), "fonts/proxima-nova-soft-regular.ttf");
        tfMediumFont = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSoft-Medium.otf");
        tfSemiboldFont = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSoft-Semibold.otf");
        tfRegularRoboto = Typeface.createFromAsset(getAssets(), "fonts/RobotoRegular.ttf");

        if (sharedPref.getBoolean("deltaRequired", true)) {
            getCurrentTime();

        } else {

            timeDelta = sharedPref.getLong("timeDelta", 0);


        }


        deviceId = new DeviceUuidFactory(this).getDeviceUuid();

        activeActivitiesCount = 0;


        //  Log.d("log23", applicationKilled + " app started");

    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        appComponent = DaggerAppComponent.builder()
                .application(this)
                .netModule(new NetworkModule())
                .appUtilModule(new AppUtilModule())
                .build();
        appComponent.inject(this);
        return appComponent;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public String getGroupChatsDocId() {
        return groupChatsDocId;
    }

    public void getUserDocIdsFromDb(String userId) {


        String userDocId = db.getUserInformationDocumentId(indexDocId, userId);


        ArrayList<String> docIds = db.getUserDocIds(userDocId);


        chatDocId = docIds.get(0);

        unsentMessageDocId = docIds.get(1);
        mqttTokenDocId = docIds.get(2);
//        unackMessageDocId = docIds.get(3);

        callsDocId = docIds.get(3);
        contactsDocId = docIds.get(4);
        //activeTimersDocId = docIds.get(5);
        allContactsDocId = docIds.get(5);
        statusDocId = docIds.get(6);


        notificationDocId = docIds.get(7);


        groupChatsDocId = docIds.get(8);
        mutedDocId = docIds.get(9);

        blockedDocId = docIds.get(10);

        getUserInfoFromDb(userDocId);

        tokenMapping = db.fetchMqttTokenMapping(mqttTokenDocId);

    }

    public void getUserInfoFromDb(String docId) {


        Map<String, Object> userInfo = db.getUserInfo(docId);


        userName = (String) userInfo.get("userName");
        userIdentifier = (String) userInfo.get("userIdentifier");


        apiToken = (String) userInfo.get("apiToken");


        userStatus = (String) userInfo.get("socialStatus");

        userId = (String) userInfo.get("userId");

        userImageUrl = (String) userInfo.get("userImageUrl");

        //  Log.d("log71", apiToken + " " + userName + " " + userIdentifier + " " + userId + " " + userImageUrl);


//        if (userImageUrl == null || userImageUrl.isEmpty()) {
//
//
//            /*
//             * Have put it as of now just for the sake of convenience, although needs to be removed later
//             */
//
//
//            userImageUrl = defaultProfilePicUrl;
//
//        }

//        unreadChatCount = db.getUnreadChatsReceiverUidsCount(chatDocId);

        notifications = db.fetchAllNotifications(notificationDocId);
    }

    /**
     * To check play services before starting service for generating push token
     */

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        return resultCode == ConnectionResult.SUCCESS;
    }

    public String getPushToken() {

        return pushToken;


    }

    public void setPushToken(String token) {

        this.pushToken = token;


        if (notificationService != null) {
            stopService(notificationService);
        }


    }

    /**
     * Prepare image or audio or video file for upload
     */
    @SuppressWarnings("all")

    public File convertByteArrayToFileToUpload(byte[] data, String name, String extension) {


        File file = null;

        try {


            File folder = new File(Environment.getExternalStorageDirectory().getPath() + ApiOnServer.CHAT_UPLOAD_THUMBNAILS_FOLDER);

            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }


            file = new File(Environment.getExternalStorageDirectory().getPath() + ApiOnServer.CHAT_UPLOAD_THUMBNAILS_FOLDER, name + extension);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(file);

            fos.write(data);
            fos.flush();
            fos.close();
        } catch (IOException e) {

        }


        return file;

    }

    /**
     * To prepare image file for upload
     */
    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {


        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;


            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    private Bitmap decodeSampledBitmapFromResource(String pathName,
                                                   int reqWidth, int reqHeight) {


        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);


        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);


        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, options);

    }

    /**
     * To upload image or video or audio to server using multipart upload to avoid OOM exception
     */
    @SuppressWarnings("TryWithIdenticalCatches,all")

    private void uploadFile(final Uri fileUri, final String name, final int messageType,
                            final JSONObject obj, final String receiverUid, final String id,
                            final HashMap<String, Object> mapTemp, final String secretId, final String extension,
                            final boolean toDelete, final boolean isGroupMessage, final Object groupMembersDocId) {


        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);


        final File file = FileUtils.getFile(this, fileUri);

        String url = null;
        if (messageType == 1) {

            url = name + ".jpg";


        } else if (messageType == 2) {

            url = name + ".mp4";


        } else if (messageType == 5) {

            url = name + ".mp3";


        } else if (messageType == 7) {

            url = name + ".jpg";


        } else if (messageType == 9) {

            url = name + extension;


        }


        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);


        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo", url, requestFile);


        String descriptionString = "Hola File Uploading";
        RequestBody description =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), descriptionString);

        // finally, execute the request
        Call<ResponseBody> call = service.upload(description, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {

/**
 *
 *
 * has to get url from the server in response
 *
 *
 * */


                try {


                    if (response.code() == 200) {


                        String url = null;
                        if (messageType == 1) {

                            url = name + ".jpg";


                        } else if (messageType == 2) {

                            url = name + ".mp4";


                        } else if (messageType == 5) {

                            url = name + ".mp3";


                        } else if (messageType == 7) {

                            url = name + ".jpg";


                        } else if (messageType == 9) {

                            url = name + extension;


                        }


                        obj.put("payload", Base64.encodeToString((ApiOnServer.CHAT_UPLOAD_PATH + url).getBytes("UTF-8"), Base64.DEFAULT));
                        obj.put("dataSize", file.length());
                        obj.put("timestamp", new Utilities().gmtToEpoch(Utilities.tsInGmt()));


                        if (toDelete) {
                            File fdelete = new File(fileUri.getPath());
                            if (fdelete.exists()) fdelete.delete();
                        }

                    }


                } catch (JSONException e) {

                } catch (IOException e) {

                }


                /**
                 *
                 *
                 * emitting to the server the values after the file has been uploaded
                 *
                 **/


                String tsInGmt = Utilities.tsInGmt();
                String docId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);
                db.updateMessageTs(docId, id, tsInGmt);


                if (isGroupMessage) {


                    AppController.getInstance().publishGroupChatMessage((String) groupMembersDocId, obj, mapTemp);

                } else {

                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + receiverUid, obj, 1, false, mapTemp);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


            }
        });
    }

    /**
     * Update change in signin status
     * If signed in then have to connect socket,start listening on various socket events and disconnect socket in case of signout, stop listening on various socket events
     */
    public void setSignedIn(boolean signedIn) {
        this.signedIn = signedIn;
    }

    public void setSignedIn(final boolean signedIn, final String userId, final String userName, final String userIdentifier, int signInType) {

        this.signedIn = signedIn;


        if (signedIn) {
            /*
             *
             * A temporary fix for now,have to commented out later,once things are sorted from server side
             *
             *
             * */


            mInstance.userId = userId;
            mInstance.userIdentifier = userIdentifier;
            mInstance.signInType = signInType;

            mInstance.userName = userName;
            newSignupOrLogin = true;

            getUserDocIdsFromDb(userId);
            createMQttConnection(userId, true);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {




                    /*
                     *
                     * for checking internet availability
                     * */

                    if (sharedPref.getString("chatNotificationArray", null) == null) {
                        SharedPreferences.Editor prefsEditor = sharedPref.edit();


                        prefsEditor.putString("chatNotificationArray", new Gson().toJson(new ArrayList<Map<String, String>>()));
                        prefsEditor.apply();
                    }


                }
            }, 1000);


        } else {

            flag = true;
            mInstance.userId = null;

            /*
             *
             * although userid is set to null but deviceid is still made to persist
             *
             *
             * */

//
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//
//                    if (intent != null)
//                        stopService(intent);
//
//
//                }
//            }, 1000);
        }


    }

    public long getTimeDelta() {
        return timeDelta;
    }

    public boolean getSignedIn() {
        return this.signedIn;
    }

    public String getChatDocId() {


        return chatDocId;
    }

    public String getUserIdentifier() {


        return userIdentifier;
    }

    public String getApiToken() {


        return apiToken;
    }

    public boolean isForeground() {
        return foreground;
    }

    public SharedPreferences getSharedPreferences() {

        return sharedPref;
    }

    public boolean isActiveOnVideoCall() {
        return activeOnVideoCall;
    }

    public boolean isSignStatusChanged() {
        return signStatusChanged;
    }

    public void setSignStatusChanged(boolean signStatusChanged) {
        this.signStatusChanged = signStatusChanged;
    }

    public String getActiveReceiverId() {


        return activeReceiverId;
    }

    public void setActiveReceiverId(String receiverId) {


        this.activeReceiverId = receiverId;
    }

    public void setActiveSecretId(String secretId) {


        this.activeSecretId = secretId;
    }


    public String getActiveSecretId() {


        return activeSecretId;
    }

    public boolean isCallMinimized() {
        return callMinimized;
    }

    public void setCallMinimized(boolean callMinimized) {
        this.callMinimized = callMinimized;


    }

    public boolean getContactSynced() {

        return contactSynced;


    }

    public boolean getChatSynced() {

        return chatSynced;


    }


    public void setChatSynced(boolean synced) {
        chatSynced = synced;
        sharedPref.edit().putBoolean("chatSynced", synced).apply();


    }


    public void setContactSynced(boolean synced) {

        contactSynced = synced;


        sharedPref.edit().putBoolean("contactSynced", synced).apply();
        String time = String.valueOf(Utilities.getGmtEpoch());
        sharedPref.edit().putString("lastUpdated", time).apply();

        sharedPref.edit().putString("lastDeleted", time).apply();

    }

    public boolean isActiveOnACall() {
        return activeOnACall;
    }

    public void setActiveOnACall(boolean activeOnACall, boolean notCallCut) {
        this.activeOnACall = activeOnACall;


        if (!activeOnACall && notCallCut) {

            this.callMinimized = false;

        }
    }

    public boolean isFirstTimeAfterCallMinimized() {
        return firstTimeAfterCallMinimized;
    }

    public void setFirstTimeAfterCallMinimized(boolean firstTimeAfterCallMinimized) {
        this.firstTimeAfterCallMinimized = firstTimeAfterCallMinimized;
    }

    public CouchDbController getDbController() {


        return db;
    }

    public void setApplicationKilled(boolean applicationKilled) {


        this.applicationKilled = applicationKilled;
//        sharedPref.edit().putBoolean("applicationKilled", applicationKilled).commit();


        sharedPref.edit().putBoolean("applicationKilled", applicationKilled).apply();


        if (applicationKilled) {


            unregisterContactsObserver();
            sharedPref.edit().putString("lastSeenTime", Utilities.tsInGmt()).apply();

        }


    }

    public boolean profileSaved() {
        return profileSaved;
    }


    public void setProfileSaved(boolean profileSaved) {
        this.profileSaved = profileSaved;
    }


    public int getSignInType() {
        return signInType;
    }


    //typefaces changed..
    public Typeface getRegularFont() {
        return tfRegularFont;
    }

    public Typeface getSemiboldFont() {
        return tfSemiboldFont;
    }

    public Typeface getMediumFont() {
        return tfMediumFont;
    }


    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserStatus() {
        return userStatus;
    }


    /**
     * To update the user status
     */

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;


        db.updateUserStatus(db.getUserDocId(userId, indexDocId), userStatus);

    }


    public String getUserImageUrl() {
        return userImageUrl;
    }


    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;


        db.updateUserImageUrl(db.getUserDocId(userId, indexDocId), userImageUrl);

    }


    public String getDefaultUserImageUrl() {
        return defaultProfilePicUrl;
    }


    public int getActiveActivitiesCount() {
        return activeActivitiesCount;
    }


    public String getDeviceId() {


        return deviceId;
    }


    public void setUserName(String userName) {
        this.userName = userName;


        db.updateUserName(db.getUserDocId(userId, indexDocId), userName);


    }


    public void setActiveCallId(String activeCallId) {
        this.activeCallId = activeCallId;
    }

    public void setActiveCallerId(String activeCallerId) {


        this.activeCallerId = activeCallerId;
    }

    public String getunsentMessageDocId() {
        return unsentMessageDocId;
    }

    public String getContactsDocId() {
        return contactsDocId;
    }

    public String getAllContactsDocId() {
        return allContactsDocId;
    }
    /*
     * volley methods to hit api on server
     */

    public String getCallsDocId() {
        return callsDocId;
    }


    public String getMutedDocId() {
        return mutedDocId;
    }

    public String getBlockedDocId() {
        return blockedDocId;
    }


//    public void decrementUnreadChatCount() {
//        unreadChatCount--;
//    }
//
//    public void incrementChatCount() {
//
//
//        unreadChatCount++;
//    }

    public String getIndexDocId() {

        return indexDocId;
    }


    public String getStatusDocId() {

        return statusDocId;
    }


    /**
     * To search if there exists any document containing chat for a particular receiver and if founde returns its document
     * id else return empty string
     */
    @SuppressWarnings("unchecked")
    public String findDocumentIdOfReceiver(String ReceiverUid, String secretId) {

        String docId = "";

        Map<String, Object> chatDetails = db.getAllChatDetails(AppController.getInstance().getChatDocId());

        if (chatDetails != null) {

            ArrayList<String> receiverUidArray = (ArrayList<String>) chatDetails.get("receiverUidArray");
            ArrayList<String> receiverDocIdArray = (ArrayList<String>) chatDetails.get("receiverDocIdArray");
            ArrayList<String> secretIdArray = (ArrayList<String>) chatDetails.get("secretIdArray");


            for (int i = 0; i < receiverUidArray.size(); i++) {
                if (receiverUidArray.get(i) != null && receiverUidArray.get(i).equals(ReceiverUid)) {
                    if (secretId.isEmpty()) {
                        if (secretIdArray.get(i).isEmpty()) {
                            return receiverDocIdArray.get(i);
                        }
                    } else {
                        if (secretIdArray.get(i).equals(secretId)) {
                            return receiverDocIdArray.get(i);
                        }
                    }
                }
            }
        }

        return docId;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    private void setBackgroundColorArray() {
        colors = new ArrayList<>();
        colors.add("#FFCDD2");
        colors.add("#D1C4E9");
        colors.add("#B3E5FC");
        colors.add("#C8E6C9");
        colors.add("#FFF9C4");
        colors.add("#FFCCBC");
        colors.add("#CFD8DC");
        colors.add("#F8BBD0");
        colors.add("#C5CAE9");
        colors.add("#B2EBF2");
        colors.add("#DCEDC8");
        colors.add("#FFECB3");
        colors.add("#D7CCC8");
        colors.add("#F5F5F5");
        colors.add("#FFE0B2");
        colors.add("#F0F4C3");
        colors.add("#B2DFDB");
        colors.add("#BBDEFB");
        colors.add("#E1BEE7");
    }

    public String getColorCode(int position) {
        return colors.get(position);
    }

    /**
     * @param clientId is same as the userId
     */
    @SuppressWarnings("unchecked")
    public MqttAndroidClient createMQttConnection(String clientId, boolean notFromJobScheduler) {


        if (mqttAndroidClient == null || mqttConnectOptions == null) {
            String serverUri = "tcp://" + ApiOnServer.DUBLY + ":" + ApiOnServer.MQTT_PORT;
            mqttAndroidClient = new MqttAndroidClient(mInstance, serverUri, clientId);
            mqttAndroidClient.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {

                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("eventName", MqttEvents.Disconnect.value);
                        networkConnector.setConnected(false);
                        connectionObserver.isConnected(networkConnector);
                        bus.post(obj);
                    } catch (Exception e) {

                    }
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {


                    //  Log.d("log21", message.toString());
                    JSONObject obj = convertMessageToJsonObject(message);

                    if (topic.equals(MqttEvents.Acknowledgement.value + "/" + userId)) {
                        /*
                         * For an acknowledgement message received
                         */
                        String sender = obj.getString("from");
                        String document_id_DoubleTick = obj.getString("doc_id");
                        String status = obj.getString("status");

                        JSONArray arr_temp = obj.getJSONArray("msgIds");
                        String id = arr_temp.getString(0);

                        /*
                         * For callback in to activity to update UI
                         */
                        try {
                            obj.put("msgId", id);
                            obj.put("eventName", topic);
                            bus.post(obj);
                        } catch (JSONException e) {

                        }


                        String secretId = "";
                        if (obj.has("secretId")) {
                            secretId = obj.getString("secretId");


                        }
                        if (status.equals("2")) {

                            /*
                             * message delivered
                             */
                            if (!(db.updateMessageStatus(document_id_DoubleTick, id, 1, obj.getString("deliveryTime")))) {
                                db.updateMessageStatus(
                                        db.getDocumentIdOfReceiverChatlistScreen(
                                                AppController.getInstance().getChatDocId(), sender, secretId), id, 1, obj.getString("deliveryTime"));

                            }
                        } else {

                            /*
                             * message read
                             */
                            if (!secretId.isEmpty()) {

                                if (obj.getLong("dTime") != -1) {
                                    String docId = findDocumentIdOfReceiver(sender, secretId);
                                    if (!docId.isEmpty()) {
                                        setTimer(docId, id, obj.getLong("dTime") * 1000);
                                    }
                                }
                            }


                            if (!(db.drawBlueTickUptoThisMessage(document_id_DoubleTick, id, obj.getString("readTime")))) {


                                db.drawBlueTickUptoThisMessage(
                                        db.getDocumentIdOfReceiverChatlistScreen(
                                                AppController.getInstance().getChatDocId(), sender, secretId), id, obj.getString("readTime"));


                            }

                        }


                    } else if (topic.equals(MqttEvents.Message.value + "/" + userId))

                    {

                        /*
                         * For an actual message(Like text,image,video etc.) received
                         */


                        String receiverUid = obj.getString("from");
                        String receiverIdentifier = obj.getString("receiverIdentifier");


                        String messageType = obj.getString("type");
                        String actualMessage = obj.getString("payload").trim();
                        String timestamp = String.valueOf(obj.getString("timestamp"));


                        String id = obj.getString("id");

                        String mimeType = "", fileName = "", extension = "";
                        String docIdForDoubleTickAck = obj.getString("toDocId");
                        int dataSize = -1;

                        String postId = "", postTitle = "";
                        int postType = -1;


                        if (messageType.equals("1") || messageType.equals("2") || messageType.equals("5") || messageType.equals("7")
                                || messageType.equals("9")) {
                            dataSize = obj.getInt("dataSize");


                            if (messageType.equals("9")) {


                                mimeType = obj.getString("mimeType");
                                fileName = obj.getString("fileName");
                                extension = obj.getString("extension");


                            }


                        } else if (messageType.equals("10")) {

                            String replyType = obj.getString("replyType");

                            if (replyType.equals("1") || replyType.equals("2") || replyType.equals("5") || replyType.equals("7")
                                    || replyType.equals("9")) {
                                dataSize = obj.getInt("dataSize");


                                if (replyType.equals("9")) {


                                    mimeType = obj.getString("mimeType");
                                    fileName = obj.getString("fileName");
                                    extension = obj.getString("extension");


                                }


                            } else if (replyType.equals("13")) {

                                postId = obj.getString("postId");
                                postTitle = obj.getString("postTitle");
                                postType = obj.getInt("postType");


                            }
                        } else if (messageType.equals("13")) {

                            postId = obj.getString("postId");
                            postTitle = obj.getString("postTitle");
                            postType = obj.getInt("postType");


                        }


                        String secretId = "";
                        long dTime = -1;
                        if (obj.has("secretId")) {
                            secretId = obj.getString("secretId");
                            dTime = obj.getLong("dTime");
                        }

                        String receiverName;
                        String userImage;

                         if (signInType == 0) {

                            receiverName = obj.getString("name");
                            userImage = obj.getString("userImage");
                        } else {

                            Map<String, Object> contactInfo = db.getContactInfoFromUid(AppController.getInstance().getContactsDocId(), receiverUid);

                            if (contactInfo != null) {
                                receiverName = (String) contactInfo.get("contactName");
                                userImage = (String) contactInfo.get("contactPicUrl");


                                /*
                                 * For showing correct name in secret chat invitation message
                                 */


                            } else {


                                /*
                                 * If userId doesn't exists in contact
                                 */
                                receiverName = receiverIdentifier;
//                                receiverName = obj.getString("userName");
                                userImage = obj.getString("userImage");

                            }
                            obj.put("contactName", receiverName);
                        }


                        String documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);
                        if (documentId.isEmpty()) {


                            /*
                             * Here, chatId is assumed to be empty
                             */
                            documentId = findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                    receiverName, userImage, secretId, true, receiverIdentifier, "", false);


                        }

                        /*
                         * For message removal
                         */


                        db.setDocumentIdOfReceiver(documentId, docIdForDoubleTickAck, receiverUid);
                        db.updateDestructTime(documentId, dTime);


                        /*
                         * For callback in to activity to update UI
                         */


                        if (!db.checkAlreadyExists(documentId, id)) {
                            String replyType = "";

                            if (messageType.equals("1") || messageType.equals("2") || messageType.equals("7")) {


                                AppController.getInstance().putMessageInDb(receiverUid, messageType,
                                        actualMessage, timestamp, id, documentId, obj.getString("thumbnail").trim(),
                                        dataSize, dTime, receiverName, false, -1, false, -1, null, obj.has("wasEdited"));
                            } else if (messageType.equals("9")) {
                                /*
                                 * For document received
                                 */

                                AppController.getInstance().putMessageInDb(receiverUid,
                                        actualMessage, timestamp, id, documentId, dataSize,
                                        dTime, false, -1, false, -1, mimeType, fileName, extension);


                            } else if (messageType.equals("10")) {

                                /*
                                 * For the reply message received
                                 */

                                replyType = obj.getString("replyType");

                                String previousMessageType = obj.getString("previousType");

                                String previousFileType = "", thumbnail = "";


                                if (previousMessageType.equals("9")) {

                                    previousFileType = obj.getString("previousFileType");

                                }
                                if (replyType.equals("1") || replyType.equals("2") || replyType.equals("7")) {


                                    thumbnail = obj.getString("thumbnail").trim();

                                }


                                AppController.getInstance().putReplyMessageInDb(receiverUid,
                                        actualMessage, timestamp, id, documentId,
                                        dataSize, dTime, false, -1, false, -1, mimeType, fileName, extension, thumbnail, receiverName,
                                        replyType, obj.getString("previousReceiverIdentifier"), obj.getString("previousFrom"),
                                        obj.getString("previousPayload"), obj.getString("previousType"), obj.getString("previousId"), previousFileType, obj.has("wasEdited"), postId, postTitle, postType);


                            } else if (messageType.equals("11")) {


                                db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("removedAt")));
                                db.markMessageAsRemoved(documentId, id, removedMessageString, Utilities.epochtoGmt(obj.getString("removedAt")));
                                try {

                                    obj.put("eventName", topic);
                                    bus.post(obj);


                                } catch (Exception e) {

                                }
                                return;
                            } else if (messageType.equals("12")) {


                                db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("editedAt")));
                                db.markMessageAsEdited(documentId, id, actualMessage);
                                try {

                                    obj.put("eventName", topic);
                                    bus.post(obj);


                                } catch (Exception e) {

                                }
                                return;
                            } else if (messageType.equals("13")) {

                                AppController.getInstance().putPostMessageInDb(receiverUid,
                                        actualMessage, timestamp, id, documentId,
                                        dTime, false, -1, false, -1, postId, postTitle, postType);


                            } else {

                                AppController.getInstance().putMessageInDb(receiverUid,
                                        messageType, actualMessage, timestamp, id, documentId, null, dataSize, dTime, receiverName, false, -1, false, -1, null, obj.has("wasEdited"));


                            }


                            //     if (!messageType.equals("11") && !messageType.equals("12") &&(!messageType.equals("0") || !actualMessage.isEmpty())) {

                            if (!messageType.equals("0") || !actualMessage.isEmpty()) {

                                JSONObject obj2 = new JSONObject();
                                obj2.put("from", AppController.getInstance().userId);
                                obj2.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));
                                obj2.put("doc_id", docIdForDoubleTickAck);
                                obj2.put("to", receiverUid);

                                obj2.put("status", "2");


                                if (!secretId.isEmpty()) {
                                    obj2.put("secretId", secretId);
                                    obj2.put("dTime", dTime);

                                }
                                obj2.put("deliveryTime", Utilities.getGmtEpoch());

                                AppController.getInstance().publish(MqttEvents.Acknowledgement.value + "/" + receiverUid, obj2, 2, false);


                            }
                            /*
                             * For callback in to activity to update UI
                             */


                            try {

                                obj.put("eventName", topic);
                                bus.post(obj);


                            } catch (Exception e) {

                            }

//                        if (contactSynced)
                            {
                                if (!db.checkIfReceiverChatMuted(mutedDocId, receiverUid, secretId)) {
                                    Intent intent;


//                        if (signInType != 0) {


//                            if (secretId.isEmpty()) {
//                                intent = new Intent(mInstance, com.example.moda.mqttchat.ContactSync.Activities.ChatMessagesScreen.class);
//                            } else {
//
//                                intent = new Intent(mInstance, com.example.moda.mqttchat.ContactSync.SecretChat.SecretChatMessageScreen.class);
//                                intent.putExtra("secretId", secretId);
//                            }


                                    /*
                                     * To allow retrieval
                                     */


                                    if (secretId.isEmpty()) {
                                        intent = new Intent(mInstance, ChatMessageScreen.class);
                                    } else {

                                        intent = new Intent(mInstance, SecretChatMessageScreen.class);
                                        intent.putExtra("secretId", secretId);
                                    }


                                    //  }

                                    intent.putExtra("receiverUid", receiverUid);
                                    intent.putExtra("receiverName", receiverName);

                                    intent.putExtra("receiverIdentifier", receiverIdentifier);

                                    intent.putExtra("documentId", documentId);


                                    intent.putExtra("colorCode", AppController.getInstance().getColorCode(5));

                                    intent.putExtra("receiverImage", userImage);


                                    intent.putExtra("fromNotification", true);




                                    /*
                                     *To generate the push notification locally
                                     */


                                    generatePushNotificationLocal(documentId, messageType, receiverName,
                                            actualMessage, intent, dTime, secretId, receiverUid, replyType);
                                }
                            }

                            int type = Integer.parseInt(messageType);
                            int replyTypeInt = -1;
                            if (!replyType.isEmpty()) {
                                replyTypeInt = Integer.parseInt(replyType);
                            }

                            if (type == 1 || type == 2 || type == 5 || type == 7 || type == 9 ||
                                    (type == 10 && (replyTypeInt == 1 || replyTypeInt == 2 || replyTypeInt == 5
                                            || replyTypeInt == 7 || replyTypeInt == 9))) {
                                if (ActivityCompat.checkSelfPermission(mInstance, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                        == PackageManager.PERMISSION_GRANTED) {
//                            if (autoDownloadAllowed) {

                                    if (checkWifiConnected()) {

                                        Object[] params = new Object[8];
                                        try {


                                            params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                        } catch (UnsupportedEncodingException e) {

                                        }

                                        params[1] = messageType;
                                        params[5] = id;
                                        params[6] = documentId;
                                        params[7] = receiverUid;
                                        switch (type) {
                                            case 1:

                                            {
                                                if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;

                                            }
                                            case 2: {
                                                if (sharedPref.getBoolean("wifiVideo", false)) {
                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 5: {
                                                if (sharedPref.getBoolean("wifiAudio", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                    new DownloadMessage().execute(params);
                                                }
                                                break;

                                            }
                                            case 7: {
                                                if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 9: {
                                                if (sharedPref.getBoolean("wifiDocument", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                            + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                                    new DownloadMessage().execute(params);
                                                }
                                                break;
                                            }
                                            case 10: {


                                                params[2] = obj.getString("replyType");

                                                switch (Integer.parseInt((String) params[2])) {

                                                    case 1:

                                                    {
                                                        if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;

                                                    }
                                                    case 2:

                                                    {
                                                        if (sharedPref.getBoolean("wifiVideo", false)) {
                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 5: {
                                                        if (sharedPref.getBoolean("wifiAudio", false)) {
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";

                                                            new DownloadMessage().execute(params);

                                                        }
                                                        break;
                                                    }

                                                    case 7: {
                                                        if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 9: {
                                                        if (sharedPref.getBoolean("wifiDocument", false)) {
                                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                    + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                }
                                                break;
                                            }

                                        }


                                    } else if (checkMobileDataOn()) {


                                        Object[] params = new Object[8];
                                        try {


                                            params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                        } catch (UnsupportedEncodingException e) {

                                        }

                                        params[1] = messageType;
                                        params[5] = id;
                                        params[6] = documentId;
                                        params[7] = receiverUid;
                                        switch (type) {
                                            case 1:

                                            {
                                                if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;

                                            }
                                            case 2: {
                                                if (sharedPref.getBoolean("mobileVideo", false)) {
                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 5: {
                                                if (sharedPref.getBoolean("mobileAudio", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                    new DownloadMessage().execute(params);
                                                }
                                                break;

                                            }
                                            case 7: {
                                                if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 9: {
                                                if (sharedPref.getBoolean("mobileDocument", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                            + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                                    new DownloadMessage().execute(params);
                                                }
                                                break;
                                            }
                                            case 10: {


                                                params[2] = obj.getString("replyType");

                                                switch (Integer.parseInt((String) params[2])) {

                                                    case 1:

                                                    {
                                                        if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;

                                                    }
                                                    case 2:

                                                    {
                                                        if (sharedPref.getBoolean("mobileVideo", false)) {
                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 5: {
                                                        if (sharedPref.getBoolean("mobileAudio", false)) {
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";

                                                            new DownloadMessage().execute(params);

                                                        }
                                                        break;
                                                    }

                                                    case 7: {
                                                        if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 9: {
                                                        if (sharedPref.getBoolean("mobileDocument", false)) {
                                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                    + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                }
                                                break;
                                            }

                                        }


                                    }


                                }
                            }
                        } else {


                            if (messageType.equals("11")) {
                                db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("removedAt")));
                                db.markMessageAsRemoved(documentId, id, removedMessageString, Utilities.epochtoGmt(obj.getString("removedAt")));
                                try {

                                    obj.put("eventName", topic);
                                    bus.post(obj);


                                } catch (Exception e) {

                                }
                                return;
                            } else if (messageType.equals("12")) {


                                db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("editedAt")));
                                db.markMessageAsEdited(documentId, id, actualMessage);
                                try {

                                    obj.put("eventName", topic);
                                    bus.post(obj);


                                } catch (Exception e) {

                                }
                                return;
                            }

                        }
                        if (!secretId.isEmpty()) {
                            if (!actualMessage.trim().isEmpty() || dTime != -1) {
                                db.updateSecretInviteImageVisibility(documentId, false);
                            }
                        }


                    } else if (topic.substring(0, 3).equals("Onl"))

                    {

                        /*
                         * For a message received on the online status topic
                         */


                        /*
                         * To check for the online status
                         */


                        try {

                            obj.put("eventName", topic);
                            bus.post(obj);

                        } catch (Exception e) {

                        }

                    } else if (topic.substring(0, 4).equals(MqttEvents.Typing.value)) {





                        /*
                         * For a message received on the Typing status topic
                         */


                        if (!activeReceiverId.isEmpty()) {


                            try {

                                obj.put("eventName", topic);
                                bus.post(obj);

                            } catch (Exception e) {

                            }


                        }


                    } else if (topic.equals(MqttEvents.Calls.value + "/" + userId)) {

                        /*
                         * For message received on the actual call event
                         */


                        if (obj.getInt("type") == 0) {

                            /*
                             * For receiving of the call request,will set mine status to be busy as well and open the incoming call screen
                             */


                            JSONObject tempObj = new JSONObject();
                            tempObj.put("status", 0);


                            /*
                             * Have to retain the message of currently being busy
                             */
                            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + userId, tempObj, 0, true);
                            AppController.getInstance().setActiveOnACall(true, true);

                            if (signInType == 0) {


                                CallingApis.OpenIncomingCallScreen(obj, mInstance);
                            } else {

                                OpenIncomingCallScreen(obj, mInstance);

                            }
                        }


                        obj.put("eventName", topic);


                        bus.post(obj);


                    } else if (topic.substring(0, 6).equals(MqttEvents.Calls.value + "A"))

                    {


                        obj.put("eventName", topic);
                        /*
                         * Will have to eventually unsubscribe from this topic,as only use  of this is to check if opponent is available to receive the call
                         */

                        bus.post(obj);

                        /*
                         * For message received on the call event to check for the availability
                         */


                        unsubscribeToTopic(topic);
                    } else if (topic.equals(MqttEvents.ContactSync.value + "/" + userId)) {


                        /*
                         * To receive the result of the posty contacts api
                         */


                        try {

                            obj.put("eventName", topic);
                            bus.post(obj);

                        } catch (Exception e) {

                        }
                    } else if (topic.equals(MqttEvents.UserUpdates.value + "/" + userId))

                    {



                        /*
                         * Instead of taking callbacks on 2 sepearte interface implementations,its better to use the otto bus
                         */


                        if (obj.getInt("type") != 4 && obj.getInt("type") != 3) {
                            obj.put("eventName", topic);
                            bus.post(obj);
                        }

                        switch (obj.getInt("type")) {


                            case 6:
                                if (obj.getBoolean("blocked")) {
                                    db.addBlockedUser(contactsDocId, obj.getString("initiatorId"), obj.getString("initiatorIdentifier"), false);

                                } else {


                                    db.removeUnblockedUser(contactsDocId, obj.getString("initiatorId"));
                                }

                                break;


                            case 1:
                                db.updateContactDetails(AppController.getInstance().getContactsDocId(),
                                        obj.getString("userId"), null, obj.getString("socialStatus"));

                                break;
                            case 2:

                                db.updateContactDetails(AppController.getInstance().getContactsDocId(),
                                        obj.getString("userId"), obj.getString("profilePic"), null);

                                break;


                            case 3:
                                obj = obj.getJSONArray("contacts").getJSONObject(0);


                                String name = findNewlyJoinedContactName(obj.getString("number"), obj.getString("userId"));

                                if (!name.isEmpty()) {
                                    JSONObject tempObj = new JSONObject();

                                    tempObj.put("name", name);
                                    tempObj.put("number", obj.getString("number"));
                                    tempObj.put("userId", obj.getString("userId"));
                                    tempObj.put("type", 3);
                                    tempObj.put("eventName", topic);


                                    bus.post(tempObj);
                                }

                                break;
                            case 4:
                                pendingContactUpdateRequests--;

                                JSONArray array = obj.getJSONArray("contacts");

                                JSONObject contact;
                                boolean updatedContactExists;
                                String contactProfilePic, contactStatus;
                                Map<String, Object> dirtyContact;
                                Map<String, Object> newContact;
                                int k = 0;


                                boolean alreadyFound;
                                for (int j = 0; j < array.length(); j++) {
                                    try {
                                        contact = array.getJSONObject(j);
                                        alreadyFound = false;

                                        if (dirtyContacts.size() > 0) {

                                            updatedContactExists = false;

                                            for (int i = 0; i < dirtyContacts.size(); i++) {

                                                dirtyContact = dirtyContacts.get(i);

                                                if ((dirtyContact.get("contactNumber").equals(contact.getString("localNumber")))
                                                        &&
                                                        (dirtyContact.get("contactUid").equals(contact.getString("_id")))) {

                                                    updatedContactExists = true;

                                                    k = i;
                                                    break;


                                                }


                                            }

                                            if (updatedContactExists) {

                                                alreadyFound = true;
                                                dirtyContact = dirtyContacts.get(k);
                                                dirtyContacts.remove(k);
                                                /*
                                                 * For the updated contact name and contact identifier to be sent as callback
                                                 */


                                                JSONObject obj2 = new JSONObject();
                                                obj2.put("eventName", topic);
                                                obj2.put("subtype", 0);
                                                obj2.put("type", 4);
                                                obj2.put("contactUid", dirtyContact.get("contactUid"));
                                                obj2.put("contactName", dirtyContact.get("contactName"));


                                                bus.post(obj2);


                                                /*
                                                 *  Update the corresponding contact details
                                                 */


                                                Map<String, Object> contactDetails = new HashMap<>();
                                                contactDetails.put("contactLocalId", dirtyContact.get("contactId"));


                                                contactDetails.put("contactLocalNumber", contact.getString("localNumber"));

                                                contactDetails.put("contactUid", contact.getString("_id"));
                                                contactDetails.put("contactName", dirtyContact.get("contactName"));

                                                contactDetails.put("contactIdentifier", contact.getString("number"));
                                                contactProfilePic = "";


                                                if (contact.has("profilePic")) {
                                                    contactProfilePic = contact.getString("profilePic");
                                                }


                                                if (contact.has("socialStatus")) {

                                                    contactStatus = contact.getString("socialStatus");

                                                } else {
                                                    contactStatus = getString(R.string.default_status);
                                                }
                                                contactDetails.put("contactStatus", contactStatus);
                                                contactDetails.put("contactPicUrl", contactProfilePic);


                                                db.addUpdatedContact(contactsDocId, contactDetails, contact.getString("_id"));


                                            }


                                        }

                                        if (!alreadyFound && newContacts.size() > 0) {

                                            /*
                                             * New contacts were added
                                             */


                                            for (int i = 0; i < newContacts.size(); i++) {


                                                newContact = newContacts.get(i);


                                                if ((newContact.get("phoneNumber").equals(contact.getString("localNumber")))) {
                                                    alreadyFound = true;
                                                    Map<String, Object> contactDetails = new HashMap<>();
                                                    contactDetails.put("contactLocalId", newContact.get("contactId"));


                                                    contactDetails.put("contactLocalNumber", contact.getString("localNumber"));

                                                    contactDetails.put("contactUid", contact.getString("_id"));
                                                    contactDetails.put("contactName", newContact.get("userName"));

                                                    contactDetails.put("contactIdentifier", contact.getString("number"));
                                                    contactProfilePic = "";


                                                    if (contact.has("profilePic")) {
                                                        contactProfilePic = contact.getString("profilePic");
                                                    }


                                                    if (contact.has("socialStatus")) {

                                                        contactStatus = contact.getString("socialStatus");

                                                    } else {
                                                        contactStatus = getString(R.string.default_status);
                                                    }
                                                    contactDetails.put("contactStatus", contactStatus);
                                                    contactDetails.put("contactPicUrl", contactProfilePic);


                                                    db.addUpdatedContact(contactsDocId, contactDetails, contact.getString("_id"));


                                                    JSONObject obj2 = new JSONObject();
                                                    obj2.put("subtype", 2);
                                                    obj2.put("contactUid", contact.getString("_id"));
                                                    obj2.put("contactStatus", contactStatus);
                                                    obj2.put("contactPicUrl", contactProfilePic);
                                                    obj2.put("contactName", newContact.get("userName"));
                                                    obj2.put("contactIdentifier", contact.getString("number"));
                                                    obj2.put("contactLocalId", newContact.get("contactId"));
                                                    obj2.put("contactLocalNumber", contact.getString("localNumber"));
                                                    obj2.put("eventName", topic);
                                                    obj2.put("type", 4);
                                                    bus.post(obj2);


                                                    newContacts.remove(i);
                                                }

                                            }


                                            /*
                                             * Not clearing it here intentionally
                                             */

                                        }


                                        if (!alreadyFound && inactiveContacts.size() > 0) {
                                            Map<String, Object> inactiveContact;
                                            for (int i = 0; i < inactiveContacts.size(); i++) {


                                                inactiveContact = inactiveContacts.get(i);


                                                if ((inactiveContact.get("phoneNumber").equals(contact.getString("localNumber")))) {

                                                    Map<String, Object> contactDetails = new HashMap<>();
                                                    contactDetails.put("contactLocalId", inactiveContact.get("contactId"));


                                                    contactDetails.put("contactLocalNumber", contact.getString("localNumber"));

                                                    contactDetails.put("contactUid", contact.getString("_id"));
                                                    contactDetails.put("contactName", inactiveContact.get("userName"));

                                                    contactDetails.put("contactIdentifier", contact.getString("number"));
                                                    contactProfilePic = "";


                                                    if (contact.has("profilePic")) {
                                                        contactProfilePic = contact.getString("profilePic");
                                                    }


                                                    if (contact.has("socialStatus")) {

                                                        contactStatus = contact.getString("socialStatus");

                                                    } else {
                                                        contactStatus = getString(R.string.default_status);
                                                    }
                                                    contactDetails.put("contactStatus", contactStatus);
                                                    contactDetails.put("contactPicUrl", contactProfilePic);


                                                    db.addUpdatedContact(contactsDocId, contactDetails, contact.getString("_id"));


                                                    JSONObject obj2 = new JSONObject();
                                                    obj2.put("subtype", 2);
                                                    obj2.put("contactUid", contact.getString("_id"));
                                                    obj2.put("contactStatus", contactStatus);
                                                    obj2.put("contactPicUrl", contactProfilePic);
                                                    obj2.put("contactName", inactiveContact.get("userName"));
                                                    obj2.put("contactIdentifier", contact.getString("number"));
                                                    obj2.put("contactLocalId", inactiveContact.get("contactId"));
                                                    obj2.put("contactLocalNumber", contact.getString("localNumber"));
                                                    obj2.put("eventName", topic);
                                                    obj2.put("type", 4);
                                                    bus.post(obj2);


                                                    inactiveContacts.remove(i);
                                                }

                                            }

                                        }

                                    } catch (JSONException e) {

                                    }

                                }


                                if (pendingContactUpdateRequests == 0) {

                                    if (dirtyContacts.size() > 0) {
                                        /*
                                         * Active contact number was changed but is no longer in list of active contact numbers
                                         */

                                        for (int i = 0; i < dirtyContacts.size(); i++) {


                                            db.deleteActiveContactByUid(contactsDocId, (String) dirtyContacts.get(i).get("contactUid"));

                                            JSONObject obj2 = new JSONObject();

                                            obj2.put("subtype", 1);
                                            obj2.put("contactUid", dirtyContacts.get(i).get("contactUid"));
                                            obj2.put("eventName", topic);
                                            obj2.put("type", 4);
                                            bus.post(obj2);

                                            dirtyContacts.remove(i);
                                        }


                                    }


                                    inactiveContacts.clear();
                                    newContacts.clear();

                                }

                                AppController.getInstance().getSharedPreferences()
                                        .edit().putString("lastUpdated", String.valueOf(Utilities.getGmtEpoch())).apply();
                                break;
                            case 5:

                                JSONArray array2 = obj.getJSONArray("contacts");

                                JSONObject contact2;


                                for (int j = 0; j < array2.length(); j++) {
                                    try {
                                        contact2 = array2.getJSONObject(j);


                                        /*
                                         * status 0- number registered on app
                                         *
                                         * status 1- number not registered on app
                                         */


                                        if (contact2.has("status")) {
                                            if (contact2.getInt("status") == 0) {


                                                /*
                                                 *Its actually a soft delete if user has chats and calls with him
                                                 *
                                                 */
                                                db.deleteActiveContact(contactsDocId, contact2.getString("userId"));


                                            }


                                            db.deleteContactFromAllContactsList(allContactsDocId, contact2.getString("localNumber"));


                                        }

                                    } catch (JSONException e) {

                                    }
                                }

                                AppController.getInstance().getSharedPreferences()
                                        .edit().putString("lastDeleted", String.valueOf(Utilities.getGmtEpoch())).apply();

                                break;

                        }


                    } else if (topic.equals(MqttEvents.FetchChats.value + "/" + userId)) {
                        /*
                         * To fetch the list of the chats
                         */
                        try {

                            JSONArray chats = obj.getJSONArray("chats");



                            /*
                             *New flag names as groupChat has been added to identify if the chat is the group chat
                             */

                            String tsInGmt, receiverName;
                            JSONObject jsonObject;
                            Map<String, Object> contactInfo;
                            boolean wasInvited;

                            int totalUnreadMessageCount;


                            boolean hasNewMessage, isSelf;
                            String profilePic;


                            for (int j = 0; j < chats.length(); j++) {

                                jsonObject = chats.getJSONObject(j);


                                if (jsonObject.getBoolean("groupChat")) {
                                    /*
                                     * For the group chat,will have seperate way of handling chatlist
                                     */


                                    if (jsonObject.getString("senderId").equals(userId)) {
                                        isSelf = true;
                                    } else {
                                        isSelf = false;
                                    }


                                    totalUnreadMessageCount = jsonObject.getInt("totalUnread");

                                    hasNewMessage = totalUnreadMessageCount > 0;
                                    tsInGmt = Utilities.epochtoGmt(String.valueOf(jsonObject.getLong("timestamp")));


                                    /*
                                     *
                                     *This is equal to the groupName
                                     */
                                    receiverName = jsonObject.getString("userName");

                                    /*
                                     * For the group image
                                     */
                                    if (jsonObject.has("profilePic") && jsonObject.getString("profilePic") != null) {
                                        profilePic = jsonObject.getString("profilePic");
                                    } else {
                                        profilePic = "";
                                    }


                                    String documentId = AppController.getInstance().
                                            findDocumentIdOfReceiver(jsonObject.getString("chatId"), "");


                                    if (documentId.isEmpty()) {


                                        documentId = findDocumentIdOfReceiver(jsonObject.getString("chatId"),
                                                tsInGmt, receiverName, profilePic, "",
                                                false, jsonObject.getString("chatId"), jsonObject.getString("chatId"), true);
                                    } else {
                                        /*
                                         * If doc already exists then have to update the corresponding chat id
                                         */

                                        db.updateChatId(documentId, jsonObject.getString("chatId"));


                                    }


                                    if (jsonObject.has("payload")) {
                                        /*
                                         * Last message has been a normal group chat message
                                         */

                                        switch (Integer.parseInt(jsonObject.getString("messageType"))) {

                                            case 0:

                                                String text = "";
                                                try {

                                                    text = new String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {

                                                }


                                                if (text.trim().isEmpty()) {


                                                    if (jsonObject.getLong("dTime") != -1) {
                                                        String message_dTime = String.valueOf(jsonObject.getLong("dTime"));

                                                        for (int i = 0; i < dTimeForDB.length; i++) {
                                                            if (message_dTime.equals(dTimeForDB[i])) {

                                                                if (i == 0) {


                                                                    text = getString(R.string.Timer_set_off);
                                                                } else {


                                                                    text = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                                                }
                                                                break;
                                                            }
                                                        }


                                                        db.updateChatListForNewMessageFromHistory(
                                                                documentId, text, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    } else {


                                                        if (jsonObject.getString("senderId").equals(userId)) {


                                                            text = getResources().getString(R.string.YouInvited) + " " + receiverName + " " +
                                                                    getResources().getString(R.string.JoinSecretChat);
                                                        } else {

                                                            text = getResources().getString(R.string.youAreInvited) + " " + receiverName + " " +
                                                                    getResources().getString(R.string.JoinSecretChat);

                                                        }

                                                        db.updateChatListForNewMessageFromHistory(
                                                                documentId, text, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                false, jsonObject.getInt("status"));


                                                    }

                                                } else {
                                                    db.updateChatListForNewMessageFromHistory(
                                                            documentId, text, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                }
                                                break;
                                            case 1:
                                                /*
                                                 * Image message
                                                 */


                                                if (totalUnreadMessageCount > 0) {
                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewImage), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                } else {
                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Image), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                }
                                                break;
                                            case 2:

                                                /*
                                                 * Video message
                                                 */
                                                if (totalUnreadMessageCount > 0) {

                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewVideo),
                                                            hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                } else {
                                                    db.updateChatListForNewMessageFromHistory(documentId,
                                                            getString(R.string.Video),
                                                            hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                }

                                                break;
                                            case 3:
                                                /*
                                                 * Location message
                                                 */
                                                if (totalUnreadMessageCount > 0) {


                                                    db.updateChatListForNewMessageFromHistory(

                                                            documentId, getString(R.string.NewLocation), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                } else {

                                                    db.updateChatListForNewMessageFromHistory(

                                                            documentId, getString(R.string.Location), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                }
                                                break;
                                            case 4:
                                                /*
                                                 * Follow message
                                                 */

                                                if (totalUnreadMessageCount > 0) {
                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewContact),
                                                            hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                } else {

                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Contact),
                                                            hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                }
                                                break;
                                            case 5:

                                                /*
                                                 * Audio message
                                                 */

                                                if (totalUnreadMessageCount > 0) {
                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewAudio), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                } else {
                                                    db.updateChatListForNewMessageFromHistory(documentId,
                                                            getString(R.string.Audio), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                }
                                                break;
                                            case 6:


                                                /*
                                                 * Sticker
                                                 */
                                                if (totalUnreadMessageCount > 0) {


                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewSticker), hasNewMessage, tsInGmt,
                                                            tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                } else {

                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Stickers), hasNewMessage, tsInGmt,
                                                            tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                }
                                                break;
                                            case 7:
                                                /*
                                                 * Doodle
                                                 */
                                                if (totalUnreadMessageCount > 0) {

                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewDoodle), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                } else {

                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Doodle), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                }

                                                break;
                                            case 8:
                                                /*
                                                 * Gif
                                                 */
                                                if (totalUnreadMessageCount > 0) {
                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewGiphy), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                } else {


                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Giphy), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                }
                                                break;


                                            case 9:
                                                /*
                                                 * Document
                                                 */
                                                if (totalUnreadMessageCount > 0) {
                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewDocument), hasNewMessage, tsInGmt,
                                                            tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                } else {


                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Document), hasNewMessage, tsInGmt, tsInGmt,
                                                            totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                }
                                                break;


                                            case 10:

                                            {


                                                switch (Integer.parseInt(jsonObject.getString("replyType"))) {
                                                    case 0:

                                                        String text2 = "";
                                                        try {

                                                            text2 = new String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT), "UTF-8");
                                                        } catch (UnsupportedEncodingException e) {

                                                        }

                                                        db.updateChatListForNewMessageFromHistory(
                                                                documentId, text2, hasNewMessage, tsInGmt,
                                                                tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                        break;
                                                    case 1:
                                                        /*
                                                         * receiverImage message
                                                         */


                                                        if (totalUnreadMessageCount > 0) {


                                                            db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewImage),
                                                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf,
                                                                    jsonObject.getInt("status"));
                                                        } else {
                                                            db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Image),
                                                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf,
                                                                    jsonObject.getInt("status"));
                                                        }
                                                        break;
                                                    case 2:

                                                        /*
                                                         * Video message
                                                         */
                                                        if (totalUnreadMessageCount > 0) {

                                                            db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewVideo),
                                                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                    isSelf, jsonObject.getInt("status"));
                                                        } else {
                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.Video),
                                                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                    isSelf, jsonObject.getInt("status"));
                                                        }

                                                        break;
                                                    case 3:
                                                        /*
                                                         * Location message
                                                         */
                                                        if (totalUnreadMessageCount > 0) {


                                                            db.updateChatListForNewMessageFromHistory(

                                                                    documentId, getString(R.string.NewLocation), hasNewMessage,
                                                                    tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                        } else {

                                                            db.updateChatListForNewMessageFromHistory(

                                                                    documentId, getString(R.string.Location), hasNewMessage,
                                                                    tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                        }
                                                        break;
                                                    case 4:
                                                        /*
                                                         * Follow message
                                                         */

                                                        if (totalUnreadMessageCount > 0) {
                                                            db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewContact),
                                                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                    isSelf, jsonObject.getInt("status"));

                                                        } else {

                                                            db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Contact),
                                                                    hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                    isSelf, jsonObject.getInt("status"));


                                                        }
                                                        break;
                                                    case 5:

                                                        /*
                                                         * Audio message
                                                         */

                                                        if (totalUnreadMessageCount > 0) {
                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.NewAudio), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                        } else {
                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.Audio), hasNewMessage, tsInGmt,
                                                                    tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                        }
                                                        break;
                                                    case 6:


                                                        /*
                                                         * Sticker
                                                         */
                                                        if (totalUnreadMessageCount > 0) {


                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.NewSticker), hasNewMessage, tsInGmt,
                                                                    tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                        } else {

                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.Stickers), hasNewMessage, tsInGmt,
                                                                    tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                        }
                                                        break;
                                                    case 7:
                                                        /*
                                                         * Doodle
                                                         */
                                                        if (totalUnreadMessageCount > 0) {

                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.NewDoodle), hasNewMessage, tsInGmt, tsInGmt,
                                                                    totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                        } else {

                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.Doodle), hasNewMessage, tsInGmt, tsInGmt,
                                                                    totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                        }

                                                        break;
                                                    case 8:
                                                        /*
                                                         * Gif
                                                         */
                                                        if (totalUnreadMessageCount > 0) {
                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.NewGiphy), hasNewMessage, tsInGmt, tsInGmt,
                                                                    totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                        } else {


                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.Giphy), hasNewMessage, tsInGmt, tsInGmt,
                                                                    totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                        }
                                                        break;


                                                    case 9:
                                                        /*
                                                         * Document
                                                         */
                                                        if (totalUnreadMessageCount > 0) {
                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.NewDocument), hasNewMessage, tsInGmt, tsInGmt,
                                                                    totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                        } else {


                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.Document), hasNewMessage, tsInGmt, tsInGmt,
                                                                    totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                        }
                                                        break;

                                                    case 13:
                                                        /*
                                                         * Post
                                                         */
                                                        if (totalUnreadMessageCount > 0) {
                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.NewPost), hasNewMessage, tsInGmt, tsInGmt,
                                                                    totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                        } else {


                                                            db.updateChatListForNewMessageFromHistory(documentId,
                                                                    getString(R.string.Post), hasNewMessage, tsInGmt, tsInGmt,
                                                                    totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                        }
                                                        break;


                                                }

                                                break;
                                            }


                                            case 11: {


                                                String text2 = "";
                                                try {

                                                    text2 = new String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {

                                                }


                                                //  db.updateChatListForNewMessageFromHistory(documentId, text2, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                db.updateChatListForNewMessageFromHistory(documentId, text2, hasNewMessage, Utilities
                                                        .epochtoGmt(jsonObject.getString("removedAt")), Utilities
                                                        .epochtoGmt(jsonObject.getString("removedAt")), totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                break;


                                            }
                                            case 13:
                                                /*
                                                 * Post
                                                 */
                                                if (totalUnreadMessageCount > 0) {
                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewPost), hasNewMessage, tsInGmt,
                                                            tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                } else {


                                                    db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Post), hasNewMessage, tsInGmt, tsInGmt,
                                                            totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                }
                                                break;


                                        }


                                    } else {


                                        /*
                                         * Last message has been the group tag message in the corresponding group chat
                                         */


                                        switch (Integer.parseInt(jsonObject.getString("messageType"))) {
                                            case 0: {
                                                /*
                                                 * Group created
                                                 *
                                                 */


                                                String initiatorName;

                                                if (jsonObject.getString("initiatorId").equals(AppController.getInstance().getUserId())) {

                                                    initiatorName = getString(R.string.You);
                                                } else {
                                                    initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("initiatorId"));
                                                    if (initiatorName == null) {

                                                        initiatorName = jsonObject.getString("initiatorIdentifier");
                                                    }
                                                }


                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.CreatedGroup, initiatorName)
                                                                + " " + jsonObject.getString("groupSubject"), hasNewMessage, tsInGmt,
                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                break;

                                            }


                                            case 1:


                                            {

                                                /*
                                                 * Member added
                                                 */


                                                String initiatorName, memberName;

                                                if (jsonObject.getString("initiatorId").equals(AppController.getInstance().getUserId())) {

                                                    initiatorName = getString(R.string.You);
                                                } else {
                                                    initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("initiatorId"));
                                                    if (initiatorName == null) {

                                                        initiatorName = jsonObject.getString("initiatorIdentifier");
                                                    }
                                                }


                                                if (jsonObject.getString("memberId").equals(AppController.getInstance().getUserId())) {

                                                    memberName = getString(R.string.YouSmall);
                                                } else {
                                                    memberName = db.getContactName(AppController.getInstance().getContactsDocId(), jsonObject.getString("memberId"));
                                                    if (memberName == null) {

                                                        memberName = jsonObject.getString("memberIdentifier");
                                                    }
                                                }


                                                db.updateChatListForNewMessageFromHistory(documentId, initiatorName + " " +
                                                                getString(R.string.AddedMember, memberName) + " " + getString(R.string.ToGroup), hasNewMessage, tsInGmt,
                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                break;
                                            }
                                            case 2:

                                            {
                                                /*
                                                 * Member removed
                                                 *
                                                 */


                                                String initiatorName, memberName;

                                                if (jsonObject.getString("initiatorId").equals(AppController.getInstance().getUserId())) {

                                                    initiatorName = getString(R.string.You);
                                                } else {
                                                    initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("initiatorId"));
                                                    if (initiatorName == null) {

                                                        initiatorName = jsonObject.getString("initiatorIdentifier");
                                                    }
                                                }


                                                if (jsonObject.getString("memberId").equals(AppController.getInstance().getUserId())) {

                                                    memberName = getString(R.string.YouSmall);
                                                } else {
                                                    memberName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("memberId"));
                                                    if (memberName == null) {

                                                        memberName = jsonObject.getString("memberIdentifier");
                                                    }
                                                }


                                                db.updateChatListForNewMessageFromHistory(documentId, initiatorName + " " + getString(R.string.Removed) + " " + memberName, hasNewMessage, tsInGmt,
                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                break;

                                            }
                                            case 3: {
                                                /*
                                                 * Made admin
                                                 *
                                                 */


                                                String initiatorName, memberName;

                                                if (jsonObject.getString("initiatorId").equals(AppController.getInstance().getUserId())) {

                                                    initiatorName = getString(R.string.You);
                                                } else {
                                                    initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("initiatorId"));
                                                    if (initiatorName == null) {

                                                        initiatorName = jsonObject.getString("initiatorIdentifier");
                                                    }
                                                }


                                                if (jsonObject.getString("memberId").equals(AppController.getInstance().getUserId())) {

                                                    memberName = getString(R.string.YouSmall);
                                                } else {
                                                    memberName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("memberId"));
                                                    if (memberName == null) {

                                                        memberName = jsonObject.getString("memberIdentifier");
                                                    }
                                                }


                                                db.updateChatListForNewMessageFromHistory(documentId, initiatorName + " " + getString(R.string.Made) + " " + memberName + " " + getString(R.string.MakeAdmin), hasNewMessage, tsInGmt,
                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                break;
                                            }
                                            case 4:

                                            {
                                                /*
                                                 * Group name updated
                                                 *
                                                 */


                                                String initiatorName;

                                                if (jsonObject.getString("initiatorId").equals(AppController.getInstance().getUserId())) {

                                                    initiatorName = getString(R.string.You);
                                                } else {
                                                    initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("initiatorId"));
                                                    if (initiatorName == null) {

                                                        initiatorName = jsonObject.getString("initiatorIdentifier");
                                                    }
                                                }


                                                db.updateChatListForNewMessageFromHistory(documentId,
                                                        initiatorName + " " + getString(R.string.UpdatedGroupSubject, jsonObject.getString("groupSubject")), hasNewMessage, tsInGmt,

                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                break;
                                            }
                                            case 5:


                                            {
                                                /*
                                                 * Group icon updated
                                                 *
                                                 */


                                                String initiatorName;

                                                if (jsonObject.getString("initiatorId").equals(AppController.getInstance().getUserId())) {

                                                    initiatorName = getString(R.string.You);
                                                } else {
                                                    initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("initiatorId"));
                                                    if (initiatorName == null) {

                                                        initiatorName = jsonObject.getString("initiatorIdentifier");
                                                    }
                                                }


                                                db.updateChatListForNewMessageFromHistory(documentId, initiatorName + " " + getString(R.string.UpdatedGroupIcon),
                                                        hasNewMessage, tsInGmt,
                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                break;

                                            }


                                            case 6: {

                                                /*
                                                 *Member left the group
                                                 */
                                                String memberName;


                                                if (jsonObject.getString("initiatorId").equals(AppController.getInstance().getUserId())) {

                                                    memberName = getString(R.string.You);
                                                } else {
                                                    memberName = db.getContactName(AppController.getInstance().getContactsDocId(),
                                                            jsonObject.getString("initiatorId"));
                                                    if (memberName == null) {

                                                        memberName = jsonObject.getString("initiatorIdentifier");
                                                    }
                                                }


                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.LeftGroup, memberName),
                                                        hasNewMessage, tsInGmt,
                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                break;
                                            }


                                        }


                                    }

                                } else {
                                    /*
                                     * For the normal or the secret chat
                                     */
                                    if (jsonObject.getString("senderId").equals(userId)) {
                                        isSelf = true;
                                    } else {
                                        isSelf = false;
                                    }


                                    totalUnreadMessageCount = jsonObject.getInt("totalUnread");

                                    hasNewMessage = totalUnreadMessageCount > 0;
                                    tsInGmt = Utilities.epochtoGmt(String.valueOf(jsonObject.getLong("timestamp")));


                                    contactInfo


                                            = db.getContactInfoFromUid(AppController.getInstance().getContactsDocId(),
                                            jsonObject.getString("recipientId"));

                                    if (contactInfo != null) {
                                        receiverName = (String) contactInfo.get("contactName");

                                    } else {


                                        /*
                                         * If userId doesn't exists in contact
                                         */
//                                        receiverName = jsonObject.getString("number");
                                        receiverName = jsonObject.getString("userName");


                                    }


                                    if (jsonObject.has("profilePic")) {
                                        profilePic = jsonObject.getString("profilePic");
                                    } else {


                                        profilePic = "";
                                    }

                                    String documentId = AppController.getInstance().
                                            findDocumentIdOfReceiver(jsonObject.getString("recipientId"), jsonObject.getString("secretId"));


                                    if (documentId.isEmpty()) {

                                        wasInvited = !jsonObject.getString("senderId").equals(userId);
//                                        documentId = findDocumentIdOfReceiver(jsonObject.getString("recipientId"),
//                                                tsInGmt, receiverName, profilePic, jsonObject.getString("secretId"),
//                                                wasInvited, jsonObject.getString("number"), jsonObject.getString("chatId"), false);
                                        documentId = findDocumentIdOfReceiver(jsonObject.getString("recipientId"),
                                                tsInGmt, receiverName, profilePic, jsonObject.getString("secretId"),
                                                wasInvited, jsonObject.getString("userName"), jsonObject.getString("chatId"), false);


                                    } else {
                                        /*
                                         * If doc already exists then have to update the corresponding chat id
                                         */

                                        db.updateChatId(documentId, jsonObject.getString("chatId"));


                                    }
                                    if (!jsonObject.getString("secretId").isEmpty()) {


                                        db.updateDestructTime(documentId, jsonObject.getLong("dTime"));
                                    }

                                    switch (Integer.parseInt(jsonObject.getString("messageType"))) {

                                        case 0:

                                            String text = "";
                                            try {

                                                text = new String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {

                                            }


                                            if (text.trim().isEmpty()) {


                                                if (jsonObject.getLong("dTime") != -1) {
                                                    String message_dTime = String.valueOf(jsonObject.getLong("dTime"));

                                                    for (int i = 0; i < dTimeForDB.length; i++) {
                                                        if (message_dTime.equals(dTimeForDB[i])) {

                                                            if (i == 0) {


                                                                text = getString(R.string.Timer_set_off);
                                                            } else {


                                                                text = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                                            }
                                                            break;
                                                        }
                                                    }


                                                    db.updateChatListForNewMessageFromHistory(
                                                            documentId, text, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                } else {


                                                    if (jsonObject.getString("senderId").equals(userId)) {


                                                        text = getResources().getString(R.string.YouInvited) + " " + receiverName + " " +
                                                                getResources().getString(R.string.JoinSecretChat);
                                                    } else {

                                                        text = getResources().getString(R.string.youAreInvited) + " " + receiverName + " " +
                                                                getResources().getString(R.string.JoinSecretChat);

                                                    }

                                                    db.updateChatListForNewMessageFromHistory(
                                                            documentId, text, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                            false, jsonObject.getInt("status"));


                                                }

                                            } else {
                                                db.updateChatListForNewMessageFromHistory(
                                                        documentId, text, hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            }
                                            break;
                                        case 1:
                                            /*
                                             * Image message
                                             */


                                            if (totalUnreadMessageCount > 0) {


                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewImage), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                            } else {
                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Image), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                            }
                                            break;
                                        case 2:

                                            /*
                                             * Video message
                                             */
                                            if (totalUnreadMessageCount > 0) {

                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewVideo),
                                                        hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                            } else {
                                                db.updateChatListForNewMessageFromHistory(documentId,
                                                        getString(R.string.Video),
                                                        hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                            }

                                            break;
                                        case 3:
                                            /*
                                             * Location message
                                             */
                                            if (totalUnreadMessageCount > 0) {


                                                db.updateChatListForNewMessageFromHistory(

                                                        documentId, getString(R.string.NewLocation), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                            } else {

                                                db.updateChatListForNewMessageFromHistory(

                                                        documentId, getString(R.string.Location), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                            }
                                            break;
                                        case 4:
                                            /*
                                             * Follow message
                                             */

                                            if (totalUnreadMessageCount > 0) {
                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewContact),
                                                        hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                            } else {

                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Contact),
                                                        hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            }
                                            break;
                                        case 5:

                                            /*
                                             * Audio message
                                             */

                                            if (totalUnreadMessageCount > 0) {
                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewAudio), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                            } else {
                                                db.updateChatListForNewMessageFromHistory(documentId,
                                                        getString(R.string.Audio), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                            }
                                            break;
                                        case 6:


                                            /*
                                             * Sticker
                                             */
                                            if (totalUnreadMessageCount > 0) {


                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewSticker), hasNewMessage, tsInGmt,
                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                            } else {

                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Stickers), hasNewMessage, tsInGmt,
                                                        tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                            }
                                            break;
                                        case 7:
                                            /*
                                             * Doodle
                                             */
                                            if (totalUnreadMessageCount > 0) {

                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewDoodle), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            } else {

                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Doodle), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                            }

                                            break;
                                        case 8:
                                            /*
                                             * Gif
                                             */
                                            if (totalUnreadMessageCount > 0) {
                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewGiphy), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            } else {


                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Giphy), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            }
                                            break;


                                        case 9:
                                            /*
                                             * Document
                                             */
                                            if (totalUnreadMessageCount > 0) {
                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewDocument), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            } else {


                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Document), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            }
                                            break;


                                        case 10:

                                        {


                                            switch (Integer.parseInt(jsonObject.getString("replyType"))) {
                                                case 0:

                                                    String text2 = "";
                                                    try {

                                                        text2 = new String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT), "UTF-8");
                                                    } catch (UnsupportedEncodingException e) {

                                                    }

                                                    db.updateChatListForNewMessageFromHistory(
                                                            documentId, text2, hasNewMessage, tsInGmt,
                                                            tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    break;
                                                case 1:
                                                    /*
                                                     * receiverImage message
                                                     */


                                                    if (totalUnreadMessageCount > 0) {


                                                        db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewImage),
                                                                hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf,
                                                                jsonObject.getInt("status"));
                                                    } else {
                                                        db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Image),
                                                                hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf,
                                                                jsonObject.getInt("status"));
                                                    }
                                                    break;
                                                case 2:

                                                    /*
                                                     * Video message
                                                     */
                                                    if (totalUnreadMessageCount > 0) {

                                                        db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewVideo),
                                                                hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                isSelf, jsonObject.getInt("status"));
                                                    } else {
                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.Video),
                                                                hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                isSelf, jsonObject.getInt("status"));
                                                    }

                                                    break;
                                                case 3:
                                                    /*
                                                     * Location message
                                                     */
                                                    if (totalUnreadMessageCount > 0) {


                                                        db.updateChatListForNewMessageFromHistory(

                                                                documentId, getString(R.string.NewLocation), hasNewMessage,
                                                                tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                    } else {

                                                        db.updateChatListForNewMessageFromHistory(

                                                                documentId, getString(R.string.Location), hasNewMessage,
                                                                tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                    }
                                                    break;
                                                case 4:
                                                    /*
                                                     * Follow message
                                                     */

                                                    if (totalUnreadMessageCount > 0) {
                                                        db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewContact),
                                                                hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                isSelf, jsonObject.getInt("status"));

                                                    } else {

                                                        db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Contact),
                                                                hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount,
                                                                isSelf, jsonObject.getInt("status"));


                                                    }
                                                    break;
                                                case 5:

                                                    /*
                                                     * Audio message
                                                     */

                                                    if (totalUnreadMessageCount > 0) {
                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.NewAudio), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                    } else {
                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.Audio), hasNewMessage, tsInGmt,
                                                                tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                    }
                                                    break;
                                                case 6:


                                                    /*
                                                     * Sticker
                                                     */
                                                    if (totalUnreadMessageCount > 0) {


                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.NewSticker), hasNewMessage, tsInGmt,
                                                                tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));
                                                    } else {

                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.Stickers), hasNewMessage, tsInGmt,
                                                                tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                    }
                                                    break;
                                                case 7:
                                                    /*
                                                     * Doodle
                                                     */
                                                    if (totalUnreadMessageCount > 0) {

                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.NewDoodle), hasNewMessage, tsInGmt, tsInGmt,
                                                                totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    } else {

                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.Doodle), hasNewMessage, tsInGmt, tsInGmt,
                                                                totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));

                                                    }

                                                    break;
                                                case 8:
                                                    /*
                                                     * Gif
                                                     */
                                                    if (totalUnreadMessageCount > 0) {
                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.NewGiphy), hasNewMessage, tsInGmt, tsInGmt,
                                                                totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    } else {


                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.Giphy), hasNewMessage, tsInGmt, tsInGmt,
                                                                totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    }
                                                    break;


                                                case 9:
                                                    /*
                                                     * Document
                                                     */
                                                    if (totalUnreadMessageCount > 0) {
                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.NewDocument), hasNewMessage, tsInGmt, tsInGmt,
                                                                totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    } else {


                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.Document), hasNewMessage, tsInGmt, tsInGmt,
                                                                totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    }
                                                    break;


                                                case 13:
                                                    /*
                                                     * Post
                                                     */
                                                    if (totalUnreadMessageCount > 0) {
                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.NewPost), hasNewMessage, tsInGmt, tsInGmt,
                                                                totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    } else {


                                                        db.updateChatListForNewMessageFromHistory(documentId,
                                                                getString(R.string.Post), hasNewMessage, tsInGmt, tsInGmt,
                                                                totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                                    }
                                                    break;


                                            }

                                            break;
                                        }


                                        case 11: {
                                            /*
                                             * Removed message
                                             */

                                            String text2 = "";
                                            try {

                                                text2 = new String(Base64.decode(jsonObject.getString("payload"), Base64.DEFAULT), "UTF-8");
                                            } catch (UnsupportedEncodingException e) {

                                            }


                                            db.updateChatListForNewMessageFromHistory(documentId, text2, hasNewMessage, Utilities.epochtoGmt(jsonObject.getString("removedAt")), Utilities.epochtoGmt(jsonObject.getString("removedAt")), totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            break;
                                        }


                                        case 13:
                                            /*
                                             * Post
                                             */
                                            if (totalUnreadMessageCount > 0) {
                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.NewPost), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            } else {


                                                db.updateChatListForNewMessageFromHistory(documentId, getString(R.string.Post), hasNewMessage, tsInGmt, tsInGmt, totalUnreadMessageCount, isSelf, jsonObject.getInt("status"));


                                            }
                                            break;
                                    }


                                }

                            }
                            obj.put("eventName", topic);
                            bus.post(obj);

                        } catch (JSONException e) {

                        }
                    } else if (topic.equals(MqttEvents.FetchMessages.value + "/" + userId)) {


                        /*
                         * To receive the result of the fetch messages api
                         */



                        /*
                         * First add the message received in the local db and then post on the bus
                         */
                        try {


                            JSONArray messages = obj.getJSONArray("messages");


                            if (messages.length() > 0) {


                                /*
                                 *Group chat
                                 */
                                if (obj.getString("opponentUid").isEmpty()) {


                                    JSONObject messageObject;


                                    String receiverUid = obj.getString("chatId");


                                    boolean isSelf;
                                    String actualMessage, timestamp, id, mimeType, fileName, extension;
                                    int dataSize;

                                    String postId = "", postTitle = "";
                                    int postType = -1;

                                    for (int i = 0; i < messages.length(); i++) {

                                        messageObject = messages.getJSONObject(i);


                                        isSelf = messageObject.getString("senderId").equals(userId);


                                        /*
                                         * For an actual message(Like text,image,video etc.) received
                                         */


                                        String messageType = messageObject.getString("messageType");


                                        if (messageObject.has("payload"))

                                        {


                                            actualMessage = messageObject.getString("payload").trim();
                                            timestamp = String.valueOf(messageObject.getLong("timestamp"));


                                            id = messageObject.getString("messageId");
                                            mimeType = "";
                                            fileName = "";
                                            extension = "";

                                            dataSize = -1;

                                            if (messageType.equals("1") || messageType.equals("2") || messageType.equals("5") ||
                                                    messageType.equals("7") || messageType.equals("9")) {
                                                dataSize = messageObject.getInt("dataSize");


                                                if (messageType.equals("9")) {


                                                    mimeType = messageObject.getString("mimeType");
                                                    fileName = messageObject.getString("fileName");
                                                    extension = messageObject.getString("extension");


                                                }
                                            } else if (messageType.equals("10")) {

                                                String replyType = messageObject.getString("replyType");

                                                if (replyType.equals("1") || replyType.equals("2") || replyType.equals("5") || replyType.equals("7")
                                                        || replyType.equals("9")) {
                                                    dataSize = messageObject.getInt("dataSize");


                                                    if (replyType.equals("9")) {


                                                        mimeType = messageObject.getString("mimeType");
                                                        fileName = messageObject.getString("fileName");
                                                        extension = messageObject.getString("extension");


                                                    } else if (replyType.equals("13")) {


                                                        postId = messageObject.getString("postId");
                                                        postTitle = messageObject.getString("postTitle");
                                                        postType = messageObject.getInt("postType");


                                                    }

                                                }
                                            } else if (messageType.equals("13")) {


                                                postId = messageObject.getString("postId");
                                                postTitle = messageObject.getString("postTitle");
                                                postType = messageObject.getInt("postType");


                                            }


                                            String documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, "");
                                            if (documentId.isEmpty()) {

                                                String receiverName = messageObject.getString("name");
                                                String userImage = messageObject.getString("userImage");

                                                /*
                                                 * Here, chatId is assumed to be empty
                                                 */
                                                documentId = findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                                        receiverName, userImage, "", true, receiverUid, obj.getString("chatId"), false);


                                            }


                                            if (!db.checkAlreadyExists(documentId, id)) {


                                                if (messageType.equals("11")) {
                                                    AppController.getInstance().putGroupMessageInDb(messageObject.getString("senderId"),
                                                            messageType, actualMessage, timestamp, id, documentId, null, dataSize,
                                                            messageObject.getString("receiverIdentifier"), isSelf, 1, Utilities.epochtoGmt(messageObject.getString("removedAt")), false);
                                                } else {
                                                    if (messageType.equals("1") || messageType.equals("2") || messageType.equals("7")) {


                                                        AppController.getInstance().putGroupMessageInDb(messageObject.getString("senderId"), messageType,
                                                                actualMessage, timestamp, id, documentId, messageObject.getString("thumbnail").trim(),
                                                                dataSize, messageObject.getString("receiverIdentifier"), isSelf, 1, null, false);
                                                    } else if (messageType.equals("9")) {


                                                        AppController.getInstance().putGroupMessageInDb(messageObject.getString("senderId"),
                                                                actualMessage, timestamp, id, documentId, dataSize,
                                                                isSelf, 1, mimeType, fileName, extension, messageObject.getString("receiverIdentifier"));


                                                    } else if (messageType.equals("13")) {


                                                        AppController.getInstance().putGroupPostMessageInDb(messageObject.getString("senderId"),
                                                                actualMessage, timestamp, id, documentId,
                                                                isSelf, 1, mimeType, fileName, extension, messageObject.getString("receiverIdentifier"), postId, postTitle, postType);


                                                    } else if (messageType.equals("10")) {

                                                        /*
                                                         * For the reply message received
                                                         */


                                                        String replyType = messageObject.getString("replyType");

                                                        String previousMessageType = messageObject.getString("previousType");

                                                        String previousFileType = "", thumbnail = "";


                                                        if (previousMessageType.equals("9")) {

                                                            previousFileType = messageObject.getString("previousFileType");

                                                        }
                                                        if (replyType.equals("1") || replyType.equals("2") || replyType.equals("7")) {


                                                            thumbnail = messageObject.getString("thumbnail").trim();

                                                        }


                                                        AppController.getInstance().putGroupReplyMessageInDb(messageObject.getString("senderId"),
                                                                actualMessage, timestamp, id, documentId,
                                                                dataSize, isSelf, 1, mimeType, fileName, extension, thumbnail, messageObject.getString("receiverIdentifier"),
                                                                replyType, messageObject.getString("previousReceiverIdentifier"), messageObject.getString("previousFrom"),
                                                                messageObject.getString("previousPayload"), messageObject.getString("previousType"),
                                                                messageObject.getString("previousId"), previousFileType, messageObject.has("wasEdited"), postId, postTitle, postType);


                                                    } else {

                                                        AppController.getInstance().putGroupMessageInDb(messageObject.getString("senderId"),
                                                                messageType, actualMessage, timestamp, id, documentId, null, dataSize, messageObject.getString("receiverIdentifier"), isSelf, 1, null, messageObject.has("wasEdited"));


                                                    }
                                                    int type = Integer.parseInt(messageType);


                                                    int replyTypeInt = -1;

                                                    if (obj.has("replyType")) {
                                                        replyTypeInt = Integer.parseInt(obj.getString("replyType"));
                                                    }
                                                    if (type == 1 || type == 2 || type == 5 || type == 7 || type == 9 ||
                                                            (type == 10 && (replyTypeInt == 1 || replyTypeInt == 2 || replyTypeInt == 5
                                                                    || replyTypeInt == 7 || replyTypeInt == 9))) {
                                                        if (ActivityCompat.checkSelfPermission(mInstance, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                                == PackageManager.PERMISSION_GRANTED) {
//                                                if (autoDownloadAllowed) {

                                                            if (checkWifiConnected()) {

                                                                Object[] params = new Object[8];
                                                                try {


                                                                    params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                                                } catch (UnsupportedEncodingException e) {

                                                                }

                                                                params[1] = messageType;
                                                                params[5] = id;
                                                                params[6] = documentId;
                                                                params[7] = receiverUid;
                                                                switch (type) {
                                                                    case 1: {

                                                                        if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;

                                                                    }
                                                                    case 2: {
                                                                        if (sharedPref.getBoolean("wifiVideo", false)) {
                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 5: {

                                                                        if (sharedPref.getBoolean("wifiAudio", false)) {

                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;

                                                                    }
                                                                    case 7: {

                                                                        if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }

                                                                    case 9: {
                                                                        if (sharedPref.getBoolean("wifiDocument", false)) {

                                                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                                    + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;

                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 10: {


                                                                        params[2] = obj.getString("replyType");

                                                                        switch (Integer.parseInt((String) params[2])) {

                                                                            case 1:

                                                                            {
                                                                                if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                                    new DownloadMessage().execute(params);
                                                                                }
                                                                                break;
                                                                            }

                                                                            case 2: {
                                                                                if (sharedPref.getBoolean("wifiVideo", false)) {

                                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";
                                                                                    new DownloadMessage().execute(params);
                                                                                }
                                                                                break;
                                                                            }
                                                                            case 5: {
                                                                                if (sharedPref.getBoolean("wifiAudio", false)) {

                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";
                                                                                    new DownloadMessage().execute(params);
                                                                                }

                                                                                break;
                                                                            }

                                                                            case 7: {
                                                                                if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                                    new DownloadMessage().execute(params);
                                                                                }
                                                                                break;
                                                                            }
                                                                            case 9:

                                                                                if (sharedPref.getBoolean("wifiDocument", false)) {
                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                                            + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;

                                                                                    new DownloadMessage().execute(params);
                                                                                }
                                                                                break;
                                                                        }
                                                                        break;
                                                                    }

                                                                }


                                                            } else if (checkMobileDataOn()) {


                                                                Object[] params = new Object[8];
                                                                try {


                                                                    params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                                                } catch (UnsupportedEncodingException e) {

                                                                }

                                                                params[1] = messageType;
                                                                params[5] = id;
                                                                params[6] = documentId;
                                                                params[7] = receiverUid;
                                                                switch (type) {
                                                                    case 1: {

                                                                        if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;

                                                                    }
                                                                    case 2: {
                                                                        if (sharedPref.getBoolean("mobileVideo", false)) {
                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 5: {

                                                                        if (sharedPref.getBoolean("mobileAudio", false)) {

                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;

                                                                    }
                                                                    case 7: {

                                                                        if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }

                                                                    case 9: {
                                                                        if (sharedPref.getBoolean("mobileDocument", false)) {

                                                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                                    + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;

                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 10: {


                                                                        params[2] = obj.getString("replyType");

                                                                        switch (Integer.parseInt((String) params[2])) {

                                                                            case 1:

                                                                            {
                                                                                if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                                    new DownloadMessage().execute(params);
                                                                                }
                                                                                break;
                                                                            }

                                                                            case 2: {
                                                                                if (sharedPref.getBoolean("mobileVideo", false)) {

                                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";
                                                                                    new DownloadMessage().execute(params);
                                                                                }
                                                                                break;
                                                                            }
                                                                            case 5: {
                                                                                if (sharedPref.getBoolean("mobileAudio", false)) {

                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";
                                                                                    new DownloadMessage().execute(params);
                                                                                }

                                                                                break;
                                                                            }

                                                                            case 7: {
                                                                                if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                                    new DownloadMessage().execute(params);
                                                                                }
                                                                                break;
                                                                            }
                                                                            case 9:

                                                                                if (sharedPref.getBoolean("mobileDocument", false)) {
                                                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                                            + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;

                                                                                    new DownloadMessage().execute(params);
                                                                                }
                                                                                break;
                                                                        }
                                                                        break;
                                                                    }

                                                                }


                                                            }

                                                        }
                                                    }
                                                }
                                            } else {


                                                if (messageType.equals("11")) {

                                                    AppController.getInstance().putGroupMessageInDb(messageObject.getString("senderId"),
                                                            messageType, actualMessage, timestamp, id, documentId, null, dataSize, messageObject.getString("receiverIdentifier"), isSelf, 1, Utilities.epochtoGmt(messageObject.getString("removedAt")), false);

                                                }


                                            }

                                        } else {


                                            String tsInGmt = Utilities.epochtoGmt(String.valueOf(messageObject.getLong("timestamp")));


                                            switch (messageObject.getInt("messageType")) {
                                                case 0: {
                                                    /*
                                                     * Group created
                                                     *
                                                     */


                                                    String groupId = obj.getString("chatId");


                                                    String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");

                                                    Map<String, Object> map = new HashMap<>();

                                                    map.put("messageType", "98");


                                                    map.put("isSelf", false);
                                                    map.put("from", groupId);
                                                    map.put("Ts", tsInGmt);
                                                    map.put("id", messageObject.getString("messageId"));

                                                    map.put("type", 0);

                                                    map.put("groupName", messageObject.getString("groupSubject"));

                                                    map.put("initiatorId", messageObject.getString("initiatorId"));
                                                    map.put("initiatorIdentifier", messageObject.getString("initiatorIdentifier"));

                                                    map.put("deliveryStatus", "0");


                                                    String text, initiatorName;
                                                    String initiatorId = messageObject.getString("initiatorId");
                                                    if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                                        initiatorName = getString(R.string.You);
                                                    } else {
                                                        initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                                        if (initiatorName == null) {

                                                            initiatorName = messageObject.getString("initiatorIdentifier");
                                                        }
                                                    }


                                                    text = getString(R.string.CreatedGroup, initiatorName) + " " + messageObject.getString("groupSubject");


                                                    addGroupChatMessageInDB(docId, map, tsInGmt, text, false);

                                                    break;

                                                }


                                                case 1:


                                                {

                                                    /*
                                                     * Member added
                                                     */

                                                    String groupId = obj.getString("chatId");

                                                    /*
                                                     *
                                                     * To check if group already exists
                                                     */
                                                    String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");


                                                    String memberId = messageObject.getString("memberId");


                                                    if (!docId.isEmpty()) {


                                                        Map<String, Object> map = new HashMap<>();

                                                        map.put("messageType", "98");


                                                        map.put("isSelf", false);
                                                        map.put("from", groupId);
                                                        map.put("Ts", tsInGmt);
                                                        map.put("id", messageObject.getString("messageId"));

                                                        map.put("type", 1);

                                                        map.put("memberId", memberId);

                                                        map.put("memberIdentifier", messageObject.getString("memberIdentifier"));


                                                        map.put("initiatorId", messageObject.getString("initiatorId"));
                                                        map.put("initiatorIdentifier", messageObject.getString("initiatorIdentifier"));

                                                        map.put("deliveryStatus", "0");


                                                        String text, initiatorName, memberName;
                                                        String initiatorId = messageObject.getString("initiatorId");
                                                        if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                                            initiatorName = getString(R.string.You);
                                                        } else {
                                                            initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                                            if (initiatorName == null) {

                                                                initiatorName = messageObject.getString("initiatorIdentifier");
                                                            }
                                                        }


                                                        if (memberId.equals(AppController.getInstance().getUserId())) {

                                                            memberName = getString(R.string.YouSmall);
                                                        } else {
                                                            memberName = db.getContactName(AppController.getInstance().getContactsDocId(), memberId);
                                                            if (memberName == null) {

                                                                memberName = messageObject.getString("memberIdentifier");
                                                            }
                                                        }


                                                        text = initiatorName + " " + getString(R.string.AddedMember, memberName) + " " + getString(R.string.ToGroup);

                                                        addGroupChatMessageInDB(docId, map, tsInGmt, text, false);


                                                    }


                                                    break;
                                                }
                                                case 2:

                                                {
                                                    /*
                                                     * Member removed
                                                     *
                                                     */

                                                    String groupId = obj.getString("chatId");


                                                    String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");

                                                    String memberId = messageObject.getString("memberId");
                                                    if (!docId.isEmpty()) {


                                                        Map<String, Object> map = new HashMap<>();

                                                        map.put("messageType", "98");


                                                        map.put("isSelf", false);
                                                        map.put("from", groupId);
                                                        map.put("Ts", tsInGmt);
                                                        map.put("id", messageObject.getString("messageId"));

                                                        map.put("type", 2);

                                                        map.put("memberId", memberId);

                                                        map.put("memberIdentifier", messageObject.getString("memberIdentifier"));


                                                        map.put("initiatorId", messageObject.getString("initiatorId"));
                                                        map.put("initiatorIdentifier", messageObject.getString("initiatorIdentifier"));

                                                        map.put("deliveryStatus", "0");


                                                        String text, initiatorName, memberName;
                                                        String initiatorId = messageObject.getString("initiatorId");
                                                        if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                                            initiatorName = getString(R.string.You);
                                                        } else {
                                                            initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                                            if (initiatorName == null) {

                                                                initiatorName = messageObject.getString("initiatorIdentifier");
                                                            }
                                                        }


                                                        if (memberId.equals(AppController.getInstance().getUserId())) {

                                                            memberName = getString(R.string.YouSmall);
                                                        } else {
                                                            memberName = db.getContactName(AppController.getInstance().getContactsDocId(), memberId);
                                                            if (memberName == null) {

                                                                memberName = messageObject.getString("memberIdentifier");
                                                            }
                                                        }


                                                        text = initiatorName + " " + getString(R.string.Removed) + " " + memberName;

                                                        addGroupChatMessageInDB(docId, map, tsInGmt, text, false);


                                                    }


                                                    break;

                                                }
                                                case 3: {
                                                    /*
                                                     * Made admin
                                                     *
                                                     */


                                                    String groupId = obj.getString("chatId");


                                                    String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");

                                                    String memberId = messageObject.getString("memberId");
                                                    if (!docId.isEmpty()) {


                                                        Map<String, Object> map = new HashMap<>();

                                                        map.put("messageType", "98");


                                                        map.put("isSelf", false);
                                                        map.put("from", groupId);
                                                        map.put("Ts", tsInGmt);
                                                        map.put("id", messageObject.getString("messageId"));

                                                        map.put("type", 3);

                                                        map.put("memberId", memberId);

                                                        map.put("memberIdentifier", messageObject.getString("memberIdentifier"));


                                                        map.put("initiatorId", messageObject.getString("initiatorId"));
                                                        map.put("initiatorIdentifier", messageObject.getString("initiatorIdentifier"));

                                                        map.put("deliveryStatus", "0");


                                                        String text, initiatorName, memberName;
                                                        String initiatorId = messageObject.getString("initiatorId");
                                                        if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                                            initiatorName = getString(R.string.You);
                                                        } else {
                                                            initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                                            if (initiatorName == null) {

                                                                initiatorName = messageObject.getString("initiatorIdentifier");
                                                            }
                                                        }


                                                        if (memberId.equals(AppController.getInstance().getUserId())) {

                                                            memberName = getString(R.string.YouSmall);
                                                        } else {
                                                            memberName = db.getContactName(AppController.getInstance().getContactsDocId(), memberId);
                                                            if (memberName == null) {

                                                                memberName = messageObject.getString("memberIdentifier");
                                                            }
                                                        }


                                                        text = initiatorName + " " + getString(R.string.Made) + " " + memberName + " " + getString(R.string.MakeAdmin);

                                                        addGroupChatMessageInDB(docId, map, tsInGmt, text, false);


                                                    }
                                                    break;
                                                }
                                                case 4:

                                                {
                                                    /*
                                                     * Group name updated
                                                     *
                                                     */
                                                    String groupId = obj.getString("chatId");


                                                    String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");


                                                    if (!docId.isEmpty()) {


                                                        Map<String, Object> map = new HashMap<>();

                                                        map.put("messageType", "98");


                                                        map.put("isSelf", false);
                                                        map.put("from", groupId);
                                                        map.put("Ts", tsInGmt);
                                                        map.put("id", messageObject.getString("messageId"));

                                                        map.put("type", 4);


                                                        map.put("groupSubject", messageObject.getString("groupSubject"));
                                                        map.put("initiatorId", messageObject.getString("initiatorId"));
                                                        map.put("initiatorIdentifier", messageObject.getString("initiatorIdentifier"));

                                                        map.put("deliveryStatus", "0");


                                                        String text, initiatorName;
                                                        String initiatorId = messageObject.getString("initiatorId");
                                                        if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                                            initiatorName = getString(R.string.You);
                                                        } else {
                                                            initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                                            if (initiatorName == null) {

                                                                initiatorName = messageObject.getString("initiatorIdentifier");
                                                            }
                                                        }


                                                        text = initiatorName + " " + getString(R.string.UpdatedGroupSubject, messageObject.getString("groupSubject"));

                                                        addGroupChatMessageInDB(docId, map, tsInGmt, text, false);


                                                    }

                                                    break;
                                                }
                                                case 5:


                                                {
                                                    /*
                                                     * Group icon updated
                                                     *
                                                     */


                                                    String groupId = obj.getString("chatId");


                                                    String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");


                                                    if (!docId.isEmpty()) {


                                                        Map<String, Object> map = new HashMap<>();

                                                        map.put("messageType", "98");


                                                        map.put("isSelf", false);
                                                        map.put("from", groupId);
                                                        map.put("Ts", tsInGmt);
                                                        map.put("id", messageObject.getString("messageId"));

                                                        map.put("type", 5);


                                                        map.put("initiatorId", messageObject.getString("initiatorId"));
                                                        map.put("initiatorIdentifier", messageObject.getString("initiatorIdentifier"));

                                                        map.put("deliveryStatus", "0");


                                                        String text, initiatorName;
                                                        String initiatorId = messageObject.getString("initiatorId");
                                                        if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                                            initiatorName = getString(R.string.You);
                                                        } else {
                                                            initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                                            if (initiatorName == null) {

                                                                initiatorName = messageObject.getString("initiatorIdentifier");
                                                            }
                                                        }


                                                        text = initiatorName + " " + getString(R.string.UpdatedGroupIcon);

                                                        addGroupChatMessageInDB(docId, map, tsInGmt, text, false);


                                                    }


                                                    break;

                                                }


                                                case 6: {

                                                    /*
                                                     * Member left the conversation
                                                     */

                                                    String groupId = obj.getString("chatId");


                                                    String memberId = messageObject.getString("initiatorId");


                                                    String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");


                                                    if (!docId.isEmpty()) {


                                                        Map<String, Object> map = new HashMap<>();

                                                        map.put("messageType", "98");


                                                        map.put("isSelf", false);
                                                        map.put("from", groupId);
                                                        map.put("Ts", tsInGmt);
                                                        map.put("id", messageObject.getString("messageId"));

                                                        map.put("type", 6);


                                                        map.put("initiatorId", memberId);
                                                        map.put("initiatorIdentifier", messageObject.getString("initiatorIdentifier"));

                                                        map.put("deliveryStatus", "0");


                                                        String text, memberName;


                                                        if (memberId.equals(AppController.getInstance().getUserId())) {

                                                            memberName = getString(R.string.You);
                                                        } else {
                                                            memberName = db.getContactName(AppController.getInstance().getContactsDocId(), memberId);
                                                            if (memberName == null) {

                                                                memberName = messageObject.getString("initiatorIdentifier");
                                                            }
                                                        }


                                                        text = getString(R.string.LeftGroup, memberName);

                                                        addGroupChatMessageInDB(docId, map, tsInGmt, text, false);


                                                    }

                                                    break;
                                                }


                                            }
                                        }
                                    }

                                } else {


                                    /*
                                     * Messages in the normal or the secret chat
                                     */


                                    JSONObject messageObject;


                                    String receiverUid = obj.getString("opponentUid");


                                    String secretId = "";

                                    long dTime, expectedDTime = -1;
                                    if (obj.has("secretId") && !obj.getString("secretId").isEmpty()) {
                                        secretId = obj.getString("secretId");


                                    }

                                    String receiverIdentifier = "";
                                    Map<String, Object> chatDetails = AppController.getInstance().getDbController().
                                            getAllChatDetails(chatDocId);

                                    if (chatDetails != null) {

                                        ArrayList<String> receiverUidArray = (ArrayList<String>) chatDetails.get("receiverUidArray");


                                        ArrayList<String> receiverDocIdArray = (ArrayList<String>) chatDetails.get("receiverDocIdArray");


                                        ArrayList<String> secretIdArray = (ArrayList<String>) chatDetails.get("secretIdArray");

                                        for (int i = 0; i < receiverUidArray.size(); i++) {


                                            if (receiverUidArray.get(i).equals(receiverUid) && secretIdArray.get(i).equals(secretId)) {

                                                receiverIdentifier = db.fetchReceiverIdentifierFromChatId
                                                        (receiverDocIdArray.get(i), obj.getString("chatId"));
                                                break;
                                            }

                                        }
                                    }


                                    if (receiverIdentifier.isEmpty()) {
                                        return;
                                    }

                                    boolean isSelf;
                                    String messageType, actualMessage, timestamp, id, mimeType, fileName, extension, docIdForDoubleTickAck;
                                    int dataSize;

                                    String postId = "", postTitle = "";
                                    int postType = -1;

                                    for (int i = 0; i < messages.length(); i++) {

                                        messageObject = messages.getJSONObject(i);


                                        if (!secretId.isEmpty()) {
                                            dTime = messageObject.getLong("dTime");
                                        } else {
                                            dTime = -1;
                                        }


                                        isSelf = messageObject.getString("senderId").equals(userId);


                                        /*
                                         * For an actual message(Like text,image,video etc.) received
                                         */


                                        messageType = messageObject.getString("messageType");
                                        actualMessage = messageObject.getString("payload").trim();
                                        timestamp = String.valueOf(messageObject.getLong("timestamp"));


                                        id = messageObject.getString("messageId");
                                        mimeType = "";
                                        fileName = "";
                                        extension = "";
                                        docIdForDoubleTickAck = messageObject.getString("toDocId");
                                        dataSize = -1;

                                        if (messageType.equals("1") || messageType.equals("2") || messageType.equals("5") ||
                                                messageType.equals("7") || messageType.equals("9")) {
                                            dataSize = messageObject.getInt("dataSize");


                                            if (messageType.equals("9")) {


                                                mimeType = messageObject.getString("mimeType");
                                                fileName = messageObject.getString("fileName");
                                                extension = messageObject.getString("extension");


                                            }
                                        } else if (messageType.equals("10")) {

                                            String replyType = messageObject.getString("replyType");

                                            if (replyType.equals("1") || replyType.equals("2") || replyType.equals("5") || replyType.equals("7")
                                                    || replyType.equals("9")) {
                                                dataSize = messageObject.getInt("dataSize");


                                                if (replyType.equals("9")) {


                                                    mimeType = messageObject.getString("mimeType");
                                                    fileName = messageObject.getString("fileName");
                                                    extension = messageObject.getString("extension");


                                                } else if (replyType.equals("13")) {


                                                    postId = messageObject.getString("postId");
                                                    postTitle = messageObject.getString("postTitle");
                                                    postType = messageObject.getInt("postType");


                                                }


                                            }
                                        } else if (messageType.equals("13")) {


                                            postId = messageObject.getString("postId");
                                            postTitle = messageObject.getString("postTitle");
                                            postType = messageObject.getInt("postType");


                                        }

                                        String receiverName;
                                        String userImage;

                                        if (signInType == 0) {

                                            receiverName = messageObject.getString("name");
                                            userImage = messageObject.getString("userImage");
                                        } else {

                                            Map<String, Object> contactInfo


                                                    = db.getContactInfoFromUid(AppController.getInstance().getContactsDocId(), receiverUid);

                                            if (contactInfo != null) {
                                                receiverName = (String) contactInfo.get("contactName");
                                                userImage = (String) contactInfo.get("contactPicUrl");
                                            } else {


                                                /*
                                                 * If userId doesn't exists in contact
                                                 */
                                                receiverName = receiverIdentifier;
                                                userImage = messageObject.getString("userImage");

                                            }
                                        }


                                        String documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);
                                        if (documentId.isEmpty()) {


                                            /*
                                             * Here, chatId is assumed to be empty
                                             */
                                            documentId = findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                                    receiverName, userImage, secretId, true, receiverIdentifier, obj.getString("chatId"), false);


                                        }


                                        db.setDocumentIdOfReceiver(documentId, docIdForDoubleTickAck, receiverUid);


                                        if (!db.checkAlreadyExists(documentId, id)) {


                                            if (messageObject.has("expectedDTime")) {


                                                expectedDTime = messageObject.getLong("expectedDTime");
                                                setTimer(documentId, id, expectedDTime);
                                            }


                                            if (messageType.equals("11")) {


                                                AppController.getInstance().putMessageInDb(receiverUid,
                                                        messageType, actualMessage, timestamp, id, documentId,


                                                        null, dataSize, dTime, receiverName, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"), expectedDTime, Utilities.epochtoGmt(messageObject.getString("removedAt")), false);

                                            } else if (messageType.equals("13")) {


                                                AppController.getInstance().putPostMessageInDb(receiverUid,
                                                        actualMessage, timestamp, id, documentId,
                                                        dTime, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"),
                                                        expectedDTime, postId, postTitle, postType);


                                            } else if (messageType.equals("1") || messageType.equals("2") || messageType.equals("7")) {


                                                AppController.getInstance().putMessageInDb(receiverUid, messageType,
                                                        actualMessage, timestamp, id, documentId, messageObject.getString("thumbnail").trim(),


                                                        dataSize, dTime, receiverName, isSelf, messageObject.getInt("status"),
                                                        messageObject.has("expectedDTime"), expectedDTime, null, false);
                                            } else if (messageType.equals("9")) {


                                                AppController.getInstance().putMessageInDb(receiverUid,
                                                        actualMessage, timestamp, id, documentId, dataSize,
                                                        dTime, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"),
                                                        expectedDTime, mimeType, fileName, extension);


                                            } else if (messageType.equals("10")) {

                                                /*
                                                 * For the reply message received
                                                 */


                                                String replyType = messageObject.getString("replyType");

                                                String previousMessageType = messageObject.getString("previousType");

                                                String previousFileType = "", thumbnail = "";


                                                if (previousMessageType.equals("9")) {

                                                    previousFileType = messageObject.getString("previousFileType");

                                                }
                                                if (replyType.equals("1") || replyType.equals("2") || replyType.equals("7")) {


                                                    thumbnail = messageObject.getString("thumbnail").trim();

                                                }


                                                AppController.getInstance().putReplyMessageInDb(receiverUid,
                                                        actualMessage, timestamp, id, documentId,
                                                        dataSize, dTime, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"),
                                                        expectedDTime, mimeType, fileName, extension, thumbnail, receiverName,
                                                        replyType, messageObject.getString("previousReceiverIdentifier"),
                                                        messageObject.getString("previousFrom"),
                                                        messageObject.getString("previousPayload"), messageObject.getString("previousType"),
                                                        messageObject.getString("previousId"),
                                                        previousFileType, messageObject.has("wasEdited"), postId, postTitle, postType);


                                            } else {

                                                AppController.getInstance().putMessageInDb(receiverUid,
                                                        messageType, actualMessage, timestamp, id, documentId,


                                                        null, dataSize, dTime, receiverName, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"), expectedDTime, null, messageObject.has("wasEdited"));


                                            }


                                            if ((messageObject.getInt("status") == 1) && isSelf && !messageType.equals("11") && (!messageType.equals("0") || !actualMessage.isEmpty())) {
                                                JSONObject obj2 = new JSONObject();
                                                obj2.put("from", AppController.getInstance().userId);
                                                obj2.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));
                                                obj2.put("doc_id", docIdForDoubleTickAck);
                                                obj2.put("to", receiverUid);

                                                obj2.put("status", "2");
                                                obj2.put("deliveryTime", Utilities.getGmtEpoch());

                                                if (!secretId.isEmpty()) {
                                                    obj2.put("secretId", secretId);
                                                    obj2.put("dTime", dTime);

                                                }


                                                AppController.getInstance().publish(MqttEvents.Acknowledgement.value + "/" + receiverUid, obj2, 2, false);
                                            }
                                            int type = Integer.parseInt(messageType);
                                            int replyTypeInt = -1;

                                            if (obj.has("replyType")) {
                                                replyTypeInt = Integer.parseInt(obj.getString("replyType"));
                                            }
                                            if (type == 1 || type == 2 || type == 5 || type == 7 || type == 9 ||
                                                    (type == 10 && (replyTypeInt == 1 || replyTypeInt == 2 || replyTypeInt == 5
                                                            || replyTypeInt == 7 || replyTypeInt == 9))) {
                                                if (ActivityCompat.checkSelfPermission(mInstance, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                        == PackageManager.PERMISSION_GRANTED) {


                                                    if (checkWifiConnected()) {

                                                        Object[] params = new Object[8];
                                                        try {


                                                            params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                                        } catch (UnsupportedEncodingException e) {

                                                        }

                                                        params[1] = messageType;
                                                        params[5] = id;
                                                        params[6] = documentId;
                                                        params[7] = receiverUid;
                                                        switch (type) {
                                                            case 1: {
                                                                if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                                    new DownloadMessage().execute(params);

                                                                }
                                                                break;
                                                            }

                                                            case 2: {
                                                                if (sharedPref.getBoolean("wifiVideo", false)) {
                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";


                                                                    new DownloadMessage().execute(params);

                                                                }
                                                                break;
                                                            }
                                                            case 5: {
                                                                if (sharedPref.getBoolean("wifiAudio", false)) {
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                                    new DownloadMessage().execute(params);

                                                                }
                                                                break;
                                                            }

                                                            case 7: {
                                                                if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                    new DownloadMessage().execute(params);

                                                                }
                                                                break;
                                                            }
                                                            case 9: {
                                                                if (sharedPref.getBoolean("wifiDocument", false)) {
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                            + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                                                    new DownloadMessage().execute(params);
                                                                }
                                                                break;


                                                            }
                                                            case 10: {


                                                                params[2] = obj.getString("replyType");

                                                                switch (Integer.parseInt((String) params[2])) {

                                                                    case 1: {
                                                                        if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;

                                                                    }
                                                                    case 2: {
                                                                        if (sharedPref.getBoolean("wifiVideo", false)) {

                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 5: {
                                                                        if (sharedPref.getBoolean("wifiAudio", false)) {

                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;

                                                                    }
                                                                    case 7: {
                                                                        if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 9: {
                                                                        if (sharedPref.getBoolean("wifiDocument", false)) {
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                                    + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                                                            new DownloadMessage().execute(params);

                                                                        }
                                                                        break;
                                                                    }
                                                                }
                                                                break;
                                                            }

                                                        }


                                                    } else if (checkMobileDataOn()) {


                                                        Object[] params = new Object[8];
                                                        try {


                                                            params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                                        } catch (UnsupportedEncodingException e) {

                                                        }

                                                        params[1] = messageType;
                                                        params[5] = id;
                                                        params[6] = documentId;
                                                        params[7] = receiverUid;
                                                        switch (type) {
                                                            case 1: {
                                                                if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                                    new DownloadMessage().execute(params);

                                                                }
                                                                break;
                                                            }

                                                            case 2: {
                                                                if (sharedPref.getBoolean("mobileVideo", false)) {
                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";


                                                                    new DownloadMessage().execute(params);

                                                                }
                                                                break;
                                                            }
                                                            case 5: {
                                                                if (sharedPref.getBoolean("mobileAudio", false)) {
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                                    new DownloadMessage().execute(params);

                                                                }
                                                                break;
                                                            }

                                                            case 7: {
                                                                if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                    new DownloadMessage().execute(params);

                                                                }
                                                                break;
                                                            }
                                                            case 9: {
                                                                if (sharedPref.getBoolean("mobileDocument", false)) {
                                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                            + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                                                    new DownloadMessage().execute(params);
                                                                }
                                                                break;


                                                            }
                                                            case 10: {


                                                                params[2] = obj.getString("replyType");

                                                                switch (Integer.parseInt((String) params[2])) {

                                                                    case 1: {
                                                                        if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;

                                                                    }
                                                                    case 2: {
                                                                        if (sharedPref.getBoolean("mobileVideo", false)) {

                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 5: {
                                                                        if (sharedPref.getBoolean("mobileAudio", false)) {

                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;

                                                                    }
                                                                    case 7: {
                                                                        if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                            new DownloadMessage().execute(params);
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 9: {
                                                                        if (sharedPref.getBoolean("mobileDocument", false)) {
                                                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                                    + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                                                            new DownloadMessage().execute(params);

                                                                        }
                                                                        break;
                                                                    }
                                                                }
                                                                break;
                                                            }

                                                        }


                                                    }


//                                            }
                                                }
                                            }
                                        } else {


                                            if (messageType.equals("11")) {


                                                AppController.getInstance().putMessageInDb(receiverUid,
                                                        messageType, actualMessage, timestamp, id, documentId,


                                                        null, dataSize, dTime, receiverName, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"), expectedDTime, Utilities.epochtoGmt(messageObject.getString("removedAt")), false);

                                            }


                                        }


                                        if (!secretId.isEmpty()) {


                                            if (!actualMessage.trim().isEmpty() || dTime != -1) {


                                                db.updateSecretInviteImageVisibility(documentId, false);
                                            }
                                        }


                                    }


                                }
                            }

                        } catch (JSONException e) {

                        }


                        /*
                         * For callback in to activity to update UI
                         */

                        try {
                            obj.put("eventName", topic);
                            bus.post(obj);

                        } catch (Exception e) {

                        }


                    } else if (topic.equals(MqttEvents.GroupChats.value + "/" + userId)) {

                        /*
                         *
                         * For the group chat
                         */
                        try {
                            processGroupMessageReceived(obj);

                        } catch (Exception e) {

                        }
                    }


                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {


                    if (set.contains(token)) {


                        String id = null, docId = null;

                        boolean removedMessage = false;


                        int size = tokenMapping.size();
                        HashMap<String, Object> map;

                        for (int i = 0; i < size; i++) {


                            map = tokenMapping.get(i);

                            if (map.get("MQttToken").equals(token)) {

                                id = (String) map.get("messageId");
                                docId = (String) map.get("docId");
                                removedMessage = map.containsKey("messageType");

                                tokenMapping.remove(i);

                                set.remove(token);
                                break;
                            }


                        }


                        if (!removedMessage) {

                            if (docId != null && id != null) {

                                try {
                                    JSONObject obj = new JSONObject();
                                    obj.put("messageId", id);
                                    obj.put("docId", docId);
                                    obj.put("eventName", MqttEvents.MessageResponse.value);


                                    bus.post(obj);
                                } catch (JSONException e) {

                                }
                                db.updateMessageStatus(docId, id, 0, null);
                                db.removeUnsentMessage(AppController.getInstance().unsentMessageDocId, id);

                            }
                        } else {



                            /*
                             *Have to update only incase the message was removed before sending due to no internet
                             */


                            db.updateMessageStatus(docId, id, 0, null);
                            db.removeUnsentMessage(AppController.getInstance().unsentMessageDocId, id);


                        }
                    }
                }

            });

            mqttConnectOptions = new

                    MqttConnectOptions();

            mqttConnectOptions.setCleanSession(false);

            mqttConnectOptions.setMaxInflight(1000);
            mqttConnectOptions.setAutomaticReconnect(true);
            JSONObject obj = new JSONObject();
            try

            {
                obj.put("lastSeenEnabled", sharedPref.getBoolean("enableLastSeen", true));
                obj.put("status", 2);
                obj.put("userId", userId);
            } catch (
                    JSONException e)

            {

            }
            mqttConnectOptions.setWill(MqttEvents.OnlineStatus.value + "/" + userId, obj.toString().

                    getBytes(), 0, true);
            mqttConnectOptions.setKeepAliveInterval(60);


            /*
             * Has been removed from here to avoid the race condition for the mqtt connection with the mqtt broker
             */


        }


        if (!serviceAlreadyScheduled) {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


                ComponentName serviceName = new ComponentName(mInstance, OreoJobService.class.getName());
                PersistableBundle extras = new PersistableBundle();
                extras.putString("command", "start");


                JobInfo jobInfo = (new JobInfo.Builder(MQTT_constants.MQTT_JOB_ID, serviceName)).setExtras(extras).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setMinimumLatency(1L).setOverrideDeadline(1L).build();


                JobScheduler jobScheduler = (JobScheduler) mInstance.getSystemService(Context.JOB_SCHEDULER_SERVICE);

                try {

                    jobScheduler.schedule(jobInfo);
                } catch (IllegalArgumentException errorMessage) {

                    errorMessage.printStackTrace();
                }


            } else {
                Intent changeStatus = new Intent(mInstance, AppKilled.class);
                startService(changeStatus);
            }


            serviceAlreadyScheduled = true;

        }


        if (notFromJobScheduler) {
            connectMqttClient();
        }

        return mqttAndroidClient;

    }

    @SuppressWarnings("TryWithIdenticalCatches")
    public void publish(String topicName, JSONObject obj, int qos, boolean retained) {


        try {

            mqttAndroidClient.publish(topicName, obj.toString().getBytes(), qos, retained);


        } catch (MqttException e) {


        } catch (NullPointerException e) {

        }


    }

    private JSONObject convertMessageToJsonObject(MqttMessage message) {

        JSONObject obj = new JSONObject();
        try {

            obj = new JSONObject(new String(message.getPayload()));
        } catch (JSONException e) {

        }
        return obj;
    }

    /*
     * To save the message received to the local couchdb for the normal and the group chat
     */
    private void putMessageInDb(String receiverUid, String messageType, String actualMessage,
                                String timestamp, String id, String receiverdocId, String thumbnailMessage,


                                int dataSize, long dTime, String senderName, boolean isSelf, int status, boolean timerStarted, long expectedDTime, String removedAt, boolean wasEdited) {


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);

        byte[] thumbnailData = null;


        if ((messageType.equals("1")) || (messageType.equals("2")) || (messageType.equals("7"))) {

            thumbnailData = Base64.decode(thumbnailMessage, Base64.DEFAULT);


        }


        String name = timestamp;
        /*
         *
         *
         * initially in db we only store path of the thumbnail and nce downloaded we will replace that field withthe actual path of the downloaded file
         *
         *
         * */

        String tsInGmt = Utilities.epochtoGmt(timestamp);

        /*
         * Text message
         */
        if (messageType.equals("0")) {

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            boolean isDTag = false;

            if (text.trim().isEmpty()) {


                isDTag = true;

                if (dTime != -1) {

                    String message_dTime = String.valueOf(dTime);

                    for (int i = 0; i < dTimeForDB.length; i++) {
                        if (message_dTime.equals(dTimeForDB[i])) {

                            if (i == 0) {


                                text = getString(R.string.Timer_set_off);
                            } else {


                                text = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                            }
                            break;
                        }
                    }
                } else {

                    if (isSelf) {


                        text = getResources().getString(R.string.YouInvited) + " " + senderName + " " +
                                getResources().getString(R.string.JoinSecretChat);

                    } else {
                        text = getResources().getString(R.string.youAreInvited) + " " + senderName + " " +
                                getResources().getString(R.string.JoinSecretChat);
                    }
                }

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "0");

            map.put("isDTag", isDTag);
            if (wasEdited)
                map.put("wasEdited", true);


            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }

            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);


                db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        } else if (messageType.equals("1")) {
            /*
             * Image message
             */

            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");

            Map<String, Object> map = new HashMap<>();


            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }

            map.put("messageType", "1");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);

            map.put("dataSize", dataSize);


            map.put("thumbnailPath", thumbnailPath);
            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }



            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);

            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }
            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewImage), true, tsInGmt, tsInGmt);

            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        } else if (messageType.equals("2")) {

            /*
             * Video message
             */
            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


            Map<String, Object> map = new HashMap<>();


            /*
             *
             *
             * message key will contail the url on server until downloaded and once downloaded
             * it will contain the local path of the video or image
             *
             * */
            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }


            map.put("messageType", "2");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);
            map.put("dataSize", dataSize);

            map.put("thumbnailPath", thumbnailPath);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewVideo), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("3")) {
            /*
             * Location message
             */

            String placeString = "";
            try {

                placeString = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", placeString);
            map.put("messageType", "3");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewLocation), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("4")) {
            /*
             * Follow message
             */

            String contactString = "";
            try {

                contactString = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", contactString);
            map.put("messageType", "4");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);

            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }
            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewContact), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("5")) {

            /*
             * Audio message
             */
            Map<String, Object> map = new HashMap<>();


            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }


            map.put("messageType", "5");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);
            map.put("downloadStatus", 0);
            map.put("dataSize", dataSize);


            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewAudio), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("6")) {


            /*
             * Sticker
             */

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }
            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "6");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);


            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }

            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }
            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);

                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewSticker), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("7")) {
            /*
             * Doodle
             */
            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


            Map<String, Object> map = new HashMap<>();

            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }

            map.put("messageType", "7");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);

            map.put("dataSize", dataSize);
            map.put("thumbnailPath", thumbnailPath);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }
            thumbnailPath = null;

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDoodle), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("8")) {

            /*
             * Gif
             */


            String url = "";
            try {

                url = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }
            Map<String, Object> map = new HashMap<>();
            map.put("message", url);
            map.put("messageType", "8");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewGiphy), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }

        } else if (messageType.equals("11")) {

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }

            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "11");
            map.put("removedAt", removedAt);
            map.put("isDTag", false);

            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }

            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);


                db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        }

    }


    private void putMessageInDb(String receiverUid, String actualMessage,
                                String timestamp, String id, String receiverdocId,


                                int dataSize, long dTime, boolean isSelf, int status, boolean timerStarted, long expectedDTime, String mimeType, String fileName, String extension) {


        /*
         * For saving the document received
         */


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);


        String tsInGmt = Utilities.epochtoGmt(timestamp);


        /*
         * document message
         */


        Map<String, Object> map = new HashMap<>();


        try {

            map.put("message", new String(data, "UTF-8"));


        } catch (UnsupportedEncodingException e) {

        }

        map.put("messageType", "9");
        map.put("isSelf", isSelf);
        map.put("from", receiverUid);
        map.put("Ts", tsInGmt);
        map.put("id", id);


        map.put("fileName", fileName);
        map.put("mimeType", mimeType);
        map.put("extension", extension);

        map.put("downloadStatus", 0);

        map.put("dataSize", dataSize);


        if (isSelf) {


            map.put("deliveryStatus", String.valueOf(status));


        }



        /*
         * For secret chat exclusively
         */

        map.put("dTime", dTime);
        map.put("timerStarted", timerStarted);

        if (timerStarted) {
            map.put("expectedDTime", expectedDTime);

        }
        if (status == -1) {
            db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
            db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDocument), true, tsInGmt, tsInGmt);

        } else {


            db.addNewChatMessageAndSort(receiverdocId, map, null);

        }


    }


    private void putPostMessageInDb(String receiverUid, String actualMessage,
                                    String timestamp, String id, String receiverdocId,


                                    long dTime, boolean isSelf, int status, boolean timerStarted, long expectedDTime, String postId, String postTitle, int postType) {


        /*
         * For saving the document received
         */


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);


        String tsInGmt = Utilities.epochtoGmt(timestamp);


        /*
         * document message
         */


        Map<String, Object> map = new HashMap<>();


        try {

            map.put("message", new String(data, "UTF-8"));


        } catch (UnsupportedEncodingException e) {

        }

        map.put("messageType", "13");
        map.put("isSelf", isSelf);
        map.put("from", receiverUid);
        map.put("Ts", tsInGmt);
        map.put("id", id);


        map.put("postId", postId);
        map.put("postType", postType);
        map.put("postTitle", postTitle);


        if (isSelf) {


            map.put("deliveryStatus", String.valueOf(status));


        }



        /*
         * For secret chat exclusively
         */

        map.put("dTime", dTime);
        map.put("timerStarted", timerStarted);

        if (timerStarted) {
            map.put("expectedDTime", expectedDTime);

        }
        if (status == -1) {
            db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
            db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewPost), true, tsInGmt, tsInGmt);

        } else {


            db.addNewChatMessageAndSort(receiverdocId, map, null);

        }


    }


    private void putReplyMessageInDb(String receiverUid, String actualMessage,
                                     String timestamp, String id, String receiverdocId,


                                     int dataSize, long dTime, boolean isSelf, int status, boolean timerStarted, long expectedDTime,
                                     String mimeType, String fileName, String extension,
                                     String thumbnail, String senderName, String replyType, String previousReceiverIdentifier, String previousFrom,

                                     String previousPayload, String previousType, String

                                             previousId, String previousFileType, boolean wasEdited, String postId, String postTitle, int postType)

    {


        String name = timestamp;
        Map<String, Object> map = new HashMap<>();
        map.put("messageType", "10");
        map.put("previousReceiverIdentifier", previousReceiverIdentifier);

        map.put("previousFrom", previousFrom);


        map.put("previousType", previousType);

        map.put("previousId", previousId);


        if (previousType.equals("9")) {

            map.put("previousFileType", previousFileType);

            map.put("previousPayload", previousPayload);
        } else if (previousType.equals("1") || previousType.equals("2") || previousType.equals("7"))

        {


            map.put("previousPayload", convertByteArrayToFile(Base64.decode(previousPayload, Base64.DEFAULT), previousId, "jpg"));
        } else {

            map.put("previousPayload", previousPayload);

        }



        /*
         * For saving the reply message received
         */


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);

        byte[] thumbnailData = null;


        if ((replyType.equals("1")) || (replyType.equals("2")) || (replyType.equals("7"))) {


            thumbnailData = Base64.decode(thumbnail, Base64.DEFAULT);


        }



        /*
         *
         *
         * Initially in db we only store path of the thumbnail and nce downloaded we will replace that field withthe actual path of the downloaded file
         *
         *
         * */

        String tsInGmt = Utilities.epochtoGmt(timestamp);
        switch (Integer.parseInt(replyType)) {


            case 0: {
                /*
                 * Text message
                 */
                String text = "";
                try {

                    text = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                boolean isDTag = false;

                if (text.trim().isEmpty()) {


                    isDTag = true;

                    if (dTime != -1) {

                        String message_dTime = String.valueOf(dTime);

                        for (int i = 0; i < dTimeForDB.length; i++) {
                            if (message_dTime.equals(dTimeForDB[i])) {

                                if (i == 0) {


                                    text = getString(R.string.Timer_set_off);
                                } else {


                                    text = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                }
                                break;
                            }
                        }
                    } else {

                        if (isSelf) {


                            text = getResources().getString(R.string.YouInvited) + " " + senderName + " " +
                                    getResources().getString(R.string.JoinSecretChat);

                        } else {
                            text = getResources().getString(R.string.youAreInvited) + " " + senderName + " " +
                                    getResources().getString(R.string.JoinSecretChat);
                        }
                    }

                }

//                Map<String, Object> map = new HashMap<>();

                map.put("message", text);
                map.put("replyType", "0");

                map.put("isDTag", isDTag);
                if (wasEdited)
                    map.put("wasEdited", true);
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }

                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }
                break;
            }

            case 1: {
                /*
                 * Image
                 */

                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "1");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);


                map.put("thumbnailPath", thumbnailPath);
                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }



                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);

                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewImage), true, tsInGmt, tsInGmt);

                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }
                break;

            }
            case 2: {
                /*
                 * Video
                 */

                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");





                /*
                 *
                 *
                 * message key will contail the url on server until downloaded and once downloaded
                 * it will contain the local path of the video or image
                 *
                 * */
                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }


                map.put("replyType", "2");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);
                map.put("dataSize", dataSize);

                map.put("thumbnailPath", thumbnailPath);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewVideo), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 3: {
                /*
                 * Location
                 */

                String placeString = "";
                try {

                    placeString = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                map.put("message", placeString);
                map.put("replyType", "3");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewLocation), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 4: {
                /*
                 * Follow
                 */
                String contactString = "";
                try {

                    contactString = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                map.put("message", contactString);
                map.put("replyType", "4");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);

                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewContact), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }

                break;

            }
            case 5: {
                /*
                 * Audio
                 */


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }


                map.put("replyType", "5");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);
                map.put("downloadStatus", 0);
                map.put("dataSize", dataSize);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewAudio), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 6: {
                /*
                 * Sticker
                 */

                String text = "";
                try {

                    text = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }

                map.put("message", text);
                map.put("replyType", "6");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }

                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);

                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewSticker), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }

            case 7: {
                /*
                 * Doodle
                 */


                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "7");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);
                map.put("thumbnailPath", thumbnailPath);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                thumbnailPath = null;

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDoodle), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }


                break;

            }


            case 8: {
                /*
                 * Gif
                 */

                String url = "";
                try {

                    url = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }

                map.put("message", url);
                map.put("replyType", "8");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewGiphy), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 9: {



                /*
                 * document message
                 */


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "9");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);


                map.put("fileName", fileName);
                map.put("mimeType", mimeType);
                map.put("extension", extension);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }



                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);

                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDocument), true, tsInGmt, tsInGmt);

                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }

                break;
            }


            case 13: {
                /*
                 * Post
                 */

                String url = "";
                try {

                    url = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }

                map.put("message", url);
                map.put("replyType", "13");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);
                map.put("postId", postId);
                map.put("postTitle", postTitle);
                map.put("postType", postType);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewPost), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
        }


    }


    public String convertByteArrayToFile(byte[] data, String name, String extension) {


        File file;


        String path = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER;


        try {


            File folder = new File(path);


            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }


            file = new File(path, name + "." + extension);


            if (!file.exists()) {

                file.createNewFile();

            }


            FileOutputStream fos = new FileOutputStream(file);


            fos.write(data);
            fos.flush();
            fos.close();
        } catch (IOException e) {

        }


        return path + "/" + name + "." + extension;
    }

    public void connectMqttClient() {


        try

        {
            mqttAndroidClient.connect(mqttConnectOptions, mInstance, listener);

        } catch (MqttException e) {

        }
    }

    public void subscribeToTopic(String topic, int qos) {

        try {


            if (mqttAndroidClient != null) {


                mqttAndroidClient.subscribe(topic, qos);
            }
        } catch (MqttException e) {

        }

    }

    @SuppressWarnings("TryWithIdenticalCatches")
    public void unsubscribeToTopic(String topic) {


        try {


            if (mqttAndroidClient != null) {
                mqttAndroidClient.unsubscribe(topic);
            }
        } catch (MqttException e) {

        } catch (NullPointerException e) {

        }
    }

    public void updatePresence(int status, boolean applicationKilled) {


        if (signedIn) {

            if (status == 0) {

                /*
                 * Background
                 */

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("status", 0);


                    if (applicationKilled) {
                        if (sharedPref.getString("lastSeenTime", null) != null) {
                            obj.put("timestamp", sharedPref.getString("lastSeenTime", null));
                        } else {


                            obj.put("timestamp", Utilities.tsInGmt());
                        }

                    } else {

                        obj.put("timestamp", Utilities.tsInGmt());
                    }
                    obj.put("userId", userId);


                    obj.put("lastSeenEnabled", sharedPref.getBoolean("enableLastSeen", true));


                    publish(MqttEvents.OnlineStatus.value + "/" + userId, obj, 0, true);
                } catch (JSONException w) {

                }

            } else {

                /*
                 *Foreground
                 */

//                if (!applicationKilled) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("status", 1);
                    obj.put("userId", userId);
                    obj.put("lastSeenEnabled", sharedPref.getBoolean("enableLastSeen", true));


                    publish(MqttEvents.OnlineStatus.value + "/" + userId, obj, 0, true);
                } catch (JSONException w) {

                }
                //    }
            }

        }
    }


    public void publishChatMessage(String topicName, JSONObject obj, int qos, boolean retained, HashMap<String, Object> map) {

        IMqttDeliveryToken token = null;


        try {


            obj.put("userImage", userImageUrl);

        } catch (JSONException e) {

        }


        if (mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
            try {


                token = mqttAndroidClient.publish(topicName, obj.toString().getBytes(), qos, retained);


            } catch (MqttException e) {


            }


            map.put("MQttToken", token);

            tokenMapping.add(map);
            set.add(token);
        }
    }

    public boolean canPublish() {


        return mqttAndroidClient != null && mqttAndroidClient.isConnected();


    }

    public void updateTokenMapping() {


        if (signedIn) {
            db.addMqttTokenMapping(mqttTokenDocId, tokenMapping);
        }

    }

    @SuppressWarnings("TryWithIdenticalCatches")

    private void resendUnsentMessages() {


        String documentId = AppController.getInstance().unsentMessageDocId;

        if (documentId != null) {


            ArrayList<Map<String, Object>> arr = db.getUnsentMessages(documentId);

            if (arr.size() > 0) {

                String to;
                for (int i = 0; i < arr.size(); i++) {

                    Map<String, Object> map = arr.get(i);

                    JSONObject obj = new JSONObject();
                    try {

                        to = (String) map.get("to");
                        obj.put("from", userId);
                        obj.put("to", map.get("to"));
                        obj.put("receiverIdentifier", userIdentifier);

                        String type = (String) map.get("type");

                        String message = (String) map.get("message");

                        String id = (String) map.get("id");


                        String secretId = "";


                        if (map.containsKey("secretId")) {
                            secretId = (String) map.get("secretId");
                        }

                        if (map.containsKey("isGroupMessage")) {

                            Map<String, Object> groupDetails = db.getGroupSubjectAndImageUrl((String) map.get("toDocId"));


                            if (groupDetails != null) {


                                obj.put("name", groupDetails.get("receiverName"));

                                obj.put("userImage", groupDetails.get("receiverImage"));
                            }


                        } else {
                            obj.put("name", map.get("name"));

                            obj.put("userImage", map.get("userImage"));

                        }
                        obj.put("toDocId", map.get("toDocId"));
                        obj.put("id", id);
                        obj.put("type", type);


                        if (!secretId.isEmpty()) {
                            obj.put("secretId", secretId);
                            obj.put("dTime", map.get("dTime"));

                        }


                        obj.put("timestamp", map.get("timestamp"));
                        HashMap<String, Object> mapTemp = new HashMap<>();
                        mapTemp.put("messageId", id);
                        mapTemp.put("docId", map.get("toDocId"));


                        if (type.equals("0")) {


                            /*
                             * Text message
                             */
                            try {


                                obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());

                            } catch (UnsupportedEncodingException e) {

                            }

                            if (map.containsKey("isGroupMessage")) {


                                AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"),
                                        obj, mapTemp);

                            } else {
                                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            }
                            String tsInGmt = Utilities.tsInGmt();


                            /*
                             * Have been intentionally made to query again to avoid the case of chat being deleted b4 message is sent
                             */

                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);
                        } else if (type.equals("1")) {
                            /*
                             * Image message
                             */

                            Uri uri = null;
                            Bitmap bm = null;


                            try {

                                final BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;
                                BitmapFactory.decodeFile(message, options);


                                int height = options.outHeight;
                                int width = options.outWidth;

                                float density = AppController.getInstance().getResources().getDisplayMetrics().density;
                                int reqHeight;


                                reqHeight = (int) ((150 * density) * (height / width));

                                bm = AppController.getInstance().decodeSampledBitmapFromResource(message, (int) (150 * density), reqHeight);


                                if (bm != null) {


                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                    bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                    byte[] b = baos.toByteArray();

                                    try {
                                        baos.close();
                                    } catch (IOException e) {

                                    }
                                    baos = null;


                                    File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".jpg");
                                    b = null;

                                    uri = Uri.fromFile(f);
                                    f = null;


                                }


                            } catch (OutOfMemoryError e) {


                            } catch (Exception e) {

                                /*
                                 *
                                 * to handle the file not found exception
                                 *
                                 *
                                 * */


                            }


                            if (uri != null) {


                                /*
                                 *
                                 *
                                 * make thumbnail
                                 *
                                 * */

                                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                                bm = null;
                                byte[] b = baos.toByteArray();

                                try {
                                    baos.close();
                                } catch (IOException e) {

                                }
                                baos = null;


                                obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                AppController.getInstance().uploadFile(uri,
                                        AppController.getInstance().userId + id, 1, obj, (String) map.get("to"),
                                        id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));

                                uri = null;
                                b = null;
                                bm = null;
                            }


                        } else if (type.equals("2")) {
                            /*
                             * Video message
                             */


                            Uri uri = null;

                            try {
                                File video = new File(message);


                                if (video.exists())


                                {

                                    byte[] b = convertFileToByteArray(video);
                                    video = null;


                                    File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".mp4");
                                    b = null;

                                    uri = Uri.fromFile(f);
                                    f = null;

                                    b = null;

                                    if (uri != null) {


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(message,
                                                MediaStore.Images.Thumbnails.MINI_KIND);

                                        if (bm != null) {

                                            bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);
                                            bm = null;
                                            b = baos.toByteArray();
                                            try {
                                                baos.close();
                                            } catch (IOException e) {

                                            }
                                            baos = null;


                                            obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                            AppController.getInstance().uploadFile(uri,
                                                    AppController.getInstance().userId + id, 2, obj,
                                                    (String) map.get("to"), id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));
                                            uri = null;
                                            b = null;

                                        }
                                    }

                                }


                            } catch (OutOfMemoryError e) {

                            } catch (Exception e) {


                                /*
                                 *
                                 * to handle the file not found exception incase file has not been found
                                 *
                                 * */


                            }


                        } else if (type.equals("3")) {


                            /*
                             * Location message
                             */

                            try {
                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                            } catch (UnsupportedEncodingException e) {

                            }
                            if (map.containsKey("isGroupMessage")) {


                                AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                            } else {
                                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            }
                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"),
                                    secretId);
                            db.updateMessageTs(docId, id, tsInGmt);
                        } else if (type.equals("4")) {

                            /*
                             * Follow message
                             */

                            try {


                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                            } catch (UnsupportedEncodingException e) {

                            }
                            if (map.containsKey("isGroupMessage")) {


                                AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                            } else {

                                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                            }
                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);
                        } else if (type.equals("5")) {

                            /*
                             * Audio message
                             */


                            Uri uri;
                            try {

                                File audio = new File(message);


                                if (audio.exists()) {


                                    byte[] b = convertFileToByteArray(audio);
                                    audio = null;


                                    File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".mp3");
                                    b = null;

                                    uri = Uri.fromFile(f);
                                    f = null;

                                    b = null;


                                    if (uri != null) {


                                        AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id,
                                                5, obj, (String) map.get("to"), id, mapTemp, secretId, null,
                                                map.containsKey("toDelete"), map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));
                                    }


                                }
                            } catch (Exception e) {


                                /*
                                 *
                                 * to handle the file not found exception incase file has not been found
                                 *
                                 * */


                            }
                        } else if (type.equals("6")) {
                            /*
                             * Sticker
                             */
                            try {
                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                            } catch (UnsupportedEncodingException e) {

                            }

                            if (map.containsKey("isGroupMessage")) {


                                AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                            } else {
                                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            }
                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);

                        } else if (type.equals("7")) {
                            /*
                             *Doodle
                             */

                            Uri uri = null;
                            Bitmap bm = null;


                            try {

                                final BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;
                                BitmapFactory.decodeFile(message, options);


                                int height = options.outHeight;
                                int width = options.outWidth;

                                float density = AppController.getInstance().getResources().getDisplayMetrics().density;
                                int reqHeight;


                                reqHeight = (int) ((150 * density) * (height / width));

                                bm = AppController.getInstance().decodeSampledBitmapFromResource(message, (int) (150 * density), reqHeight);


                                if (bm != null) {


                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                    bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                    byte[] b = baos.toByteArray();

                                    try {
                                        baos.close();
                                    } catch (IOException e) {

                                    }
                                    baos = null;


                                    File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".jpg");
                                    b = null;

                                    uri = Uri.fromFile(f);
                                    f = null;


                                }


                            } catch (OutOfMemoryError e) {


                            } catch (Exception e) {

                                /*
                                 *
                                 * to handle the file not found exception
                                 *
                                 *
                                 * */


                            }


                            if (uri != null) {


                                /*
                                 *
                                 *
                                 * make thumbnail
                                 *
                                 * */

                                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                                bm = null;
                                byte[] b = baos.toByteArray();

                                try {
                                    baos.close();
                                } catch (IOException e) {

                                }
                                baos = null;


                                obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id, 7, obj,
                                        (String) map.get("to"), id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));

                                uri = null;
                                b = null;
                                bm = null;
                            }

                        } else if (type.equals("8")) {
                            /*
                             *Gif
                             */

                            try {
                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                            } catch (UnsupportedEncodingException e) {

                            }

                            if (map.containsKey("isGroupMessage")) {


                                AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                            } else {
                                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            }
                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);

                        } else if (type.equals("9")) {

                            /*
                             * Document
                             */


                            Uri uri;
                            try {

                                File document = new File(message);


                                if (document.exists()) {


                                    uri = Uri.fromFile(document);


                                    document = null;


                                    if (uri != null) {


                                        AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id,
                                                9, obj, (String) map.get("to"), id, mapTemp, secretId,

                                                (String) map.get("extension"), false, map.containsKey("isGroupMessage"),
                                                map.get("groupMembersDocId"));
                                    }


                                }
                            } catch (Exception e) {


                                /*
                                 *
                                 * to handle the file not found exception incase file has not been found
                                 *
                                 * */


                            }
                        } else if (type.equals("10")) {
                            /*
                             * Reply message
                             */
                            String replyType = (String) map.get("replyType");


                            String previousType = (String) map.get("previousType");


                            obj.put("replyType", replyType);


                            obj.put("previousFrom", map.get("previousFrom"));

                            obj.put("previousType", previousType);

                            obj.put("previousId", map.get("previousId"));

                            obj.put("previousReceiverIdentifier", map.get("previousReceiverIdentifier"));
                            if (previousType.equals("9")) {

                                obj.put("previousFileType", map.get("previousFileType"));
                            }

                            if (previousType.equals("1") || previousType.equals("2") || previousType.equals("7"))

                            {
                                switch (Integer.parseInt(previousType)) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource((String) map.get("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail((String) map.get("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource((String) map.get("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }


                                }

                            } else {


                                obj.put("previousPayload", map.get("previousPayload"));

                            }


                            switch (Integer.parseInt(replyType)) {

                                case 0: {
                                    /*
                                     * Text
                                     */

                                    try {


                                        obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }

                                    if (map.containsKey("isGroupMessage")) {


                                        AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                                    } else {
                                        AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                                    }
                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);
                                    break;
                                }
                                case 1: {
                                    /*
                                     * Image
                                     */

                                    Uri uri = null;
                                    Bitmap bm = null;


                                    try {

                                        final BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inJustDecodeBounds = true;
                                        BitmapFactory.decodeFile(message, options);


                                        int height = options.outHeight;
                                        int width = options.outWidth;

                                        float density = AppController.getInstance().getResources().getDisplayMetrics().density;
                                        int reqHeight;


                                        reqHeight = (int) ((150 * density) * (height / width));

                                        bm = AppController.getInstance().decodeSampledBitmapFromResource(message, (int) (150 * density), reqHeight);


                                        if (bm != null) {


                                            ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                            bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                            byte[] b = baos.toByteArray();

                                            try {
                                                baos.close();
                                            } catch (IOException e) {

                                            }
                                            baos = null;


                                            File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".jpg");
                                            b = null;

                                            uri = Uri.fromFile(f);
                                            f = null;


                                        }


                                    } catch (OutOfMemoryError e) {


                                    } catch (Exception e) {

                                        /*
                                         *
                                         * to handle the file not found exception
                                         *
                                         *
                                         * */


                                    }


                                    if (uri != null) {


                                        /*
                                         *
                                         *
                                         * make thumbnail
                                         *
                                         * */

                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                        AppController.getInstance().uploadFile(uri,
                                                AppController.getInstance().userId + id, 1, obj, (String) map.get("to"),
                                                id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));

                                        uri = null;
                                        b = null;
                                        bm = null;
                                    }

                                    break;
                                }
                                case 2: {
                                    /*
                                     * Video
                                     */
                                    Uri uri = null;

                                    try {
                                        File video = new File(message);


                                        if (video.exists())


                                        {

                                            byte[] b = convertFileToByteArray(video);
                                            video = null;


                                            File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".mp4");
                                            b = null;

                                            uri = Uri.fromFile(f);
                                            f = null;

                                            b = null;

                                            if (uri != null) {


                                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                                Bitmap bm = ThumbnailUtils.createVideoThumbnail(message,
                                                        MediaStore.Images.Thumbnails.MINI_KIND);

                                                if (bm != null) {

                                                    bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);
                                                    bm = null;
                                                    b = baos.toByteArray();
                                                    try {
                                                        baos.close();
                                                    } catch (IOException e) {

                                                    }
                                                    baos = null;


                                                    obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                                    AppController.getInstance().uploadFile(uri,
                                                            AppController.getInstance().userId + id, 2, obj,
                                                            (String) map.get("to"), id, mapTemp, secretId, null, true,
                                                            map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));
                                                    uri = null;
                                                    b = null;

                                                }
                                            }

                                        }


                                    } catch (OutOfMemoryError e) {

                                    } catch (Exception e) {


                                        /*
                                         *
                                         * to handle the file not found exception incase file has not been found
                                         *
                                         * */


                                    }


                                    break;
                                }
                                case 3: {
                                    /*
                                     * Location
                                     */
                                    try {
                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }
                                    if (map.containsKey("isGroupMessage")) {


                                        AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                                    } else {
                                        AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                                    }
                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"),
                                            secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);

                                    break;
                                }
                                case 4: {
                                    /*
                                     * Follow
                                     */

                                    try {


                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }
                                    if (map.containsKey("isGroupMessage")) {


                                        AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                                    } else {

                                        AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                                    }

                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);
                                    break;
                                }
                                case 5: {
                                    /*
                                     * Audio
                                     */

                                    Uri uri;
                                    try {

                                        File audio = new File(message);


                                        if (audio.exists()) {


                                            byte[] b = convertFileToByteArray(audio);
                                            audio = null;


                                            File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".mp3");
                                            b = null;

                                            uri = Uri.fromFile(f);
                                            f = null;

                                            b = null;


                                            if (uri != null) {


                                                AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id,
                                                        5, obj, (String) map.get("to"), id, mapTemp, secretId,
                                                        null, map.containsKey("toDelete"), map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));
                                            }


                                        }
                                    } catch (Exception e) {


                                        /*
                                         *
                                         * to handle the file not found exception incase file has not been found
                                         *
                                         * */


                                    }
                                    break;
                                }
                                case 6: {
                                    /*
                                     * Sticker
                                     */
                                    try {
                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }

                                    if (map.containsKey("isGroupMessage")) {


                                        AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                                    } else {
                                        AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                                    }
                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);


                                    break;
                                }
                                case 7: {
                                    /*
                                     * Doodle
                                     */
                                    Uri uri = null;
                                    Bitmap bm = null;


                                    try {

                                        final BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inJustDecodeBounds = true;
                                        BitmapFactory.decodeFile(message, options);


                                        int height = options.outHeight;
                                        int width = options.outWidth;

                                        float density = AppController.getInstance().getResources().getDisplayMetrics().density;
                                        int reqHeight;


                                        reqHeight = (int) ((150 * density) * (height / width));

                                        bm = AppController.getInstance().decodeSampledBitmapFromResource(message, (int) (150 * density), reqHeight);


                                        if (bm != null) {


                                            ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                            bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                            byte[] b = baos.toByteArray();

                                            try {
                                                baos.close();
                                            } catch (IOException e) {

                                            }
                                            baos = null;


                                            File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".jpg");
                                            b = null;

                                            uri = Uri.fromFile(f);
                                            f = null;


                                        }


                                    } catch (OutOfMemoryError e) {


                                    } catch (Exception e) {

                                        /*
                                         *
                                         * to handle the file not found exception
                                         *
                                         *
                                         * */


                                    }


                                    if (uri != null) {


                                        /*
                                         *
                                         *
                                         * Make thumbnail
                                         *
                                         * */

                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                        AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id, 7, obj,
                                                (String) map.get("to"), id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));

                                        uri = null;
                                        b = null;
                                        bm = null;
                                    }

                                    break;
                                }
                                case 8: {
                                    /*
                                     * Gif
                                     */

                                    try {
                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }

                                    if (map.containsKey("isGroupMessage")) {


                                        AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                                    } else {
                                        AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                                    }
                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);
                                    break;
                                }
                                case 9: {

                                    /*
                                     * Document
                                     */
                                    Uri uri;
                                    try {

                                        File document = new File(message);


                                        if (document.exists()) {


                                            uri = Uri.fromFile(document);


                                            document = null;


                                            if (uri != null) {


                                                AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id,
                                                        9, obj, (String) map.get("to"), id, mapTemp, secretId,
                                                        (String) map.get("extension"), false, map.containsKey("isGroupMessage"),
                                                        map.get("groupMembersDocId"));
                                            }


                                        }
                                    } catch (Exception e) {


                                        /*
                                         *
                                         * to handle the file not found exception incase file has not been found
                                         *
                                         * */


                                    }

                                    break;
                                }
                                case 13: {

                                    /*
                                     * Post
                                     */


                                    try {
                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());
                                        obj.put("postId", (String) map.get("postId"));
                                        obj.put("postTitle", (String) map.get("postTitle"));
                                        obj.put("postType", (int) map.get("postType"));
                                    } catch (UnsupportedEncodingException e) {

                                    }

                                    if (map.containsKey("isGroupMessage")) {


                                        AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                                    } else {
                                        AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                                    }
                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);


                                    break;
                                }

                            }


                        } else if (type.equals("11")) {


                            /*
                             * Remove message
                             */
                            try {


                                obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());


                                obj.put("removedAt", map.get("timestamp"));


                            } catch (UnsupportedEncodingException e) {

                            }

                            if (map.containsKey("isGroupMessage")) {


                                AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"),
                                        obj, mapTemp);

                            } else {
                                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            }

                        } else if (type.equals("12")) {




                            /*
                             * Edit message
                             */
                            try {

                                obj.put("editedAt", map.get("timestamp"));
                                obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());

                            } catch (UnsupportedEncodingException e) {

                            }

                            if (map.containsKey("isGroupMessage")) {


                                AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"),
                                        obj, mapTemp);

                            } else {
                                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            }
                            String tsInGmt = Utilities.tsInGmt();


                            /*
                             * Have been intentionally made to query again to avoid the case of chat being deleted b4 message is sent
                             */

                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);


                        } else if (type.equals("13")) {

                            /*
                             * Post message
                             */

                            try {


                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());
                                obj.put("postId", (String) map.get("postId"));
                                obj.put("postTitle", (String) map.get("postTitle"));
                                obj.put("postType", (int) map.get("postType"));

                            } catch (UnsupportedEncodingException e) {

                            }
                            if (map.containsKey("isGroupMessage")) {


                                AppController.getInstance().publishGroupChatMessage((String) map.get("groupMembersDocId"), obj, mapTemp);

                            } else {

                                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                            }
                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);
                        }

                    } catch (JSONException e) {


                    } catch (OutOfMemoryError e) {


                    }

                }


            }


        }

    }


//    /**
//     *
//     * For sending of the pending acknowledgements
//     *
//     */
//    private void sendAcknowledgements() {
//
//
//        ArrayList<JSONObject> arr = db.getUnsentAcks(unackMessageDocId);
//
//
//
//        ArrayList<String> arr = db.getUnsentAcks(unackMessageDocId);
//
//        if (arr.size() > 0) {
//
//            count=arr.size();
//
//            JSONObject obj;
//
//            for (int i = 0; i < arr.size(); i++)
//            {
//                obj=arr.get(i);
//
//
//                try {
//                obj=new JSONObject(arr.get(i));
//
//
//
//
//
//                    if(mqttAndroidClient.isConnected()) {
//
//
//                        publish(MqttEvents.Acknowledgement + "/" + obj.getString("to"), obj, 2, false);
//
//               count--;
//                    }
//                }catch(JSONException e){
//
//                }
//
//            }
//
//
//        }
//
//
//
//        if(count==0)   {
//
//            db.removeAllUnsentAcks(unackMessageDocId);
//
//        }
//        count=-1;
//
//    }

    public void disconnect() {

        try {
            if (mqttAndroidClient != null)
                mqttAndroidClient.disconnect();
        } catch (MqttException e) {

        }
    }

    /**
     * To generate the push notifications locally
     */


    @SuppressWarnings("unchecked")
    private void generatePushNotificationLocal(String notificationId, String messageType, String senderName, String actualMessage, Intent intent, long dTime, String secretId, String receiverUid, String replyType) {


        if ((!foreground) || (activeReceiverId.isEmpty()) || (!(activeReceiverId.equals(receiverUid))) || (!(activeReceiverId.equals(receiverUid) && activeSecretId.equals(secretId)))) {
            try {

                String pushMessage = "";
                switch (Integer.parseInt(messageType)) {


                    case 0:

                    {
                        try {
                            pushMessage = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");


                            if (pushMessage.trim().isEmpty()) {


                                /*
                                 *Intentionally written the code twice,as this is not the common case
                                 */

                                if (dTime != -1) {
                                    String message_dTime = String.valueOf(dTime);

                                    for (int i = 0; i < dTimeForDB.length; i++) {
                                        if (message_dTime.equals(dTimeForDB[i])) {

                                            if (i == 0) {


                                                pushMessage = getString(R.string.Timer_set_off);
                                            } else {


                                                pushMessage = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                            }
                                            break;
                                        }
                                    }
                                } else {

                                    pushMessage = getResources().getString(R.string.youAreInvited) + " " + senderName + " " +
                                            getResources().getString(R.string.JoinSecretChat);
                                }

                            }


                        } catch (UnsupportedEncodingException e) {

                        }
                        break;
                    }
                    case 1: {

                        pushMessage = "Image";
                        break;
                    }

                    case 2: {
                        pushMessage = "Video";
                        break;
                    }
                    case 3: {
                        pushMessage = "Location";

                        break;
                    }
                    case 4: {
                        pushMessage = "Follow";

                        break;
                    }
                    case 5: {
                        pushMessage = "Audio";

                        break;
                    }
                    case 6: {

                        pushMessage = "Sticker";

                        break;
                    }
                    case 7: {

                        pushMessage = "Doodle";

                        break;

                    }
                    case 8: {

                        pushMessage = "Gif";

                        break;
                    }


                    case 10: {

                        switch (Integer.parseInt(replyType)) {


                            case 0:

                            {
                                try {
                                    pushMessage = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");


                                } catch (UnsupportedEncodingException e) {

                                }
                                break;
                            }
                            case 1: {

                                pushMessage = "Image";
                                break;
                            }

                            case 2: {
                                pushMessage = "Video";
                                break;
                            }
                            case 3: {
                                pushMessage = "Location";

                                break;
                            }
                            case 4: {
                                pushMessage = "Follow";

                                break;
                            }
                            case 5: {
                                pushMessage = "Audio";

                                break;
                            }
                            case 6: {

                                pushMessage = "Sticker";

                                break;
                            }
                            case 7: {

                                pushMessage = "Doodle";

                                break;

                            }
                            case 8: {

                                pushMessage = "Gif";

                                break;
                            }
                            default: {
                                pushMessage = "Document";
                            }
                        }
                        break;
                    }
                    default: {
                        pushMessage = "Document";
                    }


                }


                /*
                 * For clubbing of the notifications
                 */

                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();


                Map<String, Object> notificationInfo = fetchNotificationInfo(notificationId);


                int unreadMessageCount;
                int systemNotificationId;

                ArrayList<String> messages;
                if (notificationInfo == null) {

                    /*
                     * No previous notifications for the chat
                     *
                     */
                    notificationInfo = new HashMap<>();
                    messages = new ArrayList<>();

                    messages.add(pushMessage);
                    notificationInfo.put("notificationMessages", messages);

                    notificationInfo.put("notificationId", notificationId);

                    systemNotificationId = Integer.parseInt(String.valueOf(System.currentTimeMillis()).substring(9));
                    notificationInfo.put("systemNotificationId", systemNotificationId);


                    unreadMessageCount = 0;
                } else {
                    messages = (ArrayList<String>) notificationInfo.get("notificationMessages");
                    messages.add(0, pushMessage);

                    if (messages.size() > NOTIFICATION_SIZE) {
                        messages.remove(messages.size() - 1);
                    }
                    systemNotificationId = (int) notificationInfo.get("systemNotificationId");
                    notificationInfo.put("notificationMessages", messages);
                    unreadMessageCount = (int) notificationInfo.get("messagesCount");

                }
                notificationInfo.put("messagesCount", unreadMessageCount + 1);
                addOrUpdateNotification(notificationInfo, notificationId);


                for (int i = 0; i < messages.size(); i++) {


                    inboxStyle.addLine(messages.get(i));


                }

                if (unreadMessageCount > (NOTIFICATION_SIZE - 1)) {
                    inboxStyle.setSummaryText("+" + (unreadMessageCount - (NOTIFICATION_SIZE - 1)) + " " + getString(R.string.more_message));
                }


                PendingIntent pendingIntent = PendingIntent.getActivity(this, systemNotificationId, intent, PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                String title, tickerText;
                if (messages.size() == 1) {

                    pushMessage = senderName + ": " + pushMessage;
                    tickerText = pushMessage;
                    title = getString(R.string.app_name);
                } else {
                    tickerText = senderName + ": " + pushMessage;
                    title = senderName;
                }


                inboxStyle.setBigContentTitle(title);
                NotificationCompat.Builder
                        notificationBuilder = new NotificationCompat.Builder(this, "com.howdoo.chat.THREE")
                        .setSmallIcon(R.mipmap.hola_ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.hola_ic_launcher))
                        .setContentTitle(title)
                        .setContentText(pushMessage)
                        .setTicker(tickerText)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setStyle(inboxStyle)
                        .setDefaults(Notification.DEFAULT_VIBRATE)
                        .setPriority(Notification.PRIORITY_HIGH);

                if (Build.VERSION.SDK_INT >= 21) notificationBuilder.setVibrate(new long[0]);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                NotificationChannel notificationChannel;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationChannel = new NotificationChannel("com.howdoo.chat.THREE", "Channel Three", notificationManager.IMPORTANCE_HIGH);
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.RED);
                    notificationChannel.setShowBadge(true);
                    notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                    notificationManager.createNotificationChannel(notificationChannel);
                }
                /*
                 *
                 * Notification id is used to notify the same notification
                 *
                 * */

                notificationManager.notify(notificationId, systemNotificationId, notificationBuilder.build());

            } catch (Exception e) {

            }
        }

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {


        activeActivitiesCount++;


        /*
         *
         * As app can also be started when clicked on the chat notification
         *
         *
         * */


        if (AppController.getInstance().getSignedIn() && activity.getClass().getSimpleName().equals("CameraActivity")) {

//            sendAcknowledgements();
            foreground = true;


            setApplicationKilled(false);
            updatePresence(1, false);


            /*
             *
             * When the app started,update the status(JUST have put it for checking)
             *
             */


            //  if (signedIn) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("status", 1);
                publish(MqttEvents.CallsAvailability.value + "/" + userId, obj, 0, true);
                AppController.getInstance().setActiveOnACall(false, true);


            } catch (JSONException e) {

            }
            //      }
        }


    }

    @Override
    public void onActivityDestroyed(Activity activity) {


        activeActivitiesCount--;
    }

    @Override
    public void onActivityPaused(Activity activity) {


    }

    @Override
    public void onActivityResumed(Activity activity) {


    }

    @Override
    public void onActivitySaveInstanceState(Activity activity,
                                            Bundle outState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {


    }

    public void updateReconnected() {
        //  Log.d("log55", "reconnection");

        if (!applicationKilled) {
            updatePresence(1, false);
        } else {

            updatePresence(0, true);

        }


        try {

            JSONObject obj = new JSONObject();
            obj.put("eventName", MqttEvents.Connect.value);
            networkConnector.setConnected(true);
            connectionObserver.isConnected(networkConnector);
            bus.post(obj);
        } catch (Exception e) {

        }

        if (flag) {

            flag = false;
            subscribeToTopic(MqttEvents.Message.value + "/" + userId, 1);
            subscribeToTopic(MqttEvents.Acknowledgement.value + "/" + userId, 2);
            subscribeToTopic(MqttEvents.Calls.value + "/" + userId, 0);

            subscribeToTopic(MqttEvents.UserUpdates.value + "/" + userId, 1);
            subscribeToTopic(MqttEvents.GroupChats.value + "/" + userId, 1);

            /*
             *
             * Will have to control their subscription to be requirement based
             */

            subscribeToTopic(MqttEvents.FetchMessages.value + "/" + userId, 1);
        }

        if (signedIn) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("status", 1);
                publish(MqttEvents.CallsAvailability.value + "/" + userId, obj, 0, true);
                AppController.getInstance().setActiveOnACall(false, true);


            } catch (JSONException e) {

            }
        }
        resendUnsentMessages();
    }

    public void updateLastSeenSettings(boolean lastSeenSetting) {


        sharedPref.edit().putBoolean("enableLastSeen", lastSeenSetting).apply();
    }


    @SuppressWarnings("unchecked")
    public void getCurrentTime() {

        new FetchTime().execute();
    }

    public void cutCallOnKillingApp() {



        /*
         * Have to make myself available and inform  other user of the call getting cut
         */

        try {


            if (activeCallId != null && activeCallerId != null) {
                JSONObject obj = new JSONObject();

                obj.put("callId", activeCallId);
                obj.put("userId", activeCallerId);
                obj.put("type", 2);


                AppController.getInstance().publish(MqttEvents.Calls.value + "/" + activeCallerId, obj, 0, false);

            }
            JSONObject obj = new JSONObject();
            obj.put("status", 1);

            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
            AppController.getInstance().setActiveOnACall(false, true);
        } catch (JSONException e) {

        }


    }

//    /**
//     * To remove the message to be deleted from list of the active timers
//     */
//
//
//    public void removeMessageToBeDeleted(String docId, String messageId) {
//
//
//    }

    public String randomString() {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }


        sb.append("PnPLabs3Embed");
        return sb.toString();
    }

    public void setTimer(String documentId, String messageId, long expectedDTime) {

        if (expectedDTime > 0) {
            long temp = Utilities.getGmtEpoch();
            db.setTimerStarted(documentId, messageId, temp + expectedDTime);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void registerContactsObserver() {

        if (contactSynced && !registeredObserver) {
            registeredObserver = true;


            getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, false, contactUpdateObserver);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                getContentResolver().registerContentObserver(ContactsContract.DeletedContacts.CONTENT_URI, false, contactDeletedObserver);
            }


        }
    }

    public void unregisterContactsObserver() {


        if (contactSynced) {

            getContentResolver().unregisterContentObserver(contactUpdateObserver);

            getContentResolver().unregisterContentObserver(contactDeletedObserver);
            registeredObserver = false;
        }
    }


    /**
     * @param arrayList containing the list of the contact ids which have been deleted
     */

    public void updateDeletedContact(ArrayList<String> arrayList) {

        ArrayList<Map<String, Object>> allContactDetails = db.fetchAllContacts(allContactsDocId);
        Map<String, Object> localContact;
        JSONObject obj;
        JSONArray arr = new JSONArray();


        for (int i = 0; i < allContactDetails.size(); i++) {
            localContact = allContactDetails.get(i);


            if (arrayList.contains(localContact.get("contactId")))

            {


                obj = new JSONObject();
                try {

                    obj.put("number", localContact.get("phoneNumber"));

                    obj.put("type", localContact.get("type"));

                } catch (JSONException e) {

                }

                arr.put(obj);


            }
        }
        if (arr.length() > 0) {


            try {
                obj = new JSONObject();


                obj.put("contacts", arr);


                hitDeleteContactApi(obj);
            } catch (JSONException e) {

            }
        }

    }


    /**
     * If a new contact has been added
     */

    public void putUpdatedContact(ArrayList<Map<String, Object>> arrayList) {

        ArrayList<Map<String, Object>> allContactDetails = db.fetchAllContacts(allContactsDocId);


        Map<String, Object> localContact;
        JSONObject obj;
        JSONArray arr = new JSONArray();


        boolean contactAlreadyExists;


        Map<String, Object> contactChanged;


        for (int j = 0; j < arrayList.size(); j++) {


            contactAlreadyExists = false;

            contactChanged = arrayList.get(j);


            for (int i = 0; i < allContactDetails.size(); i++) {
                localContact = allContactDetails.get(i);

                /*
                 * Considering only change of the phone number and the name in the contacts list
                 */


                if ((contactChanged.get("contactId")).equals(localContact.get("contactId"))) {
                    contactAlreadyExists = true;


                    if (!((contactChanged.get("phoneNumber")).equals(localContact.get("phoneNumber")))) {

                        /*
                         * This means phone number has been changed,so will need to send to the server
                         */

                        obj = new JSONObject();
                        try {

                            obj.put("number", contactChanged.get("phoneNumber"));

                            obj.put("type", contactChanged.get("type"));

                        } catch (JSONException e) {

                        }
                        arr.put(obj);


                        Map<String, Object> wasActiveContact = db.wasActiveContact(contactsDocId, (String) contactChanged.get("contactId"));


                        if ((boolean) wasActiveContact.get("wasActive")) {
                            /*
                             * Follow whose number has been changed was on active contacts list
                             */

                            Map<String, Object> dirtyContact = new HashMap<>();
                            dirtyContact.put("contactUid", wasActiveContact.get("contactUid"));
                            dirtyContact.put("contactNumber", contactChanged.get("phoneNumber"));
                            dirtyContact.put("contactIdentifier", wasActiveContact.get("contactIdentifier"));


                            dirtyContact.put("contactId", contactChanged.get("contactId"));

                            dirtyContact.put("contactName", contactChanged.get("userName"));

                            dirtyContacts.add(dirtyContact);
                        } else {
                            /*
                             * Follow was in list of the inactive contacts
                             */

                            Map<String, Object> inactiveContact = new HashMap<>();
                            inactiveContact.put("contactUid", wasActiveContact.get("contactUid"));
                            inactiveContact.put("phoneNumber", contactChanged.get("phoneNumber"));
                            inactiveContact.put("contactIdentifier", wasActiveContact.get("contactIdentifier"));


                            inactiveContact.put("contactId", contactChanged.get("contactId"));

                            inactiveContact.put("userName", contactChanged.get("userName"));

                            inactiveContacts.add(inactiveContact);
                        }
                        db.updateAllContactNunber(allContactsDocId, (String) contactChanged.get("phoneNumber"),
                                (String) contactChanged.get("contactId"));
                    }

                    if (!((contactChanged.get("userName")).equals(localContact.get("userName")))) {



                        /*
                         * If just the name changed,then update the value in the local contacts,calls and chat list but no need to send to the server
                         */

                        try {


                            Map<String, Object> result = db.updateActiveContactName(contactsDocId, (String) contactChanged.get("userName"),
                                    (String) contactChanged.get("contactId"));

                            if ((boolean) result.get("updated")) {

                                /*
                                 * If the contact exists in list of active users
                                 */


                                JSONObject jsonObject = new JSONObject();

                                jsonObject.put("eventName", "ContactNameUpdated");
                                jsonObject.put("contactName", contactChanged.get("userName"));
                                jsonObject.put("contactUid", result.get("contactUid"));
                                bus.post(jsonObject);
                            }


                            /*
                             * To update the list of all contacts for the new name
                             */
                            db.updateAllContactName(allContactsDocId,
                                    (String) contactChanged.get("userName"), (String) contactChanged.get("contactId"));

                        } catch (JSONException e) {

                        }

                    }

                }
            }


            if (!contactAlreadyExists) {


                obj = new JSONObject();
                try {

                    obj.put("number", contactChanged.get("phoneNumber"));

                    obj.put("type", contactChanged.get("type"));


                    newContacts.add(contactChanged);
                } catch (JSONException e) {

                }
                arr.put(obj);


                db.addToAllContacts(allContactsDocId, contactChanged);


            }

        }


        if (arr.length() > 0)

        {


            try {
                obj = new JSONObject();


                obj.put("contacts", arr);


                hitContactAddedApi(obj);
            } catch (JSONException e) {

            }
        }

    }


    private void hitDeleteContactApi(final JSONObject obj) {


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.DELETE,
                ApiOnServer.SYNC_CONTACTS, null, new com.android.volley.Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }


        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");


                headers.put("contacts", obj.toString());

                headers.put("token", apiToken);

                return headers;
            }
        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        /* Add the request to the RequestQueue.*/
        AppController.getInstance().addToRequestQueue(jsonObjReq, "deleteContactsApiRequest");

    }


    private void hitContactAddedApi(JSONObject obj) {

        pendingContactUpdateRequests++;


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                ApiOnServer.SYNC_CONTACTS, obj, new com.android.volley.Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }


        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");


                headers.put("token", apiToken);


                return headers;
            }
        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        /* Add the request to the RequestQueue.*/
        AppController.getInstance().addToRequestQueue(jsonObjReq, "putContactsApiRequest");

    }

    /**
     * Convert image or video or audio to byte[] so that it can be sent on socket(Unsetn messages)
     */
    @SuppressWarnings("TryWithIdenticalCatches")

    private static byte[] convertFileToByteArray(File f) {
        byte[] byteArray = null;
        byte[] b;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {

            InputStream inputStream = new FileInputStream(f);
            b = new byte[2663];

            int bytesRead;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        } catch (IOException e) {

        } catch (OutOfMemoryError e) {

        } finally {
            b = null;

            try {
                bos.close();

            } catch (IOException e) {

            }

        }

        return byteArray;
    }

    /**
     * To find the document id of the receiver on receipt of new message,if exists or create a new document for chat with that receiver and return its document id
     */
    @SuppressWarnings("unchecked")
    public static String findDocumentIdOfReceiver(String receiverUid, String timestamp, String receiverName,
                                                  String receiverImage, String secretId, boolean invited,
                                                  String receiverIdentifier, String chatId, boolean groupChat)


    {

        CouchDbController db = AppController.getInstance().getDbController();
        Map<String, Object> chatDetails = db.getAllChatDetails(AppController.getInstance().getChatDocId());

        if (chatDetails != null) {

            ArrayList<String> receiverUidArray = (ArrayList<String>) chatDetails.get("receiverUidArray");


            ArrayList<String> receiverDocIdArray = (ArrayList<String>) chatDetails.get("receiverDocIdArray");


            ArrayList<String> secretIdArray = (ArrayList<String>) chatDetails.get("secretIdArray");

            for (int i = 0; i < receiverUidArray.size(); i++) {


                if (receiverUidArray.get(i).equals(receiverUid) && secretIdArray.get(i).equals(secretId)) {

                    return receiverDocIdArray.get(i);

                }

            }
        }


        /*  here we also need to enter receiver name*/


        String docId = db.createDocumentForChat(timestamp, receiverUid, receiverName, receiverImage, secretId, invited,
                receiverIdentifier, chatId, groupChat);


        db.addChatDocumentDetails(receiverUid, docId, AppController.getInstance().getChatDocId(), secretId);

        return docId;
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private class FetchTime extends AsyncTask {


        @Override
        protected Object doInBackground(Object[] params) {

            sharedPref.edit().putBoolean("deltaRequired", true).apply();

            String url_ping = "https://google.com/";
            URL url = null;
            try {
                url = new URL(url_ping);

            } catch (MalformedURLException e) {

            }

            try {
                /*
                 * Maybe inaccurate due to network inaccuracy
                 */

                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();


                if (urlc.getResponseCode() == 200 || urlc.getResponseCode() == 503) {
                    long dateStr = urlc.getDate();
                    //Here I do something with the Date String

                    timeDelta = System.currentTimeMillis() - dateStr;


                    sharedPref.edit().putBoolean("deltaRequired", false).apply();
                    sharedPref.edit().putLong("timeDelta", timeDelta).apply();
                    urlc.disconnect();
                }
            } catch (IOException e) {


                /*
                 * Should disable user from using the app
                 */


            } catch (NullPointerException e) {

            }
            return null;
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void checkContactAddedOrUpdated() {

        ArrayList<Map<String, Object>> localContactsList = new ArrayList<>();


        ArrayList<String> alreadyAddedContacts = new ArrayList<>();

        Uri uri = ContactsContract.Contacts.CONTENT_URI;

        /*
         * For added or updated contact
         */
        String time = String.valueOf(Utilities.getGmtEpoch());
        String lastChecked = AppController.getInstance().getSharedPreferences().getString("lastUpdated", time);
        try {

            Cursor cur
                    = AppController.getInstance().getContentResolver().query(uri,
                    null,
                    ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + ">=?",
                    new String[]{lastChecked},
                    null
            );


            Map<String, Object> contactsInfo;
            if (cur != null && cur.getCount() > 0) {


                int type;

                while (cur.moveToNext()) {


                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));


                    String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));


                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);

                        if (pCur != null) {


                            while (pCur.moveToNext()) {


                                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                /*
                                 * To replace brackets or the dashes or the spaces
                                 *
                                 * */

                                phoneNo = phoneNo.replaceAll("[\\D\\s\\-()]", "");

                                /*
                                 * To get all the phone numbers for the given contacts
                                 */
                                if (phoneNo != null && !phoneNo.trim().equals("null") && !phoneNo.trim().isEmpty()) {


                                    /*
                                     * By default assuming the number is of unknown type
                                     */

                                    type = -1;
                                    switch (pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))) {
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                            type = 1;
                                            break;
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                            type = 0;
                                            break;
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
                                            type = 8;

                                    }


                                    if (!alreadyAddedContacts.contains(phoneNo)) {

                                        alreadyAddedContacts.add(phoneNo);
                                        contactsInfo = new HashMap<>();

                                        contactsInfo.put("phoneNumber", "+" + phoneNo);
                                        contactsInfo.put("userName", name);
                                        contactsInfo.put("type", type);
                                        contactsInfo.put("contactId", id);


                                        localContactsList.add(contactsInfo);


                                    }
                                }

                            }

                            pCur.close();
                        }
                    }
                }

                if (localContactsList.size() > 0)
                    putUpdatedContact(localContactsList);
            }

            if (cur != null) {
                cur.close();
            }

        } catch (SQLiteException e) {
            e.printStackTrace();

            generatePushForContactUpdate();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void checkContactDeleted() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Uri uri = ContactsContract.DeletedContacts.CONTENT_URI;

            /*
             * For deleted contact
             */
            String time = String.valueOf(Utilities.getGmtEpoch());
            String lastChecked = AppController.getInstance().getSharedPreferences().getString("lastDeleted", time);


            Cursor cur
                    = getContentResolver().query(uri,
                    null,
                    ContactsContract.DeletedContacts.CONTACT_DELETED_TIMESTAMP + ">=?",
                    new String[]{lastChecked},
                    null
            );


            if (cur != null && cur.getCount() > 0) {


                ArrayList<String> deletedContactIds = new ArrayList<>();

                while (cur.moveToNext()) {


                    String id = cur.getString(cur.getColumnIndex(ContactsContract.DeletedContacts.CONTACT_ID));


                    deletedContactIds.add(id);


                }

                /*
                 * Have to tell the server of the contact being deleted and also to remove from the local list
                 */


                updateDeletedContact(deletedContactIds);


            }

            if (cur != null) {
                cur.close();
            }
        }
    }


//    private class startTimer extends AsyncTask<String, Void, Void> {
//
//
//        /**
//         * Is giving HTTP com.couchbase.lite.CouchbaseLiteException, Status: 409 (HTTP 409 conflict)
//         * <p>
//         * <p>
//         * Will have to check later
//         */
//
//        @Override
//        protected Void doInBackground(String... params) {
//
//            try {
//                db.setTimerStarted(params[0], params[1], Long.parseLong(params[2]));
//
//
//            } catch (Exception e) {
//                //
//            }
//
//            return null;
//        }
//    }


    private String findNewlyJoinedContactName(String number, String userId) {


        /*
         * Have to find the local contact name for the given number
         */

        number = number.substring(1);
        ArrayList<Map<String, Object>> arr = db.fetchAllContacts(allContactsDocId);
        Map<String, Object> contact;

        String localNumber = "", contactId = "", contactName = "";
        int contactType = 1;
        int diff = 65536;


        for (int i = 0; i < arr.size(); i++) {

            contact = arr.get(i);


            if (number.contains(((String) contact.get("phoneNumber")).substring(1))) {


                if (number.length() - ((String) contact.get("phoneNumber")).length() < diff) {

                    diff = number.length() - ((String) contact.get("phoneNumber")).length();

                    localNumber = ((String) contact.get("phoneNumber"));
                    contactId = ((String) contact.get("contactId"));
                    contactName = ((String) contact.get("userName"));

                    contactType = ((int) contact.get("type"));
                }


            }


        }


        if (!contactName.isEmpty()) {
            /*
             * To save a contact
             */
            Map<String, Object> contactMap = new HashMap<>();

            contactMap.put("contactLocalNumber", localNumber);
            contactMap.put("contactLocalId", contactId);
            contactMap.put("contactUid", userId);


            contactMap.put("contactType", contactType);


            contactMap.put("contactName", contactName);
            contactMap.put("contactStatus", getString(R.string.default_status));
            contactMap.put("contactPicUrl", "");
            contactMap.put("contactIdentifier", number);


            db.addActiveContact(contactsDocId, contactMap, userId);
        }
        return contactName;

    }

    /**
     * To fetch a particular notification info
     */
    public Map<String, Object> fetchNotificationInfo(String notificationId) {

        for (int i = 0; i < notifications.size(); i++) {

            if (notifications.get(i).get("notificationId").equals(notificationId)) {

                return notifications.get(i);
            }
        }
        return null;
    }

    /**
     * To add or update the content of the notifications
     */
    public void addOrUpdateNotification(Map<String, Object> notification, String notificationId) {

        db.addOrUpdateNotificationContent(notificationDocId, notificationId, notification);


        for (int i = 0; i < notifications.size(); i++) {

            if (notifications.get(i).get("notificationId").equals(notificationId)) {
                notifications.set(i, notification);
                return;
            }
        }


        notifications.add(0, notification);
    }


    /*
     * To remove a particular notification
     */

    public void removeNotification(String notificationId) {


        boolean found = false;
        for (int i = 0; i < notifications.size(); i++) {

            if (notifications.get(i).get("notificationId").equals(notificationId)) {
                notifications.remove(i);
                found = true;
                break;
            }
        }

        if (found) {


            //  db.getParticularNotificationId(notificationDocId, notificationId);

            int systemNotificationId = db.removeNotification(notificationDocId, notificationId);
            if (systemNotificationId != -1) {

                NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


                nMgr.cancel(notificationId, systemNotificationId);
            }
        }


    }


    /**
     * To add the group chat message into the db
     */
    private void addGroupChatMessageInDB(String docId, Map<String, Object> groupMessageDetails, String tsInGmt, String message, boolean newMessage) {


        db.addNewChatMessageAndSort(docId, groupMessageDetails, tsInGmt);


        if (newMessage) {
            db.updateChatListForNewMessage(docId, message, true, tsInGmt, tsInGmt);
        }

    }


    @SuppressWarnings("unchecked")
    public void publishGroupChatMessage(String groupMembersDocId, JSONObject messageObject, HashMap<String, Object> map) {
        /*
         * IMQtt delivery token of only the last message is stored to decide if message has succesfully delivered to all members in group
         */


        IMqttDeliveryToken token = null;


        try {

            Map<String, Object> groupInfo = db.fetchGroupInfo(groupMembersDocId);





            /*
             * Have to only emit,if current user is the active member of the group
             */


            if ((boolean) groupInfo.get("isActive")) {

                ArrayList<Map<String, Object>> groupMembers = (ArrayList<Map<String, Object>>) groupInfo.get("groupMembersArray");




                /*
                 * Have to add the functionality for the active flag ,so can check b4 emitting a message if current user is member of group or not
                 *
                 */


                for (int i = 0; i < groupMembers.size(); i++) {

                    if (!groupMembers.get(i).get("memberId").equals(userId)) {


                        /*
                         * Since no need to emit to the group messages topic for the sender itself
                         */


                        if (mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
                            try {


                                token = mqttAndroidClient.publish
                                        (MqttEvents.GroupChats.value + "/" + groupMembers.get(i).get("memberId"),
                                                messageObject.toString().getBytes(), 1, false);

                            } catch (MqttException e) {

                            }
                        }


                    }


                }
                /*
                 * To allow logging onto the server
                 */
                if (mqttAndroidClient != null && mqttAndroidClient.isConnected()) {


                    messageObject.put("totalMembers", String.valueOf(groupMembers.size()));


                    mqttAndroidClient.publish(MqttEvents.GroupChatMessages.value + "/" + messageObject.getString("to"),
                            messageObject.toString().getBytes(), 1, false);


                }


                if (token != null) {


                    /*
                     * Have been successfully emitted
                     */

                    map.put("MQttToken", token);

                    tokenMapping.add(map);


                    set.add(token);

                }
            }

        } catch (Exception e) {
            /*
             * To avoid the case of group being deleted while still having message to be sent
             */


        }
    }


    /*
     *To save the group chat messages
     */


    private void putGroupMessageInDb(String receiverUid, String messageType, String actualMessage,
                                     String timestamp, String id, String receiverdocId, String thumbnailMessage,


                                     int dataSize, String receiverIdentifier, boolean isSelf, int status, String removedAt, boolean wasEdited) {


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);

        byte[] thumbnailData = null;


        if ((messageType.equals("1")) || (messageType.equals("2")) || (messageType.equals("7"))) {

            thumbnailData = Base64.decode(thumbnailMessage, Base64.DEFAULT);


        }


        String name = timestamp;
        /*
         *
         *
         * initially in db we only store path of the thumbnail and nce downloaded we will replace that field withthe actual path of the downloaded file
         *
         *
         * */

        String tsInGmt = Utilities.epochtoGmt(timestamp);

        /*
         * Text message
         */
        if (messageType.equals("0")) {

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "0");


            if (wasEdited)

                map.put("wasEdited", true);

            map.put("isSelf", isSelf);
            map.put("from", receiverUid);

            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);


            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }


            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        } else if (messageType.equals("1")) {
            /*
             * Image message
             */

            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");

            Map<String, Object> map = new HashMap<>();


            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }

            map.put("messageType", "1");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);

            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);


            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);

            map.put("dataSize", dataSize);


            map.put("thumbnailPath", thumbnailPath);
            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }


            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewImage), true, tsInGmt, tsInGmt);

            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        } else if (messageType.equals("2")) {

            /*
             * Video message
             */
            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


            Map<String, Object> map = new HashMap<>();


            /*
             *
             *
             * message key will contail the url on server until downloaded and once downloaded
             * it will contain the local path of the video or image
             *
             * */
            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }


            map.put("messageType", "2");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);

            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);
            map.put("dataSize", dataSize);

            map.put("thumbnailPath", thumbnailPath);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }


            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewVideo), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("3")) {
            /*
             * Location message
             */

            String placeString = "";
            try {

                placeString = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", placeString);
            map.put("messageType", "3");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);

            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }


            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewLocation), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("4")) {
            /*
             * Follow message
             */

            String contactString = "";
            try {

                contactString = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", contactString);
            map.put("messageType", "4");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);


            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);

            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewContact), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("5")) {

            /*
             * Audio message
             */
            Map<String, Object> map = new HashMap<>();


            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }


            map.put("messageType", "5");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);

            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);

            map.put("Ts", tsInGmt);
            map.put("id", id);
            map.put("downloadStatus", 0);
            map.put("dataSize", dataSize);


            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }


            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewAudio), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("6")) {


            /*
             * Sticker
             */

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }
            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "6");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);

            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);


            map.put("Ts", tsInGmt);
            map.put("id", id);


            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }


            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);

                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewSticker), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("7")) {
            /*
             * Doodle
             */
            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


            Map<String, Object> map = new HashMap<>();

            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }

            map.put("messageType", "7");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);

            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);


            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);

            map.put("dataSize", dataSize);
            map.put("thumbnailPath", thumbnailPath);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }

            thumbnailPath = null;

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDoodle), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("8")) {

            /*
             * Gif
             */


            String url = "";
            try {

                url = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }
            Map<String, Object> map = new HashMap<>();
            map.put("message", url);
            map.put("messageType", "8");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);

            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);


            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }


            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewGiphy), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }

        }

        /*
         * Removed message
         */
        else if (messageType.equals("11")) {

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "11");


            map.put("isSelf", isSelf);
            map.put("from", receiverUid);

            map.put("removedAt", removedAt);




            /*
             * Group chat specific
             */
            map.put("receiverIdentifier", receiverIdentifier);


            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }


            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        }


    }


    private void putGroupMessageInDb(String receiverUid, String actualMessage,
                                     String timestamp, String id, String receiverdocId,


                                     int dataSize, boolean isSelf, int status,
                                     String mimeType, String fileName, String extension, String receiverIdentifier) {


        /*
         * For saving the document received
         */


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);


        String tsInGmt = Utilities.epochtoGmt(timestamp);


        /*
         * document message
         */


        Map<String, Object> map = new HashMap<>();


        try {

            map.put("message", new String(data, "UTF-8"));


        } catch (UnsupportedEncodingException e) {

        }

        map.put("messageType", "9");
        map.put("isSelf", isSelf);
        map.put("from", receiverUid);


        /*
         * Group chat specific
         */
        map.put("receiverIdentifier", receiverIdentifier);

        map.put("Ts", tsInGmt);
        map.put("id", id);


        map.put("fileName", fileName);
        map.put("mimeType", mimeType);
        map.put("extension", extension);

        map.put("downloadStatus", 0);

        map.put("dataSize", dataSize);


        if (isSelf) {


            map.put("deliveryStatus", String.valueOf(status));


        }


        if (status == -1) {
            db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
            db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDocument), true, tsInGmt, tsInGmt);

        } else {


            db.addNewChatMessageAndSort(receiverdocId, map, null);

        }


    }


    private void putGroupPostMessageInDb(String receiverUid, String actualMessage,
                                         String timestamp, String id, String receiverdocId,


                                         boolean isSelf, int status,
                                         String mimeType, String fileName, String extension, String receiverIdentifier, String postId, String postTitle, int postType) {


        /*
         * For saving the document received
         */


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);


        String tsInGmt = Utilities.epochtoGmt(timestamp);


        /*
         * document message
         */


        Map<String, Object> map = new HashMap<>();


        try {

            map.put("message", new String(data, "UTF-8"));


        } catch (UnsupportedEncodingException e) {

        }

        map.put("messageType", "13");
        map.put("isSelf", isSelf);
        map.put("from", receiverUid);


        /*
         * Group chat specific
         */
        map.put("receiverIdentifier", receiverIdentifier);

        map.put("Ts", tsInGmt);
        map.put("id", id);


        map.put("postId", postId);
        map.put("postTitle", postTitle);
        map.put("postType", postType);


        if (isSelf) {


            map.put("deliveryStatus", String.valueOf(status));


        }


        if (status == -1) {
            db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
            db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewPost), true, tsInGmt, tsInGmt);

        } else {


            db.addNewChatMessageAndSort(receiverdocId, map, null);

        }


    }


    private void putGroupReplyMessageInDb(String receiverUid, String actualMessage,
                                          String timestamp, String id, String receiverdocId,


                                          int dataSize, boolean isSelf, int status,
                                          String mimeType, String fileName, String extension,
                                          String thumbnail, String receiverIdentifier, String replyType, String previousReceiverIdentifier, String previousFrom,

                                          String previousPayload, String previousType, String

                                                  previousId, String previousFileType, boolean wasEdited, String postId, String postTitle, int postType)

    {


        String name = timestamp;
        Map<String, Object> map = new HashMap<>();
        map.put("messageType", "10");
        map.put("previousReceiverIdentifier", previousReceiverIdentifier);

        map.put("previousFrom", previousFrom);


        map.put("previousType", previousType);

        map.put("previousId", previousId);


        if (previousType.equals("9")) {

            map.put("previousFileType", previousFileType);

            map.put("previousPayload", previousPayload);
        } else if (previousType.equals("1") || previousType.equals("2") || previousType.equals("7"))

        {


            map.put("previousPayload", convertByteArrayToFile(Base64.decode(previousPayload, Base64.DEFAULT), previousId, "jpg"));
        } else {

            map.put("previousPayload", previousPayload);

        }



        /*
         * For saving the reply message received
         */

        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);

        byte[] thumbnailData = null;


        if ((replyType.equals("1")) || (replyType.equals("2")) || (replyType.equals("7"))) {


            thumbnailData = Base64.decode(thumbnail, Base64.DEFAULT);


        }



        /*
         *
         *
         * Initially in db we only store path of the thumbnail and nce downloaded we will replace that field withthe actual path of the downloaded file
         *
         *
         * */

        String tsInGmt = Utilities.epochtoGmt(timestamp);
        switch (Integer.parseInt(replyType)) {


            case 0: {
                /*
                 * Text message
                 */
                String text = "";
                try {

                    text = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                map.put("message", text);
                map.put("replyType", "0");
                if (wasEdited)
                    map.put("wasEdited", true);


                map.put("isSelf", isSelf);
                map.put("from", receiverUid);

                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);

                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }
                break;
            }

            case 1: {
                /*
                 * Image
                 */

                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "1");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);


                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);

                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);


                map.put("thumbnailPath", thumbnailPath);
                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewImage), true, tsInGmt, tsInGmt);

                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }
                break;

            }
            case 2: {
                /*
                 * Video
                 */

                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");





                /*
                 *
                 *
                 * message key will contail the url on server until downloaded and once downloaded
                 * it will contain the local path of the video or image
                 *
                 * */
                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }


                map.put("replyType", "2");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);


                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);

                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);
                map.put("dataSize", dataSize);

                map.put("thumbnailPath", thumbnailPath);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewVideo), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 3: {
                /*
                 * Location
                 */

                String placeString = "";
                try {

                    placeString = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                map.put("message", placeString);
                map.put("replyType", "3");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);


                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);

                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewLocation), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 4: {
                /*
                 * Follow
                 */
                String contactString = "";
                try {

                    contactString = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                map.put("message", contactString);
                map.put("replyType", "4");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);



                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);

                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewContact), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }

                break;

            }
            case 5: {
                /*
                 * Audio
                 */


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }


                map.put("replyType", "5");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);

                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);


                map.put("Ts", tsInGmt);
                map.put("id", id);
                map.put("downloadStatus", 0);
                map.put("dataSize", dataSize);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewAudio), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 6: {
                /*
                 * Sticker
                 */

                String text = "";
                try {

                    text = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }

                map.put("message", text);
                map.put("replyType", "6");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);

                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);


                map.put("Ts", tsInGmt);
                map.put("id", id);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);

                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewSticker), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }

            case 7: {
                /*
                 * Doodle
                 */


                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "7");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);


                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);


                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);
                map.put("thumbnailPath", thumbnailPath);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }

                thumbnailPath = null;

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDoodle), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }


                break;

            }


            case 8: {
                /*
                 * Gif
                 */

                String url = "";
                try {

                    url = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }

                map.put("message", url);
                map.put("replyType", "8");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);

                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);

                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewGiphy), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 9: {



                /*
                 * document message
                 */


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "9");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);

                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);

                map.put("Ts", tsInGmt);
                map.put("id", id);


                map.put("fileName", fileName);
                map.put("mimeType", mimeType);
                map.put("extension", extension);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDocument), true, tsInGmt, tsInGmt);

                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }

                break;
            }

            case 13: {
                /*
                 * Post
                 */

                String url = "";
                try {

                    url = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                map.put("message", url);
                map.put("replyType", "13");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("postId", postId);
                map.put("postTitle", postTitle);
                map.put("postType", postType);


                /*
                 * Group chat specific
                 */
                map.put("receiverIdentifier", receiverIdentifier);

                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }


                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewPost), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
        }


    }


    @SuppressWarnings("TryWithIdenticalCatches")
    private class requestGroupMembersOnBackgroundThread extends AsyncTask {


        @Override
        protected Object doInBackground(Object[] params) {

            requestGroupMembers((JSONObject) params[0]);
            return null;
        }
    }


    private void requestGroupMembers(final JSONObject messageReceived) {


        /*
         * Have to fetch the member details for the group in which message has been received,but it emmbers doesn't exist locally as of now
         */

        try {
            final String groupId = messageReceived.getString("groupId");

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    ApiOnServer.GROUP_MEMBER + "/" + groupId, null,
                    new com.android.volley.Response.Listener<JSONObject>() {


                        @Override
                        public void onResponse(final JSONObject response) {


                            try {


                                if (response.getInt("code") == 200) {
                                    updateGroupMembersFromApi(response.getJSONObject("data"), groupId, messageReceived);

                                }


                            } catch (JSONException e) {

                            }


                        }
                    }, new com.android.volley.Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {


                }
            }


            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");


                    headers.put("token", AppController.getInstance().getApiToken());


                    return headers;
                }
            };


            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    20 * 1000, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            /* Add the request to the RequestQueue.*/
            AppController.getInstance().addToRequestQueue(jsonObjReq, "fetchMembersApiRequest");

        } catch (JSONException e) {

        }
    }

    /**
     * @param groupData JSONObject containing the group  data which included member details and created by whom and created at details
     */
    @SuppressWarnings("unchecked")
    private void updateGroupMembersFromApi(JSONObject groupData, String groupId, final JSONObject messageReceived) {
        try {

            try {


                JSONArray members = groupData.getJSONArray("members");
                JSONObject member;


                Map<String, Object> memberMap;


                boolean isActive = false;
                String userId = AppController.getInstance().getUserId();
                ArrayList<Map<String, Object>> membersArray = new ArrayList<>();

                /*
                 *To avoid creation of multiple documents for the group members corresponding to the single group chat
                 */

                String groupMembersDocId = db.createGroupMembersDocument();
                db.addGroupChat(AppController.getInstance().getGroupChatsDocId(), groupId, groupMembersDocId);

                for (int i = 0; i < members.length(); i++) {


                    member = members.getJSONObject(i);
                    memberMap = new HashMap<>();

                    memberMap.put("memberId", member.getString("userId"));

                    if (userId.equals(member.getString("userId"))) {

                        isActive = true;
                    }

                    memberMap.put("memberIdentifier", member.getString("userIdentifier"));

                    if (member.has("profilePic"))

                    {
                        memberMap.put("memberImage", member.getString("profilePic"));
                    } else {

                        memberMap.put("memberImage", "");
                    }


                    if (member.has("socialStatus"))

                    {
                        memberMap.put("memberStatus", member.getString("socialStatus"));
                    } else {

                        memberMap.put("memberStatus", getString(R.string.default_status));
                    }

                    memberMap.put("memberIsAdmin", member.getBoolean("isAdmin"));
                    membersArray.add(memberMap);


                }


                AppController.getInstance().getDbController().addGroupMembersDetails(groupMembersDocId, membersArray,
                        groupData.getString("createdByMemberId"),
                        groupData.getString("createdByMemberIdentifier"), groupData.getLong("createdAt"), isActive);

            } catch (JSONException e) {

            }


            /*
             * Have been intentionally put on the ui thread
             */
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {


                    processGroupMessageReceived(messageReceived);
                }
            });


        } catch (Exception e) {

        }
    }

    /**
     * @param obj group message that has been received
     */


    @SuppressWarnings("unchecked")
    private void processGroupMessageReceived(JSONObject obj) {
        /*
         * Actual message being exchanged in the group
         */


        String topic = MqttEvents.GroupChats.value + "/" + userId;

        try {
            if (obj.has("payload")) {

                /*
                 * For an actual message(Like text,image,video etc.) received
                 */


                /*
                 * Sender details
                 */
                String receiverUid = obj.getString("from");
                String receiverIdentifier = obj.getString("receiverIdentifier");


                String groupId = obj.getString("to");


                String messageType = obj.getString("type");
                String actualMessage = obj.getString("payload").trim();
                String timestamp = obj.getString("timestamp");


                String id = obj.getString("id");

                String mimeType = "", fileName = "", extension = "";
                String postTitle = "", postId = "";
                int postType = -1;

                int dataSize = -1;

                if (messageType.equals("1") || messageType.equals("2") || messageType.equals("5") || messageType.equals("7")
                        || messageType.equals("9")) {
                    dataSize = obj.getInt("dataSize");


                    if (messageType.equals("9")) {


                        mimeType = obj.getString("mimeType");
                        fileName = obj.getString("fileName");
                        extension = obj.getString("extension");


                    }


                } else if (messageType.equals("10")) {

                    String replyType = obj.getString("replyType");

                    if (replyType.equals("1") || replyType.equals("2") || replyType.equals("5") || replyType.equals("7")
                            || replyType.equals("9")) {
                        dataSize = obj.getInt("dataSize");


                        if (replyType.equals("9")) {


                            mimeType = obj.getString("mimeType");
                            fileName = obj.getString("fileName");
                            extension = obj.getString("extension");


                        } else if (replyType.equals("13")) {


                            postType = obj.getInt("postType");
                            postId = obj.getString("postId");
                            postTitle = obj.getString("postTitle");


                        }

                    }
                } else if (messageType.equals("13")) {


                    postType = obj.getInt("postType");
                    postId = obj.getString("postId");
                    postTitle = obj.getString("postTitle");


                }


                String receiverName = obj.getString("name");
                String userImage = obj.getString("userImage");


                String documentId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");
                if (documentId.isEmpty()) {
                    /*
                     * Here, chatId is essentially put as empty,although it is same as groupId
                     */
                    documentId = findDocumentIdOfReceiver(groupId, Utilities.tsInGmt(),
                            receiverName, userImage, "", false, receiverIdentifier, "", true);
                }



                /*
                 * For callback in to activity to update UI
                 */


                if (!db.checkAlreadyExists(documentId, id)) {

                    String replyType = "";

                    if (messageType.equals("1") || messageType.equals("2") || messageType.equals("7")) {


                        AppController.getInstance().putGroupMessageInDb(receiverUid, messageType,
                                actualMessage, timestamp, id, documentId, obj.getString("thumbnail").trim(),
                                dataSize, receiverIdentifier, false, -1, null, false);
                    } else if (messageType.equals("9")) {
                        /*
                         * For document received
                         */

                        AppController.getInstance().putGroupMessageInDb(receiverUid,
                                actualMessage, timestamp, id, documentId, dataSize,
                                false, -1, mimeType, fileName, extension, receiverIdentifier);


                    } else if (messageType.equals("13")) {
                        /*
                         * For post received
                         */

                        AppController.getInstance().putGroupPostMessageInDb(receiverUid,
                                actualMessage, timestamp, id, documentId,
                                false, -1, mimeType, fileName, extension, receiverIdentifier, postId, postTitle, postType);


                    } else if (messageType.equals("10")) {

                        /*
                         * For the reply message received
                         */

                        replyType = obj.getString("replyType");

                        String previousMessageType = obj.getString("previousType");

                        String previousFileType = "", thumbnail = "";


                        if (previousMessageType.equals("9")) {

                            previousFileType = obj.getString("previousFileType");

                        }
                        if (replyType.equals("1") || replyType.equals("2") || replyType.equals("7")) {


                            thumbnail = obj.getString("thumbnail").trim();

                        }
                        AppController.getInstance().putGroupReplyMessageInDb(receiverUid,
                                actualMessage, timestamp, id, documentId,
                                dataSize, false, -1, mimeType, fileName, extension, thumbnail, receiverIdentifier,
                                replyType, obj.getString("previousReceiverIdentifier"), obj.getString("previousFrom"),
                                obj.getString("previousPayload"), obj.getString("previousType"), obj.getString("previousId"), previousFileType, obj.has("wasEdited"), postId, postTitle, postType);


                    } else if (messageType.equals("11")) {

                        /*
                         * For message removal
                         */
                        db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("removedAt")));
                        db.markMessageAsRemoved(documentId, id, removedMessageString, Utilities.epochtoGmt(obj.getString("removedAt")));
                        try {

                            obj.put("eventName", topic);


                            obj.put("groupId", groupId);
                            bus.post(obj);

                        } catch (Exception e) {

                        }
                        return;
                    } else if (messageType.equals("12")) {

                        /*
                         * For message edit
                         */


                        db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("editedAt")));
                        db.markMessageAsEdited(documentId, id, actualMessage);
                        try {

                            obj.put("eventName", topic);


                            obj.put("groupId", groupId);
                            bus.post(obj);

                        } catch (Exception e) {

                        }
                        return;
                    } else {

                        AppController.getInstance().putGroupMessageInDb(receiverUid,
                                messageType, actualMessage, timestamp, id, documentId, null, dataSize, receiverIdentifier, false, -1, null, obj.has("wasEdited"));


                    }

                    JSONObject obj2 = new JSONObject();
                    obj2.put("from", AppController.getInstance().userId);
                    obj2.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));
                    obj2.put("to", receiverUid);
                    obj2.put("status", "2");


                    AppController.getInstance().publish(MqttEvents.GroupChatAcks.value + "/" + receiverUid, obj2, 2, false);


                    /*
                     * For callback in to activity to update UI
                     */


                    try {

                        obj.put("eventName", topic);


                        obj.put("groupId", groupId);
                        bus.post(obj);

                    } catch (Exception e) {

                    }


                    if (contactSynced) {
                        if (!db.checkIfReceiverChatMuted(mutedDocId, groupId, "")) {
                            Intent intent = new Intent(mInstance, GroupChatMessageScreen.class);


                            intent.putExtra("receiverUid", groupId);
                            intent.putExtra("receiverName", receiverName);

                            intent.putExtra("receiverIdentifier", receiverIdentifier);

                            intent.putExtra("documentId", documentId);


                            intent.putExtra("colorCode", AppController.getInstance().getColorCode(5));

                            intent.putExtra("receiverImage", userImage);


                            intent.putExtra("fromNotification", true);


                            /*
                             *To generate the push notification locally
                             */


                            generatePushNotificationLocal(documentId, messageType, receiverName, actualMessage, intent, -1, "", groupId, replyType);
                        }

                    }
                    int type = Integer.parseInt(messageType);
                    int replyTypeInt = -1;
                    if (!replyType.isEmpty()) {
                        replyTypeInt = Integer.parseInt(replyType);
                    }
                    if (type == 1 || type == 2 || type == 5 || type == 7 || type == 9 ||
                            (type == 10 && (replyTypeInt == 1 || replyTypeInt == 2 || replyTypeInt == 5
                                    || replyTypeInt == 7 || replyTypeInt == 9))) {
                        if (ActivityCompat.checkSelfPermission(mInstance, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED) {

//                            if (autoDownloadAllowed) {

                            if (checkWifiConnected()) {

                                Object[] params = new Object[8];
                                try {


                                    params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                } catch (UnsupportedEncodingException e) {

                                }

                                params[1] = messageType;
                                params[5] = id;
                                params[6] = documentId;
                                params[7] = groupId;
                                switch (type) {
                                    case 1:


                                    {


                                        if (sharedPref.getBoolean("wifiPhoto", false)) {

                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                            new DownloadMessage().execute(params);
                                        }
                                        break;
                                    }
                                    case 2: {

                                        if (sharedPref.getBoolean("wifiVideo", false)) {
                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                            new DownloadMessage().execute(params);
                                        }
                                        break;
                                    }
                                    case 5:

                                    {

                                        if (sharedPref.getBoolean("wifiAudio", false)) {
                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";

                                            new DownloadMessage().execute(params);
                                        }
                                        break;

                                    }
                                    case 7: {

                                        if (sharedPref.getBoolean("wifiPhoto", false)) {
                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                            new DownloadMessage().execute(params);
                                        }
                                        break;
                                    }
                                    case 9: {
                                        if (sharedPref.getBoolean("wifiDocument", false)) {
                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                    + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                            new DownloadMessage().execute(params);
                                        }
                                        break;
                                    }
                                    case 10: {


                                        params[2] = obj.getString("replyType");

                                        switch (Integer.parseInt((String) params[2])) {

                                            case 1: {


                                                if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }

                                            case 2: {

                                                if (sharedPref.getBoolean("wifiVideo", false)) {

                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 5: {

                                                if (sharedPref.getBoolean("wifiAudio", false)) {

                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                    new DownloadMessage().execute(params);

                                                }
                                                break;

                                            }
                                            case 7: {

                                                if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                    new DownloadMessage().execute(params);

                                                }


                                                break;
                                            }
                                            case 9:

                                            {

                                                if (sharedPref.getBoolean("wifiDocument", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                            + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    }

                                }


                            } else if (checkMobileDataOn()) {
                                Object[] params = new Object[8];
                                try {


                                    params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                } catch (UnsupportedEncodingException e) {

                                }

                                params[1] = messageType;
                                params[5] = id;
                                params[6] = documentId;
                                params[7] = groupId;
                                switch (type) {
                                    case 1:


                                    {


                                        if (sharedPref.getBoolean("mobilePhoto", false)) {

                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                            new DownloadMessage().execute(params);
                                        }
                                        break;
                                    }
                                    case 2: {

                                        if (sharedPref.getBoolean("mobileVideo", false)) {
                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                            new DownloadMessage().execute(params);
                                        }
                                        break;
                                    }
                                    case 5:

                                    {

                                        if (sharedPref.getBoolean("mobileAudio", false)) {
                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";

                                            new DownloadMessage().execute(params);
                                        }
                                        break;

                                    }
                                    case 7: {

                                        if (sharedPref.getBoolean("mobilePhoto", false)) {
                                            params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                    ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                            new DownloadMessage().execute(params);
                                        }
                                        break;
                                    }
                                    case 9: {
                                        if (sharedPref.getBoolean("mobileDocument", false)) {
                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                    + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                            new DownloadMessage().execute(params);
                                        }
                                        break;
                                    }
                                    case 10: {


                                        params[2] = obj.getString("replyType");

                                        switch (Integer.parseInt((String) params[2])) {

                                            case 1: {


                                                if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }

                                            case 2: {

                                                if (sharedPref.getBoolean("mobileVideo", false)) {

                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 5: {

                                                if (sharedPref.getBoolean("mobileAudio", false)) {

                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                    new DownloadMessage().execute(params);

                                                }
                                                break;

                                            }
                                            case 7: {

                                                if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                    params[3] = getFilesDir() + ApiOnServer.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            ApiOnServer.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                    new DownloadMessage().execute(params);

                                                }


                                                break;
                                            }
                                            case 9:

                                            {

                                                if (sharedPref.getBoolean("mobileDocument", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                            + ApiOnServer.CHAT_DOWNLOADS_FOLDER + fileName;


                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    }

                                }


                            }


                        }
                    }

                } else {
                    if (messageType.equals("11")) {

                        /*
                         * For message removal
                         */


                        db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("removedAt")));
                        db.markMessageAsRemoved(documentId, id, removedMessageString, Utilities.epochtoGmt(obj.getString("removedAt")));
                        try {

                            obj.put("eventName", topic);


                            obj.put("groupId", groupId);
                            bus.post(obj);

                        } catch (Exception e) {

                        }
                        return;
                    } else if (messageType.equals("12")) {

                        /*
                         * For message edit
                         */
                        db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("editedAt")));
                        db.markMessageAsEdited(documentId, id, actualMessage);
                        try {

                            obj.put("eventName", topic);


                            obj.put("groupId", groupId);
                            bus.post(obj);

                        } catch (Exception e) {

                        }
                        return;
                    }
                }

                /*
                 * For sending the acknowledgement to the server of the message in the group being delivered(Only sent for the normal messages)
                 */


            } else {


                switch (obj.getInt("type")) {
                    case 0: {
                        /*
                         * Group created
                         *
                         */


                        String groupId = obj.getString("groupId");

                        /*
                         *
                         * To check if group already exists
                         */

                        String groupMembersDocId = db.fetchGroupChatDocumentId(groupChatsDocId, groupId);
                        if (groupMembersDocId == null) {
                            /*
                             * Group doesn't exists
                             */


                            groupMembersDocId = db.createGroupMembersDocument();

                        }


                        JSONArray members = obj.getJSONArray("members");


                        ArrayList<Map<String, Object>> groupMembers = new ArrayList<Map<String, Object>>();


                        JSONObject memberObject;


                        Map<String, Object> memberMap;


                        for (int i = 0; i < members.length(); i++) {
                            memberMap = new HashMap<>();
                            try {
                                memberObject = members.getJSONObject(i);

                                memberMap.put("memberId", memberObject.getString("memberId"));
                                memberMap.put("memberIdentifier", memberObject.getString("memberIdentifier"));
                                memberMap.put("memberImage", memberObject.getString("memberImage"));
                                memberMap.put("memberStatus", memberObject.getString("memberStatus"));
                                memberMap.put("memberIsAdmin", memberObject.getBoolean("memberIsAdmin"));


                            } catch (JSONException e) {

                            }
                            groupMembers.add(memberMap);

                        }


                        db.addGroupMembersDetails(groupMembersDocId, groupMembers,
                                obj.getString("createdByMemberId"),
                                obj.getString("createdByMemberIdentifier"), obj.getLong("createdAt"), true);
                        db.addGroupChat(groupChatsDocId, groupId, groupMembersDocId);

                        String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");
                        String groupPicUrl = "";

                        if (obj.has("groupImageUrl")) {

                            groupPicUrl = obj.getString("groupImageUrl");


                        }

                        if (docId.isEmpty()) {


                            docId = findDocumentIdOfReceiver(groupId, obj.getString("timestamp"), obj.getString("groupSubject"),
                                    groupPicUrl, "", false,
                                    groupId, "", true);
                        } else {

                            db.updateGroupNameAndImage(docId, obj.getString("groupSubject"), groupPicUrl);
                        }


                        Map<String, Object> map = new HashMap<>();

                        map.put("messageType", "98");


                        map.put("isSelf", false);
                        map.put("from", groupId);
                        map.put("Ts", obj.getString("timestamp"));
                        map.put("id", obj.getString("id"));

                        map.put("type", 0);

                        map.put("groupName", obj.getString("groupSubject"));

                        map.put("initiatorId", obj.getString("initiatorId"));
                        map.put("initiatorIdentifier", obj.getString("initiatorIdentifier"));

                        map.put("deliveryStatus", "0");


                        String text, initiatorName;
                        String initiatorId = obj.getString("initiatorId");
                        if (initiatorId.equals(AppController.getInstance().getUserId())) {

                            initiatorName = getString(R.string.You);
                        } else {
                            initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                            if (initiatorName == null) {

                                initiatorName = obj.getString("initiatorIdentifier");
                            }
                        }


                        text = getString(R.string.CreatedGroup, initiatorName) + " " + obj.getString("groupSubject");

                        addGroupChatMessageInDB(docId, map, obj.getString("timestamp"), text, true);
                        obj.put("docId", docId);


                        obj.put("message", text);

                        obj.put("eventName", topic);
                        bus.post(obj);
                        break;

                    }


                    case 1:


                    {

                        /*
                         * Member added
                         */

                        String groupId = obj.getString("groupId");

                        /*
                         *
                         * To check if group already exists
                         */
                        String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");
                        String groupMembersDocId = db.fetchGroupChatDocumentId(groupChatsDocId, groupId);


                        boolean updateGroupDetailsRequired = true;


                        if (groupMembersDocId == null) {
                            /*
                             * Group doesn't exists
                             */

                            if (!obj.has("members")) {


                                new requestGroupMembersOnBackgroundThread().execute(new JSONObject[]{obj});


                                return;
                            }


                            groupMembersDocId = db.createGroupMembersDocument();


                            db.addGroupChat(groupChatsDocId, groupId, groupMembersDocId);


                            String groupPicUrl = "";

                            if (obj.has("groupImageUrl")) {

                                groupPicUrl = obj.getString("groupImageUrl");


                            }

                            if (docId.isEmpty()) {


                                docId = findDocumentIdOfReceiver(groupId, obj.getString("timestamp"), obj.getString("groupSubject"),
                                        groupPicUrl, "", false,
                                        groupId, "", true);
                            } else {

                                db.updateGroupNameAndImage(docId, obj.getString("groupSubject"), groupPicUrl);
                            }


                            updateGroupDetailsRequired = false;
                        }


                        String memberId = obj.getString("memberId");


                        if (memberId.equals(userId)) {


                            JSONArray members = obj.getJSONArray("members");


                            ArrayList<Map<String, Object>> groupMembers = new ArrayList<Map<String, Object>>();


                            JSONObject memberObject;


                            Map<String, Object> memberMap;


                            /*
                             * Current member is added for the first time
                             */


                            for (int i = 0; i < members.length(); i++) {
                                memberMap = new HashMap<>();
                                try {
                                    memberObject = members.getJSONObject(i);

                                    memberMap.put("memberId", memberObject.getString("memberId"));
                                    memberMap.put("memberIdentifier", memberObject.getString("memberIdentifier"));
                                    memberMap.put("memberImage", memberObject.getString("memberImage"));
                                    memberMap.put("memberStatus", memberObject.getString("memberStatus"));
                                    memberMap.put("memberIsAdmin", memberObject.getBoolean("memberIsAdmin"));


                                } catch (JSONException e) {

                                }
                                groupMembers.add(memberMap);

                            }


                            db.addGroupMembersDetails(groupMembersDocId, groupMembers,
                                    obj.getString("createdByMemberId"), obj.getString("createdByMemberIdentifier"),
                                    obj.getLong("createdAt"), true);

                            if (updateGroupDetailsRequired) {


                                String groupPicUrl = "";

                                if (obj.has("groupImageUrl")) {

                                    groupPicUrl = obj.getString("groupImageUrl");


                                }
                                db.updateGroupNameAndImage(docId, obj.getString("groupSubject"), groupPicUrl);
                            }
                        }


                        Map<String, Object> map = new HashMap<>();
                        map.put("memberId", obj.getString("memberId"));
                        map.put("memberIdentifier", obj.getString("memberIdentifier"));
                        map.put("memberImage", obj.getString("memberImage"));
                        map.put("memberStatus", obj.getString("memberStatus"));
                        map.put("memberIsAdmin", obj.getBoolean("memberIsAdmin"));

                        db.addGroupMember(groupMembersDocId, map, memberId);
                        if (!docId.isEmpty()) {


                            map = new HashMap<>();

                            map.put("messageType", "98");


                            map.put("isSelf", false);
                            map.put("from", groupId);
                            map.put("Ts", obj.getString("timestamp"));
                            map.put("id", obj.getString("id"));

                            map.put("type", 1);

                            map.put("memberId", memberId);

                            map.put("memberIdentifier", obj.getString("memberIdentifier"));


                            map.put("initiatorId", obj.getString("initiatorId"));
                            map.put("initiatorIdentifier", obj.getString("initiatorIdentifier"));

                            map.put("deliveryStatus", "0");


                            String text, initiatorName, memberName;
                            String initiatorId = obj.getString("initiatorId");
                            if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                initiatorName = getString(R.string.You);
                            } else {
                                initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                if (initiatorName == null) {

                                    initiatorName = obj.getString("initiatorIdentifier");
                                }
                            }


                            if (memberId.equals(AppController.getInstance().getUserId())) {

                                memberName = getString(R.string.YouSmall);
                            } else {
                                memberName = db.getContactName(AppController.getInstance().getContactsDocId(), memberId);
                                if (memberName == null) {

                                    memberName = obj.getString("memberIdentifier");
                                }
                            }


                            text = initiatorName + " " + getString(R.string.AddedMember, memberName) + " " + getString(R.string.ToGroup);

                            addGroupChatMessageInDB(docId, map, obj.getString("timestamp"), text, true);

                            obj.put("docId", docId);


                            obj.put("memberName", memberName);


                            obj.put("message", text);

                            obj.put("eventName", topic);
                            bus.post(obj);
                        }


                        break;
                    }


                    case 2:

                    {
                        /*
                         * Member removed
                         *
                         */

                        String groupId = obj.getString("groupId");

                        /*
                         *
                         * To check if group already exists
                         */

                        String groupMembersDocId = db.fetchGroupChatDocumentId(groupChatsDocId, groupId);
                        if (groupMembersDocId == null) {
                            /*
                             * Group doesn't exists
                             */

                            new requestGroupMembersOnBackgroundThread().execute(new JSONObject[]{obj});

                            return;

                        }
                        db.removeGroupMember(groupMembersDocId, obj.getString("memberId"));


                        String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");

                        String memberId = obj.getString("memberId");
                        if (!docId.isEmpty()) {


                            Map<String, Object> map = new HashMap<>();

                            map.put("messageType", "98");


                            map.put("isSelf", false);
                            map.put("from", groupId);
                            map.put("Ts", obj.getString("timestamp"));
                            map.put("id", obj.getString("id"));

                            map.put("type", 2);

                            map.put("memberId", memberId);

                            map.put("memberIdentifier", obj.getString("memberIdentifier"));


                            map.put("initiatorId", obj.getString("initiatorId"));
                            map.put("initiatorIdentifier", obj.getString("initiatorIdentifier"));

                            map.put("deliveryStatus", "0");


                            String text, initiatorName, memberName;
                            String initiatorId = obj.getString("initiatorId");
                            if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                initiatorName = getString(R.string.You);
                            } else {
                                initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                if (initiatorName == null) {

                                    initiatorName = obj.getString("initiatorIdentifier");
                                }
                            }


                            if (memberId.equals(AppController.getInstance().getUserId())) {

                                memberName = getString(R.string.YouSmall);
                            } else {
                                memberName = db.getContactName(AppController.getInstance().getContactsDocId(), memberId);
                                if (memberName == null) {

                                    memberName = obj.getString("memberIdentifier");
                                }
                            }


                            text = initiatorName + " " + getString(R.string.Removed) + " " + memberName;

                            addGroupChatMessageInDB(docId, map, obj.getString("timestamp"), text, true);

                            obj.put("docId", docId);


                            obj.put("message", text);

                            obj.put("eventName", topic);
                            bus.post(obj);
                        }


                        break;

                    }
                    case 3: {
                        /*
                         * Made admin
                         *
                         */


                        String groupId = obj.getString("groupId");

                        /*
                         *
                         * To check if group already exists
                         */

                        String groupMembersDocId = db.fetchGroupChatDocumentId(groupChatsDocId, groupId);
                        if (groupMembersDocId == null) {
                            /*
                             * Group doesn't exists
                             */

                            new requestGroupMembersOnBackgroundThread().execute(new JSONObject[]{obj});

                            return;

                        }
                        db.makeGroupAdmin(groupMembersDocId, obj.getString("memberId"));


                        String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");

                        String memberId = obj.getString("memberId");
                        if (!docId.isEmpty()) {


                            Map<String, Object> map = new HashMap<>();

                            map.put("messageType", "98");


                            map.put("isSelf", false);
                            map.put("from", groupId);
                            map.put("Ts", obj.getString("timestamp"));
                            map.put("id", obj.getString("id"));

                            map.put("type", 3);

                            map.put("memberId", memberId);

                            map.put("memberIdentifier", obj.getString("memberIdentifier"));


                            map.put("initiatorId", obj.getString("initiatorId"));
                            map.put("initiatorIdentifier", obj.getString("initiatorIdentifier"));

                            map.put("deliveryStatus", "0");


                            String text, initiatorName, memberName;
                            String initiatorId = obj.getString("initiatorId");
                            if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                initiatorName = getString(R.string.You);
                            } else {
                                initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                if (initiatorName == null) {

                                    initiatorName = obj.getString("initiatorIdentifier");
                                }
                            }


                            if (memberId.equals(AppController.getInstance().getUserId())) {

                                memberName = getString(R.string.YouSmall);
                            } else {
                                memberName = db.getContactName(AppController.getInstance().getContactsDocId(), memberId);
                                if (memberName == null) {

                                    memberName = obj.getString("memberIdentifier");
                                }
                            }


                            text = initiatorName + " " + getString(R.string.Made) + " " + memberName + " " + getString(R.string.MakeAdmin);

                            addGroupChatMessageInDB(docId, map, obj.getString("timestamp"), text, true);

                            obj.put("docId", docId);


                            obj.put("message", text);

                            obj.put("eventName", topic);
                            bus.post(obj);

                        }
                        break;
                    }
                    case 4:

                    {
                        /*
                         * Group name updated
                         *
                         */
                        String groupId = obj.getString("groupId");

                        /*
                         *
                         * To check if group already exists
                         */

                        String groupMembersDocId = db.fetchGroupChatDocumentId(groupChatsDocId, groupId);
                        if (groupMembersDocId == null) {
                            /*
                             * Group doesn't exists
                             */

                            new requestGroupMembersOnBackgroundThread().execute(new JSONObject[]{obj});

                            return;

                        }


                        String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");


                        if (!docId.isEmpty()) {

                            db.updateGroupSubject(docId, obj.getString("groupSubject"));

                            Map<String, Object> map = new HashMap<>();

                            map.put("messageType", "98");


                            map.put("isSelf", false);
                            map.put("from", groupId);
                            map.put("Ts", obj.getString("timestamp"));
                            map.put("id", obj.getString("id"));

                            map.put("type", 4);


                            map.put("groupSubject", obj.getString("groupSubject"));
                            map.put("initiatorId", obj.getString("initiatorId"));
                            map.put("initiatorIdentifier", obj.getString("initiatorIdentifier"));

                            map.put("deliveryStatus", "0");


                            String text, initiatorName;
                            String initiatorId = obj.getString("initiatorId");
                            if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                initiatorName = getString(R.string.You);
                            } else {
                                initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                if (initiatorName == null) {

                                    initiatorName = obj.getString("initiatorIdentifier");
                                }
                            }


                            text = initiatorName + " " + getString(R.string.UpdatedGroupSubject, obj.getString("groupSubject"));

                            addGroupChatMessageInDB(docId, map, obj.getString("timestamp"), text, true);

                            obj.put("docId", docId);
//                                String url = db.getGroupImageUrl(docId);
//
//                                if (url != null) {
//                                    obj.put("groupImageUrl", url);
//
//                                } else {
//
//
//                                    obj.put("groupImageUrl", "");
//
//                                }

                            obj.put("message", text);

                            obj.put("eventName", topic);
                            bus.post(obj);

                        }

                        break;
                    }
                    case 5:


                    {
                        /*
                         * Group icon updated
                         *
                         */


                        String groupId = obj.getString("groupId");

                        /*
                         *
                         * To check if group already exists
                         */

                        String groupMembersDocId = db.fetchGroupChatDocumentId(groupChatsDocId, groupId);
                        if (groupMembersDocId == null) {
                            /*
                             * Group doesn't exists
                             */


                            new requestGroupMembersOnBackgroundThread().execute(new JSONObject[]{obj});

                            return;

                        }


                        String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");


                        if (!docId.isEmpty()) {

                            db.updateGroupIcon(docId, obj.getString("groupImageUrl"));

                            Map<String, Object> map = new HashMap<>();

                            map.put("messageType", "98");


                            map.put("isSelf", false);
                            map.put("from", groupId);
                            map.put("Ts", obj.getString("timestamp"));
                            map.put("id", obj.getString("id"));

                            map.put("type", 5);


                            map.put("groupImageUrl", obj.getString("groupImageUrl"));
                            map.put("initiatorId", obj.getString("initiatorId"));
                            map.put("initiatorIdentifier", obj.getString("initiatorIdentifier"));

                            map.put("deliveryStatus", "0");


                            String text, initiatorName;
                            String initiatorId = obj.getString("initiatorId");
                            if (initiatorId.equals(AppController.getInstance().getUserId())) {

                                initiatorName = getString(R.string.You);
                            } else {
                                initiatorName = db.getContactName(AppController.getInstance().getContactsDocId(), initiatorId);
                                if (initiatorName == null) {

                                    initiatorName = obj.getString("initiatorIdentifier");
                                }
                            }


                            text = initiatorName + " " + getString(R.string.UpdatedGroupIcon);

                            addGroupChatMessageInDB(docId, map, obj.getString("timestamp"), text, true);

                            obj.put("docId", docId);


                            obj.put("message", text);


                            obj.put("message", text);


//                                obj.put("groupSubject", db.getGroupSubject(docId));
                            obj.put("eventName", topic);
                            bus.post(obj);

                        }


                        break;

                    }


                    case 6: {

                        /*
                         * Member left the conversation
                         */

                        String groupId = obj.getString("groupId");

                        /*
                         *
                         * To check if group already exists
                         */

                        String groupMembersDocId = db.fetchGroupChatDocumentId(groupChatsDocId, groupId);
                        if (groupMembersDocId == null) {
                            /*
                             * Group doesn't exists
                             */


                            new requestGroupMembersOnBackgroundThread().execute(new JSONObject[]{obj});

                            return;

                        }


                        String memberId = obj.getString("initiatorId");
                        db.removeGroupMember(groupMembersDocId, memberId);


                        String docId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");


                        if (!docId.isEmpty()) {


                            Map<String, Object> map = new HashMap<>();

                            map.put("messageType", "98");


                            map.put("isSelf", false);
                            map.put("from", groupId);
                            map.put("Ts", obj.getString("timestamp"));
                            map.put("id", obj.getString("id"));

                            map.put("type", 6);


                            map.put("initiatorId", memberId);
                            map.put("initiatorIdentifier", obj.getString("initiatorIdentifier"));

                            map.put("deliveryStatus", "0");


                            String text, memberName;


                            if (memberId.equals(AppController.getInstance().getUserId())) {

                                memberName = getString(R.string.You);
                            } else {
                                memberName = db.getContactName(AppController.getInstance().getContactsDocId(), memberId);
                                if (memberName == null) {

                                    memberName = obj.getString("initiatorIdentifier");
                                }
                            }


                            text = getString(R.string.LeftGroup, memberName);

                            addGroupChatMessageInDB(docId, map, obj.getString("timestamp"), text, true);

                            obj.put("docId", docId);


                            obj.put("message", text);

                            obj.put("eventName", topic);
                            bus.post(obj);
                        }

                        break;
                    }


                }
            }

        } catch (JSONException e) {

        }
    }


    /*
     *
     *To allow the option of auto downloading of the media,when on wifi
     *
     */

    private boolean checkWifiConnected() {


        /*
         *Since connectionManager.isConnected() is deprecated in api23
         */


        if (wifiMgr == null) {
            wifiMgr = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        }
        if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON


            if (wifiInfo == null) {

                wifiInfo = wifiMgr.getConnectionInfo();

            }
            if (wifiInfo == null) {


                return false;
            } else {
                if (wifiInfo.getNetworkId() == -1) {


                    return false; // Not connected to an access point
                }
            }


            return true; // Connected to an access point
        } else {


            return false; // Wi-Fi adapter is OFF
        }


    }


    private boolean checkMobileDataOn() {


        try {

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();

            return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);


        } catch (Exception e) {

            return false;
        }
    }


    /*
     * Will try to download the media message asynchronously in the background
     *
     */


    @SuppressWarnings("TryWithIdenticalCatches")
    private class DownloadMessage extends AsyncTask {

        /*
         *         String messageType = (String) params[1];
         *
         *           String replyTypeString = (String) params[2];
         *
         *            String thumbnailPath = (String) params[3];
         *            String filePath = (String) params[4];
         *           String messageId = (String) params[5];
         *
         *            String docId = (String) params[6];
         *            String senderId = (String) params[7];
         */

        @Override
        protected Object doInBackground(final Object[] params) {


            String url = (String) params[0];


            final FileDownloadService downloadService =
                    ServiceGenerator.createService(FileDownloadService.class);


            Call<ResponseBody> call = downloadService.downloadFileWithDynamicUrlAsync(url);


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {


                    if (response.isSuccessful()) {
                        new AsyncTask<Void, Long, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {


                                int replyType = -1;


                                if ((params[1]).equals("10")) {


                                    replyType = Integer.parseInt((String) params[2]);
                                }


                                boolean writtenToDisk = writeResponseBodyToDisk(response.body(), (String) params[4],
                                        (String) params[1], (String) params[5], replyType);


                                if (writtenToDisk) {


                                    //  deleteFileFromServer(url);


                                    if (params[3] != null) {
                                        /*
                                         *
                                         * incase of image or video delete the thumbnail
                                         *
                                         * */

                                        File fDelete = new File((String) params[3]);
                                        if (fDelete.exists()) fDelete.delete();


                                    }


                                    /*
                                     *To send the callback for automatically media downloaded
                                     */
                                    JSONObject obj = new JSONObject();
                                    try {
                                        obj.put("eventName", "MessageDownloaded");

                                        obj.put("messageType", params[1]);
                                        obj.put("replyType", params[2]);
                                        obj.put("filePath", params[4]);
                                        obj.put("messageId", params[5]);
                                        obj.put("docId", params[6]);
                                        obj.put("senderId", params[7]);

                                        bus.post(obj);
                                    } catch (JSONException e) {

                                    }


                                    try {

                                        db.updateDownloadStatusAndPath((String) params[6],
                                                (String) params[4], (String) params[5]);
                                    } catch (Exception e) {

                                    }

                                }


                                return null;


                            }
                        }.execute();


                    }


                }

                @Override
                public void onFailure(final Call<ResponseBody> call, Throwable t) {


                }

            });


            return null;
        }

    }

    @SuppressWarnings("all")
    private boolean writeResponseBodyToDisk(ResponseBody body, String filePath, String messageType, final String messageId, int replyType) {

        try {
            // todo change the file location/name according to your needs


            File folder = new File(Environment.getExternalStorageDirectory().getPath() + "/hola");


            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }

            File file = new File(filePath);


            if (!file.exists()) {
                file.createNewFile();
            }


            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {

                byte[] fileReader = new byte[4096];

                final long fileSize = body.contentLength();


                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);


                while (true) {


                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);


                }

                outputStream.flush();

                return true;

            } catch (ArrayIndexOutOfBoundsException e) {
                return false;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }


            }
        } catch (IOException e) {
            return false;
        }

    }


    /**
     * For generating the push notification incase contact updated on below api level 18 devices.
     */
    private void generatePushForContactUpdate() {

        NotificationCompat.Builder
                notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.hola_ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        R.mipmap.hola_ic_launcher))
                .setContentTitle(getString(R.string.app_name))


                .setContentText(getString(R.string.NotificationContent))
                .setTicker(getString(R.string.NotificationTitle))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0))

                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setPriority(Notification.PRIORITY_HIGH);

        if (Build.VERSION.SDK_INT >= 21) notificationBuilder.setVibrate(new long[0]);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        /*
         *
         * Notification id is used to notify the same notification
         *
         * */

        notificationManager.notify(-1, notificationBuilder.build());
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}