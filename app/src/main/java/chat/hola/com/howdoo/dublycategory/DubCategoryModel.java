package chat.hola.com.howdoo.dublycategory;

import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.dubly.Dub;
import chat.hola.com.howdoo.dublycategory.modules.DubCategory;
import chat.hola.com.howdoo.dublycategory.modules.DubCategoryAdapter;
import chat.hola.com.howdoo.dublycategory.modules.DubListAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;

/**
 * <h1>DubCategoryModel</h1>
 *
 * @author 3Embed
 * @since 4/10/2018.
 */

public class DubCategoryModel {

    @Inject
    List<DubCategory> categories;
    @Inject
    DubCategoryAdapter categoryAdapter;
    @Inject
    SessionManager sessionManager;

    @Inject
    DubCategoryModel() {
    }

    public void setCategoryData(List<DubCategory> dubCategories, boolean isLoadAll) {
        if (dubCategories != null) {
            int max = isLoadAll || dubCategories.size() < 8 ? dubCategories.size() : 8;
            this.categories.clear();
            for (int i = 0; i < max; i++)
                this.categories.add(dubCategories.get(i));
            categoryAdapter.notifyDataSetChanged();
        }
    }


    public String getCategoryId(int position) {
        return categories.get(position).getId();
    }

    public String getCategoryName(int position) {
        return categories.get(position).getName();
    }
}
