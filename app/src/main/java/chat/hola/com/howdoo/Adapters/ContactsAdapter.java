package chat.hola.com.howdoo.Adapters;

/**
 * Created by moda on 24/08/17.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.howdoo.dubly.R;

import java.util.ArrayList;

import chat.hola.com.howdoo.Activities.ContactsList;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.ModelClasses.ContactsItem;
import chat.hola.com.howdoo.Utilities.TextDrawable;
import chat.hola.com.howdoo.ViewHolders.ViewHolderContact;
import chat.hola.com.howdoo.home.connect.ContactsFragment;

/**
 * Created by moda on 28/07/17.
 */

public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private ArrayList<ContactsItem> mOriginalListData = new ArrayList<>();
    private ArrayList<ContactsItem> mFilteredListData;

    private Context mContext;
    public char settLetter = 'A';
    public Boolean setFlag = true;
    private ClickListner clickListner;
    private int density;

//    private Typeface tf;


    private ContactsFragment fragment;

    /**
     * @param mContext  Context
     * @param mListData ArrayList<ContactsItem>
     */
    public ContactsAdapter(Context mContext, ArrayList<ContactsItem> mListData, ContactsFragment fragment) {


        this.fragment = fragment;
        this.mOriginalListData = mListData;


        this.mFilteredListData = mListData;
        this.mContext = mContext;


        density = (int) mContext.getResources().getDisplayMetrics().density;
        //   tf = AppController.getInstance().getRobotoCondensedFont();

    }

    /**
     * @return number of items
     */
    @Override
    public int getItemCount() {
        return this.mFilteredListData == null ? 0 : this.mFilteredListData.size();
    }


    /**
     * @param position item position
     * @return item viewType
     */
    @Override
    public int getItemViewType(int position) {
        return 1;
    }


    /**
     * @param viewGroup ViewGroup
     * @param viewType  item viewType
     * @return RecyclerView.ViewHolder
     */

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());


        View v1 = inflater.inflate(R.layout.contact_item, viewGroup, false);
        viewHolder = new ViewHolderContact(v1);

        return viewHolder;
    }


    /**
     * @param viewHolder RecyclerView.ViewHolder
     * @param position   item position
     */

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {


        ViewHolderContact vh2 = (ViewHolderContact) viewHolder;

        configureViewHolderContact(vh2, position);


    }


    /**
     * @param vh       ViewHolderContact
     * @param position item position
     */

    @SuppressWarnings("TryWithIdenticalCatches")
    private void configureViewHolderContact(final ViewHolderContact vh, int position) {
        final ContactsItem contactsItem = mFilteredListData.get(position);

        if (contactsItem != null) {
            vh.contactName.setText(contactsItem.getContactName());

            vh.contactImage.setOnClickListener(view -> clickListner.itemSelect(position));
            vh.contactName.setOnClickListener(view -> clickListner.itemSelect(position));
            vh.ibDelete.setOnClickListener(view -> clickListner.delete(contactsItem.getContactUid()));

            String contactName = contactsItem.getContactName().toUpperCase();

            if (contactName.length() > 0) {
                char firstLetter = contactName.charAt(0);
                //   settLetter=firstLetter;
                if (settLetter != firstLetter) {
                    setFlag = true;
                }
                if (setFlag) {
                    vh.contactnameIndicatorTv.setText("" + firstLetter);
                    setFlag = false;
                    settLetter = firstLetter;
                } else {
                    //settLetter=firstLetter;
                    vh.contactnameIndicatorTv.setText("  ");
                }
            }
            vh.contactStatus.setText(contactsItem.getContactStatus());
            if (contactsItem.getContactImage() != null && !contactsItem.getContactImage().isEmpty()) {
                try {

                    Glide.with(mContext).load(contactsItem.getContactImage()).asBitmap()

                            .centerCrop().placeholder(R.drawable.chat_attachment_profile_default_image_frame).
                            into(new BitmapImageViewTarget(vh.contactImage) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    vh.contactImage.setImageDrawable(circularBitmapDrawable);
                                }
                            });

                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    vh.contactImage.setImageDrawable(TextDrawable.builder()
                            .beginConfig()
                            .textColor(Color.WHITE)
                            .useFont(Typeface.DEFAULT)
                            .fontSize(24 * density) /* size in px */
                            .bold()
                            .toUpperCase()
                            .endConfig()
                            .buildRound((contactsItem.getContactName().trim()).charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(vh.getAdapterPosition() % 19))));
                } catch (NullPointerException ignored) {
                } catch (IndexOutOfBoundsException ignored) {
                }
            }
        }
    }


    /**
     * @return list of filtered items
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredListData = (ArrayList<ContactsItem>) results.values;
                ContactsAdapter.this.notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<ContactsItem> filteredResults;
                if (constraint.length() == 0) {
                    filteredResults = mOriginalListData;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());


                }
                mFilteredListData = filteredResults;
                if (fragment != null) {
                    fragment.showNoSearchResults(constraint, mFilteredListData.size() == mOriginalListData.size());
                } else {

                    ((ContactsList) mContext).showNoSearchResults(constraint, mFilteredListData.size() == mOriginalListData.size());
                }
                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }


    /**
     * @param constraint query to search for
     * @return ArrayList<ContactsItem>
     */

    private ArrayList<ContactsItem> getFilteredResults(String constraint) {
        ArrayList<ContactsItem> results = new ArrayList<>();

        for (ContactsItem item : mOriginalListData) {
            if ((item).getContactName().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }


    /**
     * @return ArrayList<ContactsItem>
     */

    public ArrayList<ContactsItem> getList() {


        return mFilteredListData;
    }

    public void setClickListner(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public interface ClickListner {
        void delete(String contactUid);

        void itemSelect(int position);
    }


}
