package chat.hola.com.howdoo.club.clubs;

import java.util.Map;

import chat.hola.com.howdoo.Utilities.BaseView;

/**
 * <h1>BlockUserContract</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public interface ClubContract {

    interface View extends BaseView {
        /**
         * redirects to  @{@link chat.hola.com.howdoo.profileScreen.ProfileActivity})
         *
         * @param channelId : channelId to get channel details
         */
        void openChannel(String channelId);

        void getClubId(String clubName, String clubId);

        void confirmDelete(int position);
    }

    interface Presenter {

        /**
         * gets lis of members of particular post
         */
        void getClubs(int access, int offset, int limit, String userId);

        /**
         * perform the click actions
         */
        ClickListner getPresenter();

        void callApiOnScroll(int access, String postId, int firstVisibleItemPosition, int visibleItemCount, int totalItemCount, String userId);

        void actionOnClub(String clubId);

        void setForSelect(boolean isForSelect);

        void deleteClub(int position);
    }
}
