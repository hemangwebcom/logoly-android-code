package chat.hola.com.howdoo.home.trending;

import android.util.Log;
import android.view.View;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooServiceTrending;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.home.trending.model.HeaderAdapter;
import chat.hola.com.howdoo.home.trending.model.TrendingContentAdapter;
import chat.hola.com.howdoo.home.trending.model.TrendingContentResponse;
import chat.hola.com.howdoo.home.trending.model.TrendingModel;
import chat.hola.com.howdoo.home.trending.model.TrendingResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h1>TrendingPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 19/6/18.
 */

public class TrendingPresenter implements TrendingContract.Presenter, HeaderAdapter.ClickListner, TrendingContentAdapter.ClickListner {

    private TrendingContract.View view;
    @Inject
    HowdooServiceTrending trending;
    @Inject
    TrendingModel model;
    String selectedCategoryId;

    @Inject
    TrendingPresenter() {
    }

    @Override
    public void attachView(TrendingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }


    @Override
    public void init() {
        if (view != null) {
            view.initHeaderRecycler();
            view.initContentRecycler();
        }
    }

    @Override
    public void loadHeader() {
        trending.getCategories(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<TrendingResponse>>() {
                    @Override
                    public void onNext(Response<TrendingResponse> response) {
                        try {
                            assert response.body() != null;
                            if (response.code() == 200 && response.body().getData() != null && !response.body().getData().isEmpty()) {
                                assert response.body() != null;
                                view.showHeader(model.setHeader(response.body().getData()));
                                loadContent(model.getDefaultCategoryId());
                                view.isContentAvailable(true);
                            } else {
                                if (response.code() == 204 || response.body().getData().isEmpty()) {
                                    view.isContentAvailable(false);
                                }
                            }
                        } catch (NullPointerException ignored) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void loadContent(String categoryId) {
        Log.i("CategoryId", categoryId);

        view.isLoading(true);
        Map<String, Object> map = new HashMap<>();
        map.put("forYouFlag", 1);
        map.put("categoryId", categoryId);
        map.put("timeZone", TimeZone.getDefault().getID());
        map.put("filterType", 2);
        trending.getTrendingData(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<TrendingContentResponse>>() {
                    @Override
                    public void onNext(Response<TrendingContentResponse> response) {
                        if (response.code() == 200 && !response.body().getData().isEmpty()) {
                            model.setSelected(categoryId);
                            model.setData(response.body().getData());
                            if (response.body().getHashTags() != null && !response.body().getHashTags().isEmpty()) {
                                try {
                                    view.setHashTags(model.generateHashtagString(response.body().getHashTags()));
                                } catch (Exception ignored) {
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            view.isLoading(false);
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void onComplete() {
                        try {
                            view.isLoading(false);
                        } catch (Exception ignored) {
                        }
                    }
                });
    }

    @Override
    public void onItemClick(String categoryId) {
        selectedCategoryId = categoryId;
        loadContent(selectedCategoryId);
    }

    @Override
    public void onPostClick(int position, View view) {
        try {
            this.view.onPostClick(model.getItem(position), view);
        } catch (Exception ignored) {
        }
    }
}
