package chat.hola.com.howdoo.comment.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.Utilities.TypefaceManager;

/**
 * <h1>ViewHolder</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.ivProfilePic)
    SimpleDraweeView ivProfilePic;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.item)
    FrameLayout item;
    private ClickListner clickListner;

    public ViewHolder(View itemView, TypefaceManager typefaceManager, ClickListner clickListner) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        tvUserName.setTypeface(typefaceManager.getSemiboldFont());
        tvMessage.setTypeface(typefaceManager.getMediumFont());
        tvTime.setTypeface(typefaceManager.getRegularFont());
        this.clickListner = clickListner;
    }

    @OnClick(R.id.item)
    public void itemSelect() {
        clickListner.itemSelect(getAdapterPosition(), !item.isSelected());
    }

    @OnClick(R.id.ivProfilePic)
    public void profile() {
        clickListner.onUserClick(getAdapterPosition());
    }


}