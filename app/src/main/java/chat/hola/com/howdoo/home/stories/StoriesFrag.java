package chat.hola.com.howdoo.home.stories;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Dialog.ImageSourcePicker;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.TimeAgo;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.camera.CameraActivity;
import chat.hola.com.howdoo.cameraActivities.SandriosCamera;
import chat.hola.com.howdoo.cameraActivities.configuration.CameraConfiguration;
import chat.hola.com.howdoo.cameraActivities.manager.CameraOutputModel;
import chat.hola.com.howdoo.home.stories.model.StoriesAdapter;
import chat.hola.com.howdoo.home.stories.model.StoryPost;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import chat.hola.com.howdoo.poststory.StoryService;
import chat.hola.com.howdoo.preview.PreviewActivity;
import dagger.android.support.DaggerFragment;

/**
 * <h1>StoriesFrag</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/26/2018
 */

public class StoriesFrag extends DaggerFragment implements StoriesContract.View {

    static final int READ_STORAGE_REQ_CODE = 101;
    private static final int RESULT_LOAD_IMAGE = 202;
    private static final int REQUEST_CODE = 303;
    public static final String TAG = "StoriesFrag";
    private Unbinder unbinder;

    @Inject
    public StoriesPresenter mPresenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    ImageSourcePicker imageSourcePicker;
    @Inject
    StoriesAdapter mAdapter;

    @BindView(R.id.myStoryTv)
    TextView myStoryTv;
    @BindView(R.id.tapToAddStoryTv)
    TextView tapToAddStoryTv;
    @BindView(R.id.recentStoriesRv)
    RecyclerView recentStoriesRv;
    @BindView(R.id.addMyStoryIv)
    SimpleDraweeView addMyStoryIv;
    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;
    @BindView(R.id.ivAdd)
    AppCompatImageView ivAdd;
    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @BindView(R.id.tvEmptyTitle)
    TextView tvEmptyTitle;
    @BindView(R.id.tvEmptyMsg)
    TextView tvEmptyMsg;
    @Inject
    BlockDialog dialog;
    @Inject
    PreviewActivity previewActivity;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Inject
    public StoriesFrag() {
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            imageSourcePicker.setOnSelectImageSource(callback);
            mAdapter.setListener(mPresenter);
            recentStoriesRv.setHasFixedSize(true);
            recentStoriesRv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            recentStoriesRv.setAdapter(mAdapter);
            mPresenter.myStories();
            mPresenter.stories();
            mPresenter.storyObserver();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_stories, container, false);
        unbinder = ButterKnife.bind(this, view);
        setTypefaces();
        llNetworkError.setErrorListner(this);
        mPresenter.attachView(this);
        addMyStoryIv.setImageURI(sessionManager.getUserProfilePic());

        return view;
    }



    private void setTypefaces() {
        myStoryTv.setTypeface(typefaceManager.getSemiboldFont());
        tapToAddStoryTv.setTypeface(typefaceManager.getRegularFont());
        tvEmptyTitle.setTypeface(typefaceManager.getSemiboldFont());
        tvEmptyMsg.setTypeface(typefaceManager.getRegularFont());
    }

    @OnClick(R.id.fab)
    public void addStories() {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            startActivity(new Intent(getContext(), CameraActivity.class).putExtra("call", "story"));
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
        //imageSourcePicker.show();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        imageSourcePicker.setOnSelectImageSource(callback);
//        mAdapter.setListener(mPresenter);
//        recentStoriesRv.setHasFixedSize(true);
//        recentStoriesRv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        recentStoriesRv.setAdapter(mAdapter);
//        mPresenter.myStories();
//        mPresenter.stories();
//        mPresenter.storyObserver();
    }

    @OnClick(R.id.rlMyStory)
    public void seeStory() {
        List<StoryPost> storyPosts = mPresenter.getStoryPosts();
        if (storyPosts.isEmpty()) {
            //imageSourcePicker.show();
            Dexter.withActivity(getActivity())
                    .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            // check if all permissions are granted
                            if (report.areAllPermissionsGranted()) {
                                startActivity(new Intent(getContext(), CameraActivity.class).putExtra("call", "story"));
                            }

                            // check for permanent denial of any permission
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // permission is denied permenantly, navigate user to app settings
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .onSameThread()
                    .check();
        } else
            startActivity(new Intent(getContext(), PreviewActivity.class).putExtra("data", (Serializable) storyPosts).putExtra("isMine", true));
    }

    ImageSourcePicker.OnSelectImageSource callback = new ImageSourcePicker.OnSelectImageSource() {
        @Override
        public void onCamera() {
            launchCamera();
        }

        @Override
        public void onGallary() {
            checkReadImage();
        }

        @Override
        public void onCancel() {
        }
    };

    private void launchCamera() {
        SandriosCamera.with((Activity) getContext()).setShowPicker(true)
                .setVideoFileSize(Constants.Camera.FILE_SIZE)
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                .enableImageCropping(true)
                .launchCamera(mPresenter);
    }

    private void checkReadImage() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            mPresenter.launchImagePicker();
        } else {
            requestReadImagePermission();
        }
    }

    private void requestReadImagePermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_STORAGE_REQ_CODE);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        if (unbinder != null)
            unbinder.unbind();
    }


    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void myStories(String url, String timeStamp) {
        try {
            ivAdd.setVisibility(View.GONE);
            addMyStoryIv.setImageURI(url.replace("mp4", "jpg"));
            tapToAddStoryTv.setText(timeStamp == null ? getContext().getString(R.string.justnow) : TimeAgo.getTimeAgo(Long.parseLong(timeStamp)));
        } catch (Exception ignore) {
        }
    }

    @Override
    public void launchImagePicker(Intent intent) {
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onComplete(Bundle bundle) {
        getContext().startService(new Intent(getContext(), StoryService.class).putExtra("data", bundle));
    }

    @Override
    public void isDataAvailable(boolean empty) {
        llEmpty.setVisibility(empty ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_LOAD_IMAGE) {
            mPresenter.parseMedia(resultCode, data);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            mPresenter.parseCropedImage(resultCode, data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(getContext(), CameraActivity.class));
            }
        }
    }

    @Override
    public void launchCropImage(Uri data) {
        CropImage.activity(data).start(getContext(), this);
    }

    @Override
    public void launchActivity(CameraOutputModel model) {
        mPresenter.onComplete(model);
    }

    @Override
    public void preview(int position) {
        startActivity(new Intent(getContext(), PreviewActivity.class).putExtra("data", (Serializable) mPresenter.getStoryPosts(position)));
    }

    @Override
    public void reload() {
        mPresenter.myStories();
        mPresenter.stories();
    }

    public void updateViewed(int position) {
        mPresenter.updateViewed(position);
    }
}
