package chat.hola.com.howdoo.dublycategory.fagments;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.camera.CameraActivity;
import chat.hola.com.howdoo.dublycategory.modules.DubListAdapter;
import dagger.android.support.DaggerFragment;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 7/19/2018.
 */

public class DubFragment extends DaggerFragment implements DubFragmentContract.View, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rvDubList)
    RecyclerView rvDubList;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    @Inject
    DubListAdapter adapter;

    @Override
    public void onPause() {
        super.onPause();
        if (player != null && player.isPlaying())
            player.stop();
    }

    @Inject
    DubFragmentPresenter presenter;
    private LinearLayoutManager layoutManager;
    private int position = 0;
    ProgressBar pb;
    Dialog dialog;
    TextView cur_val;
    private MediaPlayer player;
    @Inject
    BlockDialog dialog1;

    @Override
    public void userBlocked() {
        dialog1.show();
    }

    @Inject
    public DubFragment() {
        // Required empty public constructor
    }

    public void getPosition(int position) {
        this.position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dub, container, false);
        ButterKnife.bind(this, rootView);
        presenter.setView(this);
        layoutManager = new LinearLayoutManager(getActivity());
        rvDubList.setLayoutManager(layoutManager);
        adapter.setListener(presenter.getPresenter());
        rvDubList.setAdapter(adapter);

        rvDubList.addOnScrollListener(recyclerViewOnScrollListener);
        //rvDubList.addItemDecoration(new DividerItemDecoration(rvDubList.getContext(), DividerItemDecoration.VERTICAL));

        presenter.loadData(0, 20);
        presenter.getData(position);
        // Initialize a new media player instance
        player = new MediaPlayer();
        swipe.setOnRefreshListener(this);
        return rootView;
    }

    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int[] firstVisibleItemPositions = new int[20];
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            presenter.callApiOnScroll(firstVisibleItemPosition, visibleItemCount, totalItemCount);
        }
    };

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {

    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    public void setMax(Integer max) {
        new Runnable() {
            public void run() {
                pb.setMax(max);
            }
        };
    }

    @Override
    public void startedDownload() {
        showProgress();
        if (player != null)
            player.stop();
        presenter.startedDownload();
    }

    @Override
    public void progress(int downloadedSize) {
        pb.setProgress(downloadedSize);
    }

    @Override
    public void finishedDownload(String path, String name, String musicId) {
        dialog.dismiss();
        startActivity(new Intent(getContext(), CameraActivity.class).putExtra("audio", path).putExtra("name", name).putExtra("musicId", musicId).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK) );
        getActivity().finish();
    }

    @Override
    public void play(String audio, boolean play) {
        if (player != null && player.isPlaying()) {
            player.stop();
            player.reset();
            player.release();
            player = null;
        }
        if (play) {
            Uri uri = Uri.parse(audio);
            player = MediaPlayer.create(getContext(), uri);
            player.start();
        }
    }

    @Override
    public void isLoading(boolean isLoading) {
        if (!isLoading && swipe != null && swipe.isRefreshing())
            swipe.setRefreshing(false);
    }

    @Override
    public void reload() {

    }

    void showProgress() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.myprogressdialog);
        dialog.setTitle("Download Progress");

        cur_val = (TextView) dialog.findViewById(R.id.tv1);
        cur_val.setText("Downloading...");
        dialog.show();

        pb = (ProgressBar) dialog.findViewById(R.id.progress_bar);
        pb.setProgress(0);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
    }

    @Override
    public void onRefresh() {
        presenter.loadData(0, 20);
    }
}
