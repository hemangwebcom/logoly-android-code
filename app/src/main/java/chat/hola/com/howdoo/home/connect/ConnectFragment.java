package chat.hola.com.howdoo.home.connect;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.stories.StoriesFrag;
import dagger.android.support.DaggerFragment;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class ConnectFragment extends DaggerFragment implements View.OnClickListener {

    @Inject
    public ConnectFragment() {
    }

    TabLayout connectTl;
    private ViewPager connectVp;
    private FloatingActionButton faBtn;
    private Typeface fontBold;

    @Inject
    StoriesFrag storiesFrag;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_connect, container, false);
        connectTl = (TabLayout) view.findViewById(R.id.connectTl);
        connectVp = (ViewPager) view.findViewById(R.id.connectVp);
        faBtn = (FloatingActionButton) view.findViewById(R.id.faBtn);
        LandingActivity mActivity = (LandingActivity) getActivity();
        mActivity.visibleActionBar();
        connectTl.setVisibility(View.VISIBLE);
        AppController appController = AppController.getInstance();
        fontBold = appController.getSemiboldFont();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        //Adding the tabs using addTab() method
        ConnectPagerAdapter adapter = new ConnectPagerAdapter(getChildFragmentManager());
        // Add Fragments to adapter one by one
        String chats = getResources().getString(R.string.chats);
        String stories = getResources().getString(R.string.stories);
        String calls = getResources().getString(R.string.calls);


        //  ChatsFragment chatsFragment=new ChatsFragment();
        adapter.addFragment(new ChatsFragment(connectTl), chats);
        adapter.addFragment(storiesFrag, stories);
        adapter.addFragment(new CallsFragment(connectTl), calls);


        // chatsFragment.hideTablayout();
        connectVp.setAdapter(adapter);
        connectTl.setupWithViewPager(connectVp);
        faBtn.setOnClickListener(this);
        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_textview, null);
        tabOne.setTypeface(fontBold);
        tabOne.setText(chats);
        connectTl.getTabAt(0).setCustomView(tabOne);
        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_textview, null);
        tabTwo.setTypeface(fontBold);
        tabTwo.setText(stories);
        connectTl.getTabAt(1).setCustomView(tabTwo);
        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_textview, null);
        tabThree.setTypeface(fontBold);
        tabThree.setText(calls);
        connectTl.getTabAt(2).setCustomView(tabThree);
    }

    @Override
    public void onClick(View v) {
    }

    public void selectTab(int i) {
        connectVp.setCurrentItem(i);
    }
}
