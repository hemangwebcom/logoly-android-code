package chat.hola.com.howdoo.socialDetail;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.home.model.Data;

/**
 * <h1>SocialDetailContract</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 23/3/18.
 */
public interface SocialDetailContract {

    interface View extends BaseView {


        /**
         * <p>dismisses the dialog</p>
         */
        void dismissDialog();

        /**
         * <p>add reports to the array list</p>
         *
         * @param data : list of report
         */
        void addToReportList(ArrayList<String> data);

        /**
         * <p>sets details to the screen</p>
         *
         * @param data : details
         */
        void setData(Data data);

        void favourite(boolean flag, String postId);

        void liked(boolean b);

        void deleted();

        void follow(boolean isChecked, String postId);

        void send();

        void followers( List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> data);
    }

    interface Presenter {

        /**
         * <p>gets details by postId </p>
         *
         * @param postId        :posts's Id
         * @param isJustForView : says its just to for view the post
         */
        void getPostById(String postId, boolean isJustForView);

        /**
         * <p>get list of report reasons</p>
         */
        void getReportReasons();

        void reportPost(String postId, String reason, String message);

        void deletePost(String postId);

        void unlike(String postId);

        void like(String postId);

        void follow(String followingId);

        void unfollow(String followingId);

        void postLike(String postId);

        void postDislike(String postId);
    }
}
