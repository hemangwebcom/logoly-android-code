package chat.hola.com.howdoo.profileScreen.tag;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.profileScreen.story.model.StoryData;

/**
 * Created by ankit on 23/2/18.
 */

public interface TagContract {

    interface View extends BaseView{

        void setupRecyclerView();

        void showTrendingImages(List<StoryData> trendingImages);
    }

    interface Presenter extends BasePresenter<TagContract.View>{

        void init();

        void loadTagImages();

    }
}
