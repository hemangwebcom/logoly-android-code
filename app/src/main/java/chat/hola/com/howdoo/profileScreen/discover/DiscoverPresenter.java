package chat.hola.com.howdoo.profileScreen.discover;

import javax.inject.Inject;

import chat.hola.com.howdoo.models.NetworkConnector;

/**
 * <h>DiscoverPresenter</h>
 *
 * @author 3Embed.
 * @since 2/3/18.
 */

public class DiscoverPresenter implements DiscoverContract.Presenter {

    @Inject
    DiscoverContract.View view;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    public DiscoverPresenter() {
    }

    @Override
    public void init() {
        view.applyFont();
        view.viewPagerSetup();
        view.tabSetup();

    }

    @Override
    public void changeText(String text) {
        view.changeSkipText(text);
    }
}
