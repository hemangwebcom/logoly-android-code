package chat.hola.com.howdoo.club.clubs;

import android.app.Activity;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>BlockUserModule</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

@ActivityScoped
@Module
public interface ClubModule {
    @ActivityScoped
    @Binds
    ClubContract.Presenter presenter(ClubPresenter presenter);

    @ActivityScoped
    @Binds
    ClubContract.View view(ClubActivity activity);

    @ActivityScoped
    @Binds
    Activity activity(ClubActivity activity);

}
