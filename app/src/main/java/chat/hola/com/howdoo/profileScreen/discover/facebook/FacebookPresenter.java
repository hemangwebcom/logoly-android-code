package chat.hola.com.howdoo.profileScreen.discover.facebook;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverActivity;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.Contact;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.FollowResponse;
import chat.hola.com.howdoo.profileScreen.discover.facebook.apiModel.FbContactResponse;
import chat.hola.com.howdoo.profileScreen.discover.facebook.fbModel.Data;
import chat.hola.com.howdoo.profileScreen.discover.facebook.fbModel.FacebookResponse;
import chat.hola.com.howdoo.profileScreen.discover.follow.Follow;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h>FacebookPresenter</h>
 * @author 3Embed.
 * @since 02/03/18.
 */

public class FacebookPresenter implements FacebookContract.Presenter {

    private static final String TAG = FacebookPresenter.class.getSimpleName();

    @Nullable
    private FacebookContract.View view;

    @Inject
    HowdooService service;



    @Inject
    DiscoverActivity activity;



    @Inject
    public FacebookPresenter() {
    }


    @Override
    public void attachView(FacebookContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void init() {
        if (view != null) {
            view.applyFont();
            view.initPostRecycler();
            view.setFbVisibility(true);
            view.setContactVisibility(false);
        }
    }

    @Override
    public void loadFollows() {

        //need to load data from api

        ArrayList<Contact> contacts = new ArrayList<>();
        Follow follow = new Follow(200, contacts);
        if (view != null)
            view.showFollows(follow);
    }

    @Override
    public void setFbVisibility(boolean show) {
        if (view != null)
            view.setFbVisibility(show);
    }

    @Override
    public void setContactVisibility(boolean show) {
        if (view != null)
            view.setContactVisibility(show);
    }

    @Override
    public void fbLogin() {

    }

    @Override
    public void returnFbResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public void follow(String followingId) {
        Map<String,Object> params = new HashMap<>();
        params.put("followingId",followingId);
        service.follow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> body) {
                        Log.w(TAG, body.toString());
                        //view.invalidateBtn(index,true);
                        Log.w(TAG, "followed successfully");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "failed to follow: " + e.getMessage());
                        //view.invalidateBtn(index,false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public void unFollow(String followingId) {
        service.unfollow(AppController.getInstance().getApiToken(), "en",
                followingId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> body) {
                        Log.w(TAG, body.toString());
                        Log.w(TAG, "unfollowed successfully");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "failed to unFollow: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void followAll(List<String> strings) {
        Map<String, List<String>> map = new HashMap<>();
        map.put("followeeId", strings);
        service.followAll(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.followedAll(true);
                        else if (response.code() == 401)
                            view.sessionExpired();
                        else view.followedAll(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.followedAll(false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void fetchFriendList() {
        if(view != null) {
            view.isDataLoading(true);
            view.showEmptyUi(false);
        }
        GraphRequest.Callback callback = new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                if(view != null)
                    view.isDataLoading(false);
                if(response != null  && response.getJSONObject() != null) {
                    Log.w(TAG, "fbResponse: " + response.getJSONObject().toString());
                    FacebookResponse fbResponse = new Gson().fromJson(response.getJSONObject().toString(), FacebookResponse.class);
                    Log.w(TAG, "friendCount: " + fbResponse.getSummary().getTotalCount());
                    List<Data> friendList = fbResponse.getData();
                    List<String> fbFriendIds = new ArrayList<>();
                    if (friendList != null && !friendList.isEmpty()) {
                        for (Data data : friendList)
                            fbFriendIds.add(data.getId());
                        Map<String, List<String>> bodyMap = new HashMap<>();
                        bodyMap.put("facebookIds", fbFriendIds);
                        service.facebookFriend(AppController.getInstance().getApiToken(), Constants.LANGUAGE, bodyMap)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new DisposableObserver<Response<FbContactResponse>>() {
                                    @Override
                                    public void onNext(Response<FbContactResponse> responseBody) {
                                        if (responseBody.code() == 200) {
                                            Log.w(TAG, "filtered fb friend fetched successfully!!");
                                            Log.w(TAG, "data : " + responseBody.body());
                                            ArrayList<chat.hola.com.howdoo.profileScreen.discover.facebook.apiModel.Data>
                                                    dataList = responseBody.body().getData();
                                            if (dataList != null) {
                                                if (view != null) {
                                                    view.showFbContacts(dataList);
                                                    if (dataList.isEmpty())
                                                        view.showFbContactUi(true);
                                                    else
                                                        view.showFbContactUi(false);
                                                }
                                            }
                                        } else if (responseBody.code() == 401) {
                                            if(view != null)
                                                view.sessionExpired();
                                        }
                                        else if(responseBody.code() == 204){
                                            if(view != null)
                                                view.showEmptyUi(true);
                                        }
                                        else{
                                            if(view != null)
                                                view.showFbContactUi(true);
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.e(TAG, "failed to fetch fb friend from api!!: " + e.getMessage());
                                        if (view != null)
                                            view.showFbContactUi(true);
                                    }

                                    @Override
                                    public void onComplete() {

                                    }
                                });
                    } else {
                        Log.e(TAG, "failed to get fb friend!!");
                        if (view != null)
                            view.showFbContactUi(true);
                    }
                }
                else{
                    Log.e(TAG, "fb featch friend list response can not be null");
                    if(view != null) {
                        view.showFbContactUi(true);
                    }
                }
            }
        };
    }

    @Override
    public void checkForFbLogin() {

    }


}
