package chat.hola.com.howdoo.club.detail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.policy.TimeWindow;
import com.howdoo.dubly.R;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.ApiOnServer;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.UriUtil;
import chat.hola.com.howdoo.Utilities.Utilities;
import chat.hola.com.howdoo.club.clubs.ClubResponse;
import chat.hola.com.howdoo.club.create.CreateClubContract;
import chat.hola.com.howdoo.home.model.Posts;
import chat.hola.com.howdoo.manager.session.SessionManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by ankit on 22/2/18.
 */

public class ClubDetailPresenter implements ClubDetailContract.Presenter {

    private final String TAG = ClubDetailPresenter.class.getSimpleName();

    @Inject
    ClubDetailContract.View view;
    @Inject
    HowdooService service;
    @Inject
    SessionManager sessionManager;
    @Inject
    Context context;

    @Inject
    public ClubDetailPresenter() {
    }


    @Override
    public void clubDetail(String clubId) {
        service.getClubDetails(AppController.getInstance().getApiToken(), Constants.LANGUAGE, clubId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ClubDetailResponse>>() {
                    @Override
                    public void onNext(Response<ClubDetailResponse> response) {
                        switch (response.code()) {
                            case 200:
                                view.displayDetail(response.body().getClub());
                                break;
                            case 401:
                                view.sessionExpired();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void clubPosts(String clubId) {
        service.getClubPosts(AppController.getInstance().getApiToken(), Constants.LANGUAGE, clubId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Posts>>() {
                    @Override
                    public void onNext(Response<Posts> response) {
                        switch (response.code()) {
                            case 200:
                                view.setPosts(response.body().getData());
                                break;
                            case 401:
                                view.sessionExpired();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void deleteClub(String clubId) {
        service.deleteClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, clubId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        view.deleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void like(String postId, boolean like) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", AppController.getInstance().getUserId());
        service.postLike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void dislike(String postId, boolean like) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", AppController.getInstance().getUserId());
        service.postDislike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void favorite(String postId, boolean like) {
        if (like)
            fav(postId);
        else
            unFav(postId);
    }

    public void fav(String postId) {
        Map<String, String> map = new HashMap<>();
        map.put("userId", AppController.getInstance().getUserId());
        service.like(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    public void unFav(String postId) {
        Map<String, String> map = new HashMap<>();
        map.put("userId", AppController.getInstance().getUserId());
        service.unlike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
