package chat.hola.com.howdoo.club.clubs;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.club.detail.ClubDetailActivity;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>ClubActivity</h1>
 * <p>All theMembers appears on this screen.
 * User can add newMember also</p>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class ClubActivity extends DaggerAppCompatActivity implements ClubContract.View {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;

    private Unbinder unbinder;
    private String postId;

    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    ClubContract.Presenter memberPresenter;
    @Inject
    ClubAdapter adapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvCommentList)
    RecyclerView rvCommentList;

    @BindView(R.id.tvTbTitle)
    TextView tvTbTitle;
    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;

    @BindView(R.id.btnSelectClub)
    Button btnSelectClub;

    private LinearLayoutManager layoutManager;
    private boolean isSelector = false;
    private String clubId;
    private String clubName;
    private String userId;
    private int access;

    AlertDialog.Builder builder;
    AlertDialog delete_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.club_activity);
        unbinder = ButterKnife.bind(this);

        layoutManager = new LinearLayoutManager(this);
        tvTbTitle.setTypeface(AppController.getInstance().getSemiboldFont());
        rvCommentList.setLayoutManager(layoutManager);
        rvCommentList.addOnScrollListener(recyclerViewOnScrollListener);
        adapter.setListener(memberPresenter.getPresenter());
        rvCommentList.setAdapter(adapter);

        userId = getIntent().getStringExtra("userId");
        if (userId == null)
            userId = AppController.getInstance().getUserId();

        access = getIntent().getIntExtra("access", 0) + 1;
        isSelector = getIntent().getBooleanExtra("forSelect", false);
        adapter.isForSelection(isSelector);
        memberPresenter.setForSelect(isSelector);
        btnSelectClub.setVisibility(isSelector ? View.VISIBLE : View.GONE);
        toolbarSetup();

        builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.confirm));
    }

    @Override
    protected void onResume() {
        super.onResume();
        memberPresenter.getClubs(access, 0, PAGE_SIZE, userId);
    }

    @OnClick(R.id.btnSelectClub)
    public void selectClub() {
        if (!TextUtils.isEmpty(clubId)) {
            Intent intent = new Intent();
            intent.putExtra("club", clubName);
            intent.putExtra("clubId", clubId);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Toast.makeText(this, "Please select club", Toast.LENGTH_SHORT).show();
        }
    }

    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {
        if (sessionManager != null)
            sessionManager.sessionExpired(this);
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void userBlocked() {

    }

    @Override
    public void reload() {
        memberPresenter.getClubs(access, 0, PAGE_SIZE, userId);
    }


    @Override
    public void openChannel(String clubId) {
        startActivity(new Intent(this, ClubDetailActivity.class).putExtra("clubId", clubId));
    }

    @Override
    public void getClubId(String clubName, String clubId) {
        this.clubId = clubId;
        this.clubName = clubName;
    }

    @Override
    public void confirmDelete(int position) {
        builder.setPositiveButton("Yes", (dialogInterface, i) -> memberPresenter.deleteClub(position));
        builder.setNegativeButton("No", (dialogInterface, i) -> delete_confirm.dismiss());
        delete_confirm = builder.create();
        delete_confirm.show();
    }


    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int[] firstVisibleItemPositions = new int[PAGE_SIZE];
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            memberPresenter.callApiOnScroll(access, postId, firstVisibleItemPosition, visibleItemCount, totalItemCount, userId);
        }
    };

}
