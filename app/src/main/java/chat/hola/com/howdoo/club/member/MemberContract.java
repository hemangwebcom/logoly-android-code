package chat.hola.com.howdoo.club.member;

import java.util.List;
import java.util.Map;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.comment.model.ClickListner;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;

/**
 * <h1>BlockUserContract</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public interface MemberContract {

    interface View extends BaseView {
        /**
         * redirects to  @{@link chat.hola.com.howdoo.profileScreen.ProfileActivity})
         *
         * @param userId : userId to get user details
         */
        void openProfile(String userId);

        void clubCreated();

        void showPopup(String userId, boolean isBlocked);

        void reloadData();

        void memberAdded();
    }

    interface Presenter {

        /**
         * gets lis of members of particular post
         */
        void getMembers(int offset, int limit);


        /**
         * perform the click actions
         */
        chat.hola.com.howdoo.club.member.ClickListner getPresenter();


        void callApiOnScroll(String postId, int firstVisibleItemPosition, int visibleItemCount, int totalItemCount);

        void createClub(Map<String, Object> params);

        void getClubMembers(String clubId);

        void removeFromClub(String clubId, String userId);

        void blockFromClub(String clubId, String userId);

        void unblockFromClub(String clubId, String userId);

        void getClubPendingRequest(String clubId);

        void addMembers(String clubId);

        void setMembers(List<String> members);
    }
}
