package chat.hola.com.howdoo.ViewHolders;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.howdoo.dubly.R;

import chat.hola.com.howdoo.AppController;


/**
 * Created by moda on 19/06/17.
 */


public class ViewHolderSelectUser extends RecyclerView.ViewHolder {
    public TextView userName, email;
    public ImageView userImage;

    public ViewHolderSelectUser(View view) {
        super(view);


        userName = (TextView) view.findViewById(R.id.userName);
        email = (TextView) view.findViewById(R.id.userIdentifier);

        userImage = (ImageView) view.findViewById(R.id.userImage);

        Typeface tf = AppController.getInstance().getRegularFont();
        userName.setTypeface(tf, Typeface.NORMAL);
        email.setTypeface(tf, Typeface.NORMAL);

    }
}
