package chat.hola.com.howdoo.profileScreen.discover;


import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.profileScreen.discover.contact.ContactAdapter;
import chat.hola.com.howdoo.profileScreen.discover.follow.Follow;
import chat.hola.com.howdoo.profileScreen.discover.follow.FollowAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * <h>DiscoverUtilModule</h>
 * @author 3Embed.
 * @since 02/03/18.
 */

@ActivityScoped
@Module
public class DiscoverUtilModule {

    @ActivityScoped
    @Provides
    FollowAdapter followAdapter(DiscoverActivity context, TypefaceManager typefaceManager){
        return new FollowAdapter(context , typefaceManager);
    }

    @ActivityScoped
    @Provides
    ContactAdapter contactAdapter(DiscoverActivity context, TypefaceManager typefaceManager){
        return new ContactAdapter(context , typefaceManager);
    }

//    @ActivityScoped
//    @Provides
//    FacebookQueryManager facebookQueryManager() {
//        return new FacebookFactory();
//    }
}
