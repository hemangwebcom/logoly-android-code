package chat.hola.com.howdoo.Utilities;

/**
 * Created by DELL on 4/3/2018.
 */


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.media.MediaCodec;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.audio.AudioCapabilities;
import com.google.android.exoplayer.audio.AudioCapabilitiesReceiver;
import com.google.android.exoplayer.audio.AudioTrack;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.Util;
import com.howdoo.dubly.R;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * <h2>CustomVideoSurfaceView</h2>
 * <p>
 * This the video player it uses google exo player in android play the video.
 * it has its won view . That view is used where ever you want play the video.
 * It manges the progress bar for the video buffer .
 * It provide play and pause of the video play and the manges the video play.
 * </P>
 *
 * @author 3Embed.
 * @version 1.1
 * @since 13/04/2017.
 */
public class CustomVideoSurfaceView extends FrameLayout implements AudioCapabilitiesReceiver.Listener, MediaCodecVideoTrackRenderer.EventListener, MediaCodecAudioTrackRenderer.EventListener, TextureView.SurfaceTextureListener {
    private String TAG = "PicoPlayer";
    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static CustomVideoSurfaceView instance;
    private AudioCapabilitiesReceiver audioCapabilitiesReceiver;
    private ExoPlayer player;
    private static final CookieManager defaultCookieManager;
    private ExtractorSampleSource sampleSource;
    private Handler mainHandler;
    private Allocator allocator;
    private DataSource dataSource;
    private Uri currentUri;
    private Context appContext;
    private int defaultWidth = 0;
    private TextureView videoSurfaceView;
    private MediaCodecVideoTrackRenderer videoRenderer;
    private MediaCodecAudioTrackRenderer audioRenderer;
    private boolean surfaceViewViable = false;
    public int position = 0;
    private Listner listner;

    public void setListner(Listner listner) {
        this.listner = listner;
    }

    public interface Listner {
        void onEnd();
    }
    // private Video_Double_callback callback;

    static {
        defaultCookieManager = new CookieManager();
        defaultCookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    private ImageView sound_suffel;

    /*
    * double click listener.*/
    public void setDouble_callback(Video_Double_callback mcallback) {
        //   this.callback = mcallback;
    }

    /*
    * Preparing the video player with the given url and start paling the video.
    * setting the video player uri.*/
    public void startPlayer(Uri uri) {
        currentUri = uri;
        player.seekTo(0);
        sampleSource = new ExtractorSampleSource(uri, dataSource, allocator, 10 * BUFFER_SEGMENT_SIZE);
        videoRenderer = new MediaCodecVideoTrackRenderer(sampleSource,
                MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT, -1, mainHandler, this, -1);
        audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource, mainHandler, this);
        player.prepare(videoRenderer, audioRenderer);
        player.setPlayWhenReady(true);
        playVideo();
    }

    /*
     *Providing the surface texture to the video player
     *and starting the video play.  */
    private void playVideo() {
        if (surfaceViewViable) {
            SurfaceTexture surfaceTexture = videoSurfaceView.getSurfaceTexture();
            Surface surface = new Surface(surfaceTexture);
            // setMute(true);
            player.sendMessage(videoRenderer,
                    MediaCodecVideoTrackRenderer.MSG_SET_SURFACE,
                    surface);
        }
    }


    public boolean isMuted = false;

    public void setMute(boolean toMute) {
        if (player != null) {
            if (toMute) {
                player.sendMessage(audioRenderer, MediaCodecAudioTrackRenderer.MSG_SET_VOLUME, 0f);
                isMuted = true;
                sound_suffel.setSelected(false);
            } else {
                player.sendMessage(audioRenderer, MediaCodecAudioTrackRenderer.MSG_SET_VOLUME, 1f);
                sound_suffel.setSelected(true);
                isMuted = false;
            }
        }
    }

    /*
     * Releasing the video player.*/
    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    /**
     * release memory and other resources.
     */
    public void onRelease() {
        try {
            releasePlayer();
            audioCapabilitiesReceiver.unregister();
            if (mainHandler != null) {
                mainHandler = null;
            }
            allocator = null;
            dataSource = null;
            videoRenderer = null;
            audioRenderer = null;
            sampleSource = null;
            instance = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static CustomVideoSurfaceView getInstance(Context context) {
        if (instance != null) {
            return instance;
        } else {
            instance = new CustomVideoSurfaceView(context);
            return instance;
        }
    }

    public CustomVideoSurfaceView(Context context) {
        super(context);
        initialize(context);
    }

    private CustomVideoSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private CustomVideoSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }


    public void onRestartPlayer() {
        makePlayer();
        if (currentUri != null) {
            startPlayer(currentUri);
        }
    }

    /*
    * double tap listener.*/
//    DoubleClickListener doubleClickListener = new DoubleClickListener() {
//        @Override
//        public void onSingleClick(View view) {
//            // player.setPlayWhenReady(!player.getPlayWhenReady());
//        }
//
//        @Override
//        public void onDoubleClick(View view) {
//            callback.onDoubleClick(position);
//        }
//    };

    @SuppressLint("InflateParams")
    protected void initialize(Context context) {
        appContext = context.getApplicationContext();
         /*
         * view changed updated.*/
        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Log.d(TAG, "Yes Focus");
                } else {
                    Log.d(TAG, "Yes no focus");
                }
            }
        });
        setVisibility(INVISIBLE);
        Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        defaultWidth = point.x;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addView(inflater.inflate(R.layout.exoplayer_video_surface_view, null));
        videoSurfaceView = (TextureView) this.findViewById(R.id.video_surface_view);
        sound_suffel = (ImageView) this.findViewById(R.id.sound_suffle);
        sound_suffel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setMute(!isMuted);
            }
        });
        initializeVideoPlayer();
    }

    /*
     *initialization of the required resources for the player .
     * and allocating the buffer for the video player.  */
    private void initializeVideoPlayer() {
        mainHandler = new Handler();
        allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        dataSource =
                new DefaultUriDataSource(appContext,
                        new DefaultBandwidthMeter(mainHandler, null),
                        Util.getUserAgent(appContext, "PicogramExpo"));

        videoSurfaceView.setSurfaceTextureListener(this);

        CookieHandler currentHandler = CookieHandler.getDefault();
        if (currentHandler != defaultCookieManager) {
            CookieHandler.setDefault(defaultCookieManager);
        }
        audioCapabilitiesReceiver = new AudioCapabilitiesReceiver(appContext, this);
        audioCapabilitiesReceiver.register();
        makePlayer();
    }


    private void makePlayer() {
        if (player != null) {
            return;
        }

        player = ExoPlayer.Factory.newInstance(2);
        player.addListener(new ExoPlayer.Listener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                String text = "playWhenReady=" + playWhenReady + ", playbackState=";
                switch (playbackState) {
                    case ExoPlayer.STATE_BUFFERING:
                        text += "buffering";
                        setVisibility(VISIBLE);
                        videoSurfaceView.setAlpha(0);
                        videoSurfaceView.setOnClickListener(null);
                        break;
                    case ExoPlayer.STATE_ENDED:
                        // player.seekTo(0);
                        listner.onEnd();
                        text += "ended";
                        break;
                    case ExoPlayer.STATE_IDLE:
                        text += "idle";
                        break;
                    case ExoPlayer.STATE_PREPARING:
                        text += "preparing";
                        break;
                    case ExoPlayer.STATE_READY:
                        //  videoSurfaceView.setOnClickListener(doubleClickListener);
                        videoSurfaceView.setAlpha(1);
                        text += "ready";
                        break;
                    default:
                        text += "unknown";
                        break;
                }

                Log.d(TAG, text);
            }

            @Override
            public void onPlayWhenReadyCommitted() {
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                Log.d(TAG, "somethingwrong:" + "onPlayerError:" + error.toString());

            }
        });


    }


    public void restartPlayer() {
        if (player != null)
            player.seekTo(0);
    }

    public void stopPlayer() {
        if (player != null) {
            player.stop();
        }
    }


    protected void calculateAspectRatio(int width, int height) {
        int viewWidth = defaultWidth;
        int viewHeight = defaultWidth;
        float aspect = (float) width / height;
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        if (((float) viewWidth / width) > ((float) viewHeight / height)) {
            layoutParams.width = (int) (viewHeight * aspect + 1F);
            layoutParams.height = viewHeight;
        } else {
            layoutParams.width = viewWidth;
            layoutParams.height = (int) (viewWidth / aspect + 1F);
        }
        layoutParams.gravity = Gravity.CENTER;
        Log.d(TAG, "calculateAspectRatio:" + layoutParams.width + "--" + layoutParams.height);
    }

    @Override
    public void onDroppedFrames(int count, long elapsed) {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        //calculateAspectRatio(width, height);
    }

    @Override
    public void onDrawnToSurface(Surface surface) {
    }

    @Override
    public void onDecoderInitializationError(MediaCodecTrackRenderer.DecoderInitializationException e) {
    }

    @Override
    public void onCryptoError(MediaCodec.CryptoException e) {
    }

    @Override
    public void onDecoderInitialized(String decoderName, long elapsedRealtimeMs, long initializationDurationMs) {
    }

    @Override
    public void onAudioCapabilitiesChanged(AudioCapabilities audioCapabilities) {
    }

    @Override
    public void onAudioTrackInitializationError(AudioTrack.InitializationException e) {
        Log.d(TAG, "somethingwrong:" + "onAudioTrackInitializationError:" + e.toString());
    }

    @Override
    public void onAudioTrackWriteError(AudioTrack.WriteException e) {
        Log.d(TAG, "somethingwrong:" + "onAudioTrackWriteError:" + e.toString());
    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        surfaceViewViable = true;
        playVideo();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        surfaceViewViable = false;
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    /*
    * double tap call back*/
    public interface Video_Double_callback {
        void onDoubleClick(int position);
    }
}

