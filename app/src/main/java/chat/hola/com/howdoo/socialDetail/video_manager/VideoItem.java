package chat.hola.com.howdoo.socialDetail.video_manager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.howdoo.dubly.R;
import com.squareup.picasso.Picasso;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Utilities.CountFormat;
import chat.hola.com.howdoo.Utilities.OnSwipeTouchListener;
import chat.hola.com.howdoo.Utilities.TagSpannable;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.socialDetail.ViewHolder;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 11/30/2018.
 */

public class VideoItem extends BaseVideoItem {
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private final Picasso mImageLoader;
    private final Data data;
    private int likes;
    private String userId;
    private String postId;
    private String musicId;
    private String channelId;
    private String categoryId;
    private Context context;
    private int dislikes;
    private int favoriteCount;

    public VideoItem(VideoPlayerManager videoPlayerManager, Picasso imageLoader, Data data, Context context) {
        super(videoPlayerManager);
        mImageLoader = imageLoader;
        this.data = data;
        this.context = context;
    }

    @Override
    public void update(int position, ViewHolder viewHolder, VideoPlayerManager videoPlayerManager) {
        viewHolder.mCover.setVisibility(View.VISIBLE);
        mImageLoader.load(data.getImageUrl1().replace("mp4", "jpg")).into(viewHolder.mCover);
        displayData(viewHolder, data);
    }

    @Override
    public void playNewVideo(MetaData currentItemMetaData, VideoPlayerView player, VideoPlayerManager<MetaData> videoPlayerManager) {
//        if (data.getMediaType1() == 1)
        videoPlayerManager.playNewVideo(currentItemMetaData, player, data.getImageUrl1());
    }

    @Override
    public void stopPlayback(VideoPlayerManager videoPlayerManager) {
        videoPlayerManager.stopAnyPlayback();
    }

    //display data
    @SuppressLint("ClickableViewAccessibility")
    public void displayData(ViewHolder holder, Data data) {
        userId = data.getUserId();
        likes = Integer.parseInt(data.getLikesCount());
        dislikes = Integer.parseInt(data.getDislikesCount());
        favoriteCount = Integer.parseInt(data.getFavouritesCount());
        postId = data.getPostId();

        holder.ivProfilePic.setImageURI(data.getProfilepic());
        holder.tvUserName.setText("@" + data.getUsername());
        holder.tvViewCount.setText(CountFormat.format(Long.parseLong(data.getDistinctViews())));
        holder.tvLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(likes))));
        holder.tvDisLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(dislikes))));
        holder.tvFavouriteCount.setText(CountFormat.format(Long.parseLong(String.valueOf(favoriteCount))));
        holder.tvCommentCount.setText(CountFormat.format(Long.parseLong(data.getCommentCount())));

        holder.ivLike.setChecked(data.isLiked());
        holder.ivDisLike.setChecked(data.isDisliked());
        holder.ivFavourite.setChecked(data.isFavourite());

        //music
        if (data.getDub() != null) {
            holder.llMusic.setVisibility(View.VISIBLE);
            holder.tvMusic.setText(data.getDub().getName());
            musicId = data.getDub().getId();
        }

        //title
        if (data.getTitle() != null && !data.getTitle().trim().equals("")) {
            holder.tvTitle.setVisibility(View.VISIBLE);
            SpannableString spanString = new SpannableString(data.getTitle());
            Matcher matcher = Pattern.compile("#([A-Za-z0-9_-]+)").matcher(spanString);
            findMatch(spanString, matcher);
            Matcher userMatcher = Pattern.compile("@([A-Za-z0-9_-]+)").matcher(spanString);
            findMatch(spanString, userMatcher);
            holder.tvTitle.setText(spanString);
            holder.tvTitle.setMovementMethod(LinkMovementMethod.getInstance());
        }

        //location
        if (data.getPlace() != null && !data.getPlace().trim().equals("")) {
            holder.tvLocation.setVisibility(View.VISIBLE);
            holder.tvLocation.setText(data.getPlace());
        }

        //divide
        if (!data.getChannelName().isEmpty() && !data.getCategoryName().isEmpty())
            holder.tvDivide.setVisibility(View.VISIBLE);

        //category
        if (data.getCategoryName() != null && !data.getCategoryName().trim().equals("")) {
            holder.tvCategory.setVisibility(View.VISIBLE);
            holder.tvCategory.setText(data.getCategoryName());
            categoryId = data.getCategoryId();
        }

        //channel
        if (data.getChannelImageUrl() != null) {
            holder.tvChannel.setVisibility(View.VISIBLE);
            holder.tvChannel.setText(data.getChannelName());
            channelId = data.getChannelId();
        }

    }

    private void findMatch(SpannableString spanString, Matcher matcher) {
        while (matcher.find()) {
            final String tag = matcher.group(0);
            spanString.setSpan(new TagSpannable(context, tag, R.color.color_white), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

}
