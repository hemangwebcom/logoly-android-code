package chat.hola.com.howdoo.home.activity.followingTab;

import android.view.View;

import java.util.ArrayList;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.home.activity.followingTab.model.Following;

/**
 * @author 3Embed.
 * @since 21/2/18.
 */

public interface FollowingContract {

    interface View extends BaseView {

        void initPostRecycler();

        void showFollowings(ArrayList<Following> followings);

        void onUserClicked(String userId);

        void onMediaClick(int position, android.view.View view);
    }

    interface Presenter extends BasePresenter<FollowingContract.View> {

        void init();

        void loadFollowing();
    }
}
