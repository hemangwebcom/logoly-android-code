package chat.hola.com.howdoo.club.member;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.comment.model.Comment;
import chat.hola.com.howdoo.comment.model.CommentAdapter;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Module;
import dagger.Provides;

/**
 * <h1>BlockUserUtilModule</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */
@ActivityScoped
@Module
public class MemberUtilModule {

    @ActivityScoped
    @Provides
    List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> getMembers() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    MemberAdapter memberAdapter(List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> data, Activity mContext, TypefaceManager typefaceManager) {
        return new MemberAdapter(data, mContext, typefaceManager);
    }
}
