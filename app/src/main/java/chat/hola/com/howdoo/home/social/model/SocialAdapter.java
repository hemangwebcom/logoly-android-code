package chat.hola.com.howdoo.home.social.model;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.NativeAd;
import com.howdoo.dubly.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.TagSpannable;
import chat.hola.com.howdoo.Utilities.TimeAgo;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.Utilities.UserSpannable;
import chat.hola.com.howdoo.home.model.Data;

/**
 * <h1>SocialAdapter</h1>
 *
 * @author 3Embed
 * @since 24/2/2018.
 */

public class SocialAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private TypefaceManager typefaceManager;
    private Context mContext;
    private List<Data> dataList;
    private ClickListner clickListner;
    private ConstraintSet set = new ConstraintSet();

    @Inject
    public SocialAdapter(List<Data> data, Activity mContext, TypefaceManager typefaceManager) {
        this.dataList = data;
        this.mContext = mContext;
        this.typefaceManager = typefaceManager;
    }

    @Override
    public int getItemViewType(int position) {
        return dataList.get(position).getAd() != null ? 1 : 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = viewType == 0 ? LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_social_row, parent, false) :
                LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_social_ad, parent, false);
        return viewType == 0 ? new ViewHolder(itemView, typefaceManager, clickListner) : new AdViewHolder(itemView, typefaceManager);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        try {
            if (getItemViewType(position) == 0) {
                ViewHolder viewHolder = (ViewHolder) holder;
                initHolder(viewHolder);
            } else {
                AdViewHolder viewHolder = (AdViewHolder) holder;
                StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) viewHolder.itemView.getLayoutParams();
                layoutParams.setFullSpan(true);
                initAdHolder(viewHolder, (NativeAd) dataList.get(position).getAd());
            }
        } catch (Exception ignored) {

        }
    }

    private void initAdHolder(AdViewHolder holder, NativeAd nativeAd) {


        nativeAd.unregisterView();


        // Add the AdChoices icon
        holder.adChoicesContainer.removeAllViews();
        AdChoicesView adChoicesView = new AdChoicesView(mContext, nativeAd, true);
        holder.adChoicesContainer.addView(adChoicesView, 0);


        // Set the Text.
        holder.nativeAdTitle.setText(nativeAd.getAdvertiserName());
        holder.nativeAdBody.setText(nativeAd.getAdBodyText());
        holder.nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        holder.nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        holder.nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        holder.sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(holder.nativeAdTitle);
        clickableViews.add(holder.nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                holder.itemView,
                holder.nativeAdMedia,
                holder.nativeAdIcon,
                clickableViews);
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    private void findMatch(SpannableString spanString, Matcher matcher) {
        while (matcher.find()) {
            final String tag = matcher.group(0);
            spanString.setSpan(new TagSpannable(mContext, tag), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    private void initHolder(ViewHolder holder) {
        try {
            if (getItemCount() > 0) {
                int position = holder.getAdapterPosition();
                final Data data = dataList.get(position);

                holder.profileNameTv.setText(data.getUsername());
                SpannableString spanString = new SpannableString(data.getTitle());
                Matcher matcher = Pattern.compile("#([A-Za-z0-9_-]+)").matcher(spanString);
                findMatch(spanString, matcher);
                Matcher userMatcher = Pattern.compile("@([A-Za-z0-9_-]+)").matcher(spanString);
                findMatch(spanString, userMatcher);
                holder.tvTitle.setText(spanString);
                holder.tvTitle.setMovementMethod(LinkMovementMethod.getInstance());
                holder.postTimeTv.setText(TimeAgo.getTimeAgo(Long.valueOf(data.getTimeStamp())));

                holder.tvLikeCount.setText(data.getLikesCount());
                holder.tvDislikeCount.setText(data.getDislikesCount());
                holder.tvFavouriteCount.setText(data.getFavouritesCount());
                holder.tvCommentCount.setText(data.getCommentCount());
                holder.tvViewCount.setText(data.getDistinctViews());

                holder.llChannelContainer.setVisibility(!data.getChannelImageUrl().isEmpty() ? View.VISIBLE : View.GONE);
                if (!data.getChannelImageUrl().isEmpty()) {
                    Glide.with(mContext).load(data.getChannelImageUrl())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .dontAnimate()
                            .placeholder(mContext.getResources().getDrawable(R.drawable.ic_default))
                            .into(holder.ivChannel);
                    holder.tvChannelName.setText(data.getChannelName() != null && !data.getChannelName().equals("") ? data.getChannelName() : "");
                }

                if (!data.getProfilepic().isEmpty())
                    Glide.with(mContext).load(data.getProfilepic()).asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(mContext.getResources().getDrawable(R.drawable.profile_one))
                            .into(holder.ivProfilePic);

                String url = data.getImageUrl1();
                if (!TextUtils.isEmpty(data.getImageUrl1())) {
                    holder.ivVideoCam.setVisibility(data.getMediaType1() == 0 ? View.GONE : View.VISIBLE);
                    if (data.getMediaType1() == 1) {
                        //video
                        DrawableRequestBuilder<String> thumbnail = Glide.with(mContext).load(data.getImageUrl1().replace("upload/", "upload/vs_20,e_loop/").replace("mp4", "jpg").replace(".mov", ".jpg")).diskCacheStrategy(DiskCacheStrategy.ALL);
                        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(holder.ivMedia);
                        Glide.with(mContext).load(url.replace("upload/", "upload/vs_20,e_loop/")
                                .replace("mp4", "gif").replace(".mov", ".gif"))
                                .placeholder(mContext.getResources().getDrawable(R.drawable.ic_default))
                                .thumbnail(thumbnail)
                                .into(imageViewTarget);
                    } else {
                        //image
                        Glide.with(mContext).load(url)
                                .dontAnimate()
                                .placeholder(mContext.getResources().getDrawable(R.drawable.ic_default))
                                .into(holder.ivMedia);
                    }
                }
//                String ratio = String.format("%d:%d", data.getImageUrl1Width(), data.getImageUrl1Height());
//                set.clone(holder.mConstraintLayout);
//                set.setDimensionRatio(holder.ivProfilePic.getId(), ratio);
//                set.applyTo(holder.mConstraintLayout);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.d("Error", "initHolder: " + e.toString());
        } catch (Exception e) {
            Log.d("Error", "initHolder: " + e.toString());
        }
    }


}