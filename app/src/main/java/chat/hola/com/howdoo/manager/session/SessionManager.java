package chat.hola.com.howdoo.manager.session;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.widget.Toast;

import com.couchbase.lite.android.AndroidContext;
import com.facebook.AccessToken;
import com.google.firebase.messaging.FirebaseMessaging;
import com.howdoo.dubly.R;

import java.util.Set;

import javax.inject.Inject;

import chat.hola.com.howdoo.Activities.MainActivity;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Database.CouchDbController;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.manager.account.AccountGeneral;

/**
 * <h1>SessionManager</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 2/28/2018.
 */

public class SessionManager {
    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private boolean isLogout = false;




    @SuppressLint("CommitPrefEdits")
    @Inject
    public SessionManager(Context context) {
        this.context = context;
        this.preferences = context.getSharedPreferences("sessionManager", Context.MODE_PRIVATE);
        this.editor = this.preferences.edit();
    }

    public String getUserName() {
        return preferences.getString("userName", "");
    }

    public void setUserName(String username) {
        editor.putString("userName", username);
        editor.apply();
    }

    public String getUserProfilePic() {
        return preferences.getString("userProfilePic", Constants.PROFILE_PIC_SHAPE);
    }

    public void setUserProfilePic(String url) {
        editor.putString("userProfilePic", url.replace("upload/", Constants.PROFILE_PIC_SHAPE));
        editor.apply();
    }

    public String getAdresses() {
        return preferences.getString("adresses", "");
    }

    public void setAdresses(String adresses) {
        editor.putString("adresses", adresses);
        editor.apply();
    }

    public void clear() {
        editor.clear();
    }

    public void logOut(Context context) {
        isLogout = true;
        sessionExpired(context);
    }

    public void sessionExpired(Context context) {

        String topic = "/topics/" + AppController.getInstance().getUserId();
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
        clear();
        AppController.getInstance().setSignedIn(false);
        AppController.getInstance().setContactSynced(false);
        AppController.getInstance().unregisterContactsObserver();

        editor.putBoolean("isFacebookLogin", false);
        editor.apply();

        CouchDbController db = new CouchDbController(new AndroidContext(context));
        db.updateIndexDocumentOnSignOut(AppController.getInstance().getIndexDocId());

        AccountManager mAccountManager = AccountManager.get(context);
        Account[] accounts = mAccountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
        if (accounts.length > 0)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1)
                mAccountManager.removeAccountExplicitly(accounts[0]);

        Toast.makeText(context, isLogout ? R.string.logged_out : R.string.session_expired, Toast.LENGTH_SHORT).show();

        Intent i1 = new Intent(context, MainActivity.class);
        i1.setAction(Intent.ACTION_MAIN);
        i1.addCategory(Intent.CATEGORY_HOME);
        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i1.putExtra("caller", "PostActivity");
        context.startActivity(i1);
    }

    public boolean isHomeChanged() {
        return preferences.getBoolean("homedata", false);
    }

    public void setHomeChange(boolean b) {
        editor.putBoolean("homedata", b);
        editor.apply();
    }

    public String getCommentCount() {
        return preferences.getString("comments", "0");
    }

    public void setCommentCount(String count) {
        editor.putString("comments", count);
        editor.apply();
    }

    public void setFollowAll(boolean b) {
        editor.putBoolean("isFollowAll", b);
        editor.apply();
    }

    public boolean isFollowAll() {
        return preferences.getBoolean("isFollowAll", false);
    }

    public boolean isFacebookLogin() {
        return preferences.getBoolean("isFacebookLogin", false);
    }

    private void setFacebookLogin(boolean facebookLogin) {
        editor.putBoolean("isFacebookLogin", facebookLogin);
        editor.apply();
    }

    public String getFacebookAccessToken() {
        return preferences.getString("facebookAccessToken", null);
    }

    public void setFacebookAccessToken(AccessToken accessToken) {
        if (accessToken != null) {
            boolean isLoggedIn = !accessToken.isExpired();
            editor.putString("facebookAccessToken", accessToken.getToken());
            editor.apply();

            setFacebookLogin(isLoggedIn);

            //check permissions
            Set<String> permissions = accessToken.getPermissions();
            setFacebookFriendPermission(permissions.contains("user_friends"));
            setFacebookPublishPermission(permissions.contains("publish_actions"));
        }
    }

    private void setFacebookFriendPermission(boolean facebookFriendPermission) {
        editor.putBoolean("facebookFriendPermission", facebookFriendPermission);
        editor.apply();
    }

    private void setFacebookPublishPermission(boolean facebookPublishPermission) {
        editor.putBoolean("facebookPublishPermission", facebookPublishPermission);
        editor.apply();
    }

    public boolean isFacebookFriendPermission() {
        return preferences.getBoolean("facebookFriendPermission", false);
    }

    public boolean isFacebookPublishPermission() {
        return preferences.getBoolean("facebookPublishPermission", false);
    }

}
