package chat.hola.com.howdoo.socialDetail;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 8/20/2018.
 */
public class PostModel {

    @Inject
    PostModel() {

    }

    public Map<String, String> getParams(String postId) {
        Map<String, String> map = new HashMap<>();
        map.put("userId", AppController.getInstance().getUserId());
        return map;
    }

    public Map<String, String> getReasonParams(String postId, String reason, String message) {
        Map<String, String> map = new HashMap<>();
        map.put("postId", postId);
        map.put("message", message);
        map.put("reason", reason);
        return map;
    }
}