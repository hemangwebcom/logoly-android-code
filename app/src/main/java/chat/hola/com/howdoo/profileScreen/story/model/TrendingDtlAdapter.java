package chat.hola.com.howdoo.profileScreen.story.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.FlowLayout;
import chat.hola.com.howdoo.Utilities.TagSpannable;
import chat.hola.com.howdoo.Utilities.TimeAgo;
import chat.hola.com.howdoo.trendingDetail.TrendingDetail;

/**
 * <h>TrendingDtlAdapter.class</h>
 * <p>
 * This Adapter is used by {@link TrendingDetail} activity recyclerView.
 *
 * @author 3Embed
 * @since 14/2/18.
 */

public class TrendingDtlAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<StoryData> data;
    private String fragmentType;
    private ClickListner clickListner;

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public interface ClickListner {
        void onItemClick(int position, int type, View view);

        void onLikeClick(String postId, boolean like);

        void onDisLikeClick(String postId, boolean like);

        void onFavoriteClick(String postId, boolean like);

        void onCommentClick(String postId);
    }

    @Inject
    public TrendingDtlAdapter(Context context, List<StoryData> data) {
        this.context = context;
        this.data = data;
        this.fragmentType = fragmentType;

    }

    @Override
    public int getItemViewType(int position) {
        return fragmentType.equals("grid") ? 0 : 1;
    }


    public void setData(String fragmentType) {
        this.fragmentType = fragmentType;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0)
            return new ViewHolder1(LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_detail_item, parent, false));
        else
            return new ViewHolder2(LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_detail_item1, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final StoryData data = this.data.get(position);

        switch (holder.getItemViewType()) {
            case 0:
                ViewHolder1 holder1 = (ViewHolder1) holder;
                if (data.getMediaType1().equals("0")) {
                    holder1.ibPlay.setVisibility(View.GONE);
                    //image
                    Glide.with(context).load(data.getImageUrl1())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .dontAnimate()
                            .placeholder(context.getResources().getDrawable(R.drawable.ic_default))
                            .into(holder1.ivImage);
                } else {
                    //video
                    holder1.ibPlay.setVisibility(View.VISIBLE);
                    DrawableRequestBuilder<String> thumbnail = Glide.with(context).load(data.getImageUrl1().replace("upload/", "upload/vs_20,e_loop/").replace("mp4", "jpg").replace(".mov", ".jpg")).diskCacheStrategy(DiskCacheStrategy.ALL);
                    GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(holder1.ivImage);
                    Glide.with(context).load(data.getImageUrl1().replace("upload/", "upload/vs_20,e_loop/")
                            .replace("mp4", "gif").replace(".mov", ".gif"))
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(context.getResources().getDrawable(R.drawable.ic_default))
                            .thumbnail(thumbnail)
                            .into(imageViewTarget);
                }

                holder1.ivImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clickListner.onItemClick(position, Integer.parseInt(data.getMediaType1()), view);
                    }
                });

                holder1.ibPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListner.onItemClick(position, Integer.parseInt(data.getMediaType1()), v);
                    }
                });
                break;

            case 1:
                ViewHolder2 holder2 = (ViewHolder2) holder;

                if (data.getMediaType1().equals("0")) {
                    holder2.ibPlay.setVisibility(View.GONE);
                    Glide.with(context).load(data.getImageUrl1()).asBitmap().centerCrop()
                            .into(new BitmapImageViewTarget(holder2.ivImage));
                } else {
                    holder2.ibPlay.setVisibility(View.VISIBLE);
                    Glide.with(context).load(data.getImageUrl1().replace("mp4", "jpg")).asBitmap().centerCrop()
                            .into(new BitmapImageViewTarget(holder2.ivImage));
                }

                holder2.ivImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clickListner.onItemClick(position, Integer.parseInt(data.getMediaType1()), view);
                    }
                });

                holder2.ibPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListner.onItemClick(position, Integer.parseInt(data.getMediaType1()), v);
                    }
                });

                holder2.cbLike.setChecked(data.isFavourite());
                holder2.cbLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListner.onFavoriteClick(data.getPostId(), holder2.cbLike.isChecked());
                    }
                });

                holder2.ibComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListner.onCommentClick(data.getPostId());
                    }
                });

                Glide.with(context).load(data.getProfilepic()).asBitmap().centerCrop().placeholder(R.drawable.profile_one).into(new BitmapImageViewTarget(holder2.ivUserPhoto));

                holder2.tvUserName.setText(data.getUserName());
                //holder2.tvUserName1.setText(data.getUserName());

                if (!TextUtils.isEmpty(data.getTitle())) {
                    holder2.tvTitle.setVisibility(View.VISIBLE);
                    SpannableString spanString = new SpannableString(data.getTitle());
                    Matcher matcher = Pattern.compile("#([A-Za-z0-9_-]+)").matcher(spanString);
                    findMatch(spanString, matcher);
                    Matcher userMatcher = Pattern.compile("@([A-Za-z0-9_-]+)").matcher(spanString);
                    findMatch(spanString, userMatcher);
                    holder2.tvTitle.setText(spanString);
                    holder2.tvTitle.setMovementMethod(LinkMovementMethod.getInstance());
                } else holder2.tvTitle.setVisibility(View.GONE);

                if (!TextUtils.isEmpty(data.getPlace())) {
                    holder2.tvLocation.setVisibility(View.VISIBLE);
                    holder2.tvLocation.setText(data.getPlace());
                } else holder2.tvLocation.setVisibility(View.GONE);

                List<String> s = data.getHashTags();
                if (s != null && s.size() > 0)
                    s.remove(0);

                holder2.tvTime.setText(TimeAgo.getTimeAgo(Long.parseLong(data.getTimeStamp())));

                holder2.ivLike.setChecked(data.isLiked());
                holder2.ivDisLike.setChecked(data.isDisliked());

                holder2.ivLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        holder2.ivDisLike.setChecked(!b);
                        clickListner.onLikeClick(data.getPostId(), b);
                    }
                });

                holder2.ivDisLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        holder2.ivLike.setChecked(!b);
                        clickListner.onDisLikeClick(data.getPostId(), b);
                    }
                });
                break;
        }

    }

    private void findMatch(SpannableString spanString, Matcher matcher) {
        while (matcher.find()) {
            final String tag = matcher.group(0);
            spanString.setSpan(new TagSpannable(context, tag), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder1 extends RecyclerView.ViewHolder {
        ImageView ivImage;
        ImageButton ibPlay;

        public ViewHolder1(View itemView) {
            super(itemView);
            ivImage = (ImageView) itemView.findViewById(R.id.ivImage);
            ibPlay = (ImageButton) itemView.findViewById(R.id.ibVideo);
        }
    }

    class ViewHolder2 extends RecyclerView.ViewHolder {
        ImageView ivImage;
        ImageButton ibPlay;
        ImageView ivUserPhoto;
        TextView tvUserName, tvUserName1, tvLocation, tvTitle, tvTime;
        FlowLayout flHashTags;
        ImageButton ibComment, ibCoin;
        CheckBox cbLike;
        CheckBox ivLike;
        CheckBox ivDisLike;

        public ViewHolder2(View itemView) {
            super(itemView);
            ivImage = (ImageView) itemView.findViewById(R.id.ivImage);
            ibPlay = (ImageButton) itemView.findViewById(R.id.ibVideo);
            ivUserPhoto = (ImageView) itemView.findViewById(R.id.ivUserPhoto);
            cbLike = (CheckBox) itemView.findViewById(R.id.cbLike);
            ivLike = (CheckBox) itemView.findViewById(R.id.ivLike);
            ivDisLike = (CheckBox) itemView.findViewById(R.id.ivDisLike);
            ibComment = (ImageButton) itemView.findViewById(R.id.ibComment);
            tvUserName = (TextView) itemView.findViewById(R.id.tvUserName);
            tvUserName1 = (TextView) itemView.findViewById(R.id.tvUserName1);
            tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            flHashTags = (FlowLayout) itemView.findViewById(R.id.flHashTags);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
        }
    }
}