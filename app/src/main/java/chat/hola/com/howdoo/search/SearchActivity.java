package chat.hola.com.howdoo.search;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.connect.ConnectPagerAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.search.channel.ChannelFragment;
import chat.hola.com.howdoo.search.people.PeopleFragment;
import chat.hola.com.howdoo.search.tags.TagsFragment;
import dagger.android.support.DaggerAppCompatActivity;

public class SearchActivity extends DaggerAppCompatActivity implements SearchContract.View {

    @Inject
    SearchPresenter presenter;

    @Inject
    TypefaceManager typefaceManager;

    /*

    @Inject
    OtherSearchFrag otherSearchFrag;

    @Inject
    ChannelFragment peopleFragment;

    */
    @BindView(R.id.searchTl)
    TabLayout searchTl;
    @BindView(R.id.searchVp)
    ViewPager searchVp;
    @BindView(R.id.searchIv)
    ImageView searchIv;
    @BindView(R.id.searchInputEt)
    EditText searchInputEt;
    private Unbinder unbinder;
    @Inject
    SessionManager sessionManager;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        mInitialization();


    }

    private void mInitialization() {

        ConnectPagerAdapter adapter = new ConnectPagerAdapter(getSupportFragmentManager());
        String people = getResources().getString(R.string.people);
//        String trending = getResources().getString(R.string.trending);
        String tags = getResources().getString(R.string.tags);
        String channel = getResources().getString(R.string.clubs);

        // String chats = getResources().getString(R.string.chats);
        adapter.addFragment(new PeopleFragment(), people);
//        adapter.addFragment(OtherSearchFrag.newInstance(), trending);
        adapter.addFragment(new ChannelFragment(), channel);
        adapter.addFragment(new TagsFragment(), tags);

        searchVp.setAdapter(adapter);
        searchTl.setupWithViewPager(searchVp);

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_textview, null);
        tabOne.setTypeface(typefaceManager.getSemiboldFont());
        tabOne.setText(people);
        searchTl.getTabAt(0).setCustomView(tabOne);
//        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_textview, null);
//        tabTwo.setTypeface(typefaceManager.getSemiboldFont());
//        tabTwo.setText(trending);
//        searchTl.getTabAt(1).setCustomView(tabTwo);

        TextView tabfour = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_textview, null);
        tabfour.setTypeface(typefaceManager.getSemiboldFont());
        tabfour.setText(channel);
        searchTl.getTabAt(1).setCustomView(tabfour);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_textview, null);
        tabThree.setTypeface(typefaceManager.getSemiboldFont());
        tabThree.setText(tags);
        searchTl.getTabAt(2).setCustomView(tabThree);


    }

    @OnClick(R.id.searchIv)
    public void search() {
        presenter.stop();
    }

    @Override
    public void stop() {
        finish();
    }


    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(this, getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void reload() {

    }
}
