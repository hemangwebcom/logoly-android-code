package chat.hola.com.howdoo.dublycategory.favourite;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;

/**
 * Created by ankit on 23/2/18.
 */

public interface DubFavFragmentContract {

    interface View extends BaseView {
        void setMax(Integer max);

        void startedDownload();

        void progress(int downloadedSize);

        void finishedDownload(String path, String name, String musicId);

        void play(String audio, boolean isPlaying);

        void isLoading(boolean flag);
    }

    interface Presenter extends BasePresenter<DubFavFragmentContract.View> {

        void loadData(int skip, int limit);

        void startedDownload();
    }
}
