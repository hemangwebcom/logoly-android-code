package chat.hola.com.howdoo.club.detail;

import java.util.List;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.home.model.Data;

/**
 * Created by ankit on 22/2/18.
 */

public interface ClubDetailContract {

    interface View extends BaseView {

        void displayDetail(ClubDetailResponse.ClubDetail club);

        void setPosts(List<Data> data);

        void deleted();
    }

    interface Presenter {

        void clubDetail(String clubId);

        void clubPosts(String clubId);

        void deleteClub(String id);

        void like(String postId, boolean like);

        void dislike(String postId, boolean like);

        void favorite(String postId, boolean like);
    }
}
