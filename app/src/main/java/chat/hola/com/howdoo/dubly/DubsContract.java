package chat.hola.com.howdoo.dubly;

import chat.hola.com.howdoo.Utilities.BaseView;

/**
 * <h1>DubCategoryContract</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 7/16/2018.
 */

public interface DubsContract {

    interface View extends BaseView {
        void play(String audio, boolean isPlaying);

        void progress(int i);

        void finishedDownload(String s, String filename, String musicId);

        void startDownload();
    }

    interface Presenter {

        void getDubs(int offset, int limit, String categoryId);

        void callApiOnScroll(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount, String categoryId);

        ClickListner getPresenter();
    }
}
