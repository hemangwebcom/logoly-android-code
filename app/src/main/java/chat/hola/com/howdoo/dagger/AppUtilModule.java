package chat.hola.com.howdoo.dagger;


import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.howdoo.dubly.R;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import javax.inject.Singleton;

import chat.hola.com.howdoo.Database.PostDb;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Networking.connection.ContactHolder;
import chat.hola.com.howdoo.Networking.connection.NetworkStateHolder;
import chat.hola.com.howdoo.Networking.observer.ContactObserver;
import chat.hola.com.howdoo.Networking.observer.NetworkObserver;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.Utilities.twitterManager.TwitterManager;
import chat.hola.com.howdoo.Utilities.twitterManager.TwitterShareManager;
import chat.hola.com.howdoo.home.stories.model.StoryObserver;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.ConnectionObserver;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.models.PostObserver;
import chat.hola.com.howdoo.post.model.UploadObserver;
import dagger.Module;
import dagger.Provides;

//import com.facebookmanager.com.FacebookManager;

/**
 * <h1>AppUtilModule</h1>
 *
 * @author 3eMBED
 * @version 1.0
 * @since 20/2/18.
 */

@Singleton
@Module
public class AppUtilModule {
    @Singleton
    @Provides
    TypefaceManager typefaceManager(Context context) {
        return new TypefaceManager(context);
    }

    @Singleton
    @Provides
    Bus provideBus(Context context) {
        return new Bus(ThreadEnforcer.ANY);
    }

//    @Singleton
//    @Provides
//    FacebookManager provideFbManager(Context context) {
//        return new FacebookManager(context, context.getResources().getString(R.string.fb_id));
//    }
//
//    @Singleton
//    @Provides
//    FacebookShareManager provideFbShareManager() {
//        return FacebookShareManager.getInstance();
//    }

    @Singleton
    @Provides
    TwitterManager provideTwitterManager(Context context) {
        return new TwitterManager(context, context.getResources().getString(R.string.TWITTER_CONSUMER_KEY),
                context.getResources().getString(R.string.TWITTER_CONSUMER_SECRET));
    }

    @Singleton
    @Provides
    TwitterShareManager provideTwitterShareManager() {
        return new TwitterShareManager();
    }

    @Singleton
    @Provides
    UploadObserver uploadObserver() {
        return new UploadObserver();
    }

    @Singleton
    @Provides
    StoryObserver storyObserver() {
        return new StoryObserver();
    }

    @Singleton
    @Provides
    PostDb postdb(Context context) {
        return new PostDb(context);
    }

    @Provides
    @Singleton
    NetworkObserver getNetWorkObserver() {
        return new NetworkObserver();
    }

    @Provides
    @Singleton
    NetworkStateHolder getNetWorHolder() {
        return new NetworkStateHolder();
    }

    @Provides
    @Singleton
    SessionManager getSessionManager(Context context) {
        return new SessionManager(context);
    }

    @Provides
    @Singleton
    ProgressDialog getProgressDialog(Context context) {
        return new ProgressDialog(context);
    }

    @Provides
    @Singleton
    Gson getGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    ContactObserver getContactObserver() {
        return new ContactObserver();
    }

    @Provides
    @Singleton
    ContactHolder getContactHolder() {
        return new ContactHolder();
    }

    @Provides
    @Singleton
    LinearLayoutManager linearLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    @Singleton
    @Provides
    PostObserver postObserver() {
        return new PostObserver();
    }

    @Singleton
    @Provides
    ConnectionObserver connectionObserver() {
        return new ConnectionObserver();
    }

    @Singleton
    @Provides
    NetworkConnector networkConnector() {
        return new NetworkConnector();
    }

    @Provides
    @Singleton
    BlockDialog bockDialog(Context context) {
        return new BlockDialog(context);
    }


}
