package chat.hola.com.howdoo.home.social;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import java.io.Serializable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.LandingPresenter;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.home.social.model.SocialAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverActivity;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;
import chat.hola.com.howdoo.trendingDetail.TrendingDetail;
import dagger.android.support.DaggerFragment;

/**
 * <h1>ClubPostFragment</h1>
 *
 * @author 3Embed
 * @since 4/5/2018.
 */

public class SocialFragment extends DaggerFragment implements SocialContract.View, View.OnClickListener {
    private final String TAG = SocialFragment.class.getSimpleName();
    private Unbinder unbinder;
    private StaggeredGridLayoutManager layoutManager;
    private String screen;
    @Inject
    SocialAdapter mAdapter;
    @Inject
    SessionManager sessionManager;
    @Inject
    LandingPresenter landingPresenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SocialPresenter mPresenter;

    @BindView(R.id.socialRv)
    RecyclerView socialRv;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @BindView(R.id.tvEmpty)
    TextView tvEmpty;
    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;

    @BindView(R.id.fabMain)
    FloatingActionButton fabMain;
    @BindView(R.id.fabLiked)
    FloatingActionButton fabLiked;
    @BindView(R.id.fabExclusive)
    FloatingActionButton fabExclusive;
    @BindView(R.id.fabCommented)
    FloatingActionButton fabCommented;
    @BindView(R.id.fabViewed)
    FloatingActionButton fabViewed;
    @BindView(R.id.fabFavouite)
    FloatingActionButton fabFavouite;

    @BindView(R.id.textExclusive)
    TextView textExclusive;
    @BindView(R.id.textCommented)
    TextView textCommented;
    @BindView(R.id.textViewed)
    TextView textViewed;
    @BindView(R.id.textLiked)
    TextView textLiked;
    @BindView(R.id.textFavourit)
    TextView textFavourit;
    @BindView(R.id.flOverlay)
    FrameLayout flOverlay;

    private boolean isCalled = false;
    @Inject
    BlockDialog dialog;
    private boolean isFabOpen = false;
    private Animation rotate_forward, rotate_backward, fab_open, fab_close;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Inject
    public SocialFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_social, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter.attachView(this);
        landingPresenter.getActivity().visibleActionBar();
        init();
        applyFont();
        isCalled = true;

        fab_open = AnimationUtils.loadAnimation(getContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_backward);
        fabMain.setOnClickListener(this);
        fabLiked.setOnClickListener(this);
        fabExclusive.setOnClickListener(this);
        fabViewed.setOnClickListener(this);
        fabCommented.setOnClickListener(this);
        fabFavouite.setOnClickListener(this);
        textFavourit.setOnClickListener(this);
        textLiked.setOnClickListener(this);
        textViewed.setOnClickListener(this);
        textCommented.setOnClickListener(this);
        textExclusive.setOnClickListener(this);
        flOverlay.setVisibility(View.GONE);
        return view;
    }

    private void applyFont() {
        tvEmpty.setTypeface(typefaceManager.getMediumFont());
        tvMsg.setTypeface(typefaceManager.getRegularFont());
    }


    private void init() {
        layoutManager = new StaggeredGridLayoutManager(Constants.SocialFragment.COLUMNS, LinearLayoutManager.VERTICAL);
        swiperefresh.setOnRefreshListener(mPresenter.getPresenter());
        socialRv.setLayoutManager(layoutManager);
        mAdapter.setListener(mPresenter.getPresenter());
        socialRv.addOnScrollListener(recyclerViewOnScrollListener);
        socialRv.setAdapter(mAdapter);
        llNetworkError.setErrorListner(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.setScreen(screen);
        mPresenter.postObserver();
        mPresenter.callSocialApi(SocialPresenter.PAGE_SIZE * SocialPresenter.page, SocialPresenter.PAGE_SIZE, false);
    }

    @Override
    public void onPause() {
        super.onPause();
        isCalled = false;
    }

    @Override
    public void onItemClick(View view, Data data, int position) {
        data.setAd(null);
        Intent intent = new Intent(getContext(), SocialDetailActivity.class);
        intent.putExtra(Constants.SocialFragment.DATA, data);
        intent.putExtra("dataList", (Serializable) mPresenter.getDataList());
        intent.putExtra("position", position);
        assert getContext() != null;
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) getContext(), view, getString(R.string.transition));
        ActivityCompat.startActivity(getContext(), intent, options.toBundle());
    }

    @Override
    public void onUserClick(String userId) {
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra(Constants.SocialFragment.USERID, userId);
        startActivity(intent);
    }

    @Override
    public void showEmptyUi(boolean show) {
        llEmpty.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onChannelClick(String channelId) {
        Intent intent = new Intent(getContext(), TrendingDetail.class);
        intent.putExtra("call", "channel");
        intent.putExtra("channelId", channelId);
        startActivity(intent);
    }

    @Override
    public void clearRecyclerView() {
        socialRv.getRecycledViewPool().clear();
    }

    @Override
    public void showMessage(String msg, int msgId) {
        Toast.makeText(getContext(), msg != null && !msg.isEmpty() ? msg : getResources().getString(msgId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void isLoading(boolean flag) {
        assert swiperefresh != null;
        swiperefresh.setRefreshing(flag && !swiperefresh.isRefreshing());
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        mPresenter.detachView();
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroy();
    }

    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int[] firstVisibleItemPositions = new int[SocialPresenter.PAGE_SIZE];
            int firstVisibleItemPosition = ((StaggeredGridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPositions(firstVisibleItemPositions)[0];
            mPresenter.prefetchImage(firstVisibleItemPosition);
            mPresenter.callApiOnScroll(firstVisibleItemPosition, visibleItemCount, totalItemCount);
        }
    };

    @Override
    public void reload() {
        llNetworkError.setVisibility(View.GONE);
        mPresenter.onRefresh();
    }

    public void animateFAB() {

        if (isFabOpen) {
            flOverlay.setVisibility(View.GONE);
            fabMain.startAnimation(rotate_backward);


            fabExclusive.setVisibility(View.GONE);
            fabLiked.setVisibility(View.GONE);
            fabCommented.setVisibility(View.GONE);
            fabViewed.setVisibility(View.GONE);
            fabFavouite.setVisibility(View.GONE);

            textExclusive.setVisibility(View.GONE);
            textCommented.setVisibility(View.GONE);
            textViewed.setVisibility(View.GONE);
            textLiked.setVisibility(View.GONE);
            textFavourit.setVisibility(View.GONE);


//            fabExclusive.startAnimation(fab_close);
//            fabLiked.startAnimation(fab_close);
//            fabCommented.startAnimation(fab_close);
//            fabViewed.startAnimation(fab_close);
//            fabFavouite.startAnimation(fab_close);
//
//            textExclusive.startAnimation(fab_close);
//            textCommented.startAnimation(fab_close);
//            textViewed.startAnimation(fab_close);
//            textLiked.startAnimation(fab_close);
//            textFavourit.startAnimation(fab_close);

            fabExclusive.setClickable(false);
            fabLiked.setClickable(false);
            fabCommented.setClickable(false);
            fabViewed.setClickable(false);
            fabFavouite.setClickable(false);
            isFabOpen = false;

        } else {
            flOverlay.setVisibility(View.VISIBLE);
            fabMain.startAnimation(rotate_forward);

            fabExclusive.setVisibility(View.VISIBLE);
            fabLiked.setVisibility(View.VISIBLE);
            fabCommented.setVisibility(View.VISIBLE);
            fabViewed.setVisibility(View.VISIBLE);
            fabFavouite.setVisibility(View.VISIBLE);

            textExclusive.setVisibility(View.VISIBLE);
            textCommented.setVisibility(View.VISIBLE);
            textViewed.setVisibility(View.VISIBLE);
            textLiked.setVisibility(View.VISIBLE);
            textFavourit.setVisibility(View.VISIBLE);


//            fabExclusive.startAnimation(fab_open);
//            fabLiked.startAnimation(fab_open);
//            fabCommented.startAnimation(fab_open);
//            fabViewed.startAnimation(fab_open);
//            fabFavouite.startAnimation(fab_open);
//
//            textExclusive.startAnimation(fab_open);
//            textCommented.startAnimation(fab_open);
//            textViewed.startAnimation(fab_open);
//            textLiked.startAnimation(fab_open);
//            textFavourit.startAnimation(fab_open);

            fabExclusive.setClickable(true);
            fabLiked.setClickable(true);
            fabCommented.setClickable(true);
            fabViewed.setClickable(true);
            fabFavouite.setClickable(true);
            isFabOpen = true;

        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        animateFAB();
        switch (id) {
            case R.id.fabLiked:
            case R.id.textLiked:
                mPresenter.filter(screen, "1");
                break;
            case R.id.fabExclusive:
            case R.id.textExclusive:
                mPresenter.filter(screen, "2");
                break;
            case R.id.fabCommented:
            case R.id.textCommented:
                mPresenter.filter(screen, "3");
                break;
            case R.id.fabViewed:
            case R.id.textViewed:
                mPresenter.filter(screen, "4");
                break;
            case R.id.fabFavouite:
            case R.id.textFavourit:
                mPresenter.filter(screen, "5");
                break;
        }
    }

    public void setType(String screen) {
        this.screen = screen;
        mPresenter.setScreen(screen);
        mPresenter.callSocialApi(SocialPresenter.PAGE_SIZE * SocialPresenter.page, SocialPresenter.PAGE_SIZE, false);
    }
}