package chat.hola.com.howdoo.home.trending;

import android.view.View;

import java.util.ArrayList;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.home.trending.model.Header;
import chat.hola.com.howdoo.home.model.Data;

/**
 * Created by ankit on 21/2/18.
 */

public interface TrendingContract {

    interface View extends BaseView {
        void initHeaderRecycler();

        void initContentRecycler();

        void showHeader(ArrayList<Header> headers);

        void showContent(ArrayList<Data> trendings);

        void onPostClick(Data item, android.view.View view);

        void isLoading(boolean flag);

        void isContentAvailable(boolean flag);

        void setHashTags(StringBuilder tags);
    }

    interface Presenter extends BasePresenter<TrendingContract.View> {
        void init();

        void loadHeader();

        void loadContent(String categoryId);
    }

}
