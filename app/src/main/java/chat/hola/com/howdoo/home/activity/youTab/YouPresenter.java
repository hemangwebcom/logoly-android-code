package chat.hola.com.howdoo.home.activity.youTab;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.home.activity.youTab.followrequest.ResponseModel;
import chat.hola.com.howdoo.home.activity.youTab.model.ChannelSubscibe;
import chat.hola.com.howdoo.home.activity.youTab.model.YouModel;
import chat.hola.com.howdoo.home.activity.youTab.model.YouResponse;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.FollowResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 22/2/18.
 */

public class YouPresenter implements YouContract.Presenter {

    private static final String TAG = YouPresenter.class.getSimpleName();

    @Nullable
    private YouContract.View view;


    @Inject
    HowdooService service;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    public YouPresenter() {
    }

    @Override
    public void attachView(YouContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void loadYouData(int offset, int limit) {
        if (view != null) {

            view.isDataLoading(true);
        }
        service.getYouActivity(AppController.getInstance().getApiToken(), Constants.LANGUAGE, offset, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<YouResponse>>() {
                    @Override
                    public void onNext(Response<YouResponse> youResponse) {
                        if (view != null) {
                            view.isDataLoading(false);
                            if (youResponse.code() == 200) {
                                if (youResponse.body().getData() == null || youResponse.body().getData().isEmpty())
                                    view.showErrorLayout(true);
                                else {
                                    view.showYouActivity(youResponse.body().getData());
                                    view.showErrorLayout(false);
                                }
                                view.isInternetAvailable(true);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.isInternetAvailable(networkConnector.isConnected());
                            view.isDataLoading(false);
                            view.showErrorLayout(true);
                        }
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }

    @Override
    public void setModel(YouModel model) {

    }

    @Override
    public void follow(String followingId) {
        Map<String,Object> params = new HashMap<>();
        params.put("followingId",followingId);
        service.follow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> followResponseResponse) {
                        if (followResponseResponse.code() == 200)
                            Log.e(TAG, followResponseResponse.body().getMessage());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "failed to follow: " + e.getMessage());
                        //view.invalidateBtn(index,false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void unFollow(String followingId) {
        service.unfollow(AppController.getInstance().getApiToken(), "en",
                followingId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> body) {
                        Log.w(TAG, body.toString());
                        Log.w(TAG, "unfollowed successfully");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "failed to unFollow: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void channelRequest() {
        service.getRequestedChannels(AppController.getInstance().getApiToken(), "en")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ChannelSubscibe>>() {
                    @Override
                    public void onNext(Response<ChannelSubscibe> response) {
                        if (response.code() == 200 && response.body().getRequestedChannels() != null) {
                            try {
                                view.channelRequest(response.body().getRequestedChannels());
                            } catch (NullPointerException ignored) {

                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void followRequest() {
        service.getfollowRequest(AppController.getInstance().getApiToken(), "en", 0, 5)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseModel>>() {
                    @Override
                    public void onNext(Response<ResponseModel> response) {
                        if (response.code() == 200 && response.body().getData() != null) {
                            try {
                                view.followRequest(response.body().getData(), response.body().getTotalRequest());
                            } catch (NullPointerException ignored) {

                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
