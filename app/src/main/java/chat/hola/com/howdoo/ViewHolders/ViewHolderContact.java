package chat.hola.com.howdoo.ViewHolders;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.howdoo.dubly.R;

import chat.hola.com.howdoo.AppController;


/**
 * Created by moda on 28/07/17.
 */

public class ViewHolderContact extends RecyclerView.ViewHolder {


    public TextView contactName, contactStatus, contactnameIndicatorTv;

    public ImageView contactImage;
    private AppController appController;
    private Typeface fontMedium, fontRegular, fontBold;
    public ImageButton ibDelete;

    public ViewHolderContact(View view) {
        super(view);
        appController = AppController.getInstance();
        fontMedium = appController.getMediumFont();
        fontRegular = appController.getRegularFont();
        fontBold = appController.getSemiboldFont();
        contactStatus = (TextView) view.findViewById(R.id.contactStatus);
        contactnameIndicatorTv = (TextView) view.findViewById(R.id.contactnameIndicatorTv);
        contactName = (TextView) view.findViewById(R.id.contactName);
        contactImage = (ImageView) view.findViewById(R.id.storeImage2);
        ibDelete = (ImageButton) view.findViewById(R.id.ibDelete);

        contactName.setTypeface(fontMedium);
        contactStatus.setTypeface(fontRegular);
        contactnameIndicatorTv.setTypeface(fontBold);
    }
}
