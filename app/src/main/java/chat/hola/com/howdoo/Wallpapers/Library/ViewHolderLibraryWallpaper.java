package chat.hola.com.howdoo.Wallpapers.Library;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.howdoo.dubly.R;

/**
 * Created by moda on 11/10/17.
 */

public class ViewHolderLibraryWallpaper extends RecyclerView.ViewHolder {


    public ImageView wallpaper;


    public ViewHolderLibraryWallpaper(View view) {
        super(view);


        wallpaper = (ImageView) view.findViewById(R.id.wallpaper);

    }
}
