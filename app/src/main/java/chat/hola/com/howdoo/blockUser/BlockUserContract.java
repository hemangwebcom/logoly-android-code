package chat.hola.com.howdoo.blockUser;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.blockUser.model.ClickListner;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;

/**
 * <h1>BlockUserContract</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public interface BlockUserContract {

    interface View extends BaseView {


        /**
         * redirects to  @{@link chat.hola.com.howdoo.profileScreen.ProfileActivity})
         *
         * @param userId : userId to get user details
         */
        void openProfile(String userId);


    }

    interface Presenter {

        /**
         * gets lis of comments of particular post
         *
         */
        void getBlockedUsers(int offset, int limit);



        void callApiOnScroll(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount);

        ClickListner getPresenter();
    }
}
