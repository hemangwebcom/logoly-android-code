package chat.hola.com.howdoo.profileScreen.discover;

import chat.hola.com.howdoo.Utilities.BaseView;

/**
 * <h>DiscoverContract</h>
 * @author 3Embed.
 * @since 2/3/18.
 */

public interface DiscoverContract {

    interface View extends BaseView {

        void viewPagerSetup();

        void tabSetup();

        void applyFont();

        void changeSkipText(String text);
    }

    interface Presenter {
        void init();

        void changeText(String text);
    }
}
