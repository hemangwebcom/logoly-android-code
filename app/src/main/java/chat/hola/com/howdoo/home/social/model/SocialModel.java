package chat.hola.com.howdoo.home.social.model;

import android.net.Uri;

import com.facebook.ads.Ad;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.home.model.Data;

/**
 * <h1>SocialModel</h1>
 *
 * @author 3Embed
 * @since 4/5/2018.
 */

public class SocialModel {

    @Inject
    public List<Data> arrayList;
    @Inject
    SocialAdapter mAdapter;

    @Inject
    SocialModel() {
    }

    public String getUserId(int position) {
        try {
            return arrayList.get(position).getUserId();
        } catch (ArrayIndexOutOfBoundsException e) {
            return arrayList.get(arrayList.size() - 1).getUserId();
        }
    }

    public void setData(List<Data> data, boolean isFirst) {
        if (isFirst)
            this.arrayList.clear();
        for (Data posts : data) {
            posts.setImageUrl1(posts.getImageUrl1().replace("upload/", Constants.IMAGE_QUALITY));

            String channelPic = posts.getChannelImageUrl();
            if (channelPic != null && !channelPic.isEmpty())
                posts.setChannelImageUrl(channelPic.replace("upload/", Constants.CHANNEL_IMAGE_QUALITY));

            String profilePic = posts.getProfilepic();
            if (profilePic != null && !profilePic.isEmpty())
                posts.setProfilepic(profilePic.replace("upload/", Constants.PROFILE_PIC_SHAPE));
            else
                posts.setProfilepic(Constants.DEFAULT_PROFILE_PIC_LINK);
        }
        this.arrayList.addAll(data);
        mAdapter.notifyDataSetChanged();
    }

    public Data getData(int position) {
        return arrayList.get(position);
    }

    public String getChannelId(int position) {
        return arrayList.get(position).getChannelId();
    }

    public List<Data> getAllData() {
        return arrayList;
    }


}
