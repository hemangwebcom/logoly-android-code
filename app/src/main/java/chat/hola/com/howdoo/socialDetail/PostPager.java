package chat.hola.com.howdoo.socialDetail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.home.model.Data;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 8/13/2018.
 */
public class PostPager extends FragmentPagerAdapter {
    List<Data> list;

    public PostPager(FragmentManager fm, List<Data> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        PostFragment fragment = new PostFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", list.get(position));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
