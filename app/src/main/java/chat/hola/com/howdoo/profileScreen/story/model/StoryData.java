package chat.hola.com.howdoo.profileScreen.story.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 3/5/2018.
 */

public class StoryData implements Serializable {

    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("registeredOn")
    @Expose
    private String registeredOn;
    @SerializedName("private")
    @Expose
    private String _private;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("postId")
    @Expose
    private String postId;
    @SerializedName("imageUrl1")
    @Expose
    private String imageUrl1;
    @SerializedName("thumbnailUrl1")
    @Expose
    private String thumbnailUrl1;
    @SerializedName("mediaType1")
    @Expose
    private String mediaType1;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("totalComments")
    @Expose
    private String commentCount;
    @SerializedName("distinctViews")
    @Expose
    private String distinctViews;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("channelName")
    @Expose
    private String channelName;
    @SerializedName("place")
    @Expose
    private String place;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("channelId")
    @Expose
    private String channelId;
    @SerializedName("hashTags")
    @Expose
    private List<String> hashTags = null;
    @SerializedName("profilePic")
    @Expose
    private String profilepic;
    @SerializedName("likesCount")
    @Expose
    private String likesCount;
    @SerializedName("liked")
    @Expose
    private boolean liked = false;
    @SerializedName("disliked")
    @Expose
    private boolean disliked = false;
    @SerializedName("favourite")
    @Expose
    private boolean isFavourite = false;

    public String getCommentCount() {
        return commentCount;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRegisteredOn() {
        return registeredOn;
    }

    public void setRegisteredOn(String registeredOn) {
        this.registeredOn = registeredOn;
    }

    public String getPrivate() {
        return _private;
    }

    public void setPrivate(String _private) {
        this._private = _private;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getImageUrl1() {
        return imageUrl1;
    }

    public void setImageUrl1(String imageUrl1) {
        this.imageUrl1 = imageUrl1;
    }

    public String getThumbnailUrl1() {
        return thumbnailUrl1;
    }

    public void setThumbnailUrl1(String thumbnailUrl1) {
        this.thumbnailUrl1 = thumbnailUrl1;
    }

    public String getMediaType1() {
        return mediaType1;
    }

    public void setMediaType1(String mediaType1) {
        this.mediaType1 = mediaType1;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public List<String> getHashTags() {
        return hashTags;
    }

    public void setHashTags(List<String> hashTags) {
        this.hashTags = hashTags;
    }

    public String getDistinctViews() {
        return distinctViews;
    }

    public void setDistinctViews(String distinctViews) {
        this.distinctViews = distinctViews;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean isDisliked() {
        return disliked;
    }

    public void setDisliked(boolean disliked) {
        this.disliked = disliked;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }
}
