package chat.hola.com.howdoo.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.howdoo.dubly.R;

import java.io.File;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.ConnectivityReceiver;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.SocialShare;
import chat.hola.com.howdoo.Utilities.UriUtil;
import chat.hola.com.howdoo.cameraActivities.manager.CameraOutputModel;
import chat.hola.com.howdoo.profileScreen.model.Profile;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * <h1>LandingPresenter</h1>
 *
 * @author 3Embed
 * @since 21/2/18.
 */
public class LandingPresenter implements LandingContract.Presenter, ConnectivityReceiver.ConnectivityReceiverListener {

    @Inject
    LandingContract.View view;

    @Inject
    SocialShare socialShare;

    @Inject
    Context context;
    @Inject
    HowdooService service;


    private String picturePath;
    private Bitmap bitmapToUpload;

    LandingActivity contactSyncLandingPage;

    @Inject
    public LandingPresenter(LandingActivity context) {
        contactSyncLandingPage = context;

    }

    @Override
    public void instagramShare(String type, String path) {
        if (socialShare.checkAppinstall(socialShare.INSTA_PACKAGE))
            socialShare.createInstagramIntent(type, null, path);
        else {
            view.showMessage(null, R.string.installInstaMsg);
        }
    }

    @Override
    public LandingActivity getActivity() {
        return contactSyncLandingPage;
    }

    @Override
    public void parseMedia(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                Uri uri = data.getData();
                //TODO: it will prevent further crash ( getting uri as null on android 7.0 Mi phone).
                if (uri == null)
                    return;

                picturePath = UriUtil.getPath(context, uri);
                if (picturePath != null) {

                    String extension = picturePath.substring(picturePath.lastIndexOf(".") + 1);
                    if (extension.equalsIgnoreCase("mp4")) {
                        File file = new File(picturePath);
                        if (file.length() <= (Constants.Camera.FILE_SIZE * 1024 * 1024)) {
                            view.launchPostActivity(new CameraOutputModel(1, picturePath));
                        } else {
                            view.showSnackMsg(R.string.video_size_message);
                        }
                    } else {
                        //image
                        parseSelectedImage(uri, picturePath);
                    }
                } else {
                    //image can't be selected try another
                    view.showSnackMsg(R.string.string_31);
                }
            } catch (OutOfMemoryError e) {
                //out of mem try again
                view.showSnackMsg(R.string.string_15);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            //image selection canceled.
            view.showSnackMsg(R.string.string_16);
        } else {
            //failed to select image.
            view.showSnackMsg(R.string.string_113);
        }
    }


    @Override
    public void parseSelectedImage(Uri uri, String picturePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, options);

        if (options.outWidth > 0 && options.outHeight > 0) {

            //launch crop image
            view.launchCropImage(uri);

        } else {
            //image can't be selected try another
            view.showSnackMsg(R.string.string_31);
        }
    }

    @Override
    public void parseCropedImage(int requestCode, int resultCode, Intent data) {
        try {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                picturePath = UriUtil.getPath(context, result.getUri());
                if (picturePath != null) {
                    bitmapToUpload = BitmapFactory.decodeFile(picturePath);
                    Bitmap bitmap = getCircleBitmap(bitmapToUpload);
                    if (bitmap != null && bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
                        view.launchPostActivity(new CameraOutputModel(0, picturePath));
                    } else {
                        picturePath = null;
                        view.showSnackMsg(R.string.string_19);
                    }
                } else {
                    picturePath = null;
                    view.showSnackMsg(R.string.string_19);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                picturePath = null;
                view.showSnackMsg(R.string.string_19);
            }
        } catch (OutOfMemoryError e) {
            picturePath = null;
            view.showSnackMsg(R.string.string_15);
        }
    }


    private Bitmap getCircleBitmap(Bitmap bitmap) {
        try {
            final Bitmap circuleBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(circuleBitmap);

            final int color = Color.GRAY;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
            final RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawOval(rectF, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return circuleBitmap;
        } catch (Exception e) {
            return null;
        }
    }


    @Override
    public void launchImagePicker() {
        Intent intent = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setType("*/*");

        } else {
            intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }
        intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*", "video/*"});
        view.launchImagePicker(intent);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        view.intenetStatusChanged(isConnected);
    }

    public void profile() {
        service.getUserProfile(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Profile>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<Profile> response) {
                        Log.i("RESPONSE", response.toString());
                        try {
                            if (response.code() == 200 && response.body() != null) {
                                if (view != null)
                                    view.profilepic(response.body().getData().get(0).getProfilePic());
                            } else if (response.code() == 401)
                                if (view != null)
                                    view.sessionExpired();
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }
}
