package chat.hola.com.howdoo.category;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>CategoryModule</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 28/3/18
 */

@ActivityScoped
@Module
public interface CategoryModule {

    @ActivityScoped
    @Binds
    CategoryContract.Presenter categoryPresenter(CategoryPresenter presenter);

    @ActivityScoped
    @Binds
    CategoryContract.View categoryView(CategoryActivity activity);

}
