package chat.hola.com.howdoo.dagger;

import chat.hola.com.howdoo.Service.PostService;
import chat.hola.com.howdoo.Service.ShareService;
import chat.hola.com.howdoo.post.PostModule;
import chat.hola.com.howdoo.poststory.StoryService;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ankit on 20/2/18.
 */

@Module
public interface ServiceBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = PostModule.class)
    PostService postService();

    @ActivityScoped
    @ContributesAndroidInjector
    StoryService postStoryService();

    @ActivityScoped
    @ContributesAndroidInjector()
    ShareService shareService();

}
