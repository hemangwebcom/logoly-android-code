package chat.hola.com.howdoo.profileScreen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DELL on 3/2/2018.
 */

public class Data implements Serializable {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("registeredOn")
    @Expose
    private String registeredOn;
    @SerializedName("private")
    @Expose
    private String _private;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("email")
    @Expose
    private Email email;

    @SerializedName("following")
    @Expose
    private String following;
    @SerializedName("followers")
    @Expose
    private String followers;
    @SerializedName("postsCount")
    @Expose
    private String postsCount;
    @SerializedName("clubCount")
    @Expose
    private String clubCount;
    @SerializedName("coverImage")
    @Expose
    private String coverPic;
    @SerializedName("isBlocked")
    @Expose
    private Boolean block = false;

    @SerializedName("followStatus")
    @Expose
    private Integer followStatus = 1;

    @SerializedName("isFollowing")
    @Expose
    private Boolean followingMe = false;

    @SerializedName("isContact")
    @Expose
    private Boolean isContact;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public Email getEmail() {
        return email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRegisteredOn() {
        return registeredOn;
    }

    public void setRegisteredOn(String registeredOn) {
        this.registeredOn = registeredOn;
    }

    public String getPrivate() {
        return _private;
    }

    public void setPrivate(String _private) {
        this._private = _private;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getPostsCount() {
        return postsCount;
    }

    public void setPostsCount(String postsCount) {
        this.postsCount = postsCount;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public boolean getBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    public Integer getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(Integer followStatus) {
        this.followStatus = followStatus;
    }

    public String getCoverPic() {
        return coverPic;
    }

    public void setCoverPic(String coverPic) {
        this.coverPic = coverPic;
    }

    public String getClubCount() {
        return clubCount;
    }

    public void setClubCount(String clubCount) {
        this.clubCount = clubCount;
    }

    public boolean isFollowingMe() {
        return followingMe;
    }

    public void setFollowingMe(boolean followingMe) {
        this.followingMe = followingMe;
    }


    public boolean isContact() {
        return isContact;
    }

    public void setContact(boolean contact) {
        isContact = contact;
    }
}
