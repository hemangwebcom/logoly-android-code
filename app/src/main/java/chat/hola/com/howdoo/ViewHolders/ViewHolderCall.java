package chat.hola.com.howdoo.ViewHolders;

import android.graphics.Typeface;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import chat.hola.com.howdoo.AppController;
import com.howdoo.dubly.R;


/**
 * Created by moda on 27/07/17.
 */

public class ViewHolderCall extends RecyclerView.ViewHolder {
    public TextView name, callTime, callTypeTv;
    public ImageView receiverImage, callDialledIv, callTypeIv;


    public ViewHolderCall(View view) {
        super(view);
        receiverImage = (ImageView) view.findViewById(R.id.userImage);
        name = (TextView) view.findViewById(R.id.usernamecall);
        callDialledIv = (ImageView) view.findViewById(R.id.callStatus);
        callTypeIv = (AppCompatImageView) view.findViewById(R.id.Statuscall);
        callTime = (TextView) view.findViewById(R.id.timeingcall);
        callTypeTv = (TextView) view.findViewById(R.id.call_type);
        Typeface tf = AppController.getInstance().getRegularFont();
        callTypeTv.setTypeface(tf, Typeface.NORMAL);
        name.setTypeface(tf, Typeface.NORMAL);
        callTime.setTypeface(tf, Typeface.NORMAL);
    }
}