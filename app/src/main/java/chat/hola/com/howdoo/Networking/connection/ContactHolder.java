package chat.hola.com.howdoo.Networking.connection;

import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.ContactRequest;

/**
 * Created by DELL on 4/6/2018.
 */

public class ContactHolder {
    private ContactRequest contactRequest;

    public ContactRequest getContactRequest() {
        return contactRequest;
    }

    public void setContactRequest(ContactRequest contactRequest) {
        this.contactRequest = contactRequest;
    }
}
