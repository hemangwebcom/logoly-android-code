package chat.hola.com.howdoo.Service;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import com.howdoo.dubly.R;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.MQtt.MqttService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.welcomeScreen.WelcomeActivity;


/**
 * Created by moda on 21/06/17.
 */

public class AppKilled extends Service {
    NotificationManager mNotificationManager;

    private void attachNotification() {
        Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 112, intent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher_round);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setTicker("Datum chat")
                .setContentText(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.hola_ic_launcher)
                .setContentIntent(pendingIntent)
                .setOngoing(true);
        //.build();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel notificationChannel = new NotificationChannel(Constants.NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE, Constants.NOTIFICATION_CHANNEL_NAME_CHAT_SERVICE, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100});
            assert mNotificationManager != null;
            mBuilder.setChannelId(Constants.NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        startForeground(Constants.FOREGROUND_SERVICE,
                mBuilder.build());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if ((intent != null && intent.getAction() != null && intent.getAction().equals(Constants.ACTION_START_FOURGROUND)) || Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            attachNotification();
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void onTaskRemoved(Intent rootIntent) {


        if (AppController.getInstance().isActiveOnACall()) {
            /*
             *If active on a call and the app is slided off,then have to make myself available
             */


            AppController.getInstance().cutCallOnKillingApp();


        }

        AppController.getInstance().disconnect();

        AppController.getInstance().setApplicationKilled(true);

        AppController.getInstance().createMQttConnection(AppController.getInstance().getUserId(), true);


        /*
         * For sticky service not restarting on KITKAT devices
         */


        Intent restartService = new Intent(getApplicationContext(),
                MqttService.class);
        restartService.setPackage(getPackageName());

        //changed here
        PendingIntent restartServicePI = PendingIntent.getBroadcast(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePI);

        stopSelf();
    }
}