package chat.hola.com.howdoo.welcomeScreen;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.howdoo.dubly.R;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Database.CouchDbController;
import chat.hola.com.howdoo.NumberVerification.SmsVerification;
import chat.hola.com.howdoo.Profile.SaveProfile;
import chat.hola.com.howdoo.manager.account.AccountGeneral;
import chat.hola.com.howdoo.manager.session.SessionManager;

/**
 * <h1>WelcomeModel</h1>
 *
 * @author 3Embed
 * @since 4/10/2018.
 */

class WelcomeModel {

    @Inject
    SessionManager sessionManager;

    @Inject
    WelcomeModel() {
    }

    void loginSuccessful(LoginResponse response) {

        CouchDbController db = AppController.getInstance().getDbController();

        Map<String, Object> map = new HashMap<>();

        String profilePic = response.getProfilePic();
        String userName = response.getUsername();
        String firstName = response.getFirstName();
        String lastName = response.getLastName();
        String userStatus = response.getSocialStatus();
        String userId = response.getUserId();
        String phoneNumber =response.getPhoneNumber();
        String token=response.getToken();
        boolean isPrivate=response.getPrivate();

        map.put("userImageUrl", profilePic);
        map.put("userName", userName);
        map.put("firstName", firstName);
        map.put("lastName", lastName);
        map.put("userId", userId);
        map.put("private",isPrivate);
        map.put("socialStatus", userStatus);
        map.put("userIdentifier", phoneNumber);
        map.put("apiToken",token);
        map.put("userLoginType", 1);
//        Account mAccount = new Account(bundle.getString("phoneNumber"), AccountGeneral.ACCOUNT_TYPE);
//        AccountManager mAccountManager = AccountManager.get(getApplicationContext());
//        mAccountManager.addAccountExplicitly(mAccount, input.getText().toString(), null);
//
//        Account[] accounts = mAccountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
//        if (accounts.length != 0)
//            mAccountManager.setAuthToken(mAccount, AccountGeneral.AUTHTOKEN_TYPE_READ_ONLY, response.getString("token"));

        // Store details in local db
        AppController.getInstance().getSharedPreferences().edit().putString("token", token).apply();
        if (!db.checkUserDocExists(AppController.getInstance().getIndexDocId(), userId)) {
            String userDocId = db.createUserInformationDocument(map);
            db.addToIndexDocument(AppController.getInstance().getIndexDocId(), userId, userDocId);
        } else {
            db.updateUserDetails(db.getUserDocId(userId, AppController.getInstance().getIndexDocId()), map);
        }
        if (!userName.isEmpty()) {

            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(),userId, 1, true);
        } else {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(),userId, 1, false);
        }


         // To update myself as available for call
        AppController.getInstance().setSignedIn(true,userId, userName,phoneNumber, 1);
        AppController.getInstance().setSignStatusChanged(true);


        //subscribe firebase topic
        String topic = "/topics/" +userId;
        FirebaseMessaging.getInstance().subscribeToTopic(topic);
    }

    public Map<String, Object> getParams(String userName, String password) {
        Map<String, Object> params = new HashMap<>();
        params.put("username", userName);
        params.put("password", password);
        params.put("appVersion", Build.VERSION.RELEASE);
        params.put("deviceId", Build.ID);
        params.put("deviceType", "Android");
        params.put("deviceName", Build.DEVICE);
        params.put("deviceModel", Build.MODEL);
        params.put("deviceOs", String.valueOf(Build.VERSION.SDK_INT));
        return params;
    }
}
