package chat.hola.com.howdoo.socialDetail;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.howdoo.dubly.BuildConfig;
import com.howdoo.dubly.R;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.RecyclerViewItemPositionGetter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.ForwardMessage.ActivityForwardMessage;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.MqttEvents;
import chat.hola.com.howdoo.Utilities.RoundedImageView;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.Utilities.Utilities;
import chat.hola.com.howdoo.comment.CommentActivity;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.post.PostActivity;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.socialDetail.video_manager.BaseVideoItem;
import chat.hola.com.howdoo.socialDetail.video_manager.ClickListner;
import chat.hola.com.howdoo.socialDetail.video_manager.ItemFactory;
import chat.hola.com.howdoo.trendingDetail.TrendingDetail;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>SocialDetailActivity</h1>
 * <p>It shows details of post. user can like, share, report, edit and delete post</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 5/3/2018
 */

public class SocialDetailActivity extends DaggerAppCompatActivity implements ClickListner, ActionListner, ItemAdapter.ItemListener,
        SocialDetailContract.View, DialogInterface.OnClickListener {

    private static final String TAG = "SocialDetailActivity";
    @Inject
    BlockDialog dialog;
    @Inject
    SocialDetailPresenter mPresenter;
    @Inject
    SessionManager sessionManager;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    AlertDialog.Builder reportDialog;
    @Inject
    ArrayAdapter<String> arrayAdapter;

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bottom_sheet)
    View bottomSheet;
    @BindView(R.id.ivProfilePic)
    RoundedImageView ivProfilePic;
    @BindView(R.id.etMessage)
    EditText etMessage;
    @BindView(R.id.searchView)
    android.support.v7.widget.SearchView searchView;
    @BindView(R.id.shareList)
    RecyclerView shareList;

    private String postId;
    private Data data = null;
    private String userId;
    private String categoryId;
    private Menu menu;

    private String call;
    private ItemAdapter itemAdapter;
    private List<Data> dataList = new ArrayList<>();
    private BottomSheetBehavior behavior;
    private List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> list;


    private final ArrayList<BaseVideoItem> mList = new ArrayList<>();
    private final ListItemsVisibilityCalculator mVideoVisibilityCalculator = new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);
    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
    private ItemsPositionGetter mItemsPositionGetter;
    private SocialDetailAdapter socialDetailAdapter;
    private LinearLayoutManager mLayoutManager;

    private final VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {

        }
    });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_detail);
        ButterKnife.bind(this);
        toolbarSetup();
        postId = getIntent().getStringExtra("postId");
        data = (Data) getIntent().getSerializableExtra(Constants.SocialFragment.DATA);
        call = getIntent().getStringExtra("call");
        dataList = (List<Data>) getIntent().getSerializableExtra("dataList");
        setData(dataList);

        if (dataList == null)
            dataList = new ArrayList<>();
        int pos = getIntent().getIntExtra("position", 0);


        mLayoutManager = new LinearLayoutManager(this);
        rvList = (RecyclerView) findViewById(R.id.rvList);
        rvList.setLayoutManager(mLayoutManager);
        socialDetailAdapter = new SocialDetailAdapter(mVideoPlayerManager, SocialDetailActivity.this, mList, dataList);
        socialDetailAdapter.setClickListner(this);
        rvList.setAdapter(socialDetailAdapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rvList);
        rvList.scrollToPosition(pos);


        rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                mScrollState = scrollState;
                if (scrollState == RecyclerView.SCROLL_STATE_IDLE && !mList.isEmpty()) {

                    mVideoVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            mLayoutManager.findFirstVisibleItemPosition(),
                            mLayoutManager.findLastVisibleItemPosition());
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!mList.isEmpty()) {
                    mVideoVisibilityCalculator.onScroll(
                            mItemsPositionGetter,
                            mLayoutManager.findFirstVisibleItemPosition(),
                            mLayoutManager.findLastVisibleItemPosition() - mLayoutManager.findFirstVisibleItemPosition() + 1,
                            mScrollState);
                }
            }
        });
        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, rvList);


        mPresenter.getFollowUsers();
        Glide.with(this).load(sessionManager.getUserProfilePic().replace("upload/", Constants.PROFILE_PIC_SHAPE))
                .asBitmap().centerCrop().into(ivProfilePic);


        if (data != null && dataList == null) {
            userId = data.getUserId();
            postId = data.getPostId();
            getData(postId);

        } else if (postId != null) {
            getData(postId);
        } else {
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                            // Get deep link from result (may be null if no link is found)
                            Uri deepLink = null;
                            if (pendingDynamicLinkData != null) {
                                deepLink = pendingDynamicLinkData.getLink();
                                String postId = deepLink.getLastPathSegment();
                                //  loading.setVisibility(View.VISIBLE);
                                mPresenter.getPostById(postId, false);
                            }
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Log.w(TAG, "getDynamicLink:onFailure", e);
                        }
                    });
        }
        if (dataList != null && !dataList.isEmpty())
            postId = dataList.get(pos).getPostId();
        reportDialog.setTitle(R.string.report);
        mPresenter.getReportReasons();
        reportDialog.setAdapter(arrayAdapter, this);
        invalidateOptionsMenu();

        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        shareList.setHasFixedSize(true);
        shareList.setLayoutManager(new LinearLayoutManager(this));


        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // itemAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //    itemAdapter.getFilter().filter(newText);
                return false;
            }
        });

    }

    private void getData(String postId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mPresenter.getPostById(postId, false);
            }
        }).start();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        mPresenter.reportPost(postId, arrayAdapter.getItem(which), arrayAdapter.getItem(which));
    }


    private void invalidateMenu(Data data) {
        try {
            MenuItem edit = menu.findItem(R.id.action_edit);
            MenuItem report = menu.findItem(R.id.action_report);
            MenuItem delete = menu.findItem(R.id.action_delete);

            boolean flag = data.getUserId() != null && data.getUserId().equals(AppController.getInstance().getUserId());
            edit.setVisible(flag);
            report.setVisible(!flag);
            delete.setVisible(flag);
        } catch (Exception ignored) {
        }
    }


    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.social_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null) {
            switch (item.getItemId()) {
                case R.id.action_report:
                    reportDialog.show();
                    return true;
                case R.id.action_edit:
                    Intent intentEdit = new Intent(this, PostActivity.class);
                    intentEdit.putExtra("data", data);
                    intentEdit.putExtra("call", "edit");
                    intentEdit.putExtra(Constants.Post.TYPE, data.getMediaType1() == 0 ? Constants.Post.IMAGE : Constants.Post.VIDEO);
                    startActivity(intentEdit);
                    finish();
                    return true;
                case R.id.action_delete:
                    reportDialog.setMessage(R.string.postDeleteMsg);
                    reportDialog.setPositiveButton(R.string.yes, (dialog, which) -> mPresenter.deletePost(postId));
                    reportDialog.setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss());
                    reportDialog.create().show();
                    return true;
                case R.id.action_share:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String url = "https://www.appscrip.com/post/" + postId;

                            Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                                    .setLink(Uri.parse(url))
                                    .setDynamicLinkDomain("dubly.page.link")
                                    .setAndroidParameters(new DynamicLink.AndroidParameters.Builder(BuildConfig.APPLICATION_ID).setMinimumVersion(1).build())
                                    .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                                            .setTitle("Hey check out this post on '" + getString(R.string.app_name) + "' ")
                                            .setDescription(data.getTitle())
                                            .setImageUrl(Uri.parse(data.getImageUrl1().replace("mp4", "jpg")))
                                            .build()
                                    ).buildShortDynamicLink().addOnCompleteListener(SocialDetailActivity.this, task -> {
                                        if (task.isSuccessful()) {
                                            Uri dynamicLinkUri = task.getResult().getShortLink();
                                            Intent intent = new Intent(Intent.ACTION_SEND);
                                            intent.putExtra(Intent.EXTRA_TEXT, dynamicLinkUri.toString());
                                            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                                            intent.setType("text/plain");
                                            Intent chooser = Intent.createChooser(intent, getString(R.string.selectApp));
                                            startActivity(chooser);
                                        }
                                    });

                        }
                    }).start();


                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
        return false;
    }


    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
        mVideoPlayerManager.resetMediaPlayer();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    public void liked(boolean flag) {
//        dataList.get(viewPager.getCurrentItem()).setLiked(flag);
//        samplePager.notifyDataSetChanged();
    }

    @Override
    public void dismissDialog() {
    }

    @Override
    public void addToReportList(ArrayList<String> data) {
        arrayAdapter.clear();
        arrayAdapter.addAll(data);
    }

    @Override
    public void setData(Data data) {
        if (data != null) {
            this.data = data;
            postId = data.getPostId();
            if (dataList == null)
                dataList = new ArrayList<>();
            dataList.add(data);
//            samplePager.notifyDataSetChanged();
            userId = data.getUserId();
        }
    }

    @Override
    public void favourite(boolean flag, String postId) {

    }

    @Override
    public void likePost(Data data) {

    }


    @Override
    public void deleted() {
        onBackPressed();
    }

    @Override
    public void follow(boolean isChecked, String userId) {
        if (!isChecked)
            mPresenter.unfollow(userId);
        else
            mPresenter.follow(userId);
    }


    @Override
    public void showMessage(String msg, int msgId) {
        Toast.makeText(this, msgId != 0 ? getString(msgId) : msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!mList.isEmpty()) {
            rvList.post(new Runnable() {
                @Override
                public void run() {

                    mVideoVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            mLayoutManager.findFirstVisibleItemPosition(),
                            mLayoutManager.findLastVisibleItemPosition());

                }
            });
        }
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        if (data != null)
            invalidateMenu(data);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void reload() {

    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void send() {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        Intent i = new Intent(this, ActivityForwardMessage.class);
//        i.putExtra("postType", data.getMediaType1());
//        i.putExtra("postId", data.getPostId());
//        i.putExtra("postTitle", data.getTitle());
//        i.putExtra("messageType", 13);
//        i.putExtra("toUpload", false);
//        i.putExtra("payload", data.getMediaType1() == 0 ? data.getImageUrl1() : data.getImageUrl1().replace("mp4", "jpg"));
//        startActivity(i);
    }

    @Override
    public void comment(String postId) {
        startActivity(new Intent(this, CommentActivity.class).putExtra("postId", postId));
    }

    @Override
    public void profile(String userId) {
        startActivity(new Intent(this, ProfileActivity.class).putExtra("userId", userId));
    }

    @Override
    public void category() {
        Log.i(TAG, "category: ");
    }

    @Override
    public void club() {

    }

    @Override
    public void like(boolean flag, String postId) {
        mPresenter.postLike(postId);
    }

    @Override
    public void disLike(boolean isChecked, String postId) {
        mPresenter.postDislike(postId);
    }

    @Override
    public void favorite(boolean isChecked, String postId) {
        if (!isChecked)
            mPresenter.unlike(postId);
        else
            mPresenter.like(postId);
    }


    @Override
    public void music(String musicId, String musicName) {
        startActivity(new Intent(this, TrendingDetail.class).putExtra("call", "music").putExtra("musicId", musicId).putExtra("name", musicName));
    }


    @Override
    public void followers(List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> data) {
        itemAdapter = new ItemAdapter(this, data, this, typefaceManager);
        shareList.setAdapter(itemAdapter);
    }

    @Override
    public void onItemClick(chat.hola.com.howdoo.profileScreen.followers.Model.Data userdata) {
        JSONObject obj;
        obj = new JSONObject();

        /*
         * Post
         */

        try {
            String documentId = AppController.findDocumentIdOfReceiver(userdata.getId(), Utilities.tsInGmt(),
                    userdata.getFirstName(), userdata.getProfilePic(), "", false, userdata.getUsername(), "", false);

            String payload = data.getMediaType1() == 0 ? data.getImageUrl1() : data.getImageUrl1().replace("mp4", "jpg");
            String tsForServer = Utilities.tsInGmt();
            String tsForServerEpoch = new Utilities().gmtToEpoch(tsForServer);


            obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
            obj.put("from", AppController.getInstance().getUserId());
            obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());

            obj.put("timestamp", tsForServerEpoch);
            obj.put("id", tsForServerEpoch);
            obj.put("type", "13");
            obj.put("name", AppController.getInstance().getUserName());
            obj.put("postId", postId);
            obj.put("postTitle", data.getTitle());
            obj.put("postType", data.getMediaType1());

            obj.put("to", userdata.getId());
            obj.put("toDocId", documentId);

            Map<String, Object> map = new HashMap<>();
            map.put("message", payload);
            map.put("messageType", "13");
            map.put("isSelf", true);
            map.put("from", AppController.getInstance().getUserId());
            map.put("Ts", tsForServer);
            map.put("deliveryStatus", "0");
            map.put("id", tsForServerEpoch);
            map.put("postId", postId);
            map.put("postTitle", data.getTitle());
            map.put("postType", data.getMediaType1());

            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


            Map<String, Object> mapTemp = new HashMap<>();
            mapTemp.put("from", AppController.getInstance().getUserId());
            mapTemp.put("to", userdata.getId());
            mapTemp.put("toDocId", documentId);
            mapTemp.put("id", tsForServerEpoch);
            mapTemp.put("timestamp", tsForServerEpoch);

            String type = Integer.toString(13);
            mapTemp.put("type", type);
            mapTemp.put("postId", postId);
            mapTemp.put("postTitle", etMessage.getText().toString() + "\n" + data.getTitle());
            mapTemp.put("postType", data.getMediaType1());
            mapTemp.put("name", AppController.getInstance().getUserName());
            mapTemp.put("message", payload);

            AppController.getInstance().getDbController().addUnsentMessage(AppController.getInstance().getunsentMessageDocId(), mapTemp);

            obj.put("name", AppController.getInstance().getUserName());

            HashMap<String, Object> map1 = new HashMap<>();
            map1.put("messageId", tsForServerEpoch);
            map1.put("docId", documentId);
            AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + userdata.getId(), obj, 1, false, map1);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void setData(List<Data> dataList) {
        try {
            for (Data data : dataList)
                mList.add(ItemFactory.createItem(this, mVideoPlayerManager, data));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}