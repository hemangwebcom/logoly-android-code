package chat.hola.com.howdoo.search.people;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.FollowResponse;
import chat.hola.com.howdoo.search.model.SearchResponse;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 24/2/18.
 */

public class PeoplePresenter implements PeopleContract.Presenter {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;
    @Nullable
    private PeopleContract.View view;
    @Inject
    HowdooService service;


    @Inject
    PeoplePresenter() {
    }

    @Override
    public void attachView(PeopleContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {

    }

    @Override
    public void search(CharSequence charSequence, int offset, int limit) {
        isLoading = true;
        service.search(AppController.getInstance().getApiToken(), "en", charSequence.toString(), offset, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<SearchResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<SearchResponse> response) {
                        Log.i("RESPONSE", response.toString());
                        if (response.code() == 200 && response.body().getData() != null) {
                            isLastPage = response.body().getData().size() < PAGE_SIZE;
                            if (view != null){
                                view.showData(response.body().getData(),offset==0);
                            }
                        } else if (response.code() == 204) {
                            if (view != null) {
                                view.noData();
                            }
                        } else if (response.code() == 401)
                            if (view != null) {
                                view.sessionExpired();
                            }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("search", "onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }
                });
    }

    @Override
    public void follow(final int pos, String followingId) {
        Map<String, Object> params = new HashMap<>();
        params.put("followingId", followingId);
        service.follow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> response) {
                        if (view != null) {
                            if (response.code() == 200)
                                view.isFollowing(pos, 2);// private
                            else if (response.code() == 201)
                                view.isFollowing(pos, 1);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void unfollow(final int pos, String followingId) {
        service.unfollow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, followingId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> response) {
                        if (view != null) {
                            view.isFollowing(pos, 0);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void callApiOnScroll(CharSequence charSequence, int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                search(charSequence, PAGE_SIZE * page, PAGE_SIZE);
            }
        }
    }

    public void block(String currentUserId) {
        Map<String, String> map = new HashMap<>();
        map.put("reason", "block");
        service.block(AppController.getInstance().getApiToken(), Constants.LANGUAGE, currentUserId, "block", map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            view.blocked();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void addToContact(String currentUserId) {
        Map<String, Object> params = new HashMap<>();
        params.put("contactId", currentUserId);
        service.addToContact(AppController.getInstance().getApiToken(), "en", params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.reload();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
