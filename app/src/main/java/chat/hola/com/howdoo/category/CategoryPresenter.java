package chat.hola.com.howdoo.category;

import android.content.Intent;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.category.model.CategoryModel;
import chat.hola.com.howdoo.category.model.ClickListner;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.post.model.CategoryResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h1>CategoryPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 28/3/18
 */

public class CategoryPresenter implements CategoryContract.Presenter, ClickListner {

    @Inject
    HowdooService service;
    @Inject
    CategoryContract.View view;
    @Inject
    CategoryModel model;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    CategoryPresenter() {
    }

    @Override
    public void init() {
        view.applyFont();
        view.recyclerSetup();
    }

    /**
     * Returns list of categories and its data
     */
    @Override
    public void getCategory(String categoryId) {

        view.isLoadingData(true);
        service.getCategories(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<CategoryResponse>>() {
                    @Override
                    public void onNext(Response<CategoryResponse> response) {
                        if (response.code() == 200 && response.body().getData() != null) {
                            model.setChannel(response.body().getData(), categoryId);// showCategory(response.body().getData());
                        } else if (response.code() == 401)
                            view.sessionExpired();
                        view.isLoadingData(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        view.isLoadingData(false);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void onItemSelected(int position) {
        model.selectItem(position);
    }

    Intent getSelectedCategory() {
        return model.selectedCategory();
    }
}
