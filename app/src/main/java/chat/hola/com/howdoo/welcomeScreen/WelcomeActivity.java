package chat.hola.com.howdoo.welcomeScreen;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import org.json.JSONException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.CustomProgressDialog;
import chat.hola.com.howdoo.Profile.SaveProfile;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.manager.session.SessionManager;
import dagger.android.support.DaggerAppCompatActivity;

public class WelcomeActivity extends DaggerAppCompatActivity implements WelcomeContract.View {

    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    WelcomeContract.Presenter presenter;

    @BindView(R.id.agreeAndContinue)
    Button agreeAndContinue;
    @BindView(R.id.tapTv)
    TextView tapTv;
    @BindView(R.id.welcomeHowdooTv)
    TextView welcomeHowdooTv;


    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.btnForgotPassword)
    Button btnForgotPassword;

    private Unbinder unbinder;
    private CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));

        mSetTypeFaces();
        progressDialog = new CustomProgressDialog("Please wait...", this, typefaceManager);
        agreeAndContinue.startAnimation(AnimationUtils.loadAnimation(this, R.anim.tanslat));

        SpannableString ss = new SpannableString(tapTv.getText().toString());
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                String url = getString(R.string.termsUrl);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.color_white));
                ds.setUnderlineText(true);
                ds.setFakeBoldText(true);
            }
        };
        ss.setSpan(clickableSpan, 44, tapTv.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tapTv.setText(ss);
        tapTv.setMovementMethod(LinkMovementMethod.getInstance());
        tapTv.setHighlightColor(Color.WHITE);
    }

    private void mSetTypeFaces() {
        agreeAndContinue.setTypeface(typefaceManager.getSemiboldFont());
        tapTv.setTypeface(typefaceManager.getRegularFont());
        welcomeHowdooTv.setTypeface(typefaceManager.getRegularFont());
        etUsername.setTypeface(typefaceManager.getRegularFont());
        etPassword.setTypeface(typefaceManager.getRegularFont());
        btnForgotPassword.setTypeface(typefaceManager.getRegularFont());
    }

    @OnClick(R.id.agreeAndContinue)
    public void agreeAndContinue() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        if (isValidCredentials(username, password)) {
            progressDialog.getDialog().show();
            presenter.login(username, password);
        }
    }


    /**
     * <p>checks username and password fields are not empty</p>
     *
     * @param username : username
     * @param password :password
     */
    private boolean isValidCredentials(String username, String password) {
        if (TextUtils.isEmpty(username)) {
            showMessage(null, R.string.enterUserName);
            return false;
        } else if (TextUtils.isEmpty(password)) {
            showMessage(null, R.string.enterPassword);
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void showMessage(String msg, int msgId) {
        Toast.makeText(this, msgId == 0 ? msg : getString(msgId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(this);
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    public void userBlocked() {

    }

    @Override
    public void loginSuccessful(LoginResponse body) {
        Intent intent = new Intent(this, LandingActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void finishProgress() {
        if (progressDialog.getDialog() != null && progressDialog.getDialog().isShowing())
            progressDialog.getDialog().dismiss();
    }

    @Override
    public void reload() {

    }
}
