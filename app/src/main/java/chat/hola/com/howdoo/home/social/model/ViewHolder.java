package chat.hola.com.howdoo.home.social.model;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.Utilities.RoundedImageView;
import chat.hola.com.howdoo.Utilities.TypefaceManager;

/**
 * <h1>ViewHolder</h1>
 *
 * @author 3Embed
 * @since 24/2/2018.
 */

public class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.profileNameTv)
    public TextView profileNameTv;
    @BindView(R.id.tvTitle)
    public TextView tvTitle;
    @BindView(R.id.postTimeTv)
    public  TextView postTimeTv;
    @BindView(R.id.ivMedia)
    public ImageView ivMedia;
    @BindView(R.id.ivProfilePic)
    public ImageView ivProfilePic;
    @BindView(R.id.ivVideoCam)
    public ImageView ivVideoCam;
    @BindView(R.id.ivChannel)
    public ImageView ivChannel;
    @BindView(R.id.tvChannelName)
    public  TextView tvChannelName;
    @BindView(R.id.llChannelContainer)
    public  LinearLayout llChannelContainer;
    @BindView(R.id.parentContsraint)
    public  ConstraintLayout mConstraintLayout;

    @BindView(R.id.tvLikeCount)
    public   TextView tvLikeCount;
    @BindView(R.id.tvDislikeCount)
    public   TextView tvDislikeCount;
    @BindView(R.id.tvCommentCount)
    public   TextView tvCommentCount;
    @BindView(R.id.tvFavouriteCount)
    public   TextView tvFavouriteCount;
    @BindView(R.id.tvViewCount)
    public TextView tvViewCount;

    private ClickListner clickListner;

    public ViewHolder(View itemView, TypefaceManager typefaceManager, ClickListner clickListner) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.clickListner = clickListner;
        itemView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        postTimeTv.setTypeface(typefaceManager.getRegularFont());
        profileNameTv.setTypeface(typefaceManager.getMediumFont());
        tvChannelName.setTypeface(typefaceManager.getMediumFont());
        tvTitle.setTypeface(typefaceManager.getRegularFont());
        tvLikeCount.setTypeface(typefaceManager.getRegularFont());
        tvDislikeCount.setTypeface(typefaceManager.getRegularFont());
        tvCommentCount.setTypeface(typefaceManager.getRegularFont());
        tvFavouriteCount.setTypeface(typefaceManager.getRegularFont());
        tvViewCount.setTypeface(typefaceManager.getRegularFont());
    }

    @OnClick(R.id.ivMedia)
    public void item() {
        clickListner.onItemClick(getAdapterPosition(), ivMedia);
    }

    @OnClick(R.id.ivProfilePic)
    public void user() {
        clickListner.onUserClick(getAdapterPosition());
    }

    @OnClick(R.id.llChannelContainer)
    public void channel() {
        clickListner.onChannelClick(getAdapterPosition());
    }
}