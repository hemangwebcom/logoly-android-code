package chat.hola.com.howdoo.post;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.SocialShare;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;
import chat.hola.com.howdoo.post.model.CategoryResponse;
import chat.hola.com.howdoo.post.model.ChannelResponse;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 23/2/18.
 */

public class PostPresenter implements PostContract.Presenter {

    private static final String TAG = PostPresenter.class.getSimpleName();

    private Context context;
    private String path;
    private String type;
    private CompositeDisposable compositeDisposable;

    @Inject
    PostContract.View view;

    @Inject
    SocialShare socialShare;

    @Inject
    HowdooService service;

    @Inject
    PostActivity postActivity;


    @Inject
    PostPresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init(String path, String type) {
        this.path = path;
        this.type = type;
        view.displayMedia();
        view.applyFont();
    }

    @Override
    public void message(String message) {
        view.showMessage(message, 0);
    }


    @Override
    public void onBackPress() {
        view.onBackPress();
    }


    @Override
    public void getCategory() {
        //view.isLoadingData(true);
        service.getCategories(AppController.getInstance().getApiToken(), "en")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<CategoryResponse>>() {
                    @Override
                    public void onNext(Response<CategoryResponse> response) {
                        if (response.code() == 200 && response.body().getData() != null)
                            view.showCategory(response.body().getData());
                        else if (response.code() == 401)
                            view.sessionExpired();
                        // view.isLoadingData(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("getCategories", "onError: " + e.getMessage());
                        //view.isLoadingData(false);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void searchHashTag(String hashTag) {

        service.checkHashTag(AppController.getInstance().getApiToken(), Constants.LANGUAGE, hashTag)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Hash_tag_people_pojo>>() {
                    @Override
                    public void onNext(retrofit2.Response<Hash_tag_people_pojo> response) {
                        Log.i("RESPONSE", response.toString());
                        if (response.code() == 200)
                            view.setTag(response.body());
                        else if (response.code() == 401)
                            view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void searchUserTag(String userTag) {
        service.checkUserTag(AppController.getInstance().getApiToken(), Constants.LANGUAGE, userTag)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Hash_tag_people_pojo>>() {
                    @Override
                    public void onNext(retrofit2.Response<Hash_tag_people_pojo> response) {
                        if (response.code() == 200)
                            view.setUser(response.body());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    private void informUsers(final String msg) {
        new Handler(Looper.getMainLooper())
                .post(new Runnable() {
                    @Override
                    public void run() {
                        view.showMessage(msg, 0);
                    }
                });
    }

    public void disposeObservable() {
        compositeDisposable.clear();
    }


    @Override
    public void getCategories() {
        service.getCategories(AppController.getInstance().getApiToken(), "en")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<CategoryResponse>>() {
                    @Override
                    public void onNext(Response<CategoryResponse> response) {
                        if (response.code() == 200 && response.body().getData() != null)
                            view.attacheCategory(response.body().getData());
                        else if (response.code() == 401)
                            view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("getCategories", "onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void getChannels() {
        service.getChannels(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ChannelResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ChannelResponse> response) {
                        if (response.code() == 200 && response.body().getData() != null)
                            view.attacheChannels(response.body().getData());
                        else if (response.code() == 401)
                            view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("getChannels", "onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }



    public void updatePost(Map<String, String> map) {
        service.updatePost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.updated();
                        else if (response.code() == 401)
                            view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("getCategories", "onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
