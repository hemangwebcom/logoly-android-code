package chat.hola.com.howdoo.club.member;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.howdoo.dubly.R;

import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.comment.model.ClickListner;

/**
 * <h1>MemberAdapter</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class MemberAdapter extends RecyclerView.Adapter<chat.hola.com.howdoo.club.member.ViewHolder> {
    private TypefaceManager typefaceManager;
    private chat.hola.com.howdoo.club.member.ClickListner clickListner;
    private List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> dataList;
    private Context context;
    private int type = 0;

    @Inject
    public MemberAdapter(List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> dataList, Activity mContext, TypefaceManager typefaceManager) {
        this.typefaceManager = typefaceManager;
        this.dataList = dataList;
        this.context = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    @Override
    public chat.hola.com.howdoo.club.member.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);
        return new chat.hola.com.howdoo.club.member.ViewHolder(itemView, typefaceManager, clickListner);
    }

    @Override
    public void onBindViewHolder(final chat.hola.com.howdoo.club.member.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (getItemCount() > 0) {
            final chat.hola.com.howdoo.profileScreen.followers.Model.Data data = dataList.get(position);
            holder.ivProfilePic.setImageURI(data.getProfilePic());
            holder.tvName.setText(data.getFirstName() + " " + data.getLastName());
            if (type == 0 || type == 4) {
                holder.btnAction.setVisibility(View.GONE);
                holder.cbSelect.setVisibility(View.VISIBLE);
                holder.llRequestActions.setVisibility(View.GONE);
                holder.cbSelect.setChecked(data.isSelected());
            } else if (type == 1 && (!data.isAdmin() && !data.isOwner())) {
                holder.btnAction.setVisibility(View.VISIBLE);
                holder.cbSelect.setVisibility(View.GONE);
                holder.llRequestActions.setVisibility(View.GONE);
                holder.btnAction.setOnClickListener(view -> clickListner.openPopup(data.getUserId(), data.isBlocked()));
            } else if (type == 2) {
                holder.btnAction.setVisibility(View.GONE);
                holder.cbSelect.setVisibility(View.GONE);
                holder.llRequestActions.setVisibility(View.GONE);
            } else if (type == 3) {
                holder.btnAction.setVisibility(View.GONE);
                holder.cbSelect.setVisibility(View.GONE);
                holder.llRequestActions.setVisibility(View.VISIBLE);
                holder.ibAccept.setOnClickListener(view -> clickListner.requestAction(data.getUserId(), true));
                holder.ibDecline.setOnClickListener(view -> clickListner.requestAction(data.getUserId(), false));
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setListener(chat.hola.com.howdoo.club.member.ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public void setType(int type) {
        this.type = type;
    }
}