package chat.hola.com.howdoo.welcomeScreen;

import chat.hola.com.howdoo.Utilities.BaseView;

/**
 * <h1>WelcomeContract</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public interface WelcomeContract {

    interface View extends BaseView {

        void loginSuccessful(LoginResponse body);

        void finishProgress();
    }

    interface Presenter {
        void login(String userName, String password);
    }
}
