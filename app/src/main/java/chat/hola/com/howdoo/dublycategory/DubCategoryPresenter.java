package chat.hola.com.howdoo.dublycategory;

import android.util.Log;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.DublyService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.dubly.DubResponse;
import chat.hola.com.howdoo.dublycategory.modules.CategoryClickListner;
import chat.hola.com.howdoo.dublycategory.modules.ClickListner;
import chat.hola.com.howdoo.dublycategory.modules.DubCategoryResponse;
import chat.hola.com.howdoo.models.NetworkConnector;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h1>CommentPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class DubCategoryPresenter implements DubCategoryContract.Presenter,  CategoryClickListner {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;
    @Inject
    DublyService service;
    @Inject
    DubCategoryContract.View view;
    @Inject
    DubCategoryModel model;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    DubCategoryPresenter() {
    }


    @Override
    public CategoryClickListner getCategoryPresenter() {
        return this;
    }

    @Override
    public void getCategories(boolean b) {
        service.getDubCategories(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<DubCategoryResponse>>() {
                    @Override
                    public void onNext(Response<DubCategoryResponse> response) {
                        try {
                            if (response.code() == 200) {
                                isLastPage = response.body().getDubs().size() < PAGE_SIZE;
                                model.setCategoryData(response.body().getDubs(), b);
                            } else if (response.code() == 401) {
                                view.sessionExpired();
                            }
                        } catch (Exception ignored) {
                            Log.i("Exception", ignored.toString());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }
                });
    }


    @Override
    public void onItemClick(int position) {
        view.getList(model.getCategoryId(position),model.getCategoryName(position));
    }
}
