package chat.hola.com.howdoo.profileScreen.discover.contact;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.annotations.Expose;
import com.howdoo.dubly.R;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.Contact;
import chat.hola.com.howdoo.profileScreen.discover.facebook.FacebookFrag;

/**
 * <h>youAdapter.class</h>
 * <p>
 * This adapter class is used by {@link FacebookFrag}.
 *
 * @author 3Embed
 * @since 02/03/18.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    private static final String TAG = ContactAdapter.class.getSimpleName();

    private ArrayList<Contact> contacts = new ArrayList<>();
    private Context context;
    private TypefaceManager typefaceManager;
    private ClickListner clickListner;

    @Inject
    public ContactAdapter(Context context, TypefaceManager typefaceManager) {
        this.context = context;
        this.typefaceManager = typefaceManager;
    }

    public void setData(ArrayList<Contact> contacts) {
        this.contacts = contacts;
        notifyDataSetChanged();
    }

    @Override
    public ContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.you_row_new, parent, false);
        return new ContactAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactAdapter.ViewHolder holder, final int position) {

        if (contacts.size() > 0) {
            Contact contact = contacts.get(position);

            Glide.with(context).load(contact.getProfilePic()).asBitmap().centerCrop().placeholder(R.drawable.profile_one).into(new BitmapImageViewTarget(holder.ivRow));
            holder.tvRowTitle.setText(contact.getUserName());
            holder.tvRowTime.setText("");

//            try {
//                holder.tbFollow.setTextOn(context.getString(contact.get_private() == 1 ? R.string.requested : R.string.following));
//                Log.i(contact.getUserName(), holder.tbFollow.getTextOn().toString());
//                holder.tbFollow.setChecked(contact.getFollow().getStatus() == 1 || contact.getFollow().getStatus() == 2);
//                contact.setFollowing(contact.getFollow().getStatus() != null && (contact.getFollow().getStatus() == (contact.get_private() == 1 ? 2 : 1)));
//            } catch (NullPointerException e) {
//                Log.e(TAG, e.getMessage());
//            }

            try {

                boolean isPrivate = contact.get_private() == 1;
                boolean isChecked = false;

                switch (contact.getFollow().getStatus()) {
                    case 0:
                        //public - unfollow
                        isPrivate = contact.get_private() == 1;
                        isChecked = false;
                        break;
                    case 1:
                        //public - follow
                        isPrivate = false;
                        isChecked = true;
                        break;
                    case 2:
                        //private - requested
                        isPrivate = true;
                        isChecked = true;
                        break;
                    case 3:
                        //private - request
                        isPrivate = true;
                        isChecked = false;
                        break;


                }
                holder.tbFollow.setTextOn(context.getResources().getString(isPrivate ? R.string.requested : R.string.following));
                holder.tbFollow.setTextOff(context.getResources().getString(isPrivate ? R.string.request : R.string.follow));
                holder.tbFollow.setChecked(isChecked);
            } catch (NullPointerException ignored) {
            }

            holder.tbFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListner != null)
                        clickListner.follow(position, !holder.tbFollow.isChecked());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ivRow)
        ImageView ivRow;
        @BindView(R.id.tvRowTitle)
        TextView tvRowTitle;
        @BindView(R.id.tvRowTime)
        TextView tvRowTime;
        @BindView(R.id.tbFollow)
        ToggleButton tbFollow;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvRowTime.setVisibility(View.GONE);
            tvRowTitle.setTypeface(typefaceManager.getMediumFont());
            tvRowTitle.setTypeface(typefaceManager.getMediumFont());
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ProfileActivity.class);
            intent.putExtra("userId", contacts.get(getAdapterPosition()).getId());
            context.startActivity(intent);
            //((FollowersActivity) context).finish();
        }
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public interface ClickListner {
        void follow(int position, boolean isFollowing);
    }
}
