package chat.hola.com.howdoo.profileScreen.followers;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.ConnectivityReceiver;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import chat.hola.com.howdoo.profileScreen.followers.Model.Data;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by ankit on 19/3/18.
 */

public class FollowersActivity extends DaggerAppCompatActivity implements FollowersContract.View, ConnectivityReceiver.ConnectivityReceiverListener
        , FollowerAdapter.OnFollowUnfollowClickCallback, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    FollowersPresenter presenter;
    @Inject
    SessionManager sessionManager;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    FollowerAdapter followerAdapter;

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @BindView(R.id.recyclerFollowers)
    RecyclerView recyclerFollowers;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tvEmptyMsg)
    TextView tvEmptyMsg;
    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;

    private Unbinder unbinder;
    boolean isFollowerPage = true;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int PAGE_SIZE = Constants.PAGE_SIZE;
    private int page = 0;
    private String userId;
    LinearLayoutManager linearLayoutManager;

    List<Data> dataList = new ArrayList<>();
    AlertDialog.Builder actionDialog;
    ArrayAdapter<String> actionList;
    String currentUserId;

    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int[] firstVisibleItemPositions = new int[PAGE_SIZE];
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            presenter.callApiOnScroll(isFollowerPage, userId, firstVisibleItemPosition, visibleItemCount, totalItemCount);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        llNetworkError.setErrorListner(this);
        setReceivedData();
        setSwipeCallback();


        //for followers
        actionDialog = new AlertDialog.Builder(this);
        actionDialog.setTitle("Action for this user");


    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void reload() {
        onRefresh();
    }

    private void setSwipeCallback() {
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setReceivedData() {
        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("title", "Followers");
        userId = bundle.getString("userId", AppController.getInstance().getUserId());
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(true);
        if (title != null && title.equals("Followers")) {
            String followers = bundle.getString("followers", "0");
            tvTitle.setText("" + title);
            isFollowerPage = true;
            //tvFollowerCount.setText(""+followers+" "+getResources().getString(R.string.followersTitleText));
            presenter.loadFollowersData(PAGE_SIZE * page, PAGE_SIZE, userId);
            followerAdapter.setFollowers(true);
        } else {
            String following = bundle.getString("following", "0");
            tvTitle.setText("" + title);
            isFollowerPage = false;
            //tvFollowerCount.setText(""+following+" "+getResources().getString(R.string.followingTitleText));
            presenter.loadFolloweesData(PAGE_SIZE * page, PAGE_SIZE, userId);
            followerAdapter.setFollowers(false);
        }
        followerAdapter.setOnFollowUnfollowClickCallback(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        // AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(this, getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void applyFont() {
        tvTitle.setTypeface(typefaceManager.getSemiboldFont());
        //tvFollowerCount.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void recyclerViewSetup() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerFollowers.setLayoutManager(linearLayoutManager);
        recyclerFollowers.setHasFixedSize(false);
        recyclerFollowers.setAdapter(followerAdapter);
        recyclerFollowers.addOnScrollListener(recyclerViewOnScrollListener);
    }

    @Override
    public void showFollowers(List<Data> followers) {
        swipeRefreshLayout.setVisibility(followers.isEmpty() ? View.GONE : View.VISIBLE);
        llEmpty.setVisibility(followers.isEmpty() ? View.VISIBLE : View.GONE);
        tvEmptyMsg.setText(getString(R.string.followers_empty));
        if (followers.size() < PAGE_SIZE)
            isLastPage = true;
        this.dataList.addAll(followers);
        followerAdapter.setData(this.dataList);
    }

    @Override
    public void showFollowees(List<Data> followees) {
        swipeRefreshLayout.setVisibility(followees.isEmpty() ? View.GONE : View.VISIBLE);
        llEmpty.setVisibility(followees.isEmpty() ? View.VISIBLE : View.GONE);
        tvEmptyMsg.setText(getString(R.string.follow_someone));
        if (followees.size() < PAGE_SIZE)
            isLastPage = true;
        this.dataList.addAll(followees);
        followerAdapter.setData(this.dataList);
    }


    @Override
    public void invalidateBtn(int index, boolean isFollowing) {

    }

    @Override
    public void isDataLoading(boolean show) {
        if (show) {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(true);
        } else {
            if (swipeRefreshLayout != null)
                swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void blocked() {
    }

    @OnClick(R.id.ivBack)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Toast.makeText(this, isConnected ? "Internet connected" : "No internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFollow(String userId) {
        presenter.follow(userId, isFollowerPage);
    }

    @Override
    public void onUnfollow(String userId) {
        presenter.unFollow(userId);
    }

    @Override
    public void openActionPopup(Data data, boolean isBlocked) {
        currentUserId = data.getFollower();
        ArrayList<String> options = new ArrayList<>();
        actionList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, options);
        if (data.isBlocked()) {
            options.add("Unblock");
            actionDialog.setAdapter(actionList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            presenter.block(currentUserId, "unblock");
                            break;
                    }
                }
            });

        } else if (data.isContact() && !data.isBlocked()) {
            options.add("Block");
            options.add("Delete");
            actionDialog.setAdapter(actionList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            presenter.block(currentUserId, "block");
                            break;
                        case 1:
                            presenter.deleteContact(currentUserId);
                            break;
                    }
                }
            });
        } else if (data.isContact()) {
            options.add("Delete");
            actionDialog.setAdapter(actionList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            presenter.deleteContact(currentUserId);
                            break;
                    }
                }
            });
        } else {
            options.add("Refollow");
            options.add("Block");
            options.add("Add to contact");
            actionDialog.setAdapter(actionList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            presenter.follow(currentUserId, isFollowerPage);
                            break;
                        case 1:
                            presenter.block(currentUserId, "block");
                            break;
                        case 2:
                            presenter.addToContact(currentUserId);
                            break;
                    }
                }
            });

        }
        actionDialog.show();
    }

    @Override
    public void onRefresh() {
        if (!isLoading) {
            dataList.clear();
            page = 0;
            if (isFollowerPage) {
                presenter.loadFollowersData(PAGE_SIZE * page, PAGE_SIZE, userId);
            } else {
                presenter.loadFolloweesData(PAGE_SIZE * page, PAGE_SIZE, userId);
            }
        }
    }
}
