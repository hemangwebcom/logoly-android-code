package chat.hola.com.howdoo.Utilities;

import android.content.Context;
import android.graphics.Typeface;

import javax.inject.Inject;

/**
 * Created by ankit on 20/2/18.
 */

public class TypefaceManager {

    private Typeface tfSemiboldFont;
    private Typeface tfMediumFont;
    private Typeface tfRegularFont;
    private Typeface dublyLogo;
    private Context context;

    @Inject
    public TypefaceManager(Context context) {
        this.context = context;
        //initialization is pending
        tfRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/proxima-nova-soft-regular.ttf");
        tfMediumFont = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSoft-Medium.otf");
        tfSemiboldFont = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSoft-Semibold.otf");
        dublyLogo = Typeface.createFromAsset(context.getAssets(), "fonts/HominFunDemo.otf");
    }

    public Typeface getSemiboldFont() {
        return tfSemiboldFont;
    }

    public Typeface getMediumFont() {
        return tfMediumFont;
    }

    public Typeface getRegularFont() {
        return tfRegularFont;
    }

    public Typeface getDublyLogo() {
        return dublyLogo;
    }
}

