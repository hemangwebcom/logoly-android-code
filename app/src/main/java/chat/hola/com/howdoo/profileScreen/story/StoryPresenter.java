package chat.hola.com.howdoo.profileScreen.story;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.home.stories.StoriesPresenter;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.profileScreen.story.model.ProfilePostRequest;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 23/2/18.
 */

public class StoryPresenter implements StoryContract.Presenter {

    private static final String TAG = StoriesPresenter.class.getSimpleName();
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;


    @Nullable
    StoryContract.View view;
    @Inject
    HowdooService service;
    @Inject
    NetworkConnector networkConnector;


    @Inject
    StoryPresenter() {
    }

    @Override
    public void init() {
        if (view != null)
            view.setupRecyclerView();
    }

    @Override
    public void loadData(int skip, int limit) {
        isLoading = true;
        if (view != null) {
            view.isLoading(true);
        }
        service.getProfilePost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, skip, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ProfilePostRequest>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ProfilePostRequest> response) {
                        if (response.code() == 200 && response.body().getData() != null) {
                            isLastPage = response.body().getData().size() < PAGE_SIZE;
                            if (response.body().getData().size() > 0 && view != null) {
                                view.showData(response.body().getData(), skip == 0);
                                view.isLoading(false);
                            }
                        } else if (response.code() == 204) {
                            view.noData();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.isInternetAvailable(networkConnector.isConnected());
                            view.showMessage(e.getMessage(), 0);
                            view.isLoading(false);
                        }
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }
                });
    }

    @Override
    public void loadMemberData(String userId, int skip, int limit) {
        isLoading = true;
        if (view != null) {
            view.isLoading(true);
        }
        service.getMemberPosts(AppController.getInstance().getApiToken(), Constants.LANGUAGE, userId, skip, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ProfilePostRequest>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ProfilePostRequest> response) {
                        if (response.code() == 200 && response.body().getData() != null) {
                            isLastPage = response.body().getData().size() < PAGE_SIZE;
                            if (response.body().getData().size() > 0 && view != null) {
                                view.showData(response.body().getData(), skip == 0);
                            }
                        } else if (response.code() == 204)
                            view.noData();
                        if (view != null)
                            view.isLoading(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //view.showMessage(e.getMessage(),0);
                        if (view != null) {
                            view.isInternetAvailable(networkConnector.isConnected());
                            view.isLoading(false);
                        }
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }
                });
    }


    @Override
    public void unfavorite(String postId) {
        Map<String, String> map = new HashMap<>();
        map.put("userId", AppController.getInstance().getUserId());
        service.unlike(AppController.getInstance().getApiToken(), "en", postId, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            Log.w(TAG, "unlike successful!!");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "unlike failed!!");
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void favorite(String postId) {
        Map<String, String> map = new HashMap<>();
        map.put("userId", AppController.getInstance().getUserId());
        service.like(AppController.getInstance().getApiToken(), "en", postId, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            Log.w(TAG, "like successful!!");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w(TAG, "like failed!!");
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    @Override
    public void attachView(StoryContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    public void callApiOnScroll(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount, String userId) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                if (userId != null && !userId.equals("")) {
                    if (userId.equals(AppController.getInstance().getUserId()))
                        loadData(page * PAGE_SIZE, PAGE_SIZE);
                    else
                        loadMemberData(userId, page * PAGE_SIZE, PAGE_SIZE);
                } else {
                    loadData(page * PAGE_SIZE, PAGE_SIZE);
                }
            }
        }
    }

    public void like(boolean like, String postId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", AppController.getInstance().getUserId());
        service.postLike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void dislike(boolean like, String postId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", AppController.getInstance().getUserId());
        service.postDislike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
