package chat.hola.com.howdoo.ViewHolders;
/*
 * Created by moda on 15/04/16.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.howdoo.dubly.R;
import chat.hola.com.howdoo.Utilities.AdjustableImageView;


/**
 * View holder for media history video recycler view item
 */
public class ViewHolderVideoMedia extends RecyclerView.ViewHolder {


    public AdjustableImageView thumbnail;

    public TextView fnf;

    public  ViewHolderVideoMedia(View view) {
        super(view);
        fnf = (TextView) view.findViewById(R.id.fnf);
        thumbnail = (AdjustableImageView) view.findViewById(R.id.vidshow);
    }
}
