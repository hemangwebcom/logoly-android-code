package chat.hola.com.howdoo.comment.model;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.howdoo.dubly.R;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.TagSpannable;
import chat.hola.com.howdoo.Utilities.TimeAgo;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.Utilities.UserSpannable;

/**
 * <h1>BlockUserAdapter</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class CommentAdapter extends RecyclerView.Adapter<ViewHolder> {
    private TypefaceManager typefaceManager;
    private ClickListner clickListner;
    private List<Comment> comments;
    private Context context;

    @Inject
    public CommentAdapter(List<Comment> comments, Activity mContext, TypefaceManager typefaceManager) {
        this.typefaceManager = typefaceManager;
        this.comments = comments;
        this.context = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
        return new ViewHolder(itemView, typefaceManager, clickListner);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (getItemCount() > 0) {
            final Comment comment = comments.get(position);

            SpannableString spanString = new SpannableString(comment.getComment());
            Matcher matcher = Pattern.compile("#([A-Za-z0-9_-]+)").matcher(spanString);
            findMatch(spanString, matcher);
            holder.tvMessage.setText(spanString);

            SpannableString userSpanString = new SpannableString(comment.getComment());
            Matcher userMatcher = Pattern.compile("@([A-Za-z0-9_-]+)").matcher(spanString);
            findUserMatch(userSpanString, userMatcher);

            holder.tvMessage.setText(userSpanString);
            holder.tvMessage.setMovementMethod(LinkMovementMethod.getInstance());

            holder.tvUserName.setText(comment.getCommentedBy());
            holder.ivProfilePic.setImageURI(comment.getProfilePic());
            holder.tvTime.setText(TimeAgo.getTimeAgo(Long.parseLong(comment.getTimeStamp())));

            holder.item.setSelected(comment.isSelected());
        }
    }

    private void findMatch(SpannableString spanString, Matcher matcher) {
        while (matcher.find()) {
            final String tag = matcher.group(0);
            spanString.setSpan(new TagSpannable(context, tag), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    private void findUserMatch(SpannableString spanString, Matcher matcher) {
        while (matcher.find()) {
            final String tag = matcher.group(0);
            spanString.setSpan(new UserSpannable(context, tag), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }
}