package chat.hola.com.howdoo.welcomeScreen;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>BlockUserPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class WelcomePresenter implements WelcomeContract.Presenter {

    @Inject
    HowdooService service;
    @Inject
    WelcomeContract.View view;
    @Inject
    WelcomeModel model;

    @Inject
    WelcomePresenter() {
    }


    @Override
    public void login(String userName, String password) {

        service.login(Constants.AUTH, Constants.LANGUAGE, model.getParams(userName, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<LoginResponse>>() {
                    @Override
                    public void onNext(Response<LoginResponse> response) {
                        switch (response.code()) {
                            case 200:
                                model.loginSuccessful(response.body());
                                view.loginSuccessful(response.body());
                                break;
                            case 401:
                                view.sessionExpired();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                        view.finishProgress();
                    }
                });
    }
}
