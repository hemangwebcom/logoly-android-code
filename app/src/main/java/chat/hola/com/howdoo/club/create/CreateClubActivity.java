package chat.hola.com.howdoo.club.create;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.howdoo.dubly.R;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.Dialog.ImageSourcePicker;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Utilities.ConnectivityReceiver;
import chat.hola.com.howdoo.Utilities.RoundedImageView;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.club.detail.ClubDetailActivity;
import chat.hola.com.howdoo.club.detail.ClubDetailResponse;
import chat.hola.com.howdoo.club.member.MemberActivity;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.post.model.CategoryData;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.profileScreen.addChannel.AddChannelActivity;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by ankit on 19/2/18.
 */

public class CreateClubActivity extends DaggerAppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener, CreateClubContract.View {

    private static final String[] ACCESS_OPTIONS = {"Public", "Private", "Exclusive"};
    private final String TAG = AppCompatActivity.class.getSimpleName();
    private int CAMERA_REQ_CODE = 24;
    private int READ_STORAGE_REQ_CODE = 26;
    private int WRITE_STORAGE_REQ_CODE = 27;

    private static final int RESULT_CAPTURE_IMAGE = 0;
    private static final int RESULT_LOAD_IMAGE = 1;

    @Inject
    CreateClubPresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    ImageSourcePicker imageSourcePicker;
    @Inject
    SessionManager sessionManager;

    @BindView(R.id.root)
    CoordinatorLayout root;
    @BindView(R.id.ivProfile)
    RoundedImageView ivProfilePic;
    @BindView(R.id.ivCoverPic)
    ImageView ivCoverPic;

    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.spAccess)
    Spinner spAccess;
    @BindView(R.id.etStatus)
    TextInputEditText etStatus;
    @BindView(R.id.etAbout)
    TextInputEditText etAbout;
    @BindView(R.id.btnCreate)
    Button btnCreate;
    private boolean isEdit = false;
    private String clubId;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void userBlocked() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_club);
        ButterKnife.bind(this);
        toolbarSetup();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ACCESS_OPTIONS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAccess.setAdapter(adapter);

        ClubDetailResponse.ClubDetail clubDetail = (ClubDetailResponse.ClubDetail) getIntent().getSerializableExtra("clubDetail");
        if (clubDetail != null)
            fillDetail(clubDetail);
    }

    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    private void fillDetail(ClubDetailResponse.ClubDetail clubDetail) {
        isEdit = true;
        spAccess.setEnabled(false);
        clubId = clubDetail.getId();
        btnCreate.setText("Update Club");
        etName.setText(clubDetail.getSubject());
        spAccess.setSelection(clubDetail.getAccess() - 1);
        etStatus.setText(clubDetail.getStatus());
        etAbout.setText(clubDetail.getAbout());

        Glide.with(getBaseContext()).load(clubDetail.getImage()).asBitmap().centerCrop()
                .placeholder(R.drawable.profile_one).into(ivProfilePic);

        Glide.with(getBaseContext()).load(clubDetail.getCoverPic()).asBitmap().centerCrop().into(ivCoverPic);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.btnCreate)
    public void btnCreate() {
        String name = etName.getText().toString();
        int access = spAccess.getSelectedItemPosition();
        String status = etStatus.getText().toString();
        String about = etAbout.getText().toString();
        access++;
        if (validate(name, status, about)) {
            if (!isEdit)
                presenter.createClubValidate(name, about, access, status);
            else
                presenter.updateClubValidate(clubId, name, about, access, status);
        } else {
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validate(String name, String status, String about) {
        return !TextUtils.isEmpty(name) && !TextUtils.isEmpty(status) && !TextUtils.isEmpty(about);
    }

    @OnClick(R.id.ivProfile)
    public void profilePic() {
        presenter.setPictype(true);
        hideKeyboard();
        ImageSourcePicker.OnSelectImageSource callback = new ImageSourcePicker.OnSelectImageSource() {

            @Override
            public void onCamera() {
                checkCameraPermissionImage();
            }

            @Override
            public void onGallary() {
                checkReadImage();
            }

            @Override
            public void onCancel() {
                //nothing to do
            }
        };
        imageSourcePicker.setOnSelectImageSource(callback);
        imageSourcePicker.show();
    }

    @OnClick(R.id.ivEditCover)
    public void coverPic() {
        presenter.setPictype(false);
        hideKeyboard();
        ImageSourcePicker.OnSelectImageSource callback = new ImageSourcePicker.OnSelectImageSource() {

            @Override
            public void onCamera() {
                checkCameraPermissionImage();
            }

            @Override
            public void onGallary() {
                checkReadImage();
            }

            @Override
            public void onCancel() {
                //nothing to do
            }
        };
        imageSourcePicker.setOnSelectImageSource(callback);
        imageSourcePicker.show();
    }

    private void checkCameraPermissionImage() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                presenter.launchCamera(getPackageManager());
            } else {
                /*
                 *permission required to save the image captured
                 */
                requestReadImagePermission(0);
            }
        } else {
            requestCameraPermissionImage();
        }
    }

    private void requestCameraPermissionImage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {

            Snackbar snackbar = Snackbar.make(root, R.string.string_221,
                    Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ActivityCompat.requestPermissions(CreateClubActivity.this, new String[]{Manifest.permission.CAMERA},
                            24);
                }
            });


            snackbar.show();


            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);


        } else {


            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    24);
        }
    }

    private void checkReadImage() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            presenter.launchImagePicker();
        } else {
            requestReadImagePermission(1);
        }
    }

    private void requestReadImagePermission(int k) {
        if (k == 1) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar snackbar = Snackbar.make(root, R.string.string_222,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityCompat.requestPermissions(CreateClubActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                READ_STORAGE_REQ_CODE);
                    }
                });
                snackbar.show();
                View view = snackbar.getView();
                ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                        .setGravity(Gravity.CENTER_HORIZONTAL);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        READ_STORAGE_REQ_CODE);
            }
        } else if (k == 0) {
            /*
             * For capturing the image permission
             */
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar snackbar = Snackbar.make(root, R.string.string_1218,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        ActivityCompat.requestPermissions(CreateClubActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                WRITE_STORAGE_REQ_CODE);
                    }
                });

                snackbar.show();
                View view = snackbar.getView();
                ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                        .setGravity(Gravity.CENTER_HORIZONTAL);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_STORAGE_REQ_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE) {

            presenter.parseSelectedImage(requestCode, resultCode, data);

        } else if (requestCode == RESULT_CAPTURE_IMAGE) {

            presenter.parseCapturedImage(requestCode, resultCode, data);

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            presenter.parseCropedImage(requestCode, resultCode, data);
        }
    }


    private void hideKeyboard() {
//        if (imm != null) {
//            imm.hideSoftInputFromWindow(etChannelName.getWindowToken(), 0);
//            imm.hideSoftInputFromWindow(etDescription.getWindowToken(), 0);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void applyFont() {

    }

    @Override
    public void showCategories(List<CategoryData> categories) {
    }

    @Override
    public void showProgress(boolean show) {
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void launchCamera(Intent intent) {
        startActivityForResult(intent, RESULT_CAPTURE_IMAGE);
    }

    @Override
    public void showSnackMsg(int msgId) {

    }

    @Override
    public void launchImagePicker(Intent intent) {
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    public void launchCropImage(Uri data) {
        CropImage.activity(data).start(this);
    }

    @Override
    public void setProfileImage(Bitmap bitmap) {
        ivProfilePic.setImageBitmap(bitmap);
    }

    @Override
    public void setCoverPic(Bitmap bitmap) {
        ivCoverPic.setImageBitmap(bitmap);
    }

    @Override
    public void addMembers(String profilePic, String coverPic, String subject, String status, String about, int access) {
        Intent intent = new Intent(this, MemberActivity.class);
        intent.putExtra("subject", subject);
        intent.putExtra("image", profilePic);
        intent.putExtra("coverImage", coverPic);
        intent.putExtra("about", about);
        intent.putExtra("access", access);
        intent.putExtra("status", status);
        startActivity(intent);
    }

    @Override
    public void clubUpdated() {
        Toast.makeText(this, "Club updated successfully", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ClubDetailActivity.class);
        intent.putExtra("clubId", clubId);
        startActivity(intent);
        finish();
    }

    @Override
    public void clubCreated() {
        showMessage("Club created successfully", -1);
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(this, getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @OnClick(R.id.flAddProfile)
    void addProfile() {
        presenter.setPictype(true);
        hideKeyboard();
        ImageSourcePicker.OnSelectImageSource callback = new ImageSourcePicker.OnSelectImageSource() {

            @Override
            public void onCamera() {
                checkCameraPermissionImage();
            }

            @Override
            public void onGallary() {
                checkReadImage();
            }

            @Override
            public void onCancel() {
                //nothing to do
            }
        };
        imageSourcePicker.setOnSelectImageSource(callback);
        imageSourcePicker.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    /**
     * Result of the permission request
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == CAMERA_REQ_CODE) {

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(CreateClubActivity.this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(CreateClubActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        presenter.launchCamera(getPackageManager());
                    } else {
                    }
                } else {
                    //camera permission denied msg
                    showSnackMsg(R.string.string_62);
                }
            } else {
                //camera permission denied msg
                showSnackMsg(R.string.string_62);
            }

        } else if (requestCode == READ_STORAGE_REQ_CODE) {

            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(CreateClubActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    presenter.launchImagePicker();

                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            } else {
                //Access storage permission denied
                showSnackMsg(R.string.string_1006);
            }
        } else if (requestCode == WRITE_STORAGE_REQ_CODE) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(CreateClubActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    presenter.launchCamera(getPackageManager());
                } else {
                    showSnackMsg(R.string.string_1006);
                }
            }
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected)
            Toast.makeText(this, "Internet connected", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "No internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void reload() {

    }
}
