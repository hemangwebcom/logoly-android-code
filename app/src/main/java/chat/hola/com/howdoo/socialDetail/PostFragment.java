package chat.hola.com.howdoo.socialDetail;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import chat.hola.com.howdoo.Activities.MainActivity;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Utilities.CountFormat;
import chat.hola.com.howdoo.Utilities.OnSwipeTouchListener;
import chat.hola.com.howdoo.Utilities.SlideAnimationUtil;
import chat.hola.com.howdoo.Utilities.TagSpannable;
import chat.hola.com.howdoo.comment.CommentActivity;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.trendingDetail.TrendingDetail;
import chat.hola.com.howdoo.welcomeScreen.WelcomeActivity;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 8/13/2018.
 */
public class PostFragment extends Fragment implements TextureView.SurfaceTextureListener, PostContract.View {
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();


    @BindView(R.id.ivMedia)
    ImageView ivMedia;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.tvCategory)
    TextView tvCategory;
    @BindView(R.id.tvChannel)
    TextView tvChannel;
    @BindView(R.id.ivProfilePic)
    SimpleDraweeView ivProfilePic;
    @BindView(R.id.ivFavourite)
    CheckBox ivFavourite;
    @BindView(R.id.ivLike)
    CheckBox ivLike;
    @BindView(R.id.ivDisLike)
    CheckBox ivDisLike;
    @BindView(R.id.ivComment)
    ImageView ivComment;
    @BindView(R.id.tvLikeCount)
    TextView tvLikeCount;
    @BindView(R.id.tvCommentCount)
    TextView tvCommentCount;
    @BindView(R.id.tvViewCount)
    TextView tvViewCount;
    @BindView(R.id.tvDisLikeCount)
    TextView tvDisLikeCount;
    @BindView(R.id.tvFavouriteCount)
    TextView tvFavouriteCount;
    @BindView(R.id.rlItem)
    RelativeLayout rlItem;
    @BindView(R.id.flMediaContainer)
    RelativeLayout flMediaContainer;
    @BindView(R.id.ibReplay)
    ImageButton ibReplay;
    @BindView(R.id.video)
    TextureView video;
    @BindView(R.id.vBgLike)
    View vBgLike;
    @BindView(R.id.ivLikeIt)
    ImageView ivLikeIt;
    @BindView(R.id.videoContainer)
    RelativeLayout videoContainer;
    @BindView(R.id.ibPlay)
    CheckBox ibPlay;
    @BindView(R.id.tvMusic)
    TextView tvMusic;
    @BindView(R.id.llMusic)
    LinearLayout llMusic;
    @BindView(R.id.tvDivide)
    TextView tvDivide;
    //    @BindView(R.id.cbFollow)
//    CheckBox cbFollow;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.llFabContainer)
    LinearLayout llFabContainer;
    @BindView(R.id.ivPlay)
    ImageView ivPlay;
    @Inject
    SessionManager sessionManager;

    private PostPresenter presenter;
    private String postId;
    private Data data = null;
    private String userId;
    private String musicId;
    private String categoryId;
    private MediaPlayer mediaPlayer;
    private String path;
    private int type;
    private int favourite = 0;
    private int like = 0;
    private int dislike = 0;
    private String channelId;
    private boolean show = true;
    Handler handler = new Handler();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.post_detail, container, false);
        ButterKnife.bind(this, rootView);
        Bundle bundle = this.getArguments();
        presenter = new PostPresenter(getContext());
        if (bundle != null) {
            data = (Data) bundle.getSerializable("data");

            displayData(data);

            flMediaContainer.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                public void onSingleTap() {
                    if (mediaPlayer != null) {
//                        ibPlay.setVisibility(View.VISIBLE);
//                        ibPlay.setChecked(!ibPlay.isChecked());
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                        } else {
                            play(path);
                        }
                    }
                }

                public void onSwipeRight() {
                    SlideAnimationUtil.slideOutToRight(getActivity().getApplicationContext(), llFabContainer);
                    llFabContainer.setVisibility(View.GONE);
                }

                public void onSwipeLeft() {
                    SlideAnimationUtil.slideInFromLeft(getActivity().getApplicationContext(), llFabContainer);
                    llFabContainer.setVisibility(View.VISIBLE);
                }
            });
        }


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    SlideAnimationUtil.slideOutToRight(getActivity().getApplicationContext(), llFabContainer);
                    llFabContainer.setVisibility(View.GONE);
                } catch (Exception ignored) {
                }
            }
        }, 5000);

        return rootView;
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser)   // If we are becoming invisible, then...
        {
            if (mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
            } else if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
        }

        if (isVisibleToUser) // If we are becoming visible, then...
        {
            /*  BUG-ID : DUB-375
             *  BUG-DESC : Auto play does not work, till you tap on the video to play
             * */
            Bundle bundle = this.getArguments();
            data = (Data) bundle.getSerializable("data");


            path = data.getImageUrl1();//.replace("upload/", "upload/ar_" + String.valueOf(MainActivity.ratio) + ",c_fill/");
            play(path);
        }

//        if (isVisibleToUser) // If we are becoming visible, then...
//        {
//            /*  BUG-ID : DUB-375
//             *  BUG-DESC : Auto play does not work, till you tap on the video to play
//             * */
//            Bundle bundle = this.getArguments();
//            data = (Data) bundle.getSerializable("data");
//            path = data.getImageUrl1();
//            play(path);
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        /*  BUG-ID : DUB-373
//         *  BUG-DESC : open the page with video, minimise in android 9 and then open the same page against will show the black page.
//         * */
//        if (mediaPlayer == null)
//            mediaPlayer = new MediaPlayer();
//        if (!mediaPlayer.isPlaying()) {
//            Log.i("PLAYING", "2");
//            play(path);
//        }
    }

    @OnClick({R.id.tvMusic, R.id.llMusic})
    public void music() {
        startActivity(new Intent(getContext(), TrendingDetail.class).putExtra("call", "music").putExtra("musicId", musicId).putExtra("name", data.getDub().getName()));
    }

    @OnClick(R.id.tvLocation)
    public void tvLocation() {
        startActivity(new Intent(getContext(), TrendingDetail.class).putExtra("call", "location").putExtra("location", tvLocation.getText().toString()).putExtra("latlong", data.getLocation()));
    }


    @OnClick(R.id.tvCategory)
    public void tvCategory() {
        startActivity(new Intent(getContext(), TrendingDetail.class).putExtra("categoryId", categoryId).putExtra("call", "category").putExtra("category", tvCategory.getText().toString()));
    }

    @OnClick(R.id.ivComment)
    public void comment() {
        startActivity(new Intent(getContext(), CommentActivity.class).putExtra("postId", postId));
    }

    @OnClick({R.id.tvUserName, R.id.ivProfilePic})
    public void profile() {
        startActivity(new Intent(getContext(), ProfileActivity.class).putExtra("userId", userId));
    }

    private void findMatch(SpannableString spanString, Matcher matcher) {
        while (matcher.find()) {
            final String tag = matcher.group(0);
            spanString.setSpan(new TagSpannable(getContext(), tag, R.color.color_white), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    //display data
    public void displayData(Data data) {
        try {
            userId = data.getUserId();
            if (data.getFavouritesCount() != null && data.getFavouritesCount().isEmpty())
                favourite = Integer.parseInt(data.getFavouritesCount());
            if (data.getLikesCount() != null && data.getLikesCount().isEmpty())
                like = Integer.parseInt(data.getLikesCount());
            if (data.getDislikesCount() != null && data.getDislikesCount().isEmpty())
                dislike = Integer.parseInt(data.getDislikesCount());
            postId = data.getPostId();
            Uri uri = Uri.parse(data.getImageUrl1());
            Glide.with(getContext())
                    .load(uri.toString().replace("mp4", "jpg"))
                    .asBitmap()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivMedia);

//            if (uri.toString().contains("mp4") && !mediaPlayer.isPlaying())
//                ibPlay.setVisibility(View.VISIBLE);

            ivProfilePic.setImageURI(data.getProfilepic());
            tvUserName.setText("@" + data.getUsername());
            tvViewCount.setText(CountFormat.format(Long.parseLong(data.getDistinctViews())));
            tvFavouriteCount.setText(CountFormat.format(Long.parseLong(String.valueOf(favourite))));
            tvCommentCount.setText(CountFormat.format(Long.parseLong(data.getCommentCount())));
            tvLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(like))));
            tvDisLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(dislike))));
//            cbFollow.setVisibility(AppController.getInstance().getUserId().equals(userId) ? View.GONE : View.VISIBLE);
//            cbFollow.setChecked(data.getFollowStatus() != 0);


            if (data.getDub() != null) {
                llMusic.setVisibility(View.VISIBLE);
                tvMusic.setText(data.getDub().getName());
                musicId = data.getDub().getId();
            }
            path = null;
            type = data.getMediaType1();
            ivFavourite.setChecked(data.isFavourite());
            ivLike.setChecked(data.isLiked());
            ivDisLike.setChecked(data.isDisliked());
            if (data.getMediaType1() == 1) {
                path = data.getImageUrl1();

                videoContainer.setVisibility(View.VISIBLE);
                video.setSurfaceTextureListener(this);
            }

            //title
            if (data.getTitle() != null && !data.getTitle().trim().equals("")) {
                tvTitle.setVisibility(View.VISIBLE);
                SpannableString spanString = new SpannableString(data.getTitle());
                Matcher matcher = Pattern.compile("#([A-Za-z0-9_-]+)").matcher(spanString);
                findMatch(spanString, matcher);
                Matcher userMatcher = Pattern.compile("@([A-Za-z0-9_-]+)").matcher(spanString);
                findMatch(spanString, userMatcher);
                tvTitle.setText(spanString);
                tvTitle.setMovementMethod(LinkMovementMethod.getInstance());
            }

            //location
            if (data.getPlace() != null && !data.getPlace().trim().equals("")) {
                tvLocation.setVisibility(View.VISIBLE);
                tvLocation.setText(data.getPlace());
            }

            //divide
            if (!data.getChannelName().isEmpty() && !data.getCategoryName().isEmpty())
                tvDivide.setVisibility(View.VISIBLE);

            //category
            if (data.getCategoryName() != null && !data.getCategoryName().trim().equals("")) {
                tvCategory.setVisibility(View.VISIBLE);
                tvCategory.setText(data.getCategoryName());
                categoryId = data.getCategoryId();
            }

            //channel
            if (data.getChannelImageUrl() != null) {
                tvChannel.setVisibility(View.VISIBLE);
                tvChannel.setText(data.getChannelName());
                channelId = data.getChannelId();
            }
//        ivFavourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                new Handler(Looper.getMainLooper()).post(() -> {
//                    ((SocialDetailActivity) Objects.requireNonNull(getActivity())).like(isChecked, postId);
//                    if (!ivFavourite.isChecked()) {
//                        likes++;
//                    } else if (likes > 0) {
//                        likes--;
//                    }
//                });
//                ivFavourite.setChecked(isChecked);
//                Log.i("LIKE", "" + likes);
//
//                tvLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(likes))));
//            }
//        });
        } catch (Exception ignored) {

        }
    }

    @OnClick(R.id.tvChannel)
    public void channel() {
        if (channelId != null)
            startActivity(new Intent(getContext(), TrendingDetail.class).putExtra("call", "channel").putExtra("channelId", channelId));
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int i, int i1) {
        if (mediaPlayer == null)
            mediaPlayer = new MediaPlayer();
        mediaPlayer.setSurface(new Surface(surface));
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }

    private void play(String path) {

        try {

            mediaPlayer.reset();
            mediaPlayer.setDataSource(path);
            mediaPlayer.setLooping(true);
            mediaPlayer.prepare();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.start();
            mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
                    if (mediaPlayer.isPlaying())
                        progressBar.setVisibility(View.GONE);
                }
            });

//            ibPlay.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void liked(boolean flag) {
//        ivFavourite.setSelected(flag);
//        if (flag) likes++;
//        else if (likes > 0) likes--;
//        tvLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(likes))));
    }

    @Override
    public void deleted() {
    }

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {

    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    public void userBlocked() {

    }

    @Override
    public void reload() {

    }

    @OnClick(R.id.ivShare)
    public void share() {
        //  ((SocialDetailActivity) getActivity()).send();
    }

//    @OnCheckedChanged(R.id.cbFollow)
//    public void onFOllowChanged(CheckBox view, boolean isChecked) {
//        new Handler(Looper.getMainLooper()).post(() -> {
//            ((SocialDetailActivity) getActivity()).follow(isChecked, postId);
//        });
//    }

    @OnCheckedChanged(R.id.ivFavourite)
    public void onCheckedChangedFavourite(CheckBox view, boolean isChecked) {
        new Favourite(isChecked).execute();
    }

    @OnCheckedChanged(R.id.ivLike)
    public void onCheckedChangedLike(CheckBox view, boolean isChecked) {
        new Like(isChecked).execute();
    }

    @OnCheckedChanged(R.id.ivDisLike)
    public void onCheckedChangedDislike(CheckBox view, boolean isChecked) {
        new Dislike(isChecked).execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class Favourite extends AsyncTask<Void, Void, Void> {
        private boolean isChecked;

        public Favourite(boolean isChecked) {
            this.isChecked = isChecked;
        }

        @Override
        protected void onPreExecute() {
            //start showing progress here
        }

        @Override
        protected Void doInBackground(Void... params) {
            new Handler(Looper.getMainLooper()).post(() -> {
                // ((SocialDetailActivity) Objects.requireNonNull(getActivity())).favourite(isChecked, postId);
                if (isChecked) {
                    favourite++;
                } else if (favourite > 0) {
                    favourite--;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            tvFavouriteCount.setText(CountFormat.format(Long.parseLong(String.valueOf(favourite))));
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class Like extends AsyncTask<Void, Void, Void> {
        private boolean isChecked;

        public Like(boolean isChecked) {
            this.isChecked = isChecked;
        }

        @Override
        protected void onPreExecute() {
            //start showing progress here
        }

        @Override
        protected Void doInBackground(Void... params) {
            new Handler(Looper.getMainLooper()).post(() -> {
                // ((SocialDetailActivity) Objects.requireNonNull(getActivity())).like(isChecked, postId);
                if (isChecked) {
                    like++;
                } else if (like > 0) {
                    like--;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            tvLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(like))));
            if (isChecked)
                ivDisLike.setChecked(false);
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class Dislike extends AsyncTask<Void, Void, Void> {
        private boolean isChecked;

        public Dislike(boolean isChecked) {
            this.isChecked = isChecked;
        }

        @Override
        protected void onPreExecute() {
            //start showing progress here
        }

        @Override
        protected Void doInBackground(Void... params) {
            new Handler(Looper.getMainLooper()).post(() -> {
                //  ((SocialDetailActivity) Objects.requireNonNull(getActivity())).disLike(isChecked, postId);
                if (isChecked) {
                    dislike++;
                } else if (dislike > 0) {
                    dislike--;
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            tvDisLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(dislike))));
            if (isChecked)
                ivLike.setChecked(false);
        }

    }


    // slide the view from below itself to the current position
    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                view.getWidth(),                 // fromXDelta
                0,                 // toXDelta
                0,  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                view.getWidth(),                 // toXDelta
                0,                 // fromYDelta
                0); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public void onSlideViewButtonClick(View view) {
        if (show) {
            slideDown(view);
        } else {
            slideUp(view);
        }
        show = !show;
    }


}
