package chat.hola.com.howdoo.Notifications;

/*
 * Created by moda on 9/12/15.
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.howdoo.dubly.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import chat.hola.com.howdoo.Activities.ChatMessageScreen;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.GroupChat.Activities.GroupChatMessageScreen;
import chat.hola.com.howdoo.SecretChat.SecretChatMessageScreen;
import chat.hola.com.howdoo.Utilities.Utilities;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.ChannelRequestersActivity;
import chat.hola.com.howdoo.home.activity.youTab.followrequest.FollowRequestActivity;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;
import chat.hola.com.howdoo.trendingDetail.TrendingDetail;


/***
 *  FirebaseMessagingService to receive and display the push notifications received via firebase
 *
 */

public class MyFcmListenerService extends FirebaseMessagingService {
    private static final int NOTIFICATION_SIZE = 5;
    private static final int NOTIFICATION_ID = 1001;
    final String[] dTimeForDB = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "15", "30", "60", "3600", "86400", "604800"};//getResources().getStringArray(R.array.dTimeForDB);
    final String[] dTimeOptions = {"off", "1 second", "2 seconds", "3 seconds", "4 seconds", "5 seconds", "6 seconds", "7 seconds",
            "8 seconds", "9 seconds",
            "10 seconds", "15 seconds", "30 seconds", "1 minute", "1 hour", "1 day", "1 week"};//getResources().getStringArray(R.array.dTimeOptions);

    public static final String CHANNEL_ONE_ID = "com.howdoo.chat.ONE";
    public static final String CHANNEL_ONE_NAME = "Channel One";

    @Override
    public void onMessageReceived(final RemoteMessage message) {
        super.onMessageReceived(message);

        String topic = "/topics/" + AppController.getInstance().getUserId();
        if (message.getFrom().equals(topic)) {
            showNotification(message);
        } else {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> {
                if (!AppController.getInstance().isForeground() || !(AppController.getInstance().getActiveReceiverId().equals(message.getData().get("senderId")))) {
                    createChatNotification(message.getData());
                }
            });
        }
    }

    private void showNotification(RemoteMessage data) {
        Log.i("NOTIFICATION", "" + data.getData().toString());

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ONE_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(data.getData().get("title"))
                .setContentText(data.getData().get("msg"))
                .setAutoCancel(true)
                .setSound(soundUri);


        try {
            Intent intent;
            JSONObject object = new JSONObject(data.getData().get("data"));//new Gson().fromJson(object, JSONObject.class);
            Log.i("OBJECT", object.toString());
            NotificationCompat.BigTextStyle style1;
            switch (object.getString("type")) {
                case "channelSubscribe":
                    intent = new Intent(this, TrendingDetail.class);
                    String channelId = object.getString("channelId");
                    intent.putExtra("channelId", channelId);
                    intent.putExtra("call", "channel");
                    style1 = new NotificationCompat.BigTextStyle().bigText(data.getData().get("msg"));
                    builder.setStyle(style1);
                    break;
                case "channelRequest":
                    intent = new Intent(this, ChannelRequestersActivity.class);
                    intent.putExtra("call", "notification");
                    style1 = new NotificationCompat.BigTextStyle().bigText(data.getData().get("msg"));
                    builder.setStyle(style1);
                    break;
                case "postLiked":
                case "postCommented":
                case "newPost":
                    intent = new Intent(this, SocialDetailActivity.class);
                    String postId = object.getString("postId");
                    intent.putExtra("postId", postId);
                    String ImageUrl = object.getString("imageUrl");
                    builder.setLargeIcon(getCircularBitmapFrom(getBitmapFromUrl(object.getString("userImageUrl").replace("upload/", "upload/w_50,h_50,c_thumb,g_face,r_max/"))));
                    NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle()
                            .setBigContentTitle(data.getData().get("title"))
                            .setSummaryText(data.getData().get("msg"))
                            .bigPicture(getBitmapFromUrl(ImageUrl));
                    builder.setStyle(style);
                    break;
                case "followRequest":
                    intent = new Intent(this, FollowRequestActivity.class);
                    intent.putExtra("to", "youFrag");
                    style1 = new NotificationCompat.BigTextStyle().bigText(data.getData().get("msg"));
                    builder.setStyle(style1);
                    break;
                case "followed":
                case "following":
                    intent = new Intent(this, ProfileActivity.class);
                    String userId = object.getString("userId");
                    intent.putExtra("userId", userId);
                    style1 = new NotificationCompat.BigTextStyle().bigText(data.getData().get("msg"));
                    builder.setStyle(style1);
                    break;
                default:
                    intent = new Intent(this, LandingActivity.class);
                    style1 = new NotificationCompat.BigTextStyle().bigText(data.getData().get("msg"));
                    builder.setStyle(style1);
                    break;
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentIntent(pendingIntent);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationChannel notificationChannel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ONE_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        if (notificationManager != null) {
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }
    }

    public static Bitmap getCircularBitmapFrom(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            return null;
        }
        float radius = bitmap.getWidth() > bitmap.getHeight() ? ((float) bitmap
                .getHeight()) / 2f : ((float) bitmap.getWidth()) / 2f;
        Bitmap canvasBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP,
                Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(shader);

        Canvas canvas = new Canvas(canvasBitmap);

        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                radius, paint);

        return canvasBitmap;
    }

    public Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return Bitmap.createScaledBitmap(BitmapFactory.decodeStream(input), 630, 357, true);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    private void createChatNotification(Map data) {


        if (data.containsKey("groupChat")) {

            /*
             * Group chat
             */


            String groupId = (String) data.get("groupId");

            Intent intent = new Intent(this, GroupChatMessageScreen.class);


            intent.putExtra("receiverUid", groupId);
            intent.putExtra("receiverName", (String) data.get("groupName"));

            intent.putExtra("receiverIdentifier", groupId);


            intent.putExtra("colorCode", AppController.getInstance().getColorCode(5));

            String groupImage = "";
            if (data.containsKey("groupImage") && !((String) data.get("groupImage")).isEmpty()) {

                groupImage = (String) data.get("groupImage");


            }


            intent.putExtra("receiverImage", groupImage);


            intent.putExtra("fromNotification", true);


            String documentId = AppController.getInstance().findDocumentIdOfReceiver(groupId, "");
            if (documentId.isEmpty()) {
                /*
                 * Here, chatId is essentially put as empty,although it is same as groupId
                 */
                documentId = AppController.findDocumentIdOfReceiver(groupId, Utilities.tsInGmt(),
                        (String) data.get("groupName"), groupImage, "", false, groupId, "", true);
            }

            intent.putExtra("documentId", documentId);
            String messageType = "-1", replyType = "-1";

            if (data.containsKey("messageType")) {

                messageType = (String) data.get("messageType");


            }
            if (data.containsKey("replyType")) {

                messageType = (String) data.get("replyType");


            }
            if (AppController.getInstance().getContactSynced()) {
                generatePushNotificationLocal(documentId, messageType, (String) data.get("groupName"),
                        (String) data.get("payload"), intent, -1, "", groupId, replyType);
            }

        } else {


            /*
             *Normal or secret chat
             */


            Intent intent;
            long dTime = -1;
            String secretId = "";
            if (data.containsKey("")) {

                secretId = (String) data.get("secretId");

                dTime = (Long) data.get("dTime");
            }
            if (secretId.isEmpty()) {
                intent = new Intent(this, ChatMessageScreen.class);
            } else {

                intent = new Intent(this, SecretChatMessageScreen.class);
                intent.putExtra("secretId", secretId);
            }

            String senderId = (String) data.get("senderId");

            String senderIdentifier = (String) data.get("senderIdentifier");


            intent.putExtra("receiverUid", senderId);

            intent.putExtra("receiverIdentifier", senderIdentifier);

            intent.putExtra("colorCode", AppController.getInstance().getColorCode(5));


            intent.putExtra("fromNotification", true);


            String receiverName;
            String userImage = "";


            Map<String, Object> contactInfo


                    = AppController.getInstance().getDbController().getContactInfoFromUid(AppController.getInstance().getContactsDocId(), senderId);

            if (contactInfo != null) {
                receiverName = (String) contactInfo.get("contactName");
                userImage = (String) contactInfo.get("contactPicUrl");


                /*
                 * For showing correct name in secret chat invitation message
                 */


            } else {


                /*
                 * If userId doesn't exists in contact
                 */
                receiverName = senderIdentifier;

                if (data.containsKey("groupImage") && !((String) data.get("groupImage")).isEmpty()) {

                    userImage = (String) data.get("groupImage");


                }


            }


            String documentId = AppController.getInstance().findDocumentIdOfReceiver(senderId, secretId);
            if (documentId.isEmpty()) {


                /*
                 * Here, chatId is assumed to be empty
                 */

                try {

                    documentId = AppController.findDocumentIdOfReceiver(senderId, Utilities.tsInGmt(),
                            receiverName, userImage, secretId, true, senderIdentifier, "", false);
                } catch (NullPointerException ignored) {

                }

            }

            intent.putExtra("documentId", documentId);
            intent.putExtra("receiverName", receiverName);
            intent.putExtra("receiverImage", userImage);
            String messageType = "-1", replyType = "-1";

            if (data.containsKey("messageType")) {

                messageType = (String) data.get("messageType");


            }
            if (data.containsKey("replyType")) {

                messageType = (String) data.get("replyType");


            }

            if (AppController.getInstance().getContactSynced()) {
                generatePushNotificationLocal(documentId, messageType, receiverName,
                        (String) data.get("payload"), intent, dTime, secretId, senderId, replyType);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void generatePushNotificationLocal(String notificationId, String messageType, String senderName, String actualMessage, Intent intent, long dTime, String secretId, String receiverUid, String replyType) {


        if ((!AppController.getInstance().isForeground()) || (AppController.getInstance().getActiveReceiverId().isEmpty()) || (!(AppController.getInstance().getActiveReceiverId().equals(receiverUid))) || (!(AppController.getInstance().getActiveReceiverId().equals(receiverUid) && AppController.getInstance().getActiveSecretId().equals(secretId)))) {
            try {

                String pushMessage = "";
                switch (Integer.parseInt(messageType)) {


                    case 0:

                    {
                        try {
                            pushMessage = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");


                            if (pushMessage.trim().isEmpty()) {


                                /*
                                 *Intentionally written the code twice,as this is not the common case
                                 */

                                if (dTime != -1) {
                                    String message_dTime = String.valueOf(dTime);

                                    for (int i = 0; i < dTimeForDB.length; i++) {
                                        if (message_dTime.equals(dTimeForDB[i])) {

                                            if (i == 0) {


                                                pushMessage = getString(R.string.Timer_set_off);
                                            } else {


                                                pushMessage = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                            }
                                            break;
                                        }
                                    }
                                } else {

                                    pushMessage = getResources().getString(R.string.youAreInvited) + " " + senderName + " " +
                                            getResources().getString(R.string.JoinSecretChat);
                                }

                            }


                        } catch (UnsupportedEncodingException e) {

                        }
                        break;
                    }
                    case 1: {

                        pushMessage = "Image";
                        break;
                    }

                    case 2: {
                        pushMessage = "Video";
                        break;
                    }
                    case 3: {
                        pushMessage = "Location";

                        break;
                    }
                    case 4: {
                        pushMessage = "Follow";

                        break;
                    }
                    case 5: {
                        pushMessage = "Audio";

                        break;
                    }
                    case 6: {

                        pushMessage = "Sticker";

                        break;
                    }
                    case 7: {

                        pushMessage = "Doodle";

                        break;

                    }
                    case 8: {

                        pushMessage = "Gif";

                        break;
                    }


                    case 10: {

                        switch (Integer.parseInt(replyType)) {


                            case 0:

                            {
                                try {
                                    pushMessage = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");


                                } catch (UnsupportedEncodingException e) {

                                }
                                break;
                            }
                            case 1: {

                                pushMessage = "Image";
                                break;
                            }

                            case 2: {
                                pushMessage = "Video";
                                break;
                            }
                            case 3: {
                                pushMessage = "Location";

                                break;
                            }
                            case 4: {
                                pushMessage = "Follow";

                                break;
                            }
                            case 5: {
                                pushMessage = "Audio";

                                break;
                            }
                            case 6: {

                                pushMessage = "Sticker";

                                break;
                            }
                            case 7: {

                                pushMessage = "Doodle";

                                break;

                            }
                            case 8: {

                                pushMessage = "Gif";

                                break;
                            }
                            default: {
                                pushMessage = "Document";
                            }
                        }
                        break;
                    }
                    default: {
                        pushMessage = "Document";
                    }


                }


                /*
                 * For clubbing of the notifications
                 */

                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();


                Map<String, Object> notificationInfo = AppController.getInstance().fetchNotificationInfo(notificationId);


                int unreadMessageCount;
                int systemNotificationId;

                ArrayList<String> messages;
                if (notificationInfo == null) {

                    /*
                     * No previous notifications for the chat
                     *
                     */
                    notificationInfo = new HashMap<>();
                    messages = new ArrayList<>();

                    messages.add(pushMessage);
                    notificationInfo.put("notificationMessages", messages);

                    notificationInfo.put("notificationId", notificationId);

                    systemNotificationId = Integer.parseInt(String.valueOf(System.currentTimeMillis()).substring(9));
                    notificationInfo.put("systemNotificationId", systemNotificationId);


                    unreadMessageCount = 0;
                } else {
                    messages = (ArrayList<String>) notificationInfo.get("notificationMessages");
                    messages.add(0, pushMessage);

                    if (messages.size() > NOTIFICATION_SIZE) {
                        messages.remove(messages.size() - 1);
                    }
                    systemNotificationId = (int) notificationInfo.get("systemNotificationId");
                    notificationInfo.put("notificationMessages", messages);
                    unreadMessageCount = (int) notificationInfo.get("messagesCount");

                }
                notificationInfo.put("messagesCount", unreadMessageCount + 1);
                AppController.getInstance().addOrUpdateNotification(notificationInfo, notificationId);


                for (int i = 0; i < messages.size(); i++) {


                    inboxStyle.addLine(messages.get(i));


                }

                if (unreadMessageCount > (NOTIFICATION_SIZE - 1)) {
                    inboxStyle.setSummaryText("+" + (unreadMessageCount - (NOTIFICATION_SIZE - 1)) + " " + getString(R.string.more_message));
                }


                PendingIntent pendingIntent = PendingIntent.getActivity(this, systemNotificationId, intent, PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                String title, tickerText;
                if (messages.size() == 1) {

                    pushMessage = senderName + ": " + pushMessage;
                    tickerText = pushMessage;
                    title = getString(R.string.app_name);
                } else {
                    tickerText = senderName + ": " + pushMessage;
                    title = senderName;
                }


                inboxStyle.setBigContentTitle(title);
                NotificationCompat.Builder
                        notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(pushMessage)
                        .setTicker(tickerText)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setStyle(inboxStyle)
                        .setDefaults(Notification.DEFAULT_VIBRATE)
                        .setPriority(Notification.PRIORITY_HIGH);

                if (Build.VERSION.SDK_INT >= 21) notificationBuilder.setVibrate(new long[0]);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                /*
                 *
                 * Notification id is used to notify the same notification
                 *
                 * */

                notificationManager.notify(notificationId, systemNotificationId, notificationBuilder.build());

            } catch (Exception e) {

            }
        }

    }

}


