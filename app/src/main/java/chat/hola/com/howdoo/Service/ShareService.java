package chat.hola.com.howdoo.Service;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.FileUtil;
import chat.hola.com.howdoo.Utilities.twitterManager.TwitterShareManager;
import dagger.android.DaggerIntentService;

/**
 * Created by ankit on 19/4/18.
 */

public class ShareService extends DaggerIntentService {

    private static final String TAG = ShareData.class.getSimpleName();

    private ShareData shareData;
    private boolean fbShare = false;
    private boolean twitterShare = false;
    private boolean isFbShareCompleted = false;
    private boolean isTwitterShareCompleted = false;


    @Inject
    TwitterShareManager twitterShareManager;


    public ShareService() {
        super("share_service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        shareData = (ShareData) intent.getSerializableExtra("share_data");
        if(shareData != null) {
            fbShare = shareData.isFacebookShare();
            twitterShare = shareData.isTwitterShare();
            startSocialShare(shareData);
        }
    }

    private void startSocialShare(ShareData shareData) {
        if (shareData.getType().contains("video")) {
            if (fbShare) {
                //fbVideoShare(path, postText);
                isFbShareCompleted = false;
            } else {
                isFbShareCompleted = true;
                if (twitterShare) {
                    isTwitterShareCompleted = false;
                    //twitterVideoShare(postText);
                } else {
                    isTwitterShareCompleted = true;
                }
            }
        } else {
            if (fbShare) {
                isFbShareCompleted = false;
            } else {
                isFbShareCompleted = true;
                if (twitterShare) {
                    isTwitterShareCompleted = false;
                    twitterPhotoShare(shareData.getMediaPath(), shareData.getCaption());
                } else {
                    isTwitterShareCompleted = true;
                }
            }
        }
    }



    private void checkForTwitterShare(String path, String caption) {
        String type = FileUtil.getMimeType(path);
        if (isFbShareCompleted) {
            isFbShareCompleted = true;

            if (twitterShare) {
                if (type.contains("image")) {
                    isTwitterShareCompleted = false;
                    twitterPhotoShare(path, (caption == null) ? "" : caption);
                } else {
                    isTwitterShareCompleted = false;
                    //twitterVideoShare((caption == null) ? "" : caption);
                }
            } else {
                isTwitterShareCompleted = true;
                //checkForInstaShare();
            }
        } else {
            isTwitterShareCompleted = true;
            //checkForInstaShare();
        }
    }

    public void twitterPhotoShare(String path, String postText) {
        String type = "image/*";
        Callback<Tweet> callback = new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> result) {
                Log.w(TAG, "tweet successful: " + result.response.body().toString());
                isTwitterShareCompleted = true;
                //checkForInstaShare();
            }

            @Override
            public void failure(TwitterException exception) {
                Log.e(TAG, "tweet failure: " + exception.getMessage());
                isTwitterShareCompleted = true;
                //checkForInstaShare();
            }
        };
        twitterShareManager.tweetMedia(postText, type, path, callback);
    }
}
