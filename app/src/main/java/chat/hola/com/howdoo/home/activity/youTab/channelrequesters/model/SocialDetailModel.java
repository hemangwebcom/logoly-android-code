package chat.hola.com.howdoo.home.activity.youTab.channelrequesters.model;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;

/**
 * <h1>SocialDetailModel</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 5/3/2018.
 */

public class SocialDetailModel {

    @Inject
    SocialDetailModel() {

    }

    public Map<String, String> getParams(String postId) {
        Map<String, String> map = new HashMap<>();
        map.put("userId", AppController.getInstance().getUserId());
        return map;
    }

    public Map<String, String> getReasonParams(String postId, String reason, String message) {
        Map<String, String> map = new HashMap<>();
        map.put("postId", postId);
        map.put("message", message);
        map.put("reason", reason);
        return map;
    }
}
