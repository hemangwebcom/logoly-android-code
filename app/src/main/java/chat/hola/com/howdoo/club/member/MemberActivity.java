package chat.hola.com.howdoo.club.member;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.comment.CommentContract;
import chat.hola.com.howdoo.comment.model.CommentAdapter;
import chat.hola.com.howdoo.hastag.AutoCompleteTextView;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>ClubActivity</h1>
 * <p>All theMembers appears on this screen.
 * User can add newMember also</p>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class MemberActivity extends DaggerAppCompatActivity implements MemberContract.View {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;

    private Unbinder unbinder;
    private String postId;

    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    MemberContract.Presenter memberPresenter;
    @Inject
    MemberAdapter adapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvCommentList)
    RecyclerView rvCommentList;

    @BindView(R.id.tvTbTitle)
    TextView tvTbTitle;
    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;
    @BindView(R.id.add)
    Button add;
    private LinearLayoutManager layoutManager;
    public static String clubId;
    AlertDialog.Builder actionDialog;
    ArrayAdapter<String> actionList;
    ArrayList<String> actions = new ArrayList<>();
    private String userId;
    private boolean isBlocked = false;
    int call;
    List<String> members;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.member_activity);
        unbinder = ButterKnife.bind(this);

        layoutManager = new LinearLayoutManager(this);
        tvTbTitle.setTypeface(AppController.getInstance().getSemiboldFont());

        rvCommentList.setLayoutManager(layoutManager);

        adapter.setListener(memberPresenter.getPresenter());
        rvCommentList.setAdapter(adapter);

        call = getIntent().getIntExtra("call", 0);
        clubId = getIntent().getStringExtra("clubId");
        members = (List<String>) getIntent().getSerializableExtra("members");
        memberPresenter.setMembers(members);
        loadData(call);
        toolbarSetup();
    }

    private void loadData(int call) {
        adapter.setType(call);
        add.setVisibility(call == 0 || call == 4 ? View.VISIBLE : View.GONE);
        switch (call) {
            case 0:
            case 4:
                // all members
                rvCommentList.addOnScrollListener(recyclerViewOnScrollListener);
                memberPresenter.getMembers(0, PAGE_SIZE);
                break;
            case 1:
                //  club members for admin
                tvTbTitle.setText("Members");
                memberPresenter.getClubMembers(clubId);
                break;
            case 2:
                //  club members for non admin
                tvTbTitle.setText("Members");
                memberPresenter.getClubMembers(clubId);
                break;
            case 3:
                //  club members for non admin
                tvTbTitle.setText("Pending Requests");
                memberPresenter.getClubPendingRequest(clubId);
                break;
        }
    }

    @OnClick(R.id.add)
    public void add() {
        if (call == 4) {
            memberPresenter.addMembers(clubId);
        } else {
            Map<String, Object> params = new HashMap<>();
            params.put("subject", getIntent().getStringExtra("subject"));
            params.put("image", getIntent().getStringExtra("image"));
            params.put("coverImage", getIntent().getStringExtra("coverImage"));
            params.put("about", getIntent().getStringExtra("about"));
            params.put("access", getIntent().getIntExtra("access", 1));
            params.put("status", getIntent().getStringExtra("status"));
            memberPresenter.createClub(params);
        }
        add.setEnabled(false);
    }

    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.add)
    public void send() {
        //todo xdcfd
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void showMessage(String msg, int msgId) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sessionExpired() {
        if (sessionManager != null)
            sessionManager.sessionExpired(this);
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void userBlocked() {

    }

    @Override
    public void reload() {
        switch (call) {
            case 0:
            case 4:
                // all members
                rvCommentList.addOnScrollListener(recyclerViewOnScrollListener);
                memberPresenter.getMembers(0, PAGE_SIZE);
                break;
            case 1:
                //  club members for admin
                tvTbTitle.setText("Members");
                memberPresenter.getClubMembers(clubId);
                break;
            case 2:
                //  club members for non admin
                tvTbTitle.setText("Members");
                memberPresenter.getClubMembers(clubId);
                break;
            case 3:
                //  club members for non admin
                tvTbTitle.setText("Pending Requests");
                memberPresenter.getClubPendingRequest(clubId);
                break;
        }
    }


    @Override
    public void openProfile(String userId) {
        startActivity(new Intent(this, ProfileActivity.class).putExtra("userId", userId));
    }

    @Override
    public void clubCreated() {
        showMessage("Club created successfully", -1);
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void showPopup(String userId, boolean isBlocked) {
        this.userId = userId;
        this.isBlocked = isBlocked;
        actions.clear();
        actions.add("Remove");
        actions.add(1, isBlocked ? "Unblock" : "Block");

        actionList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, actions);
        actionDialog = new AlertDialog.Builder(this);
        actionDialog.setAdapter(actionList, (dialogInterface, i) -> {
            switch (i) {
                case 0:
                    memberPresenter.removeFromClub(clubId, userId);
                    break;
                case 1:
                    if (!isBlocked)
                        memberPresenter.blockFromClub(clubId, userId);
                    else
                        memberPresenter.unblockFromClub(clubId, userId);
                    break;
            }
        });

        actionDialog.show();
    }

    @Override
    public void reloadData() {
        switch (call) {
            case 0:
            case 4:
                // all members
                rvCommentList.addOnScrollListener(recyclerViewOnScrollListener);
                memberPresenter.getMembers(0, PAGE_SIZE);
                break;
            case 1:
                //  club members for admin
                tvTbTitle.setText("Members");
                memberPresenter.getClubMembers(clubId);
                break;
            case 2:
                //  club members for non admin
                tvTbTitle.setText("Members");
                memberPresenter.getClubMembers(clubId);
                break;
            case 3:
                //  club members for non admin
                tvTbTitle.setText("Pending Requests");
                memberPresenter.getClubPendingRequest(clubId);
                break;
        }
    }

    @Override
    public void memberAdded() {
        showMessage("Member added successfully", -1);
        finish();
    }


    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int[] firstVisibleItemPositions = new int[PAGE_SIZE];
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            memberPresenter.callApiOnScroll(postId, firstVisibleItemPosition, visibleItemCount, totalItemCount);
        }
    };

}
