package chat.hola.com.howdoo.welcomeScreen;

import android.app.Activity;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;


/**
 * <h1>WelcomeModule</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

@ActivityScoped
@Module
public interface WelcomeModule {
    @ActivityScoped
    @Binds
    WelcomeContract.Presenter presenter(WelcomePresenter presenter);

    @ActivityScoped
    @Binds
    WelcomeContract.View view(WelcomeActivity activity);

    @ActivityScoped
    @Binds
    Activity activity(WelcomeActivity activity);

}
