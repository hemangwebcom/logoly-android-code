package chat.hola.com.howdoo.Networking;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import chat.hola.com.howdoo.club.clubs.ClubResponse;
import chat.hola.com.howdoo.club.detail.ClubDetailResponse;
import chat.hola.com.howdoo.comment.model.CommentResponse;
import chat.hola.com.howdoo.home.activity.followingTab.model.FollowingResponse;
import chat.hola.com.howdoo.home.activity.youTab.followrequest.ResponseModel;
import chat.hola.com.howdoo.home.activity.youTab.model.ChannelSubscibe;
import chat.hola.com.howdoo.home.activity.youTab.model.YouResponse;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.home.model.Posts;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;
import chat.hola.com.howdoo.home.stories.model.StoryPost;
import chat.hola.com.howdoo.home.stories.model.StoryResponse;
import chat.hola.com.howdoo.post.ReportReason;
import chat.hola.com.howdoo.post.model.CategoryResponse;
import chat.hola.com.howdoo.post.model.ChannelResponse;
import chat.hola.com.howdoo.profileScreen.addChannel.model.AddChannelResponse;
import chat.hola.com.howdoo.profileScreen.addChannel.model.ChannelBody;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.ContactRequest;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.FollowResponse;
import chat.hola.com.howdoo.profileScreen.discover.facebook.apiModel.FbContactResponse;
import chat.hola.com.howdoo.profileScreen.editProfile.model.EditProfileBody;
import chat.hola.com.howdoo.profileScreen.editProfile.model.EditProfileResponse;
import chat.hola.com.howdoo.profileScreen.followers.Model.FollowersResponse;
import chat.hola.com.howdoo.profileScreen.model.Profile;
import chat.hola.com.howdoo.profileScreen.story.model.ProfilePostRequest;
import chat.hola.com.howdoo.search.model.SearchResponse;
import chat.hola.com.howdoo.search.tags.module.TagsResponse;
import chat.hola.com.howdoo.socialDetail.model.PostResponse;
import chat.hola.com.howdoo.welcomeScreen.LoginResponse;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * <h1>HowdooService</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 21/2/18.
 */


public interface HowdooService {

    @POST("/post/media")
    Observable<Response<ResponseBody>> createPost(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, Object> map);

    @GET("/hashTags")
    Observable<Response<Hash_tag_people_pojo>> checkHashTag(@Header("authorization") String auth,
                                                            @Header("lang") String lang,
                                                            @Query("hashtag") String hahashtag);

    @GET("/userTag")
    Observable<Response<Hash_tag_people_pojo>> checkUserTag(@Header("authorization") String auth,
                                                            @Header("lang") String lang,
                                                            @Query("userName") String usertag);

    @GET("/home")
    Observable<Response<Posts>> getPosts(@Header("authorization") String auth,
                                         @Header("lang") String lang,
                                         @Query("offset") int offset,
                                         @Query("limit") int limit);

    @GET("/home/club")
    Observable<Response<Posts>> getClubPosts(@Header("authorization") String auth,
                                             @Header("lang") String lang,
                                             @Query("offset") int offset,
                                             @Query("limit") int limit);

    @GET("/profile/member/{memberId}")
    Observable<Response<Profile>> getMember(@Header("authorization") String auth,
                                            @Header("lang") String lang,
                                            @Path("memberId") String memberId);

    @GET("/profile/memberPosts/{memberId}")
    Observable<Response<ProfilePostRequest>> getMemberPosts(@Header("authorization") String auth,
                                                            @Header("lang") String lang,
                                                            @Path("memberId") String memberId,
                                                            @Query("skip") int skip,
                                                            @Query("limit") int limit);

    @GET("/profile/users")
    Observable<Response<Profile>> getUserProfile(@Header("authorization") String auth,
                                                 @Header("lang") String lang);

    @GET("/profile/posts")
    Observable<Response<ProfilePostRequest>> getProfilePost(@Header("authorization") String auth,
                                                            @Header("lang") String lang,
                                                            @Query("skip") int skip,
                                                            @Query("limit") int limit);

    @GET("/category")
    Observable<Response<CategoryResponse>> getCategories(@Header("authorization") String auth,
                                                         @Header("lang") String lang);

    @GET("/channel")
    Observable<Response<ChannelResponse>> getChannels(@Header("authorization") String auth,
                                                      @Header("lang") String lang);

    @GET("/search/people")
    Observable<Response<SearchResponse>> search(@Header("authorization") String auth,
                                                @Header("lang") String lang,
                                                @Query("username") String username,
                                                @Query("offset") int skip,
                                                @Query("limit") int limit);

    @GET("/hashTagList")
    Observable<Response<TagsResponse>> searchTags(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Query("hashtag") String hashtag);

    @GET("/channelList")
    Observable<Response<chat.hola.com.howdoo.search.channel.module.ChannelResponse>> searchChannel(@Header("authorization") String auth,
                                                                                                   @Header("lang") String lang,
                                                                                                   @Query("searchText") String channel);


    @GET("/search/club")
    Observable<Response<ClubResponse>> searchClub(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Query("text") String channel);


    @POST("/follow")
    Observable<Response<FollowResponse>> follow(@Header("authorization") String auth,
                                                @Header("lang") String lang,
                                                @Body Map<String, Object> map);

    @POST("/unfollow/{followingId}")
    Observable<Response<FollowResponse>> unfollow(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Path("followingId") String followingId);

    @GET("/post/{postId}")
    Observable<Response<PostResponse>> postDetail(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Path("postId") String postId);

    @POST("/User/Contacts")
    Observable<Response<ResponseBody>> postUser(@Header("authorization") String auth,
                                                @Header("token") String token,
                                                @Body ContactRequest request);

    //createChannel
    @POST("/channel")
    Observable<Response<AddChannelResponse>> createChannel(@Header("authorization") String auth,
                                                           @Header("lang") String lang,
                                                           @Body ChannelBody Channelbody);

    //editProfile
    @PUT("/profile")
    Observable<Response<EditProfileResponse>> editProfile(@Header("authorization") String auth,
                                                          @Header("lang") String lang,
                                                          @Body EditProfileBody profileBody);

    @GET("/follow/followers/{userId}")
    Observable<Response<FollowersResponse>> getFollowers(@Header("authorization") String auth,
                                                         @Header("lang") String lang,
                                                         @Path("userId") String userId,
                                                         @Query("skip") int skip,
                                                         @Query("limit") int limit);

    @GET("/follow/followees/{userId}")
    Observable<Response<FollowersResponse>> getFollowees(@Header("authorization") String auth,
                                                         @Header("lang") String lang,
                                                         @Path("userId") String userId,
                                                         @Query("skip") int skip,
                                                         @Query("limit") int limit);

    @POST("/like/{postId}")
    Observable<Response<ResponseBody>> like(@Header("authorization") String auth,
                                            @Header("lang") String lang,
                                            @Path("postId") String postId,
                                            @Body Map<String, String> map);

    @POST("/unlike/{postId}")
    Observable<Response<ResponseBody>> unlike(@Header("authorization") String auth,
                                              @Header("lang") String lang,
                                              @Path("postId") String postId,
                                              @Body Map<String, String> map);

    @PATCH("/facebookId")
    Observable<Response<ResponseBody>> facebookId(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, String> map);

    @POST("/facebookContacts")
    Observable<Response<FbContactResponse>> facebookFriend(@Header("authorization") String auth,
                                                           @Header("lang") String lang,
                                                           @Body Map<String, List<String>> map);

    @GET("/profile/channels/{userId}")
    Observable<Response<chat.hola.com.howdoo.profileScreen.channel.Model.ChannelResponse>> getChannelPost(@Header("authorization") String auth,
                                                                                                          @Header("lang") String lang,
                                                                                                          @Path("userId") String userId,
                                                                                                          @Query("skip") int skip,
                                                                                                          @Query("limit") int limit);

    @POST("/follow/all")
    Observable<Response<ResponseBody>> followAll(@Header("authorization") String auth,
                                                 @Header("lang") String lang,
                                                 @Body Map<String, List<String>> map);

    @GET("/acitivty")
    Observable<Response<YouResponse>> getYouActivity(@Header("authorization") String auth,
                                                     @Header("lang") String lang,
                                                     @Query("offset") int offset,
                                                     @Query("limit") int limit);

    @POST("/comment/{postId}")
    Observable<Response<CommentResponse>> addComment(@Header("authorization") String auth,
                                                     @Header("lang") String lang,
                                                     @Path("postId") String postId,
                                                     @Body Map<String, Object> map);

    @GET("/comment/{postId}")
    Observable<Response<chat.hola.com.howdoo.comment.model.Response>> getComment(@Header("authorization") String auth,
                                                                                 @Header("lang") String lang,
                                                                                 @Path("postId") String postId,
                                                                                 @Query("offset") int skip,
                                                                                 @Query("limit") int limit);

    @PUT("/subscribeChannel/{channelId}")
    Observable<Response<ResponseBody>> subscribeChannel(@Header("authorization") String auth,
                                                        @Header("lang") String lang,
                                                        @Path("channelId") String channelId);

    @PUT("/unSubscribeChannel/{channelId}")
    Observable<Response<ResponseBody>> unSubscribeChannel(@Header("authorization") String auth,
                                                          @Header("lang") String lang,
                                                          @Path("channelId") String channelId);


    @GET("/postsByChannel")
    Observable<Response<chat.hola.com.howdoo.trendingDetail.model.ChannelResponse>> getChannelById(@Header("authorization") String auth,
                                                                                                   @Header("lang") String lang,
                                                                                                   @Query("channelId") String channelId);

    @GET("/postsByHashTag")
    Observable<Response<chat.hola.com.howdoo.profileScreen.channel.Model.ChannelData>> getPostByHashtag(@Header("authorization") String auth,
                                                                                                        @Header("lang") String lang,
                                                                                                        @Query("hashTag") String hashTag);

    @GET("/postsByCategory")
    Observable<Response<chat.hola.com.howdoo.profileScreen.channel.Model.ChannelData>> getPostByCategoryId(@Header("authorization") String auth,
                                                                                                           @Header("lang") String lang,
                                                                                                           @Query("categoryId") String categoryId);

    @GET("/postsByPlace/{place}")
    Observable<Response<chat.hola.com.howdoo.profileScreen.channel.Model.ChannelData>> getPostByLocation(@Header("authorization") String auth,
                                                                                                         @Header("lang") String lang,
                                                                                                         @Path("place") String place);

    @DELETE("/post/{postId}")
    Observable<Response<ResponseBody>> deletePost(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Path("postId") String postId);

    @PUT("/post")
    Observable<Response<ResponseBody>> updatePost(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, String> map);

    @GET("/reportReasons")
    Observable<Response<ReportReason>> getReportReason(@Header("authorization") String auth,
                                                       @Header("lang") String lang);

    @POST("/reportReasons")
    Observable<Response<ResponseBody>> reportPost(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, String> map);

    // STORY
    @GET("/myStory")
    Observable<Response<StoryResponse>> getMyStories(@Header("authorization") String auth,
                                                     @Header("lang") String lang);

    @GET("/story")
    Observable<Response<StoryResponse>> getStories(@Header("authorization") String auth,
                                                   @Header("lang") String lang);

    @POST("/story")
    Observable<Response<StoryPost>> postStory(@Header("authorization") String auth,
                                              @Header("lang") String lang,
                                              @Body Map<String, Object> map);

    @PATCH("/story/{storyId}")
    Observable<Response<ResponseBody>> viewStory(@Header("authorization") String auth,
                                                 @Header("lang") String lang,
                                                 @Path("storyId") String storyId);

    // CHANNEL
    @GET("/RequestedChannels")
    Observable<Response<ChannelSubscibe>> getRequestedChannels(@Header("authorization") String auth,
                                                               @Header("lang") String lang);

    @PUT("/RequestedChannels")
    Observable<Response<ResponseBody>> channelSubsciptionAction(@Header("authorization") String auth,
                                                                @Header("lang") String lang,
                                                                @Body Map<String, Object> params);

    @GET("/followRequest")
    Observable<Response<ResponseModel>> getfollowRequest(@Header("authorization") String auth,
                                                         @Header("lang") String lang,
                                                         @Query("offset") int skip,
                                                         @Query("limit") int limit);

    @POST("/followRequest")
    Observable<Response<ResponseBody>> requestAction(@Header("authorization") String auth,
                                                     @Header("lang") String lang,
                                                     @Body Map<String, Object> params);

    @GET("/followingAcitivty")
    Observable<Response<FollowingResponse>> followingActivities(@Header("authorization") String auth,
                                                                @Header("lang") String lang);

    @GET("/userReportReasons")
    Observable<Response<ReportReason>> userReportReasons(@Header("authorization") String auth,
                                                         @Header("lang") String lang);

    @POST("/userReportReasons")
    Observable<Response<ResponseBody>> reportUser(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, String> map);

    @PUT("/channel/{channelId}")
    Observable<Response<AddChannelResponse>> updateChannel(@Header("authorization") String auth,
                                                           @Header("lang") String lang,
                                                           @Path("channelId") String channelId,
                                                           @Body ChannelBody Channelbody);

    @GET("/blockReasons")
    Observable<Response<ReportReason>> getBlockReason(@Header("authorization") String auth,
                                                      @Header("lang") String lang);

    @POST("/block/{targetId}/{type}")
    Observable<Response<ResponseBody>> block(@Header("authorization") String auth,
                                             @Header("lang") String lang,
                                             @Path("targetId") String targetId,
                                             @Path("type") String type,
                                             @Body Map<String, String> map);

    @POST("/favouriteMusic")
    Observable<Response<ReportReason>> favourite(@Header("authorization") String auth,
                                                 @Header("lang") String lang,
                                                 @Body Map<String, Object> map);

    @GET("/blocks")
    Observable<Response<chat.hola.com.howdoo.blockUser.model.Response>> getBlockedUser(@Header("authorization") String auth,
                                                                                       @Header("lang") String lang,
                                                                                       @Query("offset") int skip,
                                                                                       @Query("limit") int limit);

    @GET("/postsByMusic")
    Observable<Response<chat.hola.com.howdoo.profileScreen.channel.Model.ChannelData>> getPostByMusicId(@Header("authorization") String auth,
                                                                                                        @Header("lang") String lang,
                                                                                                        @Query("musicId") String channelId);


    @GET("/followersFollowee")
    Observable<Response<FollowersResponse>> getContacts(@Header("authorization") String auth,
                                                        @Header("lang") String lang);


    //logoly
    @POST("/login")
    Observable<Response<LoginResponse>> login(@Header("authorization") String auth,
                                              @Header("lang") String lang,
                                              @Body Map<String, Object> params);

    @POST("/club")
    Observable<Response<ResponseBody>> createClub(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, Object> params);

    @GET("/club/list/{userId}")
    Observable<Response<ClubResponse>> getClubs(@Header("authorization") String auth,
                                                @Header("lang") String lang,
                                                @Path("userId") String userId);

    @PATCH("/club")
    Observable<Response<ResponseBody>> updateClub(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, Object> params);

    @PATCH("/profile/users")
    Observable<Response<ResponseBody>> updateProfile(@Header("authorization") String auth,
                                                     @Header("lang") String lang,
                                                     @Body Map<String, Object> params);

    @GET("/club/{clubId}")
    Observable<Response<ClubDetailResponse>> getClubDetails(@Header("authorization") String auth,
                                                            @Header("lang") String lang,
                                                            @Path("clubId") String clubId);

    @GET("/club/post")
    Observable<Response<Posts>> getClubPosts(@Header("authorization") String auth,
                                             @Header("lang") String lang,
                                             @Query("clubId") String clubId);

    @POST("/club/leave")
    Observable<Response<ResponseBody>> leaveClub(@Header("authorization") String auth,
                                                 @Header("lang") String lang,
                                                 @Body Map<String, Object> map);

    @POST("/club/join")
    Observable<Response<ResponseBody>> joinClub(@Header("authorization") String auth,
                                                @Header("lang") String lang,
                                                @Body Map<String, Object> map);

    @POST("/post/like/{postId}")
    Observable<Response<ResponseBody>> postLike(@Header("authorization") String auth,
                                                @Header("lang") String lang,
                                                @Path("postId") String postId,
                                                @Body Map<String, Object> map);

    @POST("/post/dislike/{postId}")
    Observable<Response<ResponseBody>> postDislike(@Header("authorization") String auth,
                                                   @Header("lang") String lang,
                                                   @Path("postId") String postId,
                                                   @Body Map<String, Object> map);

    @POST("/contact")
    Observable<Response<ResponseBody>> addToContact(@Header("authorization") String auth,
                                                    @Header("lang") String lang,
                                                    @Body Map<String, Object> map);

    @GET("/club/{clubId}/member")
    Observable<Response<FollowersResponse>> getClubMembers(@Header("authorization") String auth,
                                                           @Header("lang") String lang,
                                                           @Path("clubId") String clubId);

    @DELETE("/club/{clubId}/member/{memberId}")
    Observable<Response<ResponseBody>> removeFromClub(@Header("authorization") String auth,
                                                      @Header("lang") String lang,
                                                      @Path("clubId") String clubId,
                                                      @Path("memberId") String memberId);

    @POST("/club/{clubId}/member/{memberId}/block")
    Observable<Response<ResponseBody>> blockFromClub(@Header("authorization") String auth,
                                                     @Header("lang") String lang,
                                                     @Path("clubId") String clubId,
                                                     @Path("memberId") String memberId);

    @DELETE("/club/{clubId}/member/{memberId}/block")
    Observable<Response<ResponseBody>> unblockFromClub(@Header("authorization") String auth,
                                                       @Header("lang") String lang,
                                                       @Path("clubId") String clubId,
                                                       @Path("memberId") String memberId);

    @GET("/club/join/{clubId}")
    Observable<Response<FollowersResponse>> getClubPendingRequest(@Header("authorization") String auth,
                                                                  @Header("lang") String lang,
                                                                  @Path("clubId") String clubId);

    @POST("/club/join/action")
    Observable<Response<ResponseBody>> joinAction(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, Object> map);

    @DELETE("/club/{clubId}")
    Observable<Response<ResponseBody>> deleteClub(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Path("clubId") String clubId);

    @POST("/club/member")
    Observable<Response<ResponseBody>> addMembers(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, Object> map);

    @DELETE("/contact")
    Observable<Response<ResponseBody>> deleteContact(@Header("authorization") String auth,
                                                     @Header("lang") String lang,
                                                     @Query("contactId") String contactId);
}
