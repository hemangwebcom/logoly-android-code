package chat.hola.com.howdoo.trendingDetail;

import android.content.Intent;
import android.view.View;

import com.howdoo.dubly.R;

import org.json.JSONObject;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.dubly.DubResponse;
import chat.hola.com.howdoo.home.model.Location;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.profileScreen.channel.Model.ChannelData;
import chat.hola.com.howdoo.trendingDetail.model.ClickListner;
import chat.hola.com.howdoo.trendingDetail.model.TrendingDtlModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>TrendingDtlPresenter</h1>
 *
 * @author 3Embed.
 * @version 1.0
 * @since 21/2/18.
 */

public class TrendingDtlPresenter implements TrendingDtlContract.Presenter, ClickListner {

    @Inject
    HowdooService service;
    @Inject
    TrendingDtlModel model;
    @Inject
    TrendingDtlContract.View view;
    @Inject
    NetworkConnector networkConnector;
    private Location location;

    @Inject
    TrendingDtlPresenter() {
    }

    @Override
    public void init() {
        view.applyingFont();
        view.initDetailRecycler();
    }

    @Override
    public void subscribeChannel(String channelId) {

        service.subscribeChannel(AppController.getInstance().getApiToken(), Constants.LANGUAGE, channelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        view.invalidateSubsButton(response.code() == 200);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        view.invalidateSubsButton(false);
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });

    }

    @Override
    public void unSubscribeChannel(String channelId) {

        service.unSubscribeChannel(AppController.getInstance().getApiToken(), Constants.LANGUAGE, channelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        view.invalidateUnSubsButton(response.code() == 200);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        view.invalidateUnSubsButton(true);
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }


    @Override
    public void getChannelData(String channelId) {

        service.getChannelById(AppController.getInstance().getApiToken(), Constants.LANGUAGE, channelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<chat.hola.com.howdoo.trendingDetail.model.ChannelResponse>>() {
                    @Override
                    public void onNext(Response<chat.hola.com.howdoo.trendingDetail.model.ChannelResponse> response) {
                        if (response.code() == 200) {
                            model.setData(response.body().getData());
                            showData(response.body().getData(), response.body().getData().getChannelName(), -1);
                            String userId = response.body().getData().getUserId();
                            view.tbSubscribeVisibility(!userId.equals(AppController.getInstance().getUserId()));
                            view.llSubscribeVisibility(!userId.equals(AppController.getInstance().getUserId()));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }

    @Override
    public void getHashTagData(String hashtag) {

        service.getPostByHashtag(AppController.getInstance().getApiToken(), Constants.LANGUAGE, hashtag)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ChannelData>>() {
                    @Override
                    public void onNext(Response<ChannelData> response) {
                        if (response.code() == 200) {
                            model.setData(response.body());
                            showData(response.body(), hashtag, R.drawable.ic_hash);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }


    @Override
    public void getLocationData(String location) {

        service.getPostByLocation(AppController.getInstance().getApiToken(), Constants.LANGUAGE, location)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ChannelData>>() {
                    @Override
                    public void onNext(Response<ChannelData> response) {
                        if (response.code() == 200) {
                            model.setData(response.body());
                            if (!response.body().getData().isEmpty() && response.body().getData().get(0) != null)
                                view.setLocation(response.body().getData().get(0).getLocation());
                            showData(response.body(), location, R.drawable.ic_location_red);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }

    @Override
    public void getCategoryData(String category) {

        service.getPostByCategoryId(AppController.getInstance().getApiToken(), Constants.LANGUAGE, category)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ChannelData>>() {
                    @Override
                    public void onNext(Response<ChannelData> response) {
                        if (response.code() == 200) {
                            model.setData(response.body());
                            showData(response.body(), response.body().getChannelName(), R.drawable.ic_category_default);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }

    @Override
    public void onItemClick(int position, View view) {
        this.view.itemClick(position, view);
    }


    @Override
    public void setData(ChannelData data) {
        model.setData(data);
    }

    @Override
    public void selectType(Intent intent) {
        switch (intent.getStringExtra("call")) {
            case "music":
                getMusicData(intent.getStringExtra("musicId"));
                view.llSubscribeVisibility(false);
                break;
            case "channel":
                // String channelId = ;
                getChannelData(intent.getStringExtra("channelId"));
                // view.tbSubscribeVisibility(!channelId.equals(AppController.getInstance().getUserId()));
                //view.llSubscribeVisibility(!channelId.equals(AppController.getInstance().getUserId()));
                break;
            case "category":
                getCategoryData(intent.getStringExtra("categoryId"));
                view.tbSubscribeVisibility(false);
                view.llSubscribeVisibility(false);
                break;
            case "hashtag":
                getHashTagData(intent.getStringExtra("hashtag"));
                view.llSubscribeVisibility(false);
                break;
            case "location":
                getLocationData(intent.getStringExtra("location"));
                //   view.setLocation((Location) intent.getSerializableExtra("latlong"));
                view.mapVisibility(true);
                view.llSubscribeVisibility(false);
                break;
            default:
                ChannelData data = (ChannelData) intent.getSerializableExtra("data");
                view.tbSubscribeVisibility(!data.getUserId().equals(AppController.getInstance().getUserId()));
                view.llSubscribeVisibility(!data.getUserId().equals(AppController.getInstance().getUserId()));
                model.setData(data);
                showData(data, "", -1);
        }
    }

    private void getMusicData(String musicId) {
        service.getPostByMusicId(AppController.getInstance().getApiToken(), Constants.LANGUAGE, musicId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ChannelData>>() {
                    @Override
                    public void onNext(Response<ChannelData> response) {
                        if (response.code() == 200) {
                            model.setData(response.body());
                            showData(response.body(), response.body().getChannelName(), R.drawable.ic_music_player_colored);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }


    private void showData(ChannelData data, String text, int drawable) {
        int onText = -1, offText = -1;
        boolean isChecked = false;
        if (data.isSubscribed() != null) {
            switch (data.isSubscribed()) {
                case 0:
                    //not subscribed
                    offText = R.string.subscribe;
                    onText = data.getPrivate() ? R.string.requested : R.string.subscribed;
                    break;
                case 1:
                    // subscribed
                    onText = R.string.subscribed;
                    offText = R.string.subscribe;
                    isChecked = true;
                    break;
                case 2:
                    // requested
                    onText = R.string.requested;
                    offText = R.string.request;
                    isChecked = true;
                    break;
            }
        }

        view.showData(data, text, drawable, onText, offText, isChecked);
    }
}
