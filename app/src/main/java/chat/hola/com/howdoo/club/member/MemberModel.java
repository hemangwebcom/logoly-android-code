package chat.hola.com.howdoo.club.member;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.comment.model.Comment;
import chat.hola.com.howdoo.comment.model.CommentAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.followers.Model.Data;

/**
 * <h1>CommentModel</h1>
 *
 * @author 3Embed
 * @since 4/10/2018.
 */

class MemberModel {

    @Inject
    List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> dataList;
    @Inject
    MemberAdapter adapter;
    @Inject
    SessionManager sessionManager;

    @Inject
    MemberModel() {
    }

    public void setData1(List<String> members, List<Data> data) {
        if (data != null) {
            dataList.clear();
            for (Data data1 : data) {
                if (!members.contains(data1.getId())) {
                    if (data1.getProfilePic() == null && data1.getImage().isEmpty()) {
                        data1.setProfilePic(Constants.DEFAULT_PROFILE_PIC_LINK);
                    }
                    this.dataList.add(data1);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    public void setData(List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> data) {
        if (data != null) {
            dataList.clear();
            for (Data data1 : data) {
                if (data1.getProfilePic() == null && data1.getImage().isEmpty()) {
                    data1.setProfilePic(Constants.DEFAULT_PROFILE_PIC_LINK);
                }
                this.dataList.add(data1);
            }
            adapter.notifyDataSetChanged();
        }
    }

    public void clearList() {
        this.dataList.clear();
    }

    public String getUserId(int position) {
        return dataList.get(position).getUserId();
    }

    public void selectItem(int position, boolean isSelected) {
        dataList.get(position).setSelected(isSelected);
    }

    public ArrayList<String> getSelectedUsers() {
        ArrayList<String> selectedUsers = new ArrayList<>();
        for (chat.hola.com.howdoo.profileScreen.followers.Model.Data data : dataList) {
            if (data.isSelected())
                selectedUsers.add(data.getId());
        }
        return selectedUsers;
    }
}
