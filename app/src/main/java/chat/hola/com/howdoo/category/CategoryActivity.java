package chat.hola.com.howdoo.category;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.category.model.CategoryAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>CategoryActivity</h1>
 * <p>It shows list of categories</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 28/3/18
 */

public class CategoryActivity extends DaggerAppCompatActivity implements CategoryContract.View, SwipeRefreshLayout.OnRefreshListener {

    private Unbinder unbinder;
    String categoryId;

    @Inject
    CategoryPresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    CategoryAdapter categoryAdapter;
    @Inject
    SessionManager sessionManager;

    @BindView(R.id.tvTbTitle)
    TextView tvTbTitle;
    @BindView(R.id.btnSelect)
    Button btnSelect;
    @BindView(R.id.recyclerCategory)
    RecyclerView recyclerCategory;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.srSwipe)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        toolbarSetup();
        categoryId = getIntent().getStringExtra("categoryId");
        categoryAdapter.setListener(presenter);
        presenter.getCategory(categoryId);
        presenter.init();
        llNetworkError.setErrorListner(this);
    }

    /**
     * Setup actionbar
     */
    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    /**
     * call when session expires.
     */
    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * shows toast message.
     */
    @Override
    public void showMessage(String msg, int msgId) {
    }

    /**
     * applies fonts.
     */
    @Override
    public void applyFont() {
        tvTbTitle.setTypeface(typefaceManager.getSemiboldFont());
        btnSelect.setTypeface(typefaceManager.getMediumFont());
    }

    /**
     * list view setup.
     */
    @Override
    public void recyclerSetup() {
        recyclerCategory.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerCategory.setHasFixedSize(true);
        recyclerCategory.setAdapter(categoryAdapter);
    }

    @OnClick(R.id.btnSelect)
    public void btnSelect() {
//        presenter.getSelectedCategory()
        if (presenter.getSelectedCategory() != null) {
            setResult(RESULT_OK, presenter.getSelectedCategory());
            finish();
        } else {
            Toast.makeText(this, R.string.select_category, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * it reloads the data
     */
    @Override
    public void onRefresh() {
        presenter.getCategory(categoryId);
    }

    /**
     * checks whether data is loading or not
     */
    public void isLoadingData(boolean show) {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(show);
    }

    @Override
    public void reload() {
        onRefresh();
    }
}
