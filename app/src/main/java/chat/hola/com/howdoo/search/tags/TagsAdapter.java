package chat.hola.com.howdoo.search.tags;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.howdoo.dubly.R;

import java.util.List;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.search.tags.module.Tags;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolder> {
    private AppController appController;
    private Typeface fontMedium;
    private List<Tags> searchData;
    private Context context;
    private TagsAdapter.ClickListner clickListner;

//    @Inject
//    public ChannelAdapter(List<Data> arrayList, LandingActivity mContext, TypefaceManager typefaceManager) {
//    }

    public void setData(Context context, List<Tags> data) {
        this.context = context;
        this.searchData = data;
        notifyDataSetChanged();
    }

    @Override
    public TagsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hashtag_row, parent, false);
        appController = AppController.getInstance();
        fontMedium = appController.getMediumFont();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TagsAdapter.ViewHolder holder, final int position) {
        if (getItemCount() != -1) {
            Tags data = searchData.get(position);
            holder.profileIv.setImageBitmap(getBitmap(context.getResources().getDrawable(R.drawable.ic_hash)));
            holder.profileNameTv.setText(data.getHashTags());
            holder.tvCounts.setText(data.getTotalPublicPosts() + " " + context.getResources().getString(R.string.posted));
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListner.onItemClick(position);
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(Drawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    @Override
    public int getItemCount() {
        return searchData != null ? searchData.size() : -1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView profileIv;
        private TextView profileNameTv;
        private RelativeLayout relativeLayout;
        private TextView tvCounts;

        public ViewHolder(View itemView) {
            super(itemView);
            profileNameTv = (TextView) itemView.findViewById(R.id.profileNameTv);
            profileIv = (ImageView) itemView.findViewById(R.id.profileIv);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rlItem);
            tvCounts = (TextView) itemView.findViewById(R.id.tvCounts);
            tvCounts.setTypeface(fontMedium);
            profileNameTv.setTypeface(fontMedium);
        }
    }

    public void setListener(TagsAdapter.ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public interface ClickListner {
        void onItemClick(int position);
    }
}
