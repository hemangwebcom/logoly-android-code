package chat.hola.com.howdoo.dubly;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.camera.CameraActivity;
import chat.hola.com.howdoo.dublycategory.DubCategoryActivity;
import chat.hola.com.howdoo.manager.session.SessionManager;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>DubCategoryActivity</h1>
 * <p>All the Dubs appears on this screen.
 * User can add new Dubs also</p>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class DubsActivity extends DaggerAppCompatActivity implements DubsContract.View {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    public static int page = 0;

    private Unbinder unbinder;

    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    DubsContract.Presenter presenter;
    @Inject
    DubsAdapter adapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvDubList)
    RecyclerView rvDubList;
    @BindView(R.id.tvTbTitle)
    TextView tvTbTitle;
    @BindView(R.id.progressbar)
    ProgressBar progresbar;

    private MediaPlayer player;
    private String categoryId = "";
    private LinearLayoutManager layoutManager;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dublist_activity);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        categoryId = getIntent().getStringExtra("categoryId");
        String categoryName = getIntent().getStringExtra("categoryName");
        if (categoryName != null || !categoryName.isEmpty())
            tvTbTitle.setText(categoryName);
        layoutManager = new LinearLayoutManager(this);
        tvTbTitle.setTypeface(typefaceManager.getSemiboldFont());
        rvDubList.setLayoutManager(layoutManager);
        rvDubList.addOnScrollListener(recyclerViewOnScrollListener);
        adapter.setListener(presenter.getPresenter());
        rvDubList.setAdapter(adapter);
        presenter.getDubs(0, PAGE_SIZE, categoryId);

        player = new MediaPlayer();
        toolbarSetup();
    }

    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stop();
        startActivity(new Intent(this, DubCategoryActivity.class));
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {
        if (sessionManager != null)
            sessionManager.sessionExpired(this);
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    public void reload() {
        presenter.getDubs(0, PAGE_SIZE, categoryId);
    }

    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int[] firstVisibleItemPositions = new int[PAGE_SIZE];
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            presenter.callApiOnScroll(firstVisibleItemPosition, visibleItemCount, totalItemCount, categoryId);
        }
    };

    @Override
    public void play(String audio, boolean isPlaying) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                stop();
                if (isPlaying) {
                    Uri uri = Uri.parse(audio);
                    player = MediaPlayer.create(DubsActivity.this, uri);
                    player.start();
                }
            }
        }).start();
    }

    @Override
    public void progress(int i) {
    }

    @Override
    public void finishedDownload(String s, String filename, String musicId) {
        progresbar.setVisibility(View.GONE);
        stop();
        startActivity(new Intent(this, CameraActivity.class).putExtra("musicId", musicId).putExtra("audio", s).putExtra("name", filename).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }

    @Override
    public void startDownload() {
        progresbar.setVisibility(View.VISIBLE);
    }

    private void stop() {
        if (player != null && player.isPlaying()) {
            player.stop();
            player.reset();
            player.release();
            player = null;
        }

    }
}
