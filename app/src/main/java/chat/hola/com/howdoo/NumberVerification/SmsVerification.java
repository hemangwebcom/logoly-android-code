package chat.hola.com.howdoo.NumberVerification;
/*
 * Created by moda on 15/07/16.
 */

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.messaging.FirebaseMessaging;
import com.howdoo.dubly.R;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import chat.hola.com.howdoo.Activities.ChatMessageScreen;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Database.CouchDbController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Dialog.PromptDialog;
import chat.hola.com.howdoo.Profile.SaveProfile;
import chat.hola.com.howdoo.Utilities.ApiOnServer;
import chat.hola.com.howdoo.Utilities.CustomTypefaceSpan;
import chat.hola.com.howdoo.manager.account.AccountGeneral;


/* This class verify the number of the user.
 * A SMS reader service is started to automatically detect
 * the SMS received from the server. Else user is prompted
 * to enter the verification code manually.
 *
 *
 */

public class SmsVerification extends AppCompatActivity {


    private TextView mobile_number, enterSixDigitCodeTv, resendSmsTv;

    private Bus bus = AppController.getBus();
    private TextView timer;


    private IntentFilter intentFilter;


    private ProgressDialog pDialog2, pDialog;
    private EditText input;

    private RelativeLayout root;
    /* Object of the receiver */
    private ReadSms readSms;
    private int type;

    private RelativeLayout resend;
    //  private String code;
    /**
     * Have to restart the timer after user clicks on resend button and resend button is initially hidden
     */

    private CountDownTimer cTimer;
    private AppController appController;
    private Bundle bundle;
    private Typeface fontMedium, fontRegular, fontBold;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_verification);

        appController = AppController.getInstance();
        fontMedium = appController.getMediumFont();
        fontRegular = appController.getRegularFont();
        fontBold = appController.getSemiboldFont();
        enterSixDigitCodeTv = (TextView) findViewById(R.id.enterSixDigitCodeTv);
        enterSixDigitCodeTv.setTypeface(fontMedium);
        bundle = getIntent().getExtras();
        //   code = bundle.getString("code");


        resend = (RelativeLayout) findViewById(R.id.resend);
        root = (RelativeLayout) findViewById(R.id.smsVerfication);

        resend.setVisibility(View.GONE);

        resendSmsTv = (TextView) findViewById(R.id.resendSmsTv);
        mobile_number = (TextView) findViewById(R.id.mobileNumber_smsV);
        mobile_number.setText(bundle.getString("phoneNumber"));


        ImageView editNumber = (ImageView) findViewById(R.id.edit);
        String mobileNumber = bundle.getString("phoneNumber");
        timer = (TextView) findViewById(R.id.timer);
        timer.setTypeface(fontMedium);

        input = (EditText) findViewById(R.id.code);
        input.setTypeface(fontRegular);


        TextView tv1 = (TextView) findViewById(R.id.textView1);
//        tv1.setText(getResources().getString(R.string.string_723)+""+mobileNumber+".");
        tv1.setTypeface(fontMedium);
        resendSmsTv.setTypeface(fontBold);

        /*
         *
         *
         * Type determines whether we have choosed to automatically detect the sms or get the sms code manually
         *
         *
         *
         * Type-0
         *
         *
         *
         * Type-1
         *
         * */
        type = bundle.getInt("type");


        if (type == 0) {


//                    tv1.setText(R.string.string_294);

            String message = getResources().getString(R.string.string_723) + "\n\n" + mobileNumber + "." + "Wrong number";

            SpannableString ss = new SpannableString(message);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    // startActivity(new Intent(MyActivity.this, NextActivity.class));
                    //Log.d("exe","clicking of some thing");
                    Intent intent = new Intent(SmsVerification.this, VerifyPhoneNumber.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
            ss.setSpan(clickableSpan, message.length() - 12, message.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), message.length() - 12, message.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss.setSpan(new CustomTypefaceSpan(tv1.getText().toString(), fontMedium), 0, message.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv1.setText(ss);
            tv1.setMovementMethod(LinkMovementMethod.getInstance());
            tv1.setHighlightColor(Color.TRANSPARENT);

            // tv1.setText(getResources().getString(R.string.string_723)+"\n"+mobileNumber+".");


            /*
             *
             * Will allow a maximum time upto 60 seconds for countdown
             *
             *
             * */


            cTimer = new CountDownTimer(60000, 1000) {

                public void onTick(long millisUntilFinished) {
                    long sec = millisUntilFinished / 1000;
                    if (sec > 9) {
                        timer.setText(getString(R.string.string_296) + "" + sec);
                    } else {
                        timer.setText(getString(R.string.string_297) + "" + sec);
                    }
                }

                public void onFinish() {
                    timer.setText(R.string.string_298);
//
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            timer.setVisibility(View.GONE);
//                        }
//                    }, 1000);

                    //    resend.setVisibility(View.VISIBLE);
                    resendSmsTv.setTextColor(getResources().getColor(R.color.colorMineShaft));
                    resendSmsTv.setEnabled(true);
                }
            };


            /*
             *
             *
             * Have to show the timer
             *
             * */


            timer.setVisibility(View.VISIBLE);
            cTimer.start();
            intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
            intentFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);


            readSms = new ReadSms() {

                @Override
                protected void onSmsReceived(String s) {
                    /* Call the verify SMS code API */
                    // instead of verifying number on the server we will will verify number here itsef
//
//                    if (s.equals(code))
//
//                    {

                    /*load the homescreen in app after sending the signin details to the server*/
                    makeRequest(s);

//                    } else {
//
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                cTimer.cancel();
//                                cTimer.onFinish();
//                                timer.setText(R.string.string_298);
//                                input.setEnabled(false);
//                                input.setInputType(InputType.TYPE_NULL);
//                                input.setFocusableInTouchMode(false);
//                                input.clearFocus();
//
//                                Snackbar snackbar = Snackbar.make(root, R.string.string_149, Snackbar.LENGTH_SHORT);
//
//
//                                snackbar.show();
//                                View view = snackbar.getView();
//                                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
//                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//
//                            }
//                        });
//
//
//                    }


                }
            };


        } else {


            timer.setVisibility(View.GONE);

            tv1.setText(R.string.string_295);

        }


//          /* Initialise the progress dialog */
//


        pDialog = new ProgressDialog(this, 0);
        pDialog.setMessage(getString(R.string.Verify_Otp));
        pDialog.setCancelable(true);

        pDialog2 = new ProgressDialog(this, 0);
        pDialog2.setMessage(getString(R.string.string_547));
        pDialog2.setCancelable(true);


        editNumber.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                /* Move back to verify phone number screen */
                Intent intent = new Intent(SmsVerification.this,
                        VerifyPhoneNumber.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(SmsVerification.this).toBundle());
                supportFinishAfterTransition();
            }
        });


        /*
         *
         *
         * resend button have to be initially hidden but once the autodetect time of SMS expires then it has to be made visible again
         *
         * */

        resendSmsTv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                input.setText("");


                requestOtpAgain();

            }
        });


        TextView tv2 = (TextView) findViewById(R.id.tv2);

        TextView title = (TextView) findViewById(R.id.title);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Condensed.ttf");
        title.setTypeface(fontBold);
        tv1.setTypeface(tf, Typeface.NORMAL);
        tv2.setTypeface(tf, Typeface.BOLD);

        mobile_number.setTypeface(tf, Typeface.BOLD);
        timer.setTypeface(tf, Typeface.NORMAL);


        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (input.getText().toString().trim().length() == 4) {
//                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                    //   makeRequest(input.getText().toString().trim());
                }
            }


            /**
             *
             * assuming always length 4 otp comes
             *
             *
             * */
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ImageView close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                onBackPressed();

            }
        });

        bus.register(this);

        //TODO - remove when u r implementing sms OTP
        input.setText("1111");
        if (input.getText().toString().length() == 4)
            makeRequest(input.getText().toString().trim());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (type == 0)

            this.registerReceiver(readSms, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (type == 0)
            (this).unregisterReceiver(readSms);
    }


    @SuppressWarnings("unchecked")

    public void makeRequest(String code) {


        /*
         * If request was for verifying otp on the server
         */


        JSONObject obj = new JSONObject();


        try {

            obj.put("otp", code);


            // obj.put("phoneNumber", bundle.getString("phoneNumber"));
            obj.put("deviceId", AppController.getInstance().getDeviceId());

            obj.put("phoneNumber", bundle.getString("mobileNumber"));
            obj.put("countryCode", bundle.getString("countryCode"));
            obj.put("deviceName", Build.DEVICE);
            obj.put("deviceOs", Build.VERSION.RELEASE);
            obj.put("modelNumber", Build.MODEL);
            obj.put("deviceType", "2");
            try {
                PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
                String version = pInfo.versionName;
                obj.put("appVersion", version);
            } catch (PackageManager.NameNotFoundException e) {
                obj.put("appVersion", "123");
            }

            //  obj.put("username", "davidwarner");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        showProgressDialog();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiOnServer.VERIFY_OTP, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                try {

                    switch (response.getInt("code")) {

                        case 403:
                            BlockDialog blockDialog = new BlockDialog(SmsVerification.this);
                            blockDialog.show();
                            break;

                        case 200:

                            /*
                             *
                             * To start the Profile screen
                             */


                            response = response.getJSONObject("response");

                            String profilePic = "", userName = "", firstName = "", lastName = "";
                            // String token = response.getString("token");


                            CouchDbController db = AppController.getInstance().getDbController();

                            Map<String, Object> map = new HashMap<>();


                            if (response.has("profilePic")) {
                                profilePic = response.getString("profilePic");
                                map.put("userImageUrl", response.getString("profilePic"));

                            } else {
                                map.put("userImageUrl", "");
                            }
                            if (response.has("userName")) {
                                userName = response.getString("userName");
                                map.put("userName", response.getString("userName"));
                            }

                            if (response.has("firstName")) {
                                firstName = response.getString("firstName");
                                map.put("firstName", response.getString("firstName"));
                            }

                            if (response.has("lastName")) {
                                lastName = response.getString("lastName");
                                map.put("lastName", response.getString("lastName"));
                            }

                            String userStatus = getString(R.string.default_status);


                            map.put("userId", response.getString("userId"));

                            try {
                                map.put("private", response.getInt("private"));
                            } catch (JSONException e) {
                                Log.i("", "onResponse: " + e.getMessage());
                            } catch (Exception ignored) {
                            }


                            /*
                             * To save the social status as the text value in the properties
                             *
                             */

                            if (response.has("socialStatus")) {

                                userStatus = response.getString("socialStatus");

                            }
                            map.put("socialStatus", userStatus);
                            map.put("userIdentifier", bundle.getString("phoneNumber"));
                            map.put("apiToken", response.getString("token"));

                            try {
                                Account mAccount = new Account(bundle.getString("phoneNumber"), AccountGeneral.ACCOUNT_TYPE);
                                AccountManager mAccountManager = AccountManager.get(getApplicationContext());
                                mAccountManager.addAccountExplicitly(mAccount, input.getText().toString(), null);

                                Account[] accounts = mAccountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
                                if (accounts.length != 0)
                                    mAccountManager.setAuthToken(mAccount, AccountGeneral.AUTHTOKEN_TYPE_READ_ONLY, response.getString("token"));
                            } catch (JSONException ignored) {
                            }
                            AppController.getInstance().getSharedPreferences().edit().putString("token", response.getString("token")).apply();

                            /*
                             * By phone number verification
                             */

                            map.put("userLoginType", 1);

                            if (!db.checkUserDocExists(AppController.getInstance().getIndexDocId(), response.getString("userId"))) {


                                String userDocId = db.createUserInformationDocument(map);

                                db.addToIndexDocument(AppController.getInstance().getIndexDocId(), response.getString("userId"), userDocId);


                            } else {


                                db.updateUserDetails(db.getUserDocId(response.getString("userId"), AppController.getInstance().getIndexDocId()), map);

                            }
                            if (!userName.isEmpty()) {

                                db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), response.getString("userId"), 1, true);
                            } else {
                                db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), response.getString("userId"), 1, false);
                            }
                            /*
                             * To update myself as available for call
                             */


                            AppController.getInstance().setSignedIn(true, response.getString("userId"), userName, bundle.getString("phoneNumber"), 1);
                            AppController.getInstance().setSignStatusChanged(true);

                            String topic = "/topics/" + response.getString("userId");
                            FirebaseMessaging.getInstance().subscribeToTopic(topic);

                            Intent i = new Intent(SmsVerification.this, SaveProfile.class);
                            //   i.putExtra("userId", response.getString("userId"));
                            if (!profilePic.isEmpty())
                                i.putExtra("profilePic", response.getString("profilePic"));
                            if (!userName.isEmpty())
                                i.putExtra("userName", response.getString("userName"));
                            if (!firstName.isEmpty())
                                i.putExtra("firstName", response.getString("firstName"));
                            if (!lastName.isEmpty())
                                i.putExtra("lastName", response.getString("lastName"));

                            try {
                                i.putExtra("private", response.getInt("private"));
                            } catch (JSONException e) {
                                Log.i("", "onResponse: " + e.getMessage());
                            } catch (Exception ignored) {
                            }
//                            i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(i);
                            finish();
//                            supportFinishAfterTransition();
                            break;


                        case 138:


                            /*
                             *
                             *
                             * If code is incorrect then we have to unlock option for resending of the code
                             *
                             * */
                            // resend.setVisibility(View.VISIBLE);
                            resendSmsTv.setEnabled(true);
                            resendSmsTv.setTextColor(getResources().getColor(R.color.colorMineShaft));
                            timer.setVisibility(View.GONE);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

//                                    input.setEnabled(false);
//                                    input.setInputType(InputType.TYPE_NULL);
//                                    input.setFocusableInTouchMode(false);
//                                    input.clearFocus();
                                    Snackbar snackbar = Snackbar.make(root, R.string.string_149, Snackbar.LENGTH_SHORT);


                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);

                                }
                            });

                            break;
                        default:

                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, response.getString("message"), Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }
                            showErrorDialog();

                    }
                } catch (
                        JSONException e)

                {
                    e.printStackTrace();
                }

                hideProgressDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                if (root != null) {

                    Snackbar snackbar = Snackbar.make(root, R.string.string_4, Snackbar.LENGTH_SHORT);


                    snackbar.show();
                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                }


                hideProgressDialog();
                showErrorDialog();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");
                return headers;
            }
        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        /* Add the request to the RequestQueue.*/
        AppController.getInstance().addToRequestQueue(jsonObjReq, "verifyOtpApi");


    }


    /*
     *
     * Error dialog in case failed to send the OTP
     * */
    @SuppressWarnings("TryWithIdenticalCatches")
    public void showErrorDialog2() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SmsVerification.this);
        alertDialog.setTitle(R.string.string_356);
        alertDialog.setMessage(getString(R.string.string_549));

        alertDialog.setNegativeButton(R.string.string_580,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        Context context = ((ContextWrapper) ((Dialog) dialog).getContext()).getBaseContext();


                        if (context instanceof Activity) {


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                    dialog.dismiss();
                                }
                            } else {


                                if (!((Activity) context).isFinishing()) {
                                    dialog.dismiss();
                                }
                            }
                        } else {


                            try {
                                dialog.dismiss();
                            } catch (final IllegalArgumentException e) {
                                e.printStackTrace();

                            } catch (final Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }
                });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertDialog.show();
            }
        });
    }


    /**
     * To show the progress dialog
     */

    private void showProgressDialog2() {
        if (pDialog2 != null && !pDialog2.isShowing()) {
            pDialog2.show();
            ProgressBar bar = (ProgressBar) pDialog2.findViewById(android.R.id.progress);


            bar.getIndeterminateDrawable().setColorFilter(
                    ContextCompat.getColor(SmsVerification.this, R.color.color_black),
                    android.graphics.PorterDuff.Mode.SRC_IN);

        }
    }


    /**
     * To hide the progress dialog
     */

    @SuppressWarnings("TryWithIdenticalCatches")
    private void hideProgressDialog2() {
        if (pDialog2.isShowing()) {

            Context context = ((ContextWrapper) (pDialog2).getContext()).getBaseContext();


            if (context instanceof Activity) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                        pDialog2.dismiss();
                    }
                } else {


                    if (!((Activity) context).isFinishing()) {
                        pDialog2.dismiss();
                    }
                }
            } else {


                try {
                    pDialog2.dismiss();
                } catch (final IllegalArgumentException e) {
                    e.printStackTrace();

                } catch (final Exception e) {
                    e.printStackTrace();

                }
            }


        }
    }


    /**
     * To request resend OTP incase wrong OTP was entered by the user or the the 60 seconds expired before the OTP was received/entered by the user
     */
    @SuppressWarnings("TryWithIdenticalCatches,unchecked")

    private void requestOtpAgain() {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SmsVerification.this);
        alertDialog.setTitle(R.string.string_356);


        //alertDialog.setMessage("Verification Code Will Be Sent To :\n\n" + mobile_number.getText() + "\n\nIs This OK, Or Would You Like To Edit The Number?");


        //alertDialog.setMessage("Number verification code will be sent to phone number " + mobile_number.getText().toString() + " via SMS");


        alertDialog.setMessage(getString(R.string.string_552) + " " + mobile_number.getText().toString());
        alertDialog.setNegativeButton(R.string.string_594,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        Context context = ((ContextWrapper) ((Dialog) dialog).getContext()).getBaseContext();


                        if (context instanceof Activity) {


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                    dialog.dismiss();
                                }
                            } else {


                                if (!((Activity) context).isFinishing()) {
                                    dialog.dismiss();
                                }
                            }
                        } else {


                            try {
                                dialog.dismiss();
                            } catch (final IllegalArgumentException e) {
                                e.printStackTrace();

                            } catch (final Exception e) {
                                e.printStackTrace();

                            }
                        }


                        Intent intent = new Intent(SmsVerification.this,
                                VerifyPhoneNumber.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(SmsVerification.this).toBundle());
                        supportFinishAfterTransition();


                    }
                });

        alertDialog.setPositiveButton(R.string.string_578,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        resendSmsTv.setTextColor(getResources().getColor(R.color.colorBonJour));
                        resendSmsTv.setEnabled(false);
                        Context context = ((ContextWrapper) ((Dialog) dialog).getContext()).getBaseContext();


                        if (context instanceof Activity) {


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                    dialog.dismiss();
                                }
                            } else {


                                if (!((Activity) context).isFinishing()) {
                                    dialog.dismiss();
                                }
                            }
                        } else {


                            try {
                                dialog.dismiss();
                            } catch (final IllegalArgumentException e) {
                                e.printStackTrace();

                            } catch (final Exception e) {
                                e.printStackTrace();

                            }
                        }


                        /*make a request to serve r to send for otp*/
                        makeOtpReq();


                    }
                });


        alertDialog.show();


    }


    /**
     * To request OTP from the server
     */

    @SuppressWarnings("TryWithIdenticalCatches")
    private void makeOtpReq() {


        JSONObject obj = new JSONObject();


        try {

            obj.put("deviceId", AppController.getInstance().getDeviceId());

            obj.put("phoneNumber", bundle.getString("mobileNumber"));
            obj.put("countryCode", "+" + bundle.getString("countryCode"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        showProgressDialog2();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiOnServer.REQUEST_OTP, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                resend.setVisibility(View.GONE);


                hideProgressDialog2();


                try {
                    switch (response.getInt("code")) {
                        case 200:
                            input.setEnabled(true);
                            input.setInputType(InputType.TYPE_CLASS_NUMBER);
                            input.setFocusableInTouchMode(true);
                            input.requestFocus();


                            if (type == 0) {




                                /*
                                 *
                                 * To automatically detect the SMS,have to start the timer again
                                 *
                                 *
                                 * */


                                timer.setVisibility(View.VISIBLE);


                                cTimer.start();


                                resend.setVisibility(View.GONE);


                            } else {

                                /*
                                 *
                                 *
                                 * To detect the SMS manually
                                 *
                                 * */

                                timer.setVisibility(View.GONE);
                                resend.setVisibility(View.GONE);

                            }


                            break;


                        case 137:


                            final PromptDialog p2 = new PromptDialog(SmsVerification.this);


                            p2.setDialogType(PromptDialog.DIALOG_TYPE_INFO)
                                    .setTitleText(R.string.string_354).setContentText(R.string.string_970)
                                    .setPositiveListener(R.string.string_580, new PromptDialog.OnPositiveListener() {
                                        @Override
                                        public void onClick(PromptDialog dialog) {


                                            Context context = ((ContextWrapper) (dialog).getContext()).getBaseContext();


                                            if (context instanceof Activity) {


                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                                        dialog.dismiss();
                                                    }
                                                } else {


                                                    if (!((Activity) context).isFinishing()) {
                                                        dialog.dismiss();
                                                    }
                                                }
                                            } else {


                                                try {
                                                    dialog.dismiss();
                                                } catch (final IllegalArgumentException e) {
                                                    e.printStackTrace();

                                                } catch (final Exception e) {
                                                    e.printStackTrace();

                                                }
                                            }


                                        }
                                    }).show();


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (p2.isShowing()) {
                                        //   p.dismiss();

                                        Context context = ((ContextWrapper) (p2).getContext()).getBaseContext();


                                        if (context instanceof Activity) {


                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                                    p2.dismiss();
                                                }
                                            } else {


                                                if (!((Activity) context).isFinishing()) {
                                                    p2.dismiss();
                                                }
                                            }
                                        } else {


                                            try {
                                                p2.dismiss();
                                            } catch (final IllegalArgumentException e) {
                                                e.printStackTrace();

                                            } catch (final Exception e) {
                                                e.printStackTrace();

                                            }
                                        }


                                    }


                                }
                            }, 2000);


                            break;


                        case 138:

                            //same device 6 times succesfull
                            final PromptDialog p3 = new PromptDialog(SmsVerification.this);


                            p3.setDialogType(PromptDialog.DIALOG_TYPE_INFO)
                                    .setTitleText(R.string.string_354).setContentText(R.string.string_971)
                                    .setPositiveListener(R.string.string_580, new PromptDialog.OnPositiveListener() {
                                        @Override
                                        public void onClick(PromptDialog dialog) {


                                            Context context = ((ContextWrapper) (dialog).getContext()).getBaseContext();


                                            if (context instanceof Activity) {


                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                                        dialog.dismiss();
                                                    }
                                                } else {


                                                    if (!((Activity) context).isFinishing()) {
                                                        dialog.dismiss();
                                                    }
                                                }
                                            } else {


                                                try {
                                                    dialog.dismiss();
                                                } catch (final IllegalArgumentException e) {
                                                    e.printStackTrace();

                                                } catch (final Exception e) {
                                                    e.printStackTrace();

                                                }
                                            }


                                        }
                                    }).show();


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (p3.isShowing()) {
                                        //   p.dismiss();

                                        Context context = ((ContextWrapper) (p3).getContext()).getBaseContext();


                                        if (context instanceof Activity) {


                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                                    p3.dismiss();
                                                }
                                            } else {


                                                if (!((Activity) context).isFinishing()) {
                                                    p3.dismiss();
                                                }
                                            }
                                        } else {


                                            try {
                                                p3.dismiss();
                                            } catch (final IllegalArgumentException e) {
                                                e.printStackTrace();

                                            } catch (final Exception e) {
                                                e.printStackTrace();

                                            }
                                        }


                                    }


                                }
                            }, 2000);

                            break;


                        case 139:
                            //same device 3 failed request for the week
                            final PromptDialog p4 = new PromptDialog(SmsVerification.this);


                            p4.setDialogType(PromptDialog.DIALOG_TYPE_INFO)
                                    .setTitleText(R.string.string_354).setContentText(R.string.string_972)
                                    .setPositiveListener(R.string.string_580, new PromptDialog.OnPositiveListener() {
                                        @Override
                                        public void onClick(PromptDialog dialog) {


                                            Context context = ((ContextWrapper) (dialog).getContext()).getBaseContext();


                                            if (context instanceof Activity) {


                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                                        dialog.dismiss();
                                                    }
                                                } else {


                                                    if (!((Activity) context).isFinishing()) {
                                                        dialog.dismiss();
                                                    }
                                                }
                                            } else {


                                                try {
                                                    dialog.dismiss();
                                                } catch (final IllegalArgumentException e) {
                                                    e.printStackTrace();

                                                } catch (final Exception e) {
                                                    e.printStackTrace();

                                                }
                                            }


                                        }
                                    }).show();


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (p4.isShowing()) {
                                        //   p.dismiss();

                                        Context context = ((ContextWrapper) (p4).getContext()).getBaseContext();


                                        if (context instanceof Activity) {


                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                                    p4.dismiss();
                                                }
                                            } else {


                                                if (!((Activity) context).isFinishing()) {
                                                    p4.dismiss();
                                                }
                                            }
                                        } else {


                                            try {
                                                p4.dismiss();
                                            } catch (final IllegalArgumentException e) {
                                                e.printStackTrace();

                                            } catch (final Exception e) {
                                                e.printStackTrace();

                                            }
                                        }


                                    }


                                }
                            }, 2000);

                            break;


                        case 140:
                            //abuse of device
                            final PromptDialog p5 = new PromptDialog(SmsVerification.this);


                            p5.setDialogType(PromptDialog.DIALOG_TYPE_INFO)
                                    .setTitleText(R.string.string_354).setContentText(R.string.string_973)
                                    .setPositiveListener(R.string.string_580, new PromptDialog.OnPositiveListener() {
                                        @Override
                                        public void onClick(PromptDialog dialog) {


                                            Context context = ((ContextWrapper) (dialog).getContext()).getBaseContext();


                                            if (context instanceof Activity) {


                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                                        dialog.dismiss();
                                                    }
                                                } else {


                                                    if (!((Activity) context).isFinishing()) {
                                                        dialog.dismiss();
                                                    }
                                                }
                                            } else {


                                                try {
                                                    dialog.dismiss();
                                                } catch (final IllegalArgumentException e) {
                                                    e.printStackTrace();

                                                } catch (final Exception e) {
                                                    e.printStackTrace();

                                                }
                                            }


                                        }
                                    }).show();


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (p5.isShowing()) {
                                        //   p.dismiss();

                                        Context context = ((ContextWrapper) (p5).getContext()).getBaseContext();


                                        if (context instanceof Activity) {


                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                                    p5.dismiss();
                                                }
                                            } else {


                                                if (!((Activity) context).isFinishing()) {
                                                    p5.dismiss();
                                                }
                                            }
                                        } else {


                                            try {
                                                p5.dismiss();
                                            } catch (final IllegalArgumentException e) {
                                                e.printStackTrace();

                                            } catch (final Exception e) {
                                                e.printStackTrace();

                                            }
                                        }


                                    }


                                }
                            }, 2000);
                            break;
                        default:


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, response.getString("message"), Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                showErrorDialog2();
                            }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog2();
                        showErrorDialog2();
                    }
                });
            }
        }

        )

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");
                return headers;
            }
        };

        /* Add the request to the RequestQueue.*/


        jsonObjReq.setRetryPolicy(new

                DefaultRetryPolicy(
                20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );
        AppController.getInstance().

                addToRequestQueue(jsonObjReq, "verifyPhoneNumberApiRequest");

    }


    @Override
    public void onBackPressed() {


        if (AppController.getInstance().isActiveOnACall()) {
            if (AppController.getInstance().isCallMinimized()) {
                super.onBackPressed();
                supportFinishAfterTransition();
            }
        } else {
            super.onBackPressed();
            supportFinishAfterTransition();
        }

    }


    /**
     * Error dialog incase failed to register on server
     */
    @SuppressWarnings("TryWithIdenticalCatches")

    public void showErrorDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SmsVerification.this);
        alertDialog.setTitle(R.string.string_406);
        alertDialog.setMessage(getString(R.string.string_550));

        alertDialog.setNegativeButton(R.string.string_580,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //  dialog.dismiss();


                        Context context = ((ContextWrapper) ((Dialog) dialog).getContext()).getBaseContext();


                        if (context instanceof Activity) {


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                    dialog.dismiss();
                                }
                            } else {


                                if (!((Activity) context).isFinishing()) {
                                    dialog.dismiss();
                                }
                            }
                        } else {


                            try {
                                dialog.dismiss();
                            } catch (final IllegalArgumentException e) {
                                e.printStackTrace();

                            } catch (final Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }
                });
        alertDialog.show();
    }


    /**
     * To show the progres dialog
     */

    private void showProgressDialog() {
        if (pDialog != null && !pDialog.isShowing()) {
            pDialog.show();
            ProgressBar bar = (ProgressBar) pDialog.findViewById(android.R.id.progress);


            bar.getIndeterminateDrawable().setColorFilter(
                    ContextCompat.getColor(SmsVerification.this, R.color.color_black),
                    android.graphics.PorterDuff.Mode.SRC_IN);

        }
    }


    /**
     * To hide the progress dialog
     */

    @SuppressWarnings("TryWithIdenticalCatches")
    private void hideProgressDialog() {
        if (pDialog.isShowing()) {
            //    pDialog.dismiss();

            Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();


            if (context instanceof Activity) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                        pDialog.dismiss();
                    }
                } else {


                    if (!((Activity) context).isFinishing()) {
                        pDialog.dismiss();
                    }
                }
            } else {


                try {
                    pDialog.dismiss();
                } catch (final IllegalArgumentException e) {
                    e.printStackTrace();

                } catch (final Exception e) {
                    e.printStackTrace();

                }
            }


        }
    }

    @Subscribe
    public void getMessage(JSONObject object) {
        try {
            if (object.getString("eventName").equals("callMinimized")) {

                minimizeCallScreen(object);
            }

        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }

    }

    private void minimizeCallScreen(JSONObject obj) {
        try {
            Intent intent = new Intent(SmsVerification.this, ChatMessageScreen.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("receiverUid", obj.getString("receiverUid"));
            intent.putExtra("receiverName", obj.getString("receiverName"));
            intent.putExtra("documentId", obj.getString("documentId"));

            intent.putExtra("receiverImage", obj.getString("receiverImage"));
            intent.putExtra("colorCode", obj.getString("colorCode"));

            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        bus.unregister(this);
    }
}


