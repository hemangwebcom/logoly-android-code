package chat.hola.com.howdoo.home.activity.youTab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.activity.youTab.followrequest.FollowRequestActivity;
import chat.hola.com.howdoo.home.activity.youTab.followrequest.ReuestData;
import chat.hola.com.howdoo.home.activity.youTab.model.Data;
import chat.hola.com.howdoo.home.activity.youTab.model.RequestedChannels;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.ChannelRequestersActivity;
import chat.hola.com.howdoo.home.model.PostData;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;

/**
 * <h>FacebookFrag.class</h>
 * <p>
 * This fragment show activities of other on your post and use the
 * {@link YouAdapter} with recyclerView.
 *
 * @author 3Embed
 * @since 14/2/18.
 */

public class YouFrag extends Fragment implements YouContract.View, SwipeRefreshLayout.OnRefreshListener, YouAdapter.OnAdapterClickCallback {

    @Inject
    YouAdapter youAdapter;

    @Inject
    YouPresenter presenter;

    @Inject
    TypefaceManager typefaceManager;

    @Inject
    SessionManager sessionManager;

    @BindView(R.id.recyclerYou)
    RecyclerView recyclerYou;

    @BindView(R.id.srSwipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.rlError)
    RelativeLayout rlError;

    @BindView(R.id.tvError)
    TextView tvError;

    @BindView(R.id.btnTryAgain)
    Button btnTryAgain;

    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @BindView(R.id.tvEmptyTitle)
    TextView tvEmptyTitle;
    @BindView(R.id.tvEmptyMsg)
    TextView tvEmptyMsg;

    @BindView(R.id.ivChannelImage)
    SimpleDraweeView ivChannelImage;
    @BindView(R.id.tvSubscriptionRequestTitle)
    TextView tvSubscriptionRequestTitle;
    @BindView(R.id.tvSubscriptionRequestSubtitle)
    TextView tvSubscriptionRequestSubtitle;

    @BindView(R.id.ivUserImage)
    SimpleDraweeView ivUserImage;
    @BindView(R.id.tvFollowRequestTitle)
    TextView tvFollowRequestTitle;
    @BindView(R.id.tvFollowRequestSubtitle)
    TextView tvFollowRequestSubtitle;
    @BindView(R.id.followCount)
    TextView followCount;

    @BindView(R.id.rlFollowRequest)
    RelativeLayout rlFollowRequest;
    @BindView(R.id.rlRequest)
    RelativeLayout rlRequest;
    @BindView(R.id.devider)
    RelativeLayout devider;
    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;

    private Unbinder unbinder;
    private LinearLayoutManager linearLayoutManager;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int PAGE_SIZE = 10;
    private int page = 0;
    private ArrayList<Data> dataList = new ArrayList<>();
    List<RequestedChannels> requestedChannels = new ArrayList<>();
    List<ReuestData> followReuest = new ArrayList<>();
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Inject
    public YouFrag() {
    }

    public static YouFrag newInstance() {
        return new YouFrag();
    }

    RecyclerView.OnScrollListener recyclerOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                    page++;
                    presenter.loadYouData(PAGE_SIZE * page, PAGE_SIZE);
                }
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_you_tab, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        presenter.attachView(this);
        initRecyclerYou();
        applyFont();
        swipeRefreshLayout.setOnRefreshListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.channelRequest();
        presenter.followRequest();
    }

    public void applyFont() {
        tvError.setTypeface(typefaceManager.getMediumFont());
        btnTryAgain.setTypeface(typefaceManager.getMediumFont());
        tvEmptyTitle.setTypeface(typefaceManager.getMediumFont());
        tvEmptyMsg.setTypeface(typefaceManager.getRegularFont());
    }

    @Override
    public void showErrorLayout(boolean show) {
        if (show) {
            llEmpty.setVisibility(View.VISIBLE);
        } else
            llEmpty.setVisibility(View.GONE);
    }

    @Override
    public void channelRequest(List<RequestedChannels> response) {
        requestedChannels.clear();
        requestedChannels = response;
        if (response.isEmpty()) {
            rlRequest.setVisibility(View.GONE);
            devider.setVisibility(View.GONE);
        } else {
            rlRequest.setVisibility(View.VISIBLE);
            devider.setVisibility(View.VISIBLE);
            ivChannelImage.setImageURI(response.get(0).getChannelImageUrl());
        }
    }

    @Override
    public void followRequest(List<ReuestData> response, Integer request) {
        followReuest.clear();
        followReuest = response;
        if (response.isEmpty()) {
            rlFollowRequest.setVisibility(View.GONE);
            devider.setVisibility(View.GONE);
        } else {
            if (request > 0) {
                followCount.setVisibility(View.VISIBLE);
                followCount.setText("" + request);
            }
            rlFollowRequest.setVisibility(View.VISIBLE);
            devider.setVisibility(View.VISIBLE);
            ivUserImage.setImageURI(response.get(0).getProfilePic());
        }
    }

    @OnClick({R.id.ivUserImage, R.id.rlFollowRequest})
    public void reqFollow() {
        startActivity(new Intent(getContext(), FollowRequestActivity.class).putExtra("data", (Serializable) followReuest));
    }

    @OnClick({R.id.ivChannelImage, R.id.rlRequest})
    public void reqSubChannels() {
        startActivity(new Intent(getContext(), ChannelRequestersActivity.class).putExtra("data", (Serializable) requestedChannels));
    }

    private void initRecyclerYou() {
        llNetworkError.setErrorListner(this);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerYou.setLayoutManager(linearLayoutManager);
        recyclerYou.setAdapter(youAdapter);
        presenter.loadYouData(page * PAGE_SIZE, PAGE_SIZE);
        youAdapter.setClickCallback(this);
        recyclerYou.addOnScrollListener(recyclerOnScrollListener);
    }


    @OnClick(R.id.btnTryAgain)
    public void tryAgain() {
        onRefresh();
    }

    public void isDataLoading(boolean isLoading) {
        if (swipeRefreshLayout != null) {
            if (isLoading) {
                swipeRefreshLayout.setRefreshing(true);
                this.isLoading = true;
            } else {
                swipeRefreshLayout.setRefreshing(false);
                this.isLoading = false;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void onRefresh() {
        dataList.clear();
        page = 0;
        presenter.loadYouData(page * PAGE_SIZE, PAGE_SIZE);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showYouActivity(ArrayList<Data> dataList) {
        if (dataList != null && !dataList.isEmpty()) {
            if (dataList.size() <= PAGE_SIZE)
                isLastPage = true;
            this.dataList.addAll(dataList);
            if (this.dataList.isEmpty()) {
                llEmpty.setVisibility(View.VISIBLE);
            } else {
                llEmpty.setVisibility(View.GONE);
                youAdapter.setData(this.dataList);
            }
        }
    }


    @Override
    public void onProfilePicClick(String userId) {
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public void onPostClickCallback(PostData data, View view) {
        Intent intent = new Intent(getContext(), SocialDetailActivity.class);
        intent.putExtra("postId", data.getPostId());
        String transitionName = getString(R.string.transition);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) getContext(), view, transitionName);
        ActivityCompat.startActivity(getContext(), intent, options.toBundle());
    }

    @Override
    public void onFollow(String userId) {
        presenter.follow(userId);
    }

    @Override
    public void onUnfollow(String userId) {
        presenter.unFollow(userId);
    }

    @Override
    public void reload() {
        onRefresh();
    }
}