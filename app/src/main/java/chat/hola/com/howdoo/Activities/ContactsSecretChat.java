package chat.hola.com.howdoo.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.howdoo.dubly.R;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import chat.hola.com.howdoo.Adapters.SelectUserAdapter;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.ModelClasses.SelectUserItem;
import chat.hola.com.howdoo.SecretChat.SecretChatMessageScreen;
import chat.hola.com.howdoo.Utilities.CustomLinearLayoutManager;
import chat.hola.com.howdoo.Utilities.MqttEvents;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.RecyclerItemClickListener;
import chat.hola.com.howdoo.Utilities.SortUsers;
import chat.hola.com.howdoo.Utilities.Utilities;

/**
 * Created by moda on 26/07/17.
 */

public class ContactsSecretChat extends AppCompatActivity {

    private SelectUserAdapter mAdapter;
//    private String chatName;

    private ArrayList<SelectUserItem> mUserData = new ArrayList<>();

    private Bus bus = AppController.getBus();
    private RelativeLayout root;
    private LinearLayout llEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_list);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));

        RecyclerView recyclerViewUsers = (RecyclerView) findViewById(R.id.list_of_users);
        recyclerViewUsers.setHasFixedSize(true);
        mAdapter = new SelectUserAdapter(ContactsSecretChat.this, mUserData);
        recyclerViewUsers.setItemAnimator(new DefaultItemAnimator());
        recyclerViewUsers.setLayoutManager(new CustomLinearLayoutManager(ContactsSecretChat.this, LinearLayoutManager.VERTICAL, false));
        recyclerViewUsers.setAdapter(mAdapter);

        llEmpty = (LinearLayout) findViewById(R.id.llEmpty);
        root = (RelativeLayout) findViewById(R.id.root);

        loadContacts();

        recyclerViewUsers.addOnItemTouchListener(new RecyclerItemClickListener(ContactsSecretChat.this, recyclerViewUsers, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {


                if (position >= 0) {


                    /*
                     * To add a conversation,we add a members nested collection
                     */


                    SelectUserItem item = mUserData.get(position);


                    Intent i = new Intent(ContactsSecretChat.this, SecretChatMessageScreen.class);

                    i.putExtra("receiverIdentifier", item.getUserIdentifier());
                    i.putExtra("receiverUid", item.getUserId());
                    i.putExtra("receiverImage", item.getUserImage());
                    i.putExtra("receiverName", item.getUserName());
                    i.putExtra("secretChatInitiated", true);

                    String secretId = AppController.getInstance().randomString();
                    String docId = AppController.getInstance().findDocumentIdOfReceiver(item.getUserId(), secretId);
                    String time = Utilities.tsInGmt();

                    if (docId.isEmpty()) {


                        docId = AppController.findDocumentIdOfReceiver(item.getUserId(), time, item.getUserName(),
                                item.getUserImage(), secretId, false, item.getUserIdentifier(), "", false);


                        AppController.getInstance().getDbController().updateChatListForNewMessageFromHistory(
                                docId, getResources().getString(R.string.YouInvited) + " " + item.getUserName() + " " +
                                        getResources().getString(R.string.JoinSecretChat), false, time, time, 0, false, -1);

                    }
                    i.putExtra("documentId", docId);
                    i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(i);
                    supportFinishAfterTransition();

                }


            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        TextView title = (TextView) findViewById(R.id.title);


        Typeface tf = AppController.getInstance().getRegularFont();


        title.setTypeface(tf, Typeface.BOLD);
        ImageView close = (ImageView) findViewById(R.id.close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        bus.register(this);
    }

    @Override
    public void onBackPressed() {


        if (AppController.getInstance().isActiveOnACall()) {
            if (AppController.getInstance().isCallMinimized()) {
                super.onBackPressed();
                supportFinishAfterTransition();
            }
        } else {
            super.onBackPressed();
            supportFinishAfterTransition();
        }

    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private void loadContacts() {
        final ProgressDialog pDialog = new ProgressDialog(ContactsSecretChat.this, 0);


        pDialog.setCancelable(false);


        pDialog.setMessage(getString(R.string.Load_Contacts));
        pDialog.show();

        ProgressBar bar = (ProgressBar) pDialog.findViewById(android.R.id.progress);


        bar.getIndeterminateDrawable().setColorFilter(
                ContextCompat.getColor(ContactsSecretChat.this, R.color.color_black),
                android.graphics.PorterDuff.Mode.SRC_IN);


        Map<String, Object> map;


        SelectUserItem item;


        ArrayList<Map<String, Object>> contactsList = AppController.getInstance().getDbController().loadContacts(AppController.getInstance().getContactsDocId());
        for (int i = 0; i < contactsList.size(); i++) {

            map = contactsList.get(i);
            if (!map.containsKey("blocked")) {
                item = new SelectUserItem();

                item.setUserId((String) map.get("contactUid"));


            /*
             * Since email is shown as the lower part of each of the contacts row in case of the contact sync
             */

                item.setUserIdentifier((String) map.get("contactIdentifier"));
                item.setUserName((String) map.get("contactName"));

                item.setUserImage((String) map.get("contactPicUrl"));


                mUserData.add(item);

            }
        }


        Collections.sort(mUserData, new SortUsers());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mAdapter.notifyDataSetChanged();

            }
        });


        if (pDialog.isShowing()) {


            Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();


            if (context instanceof Activity) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                        pDialog.dismiss();
                    }
                } else {


                    if (!((Activity) context).isFinishing()) {
                        pDialog.dismiss();
                    }
                }
            } else {


                try {
                    pDialog.dismiss();
                } catch (final IllegalArgumentException e) {
                    e.printStackTrace();

                } catch (final Exception e) {
                    e.printStackTrace();

                }
            }
        }


        if (mUserData.size() == 0) {
            llEmpty.setVisibility(View.VISIBLE);
//            TextView noUsers = (TextView) findViewById(R.id.noUsers);
//
//
//            noUsers.setTypeface(AppController.getInstance().getRegularFont());
//
//            noUsers.setText(getString(R.string.none_contacts));
//            noUsers.setVisibility(View.VISIBLE);

//            if (root != null) {
//
//
//                Snackbar snackbar = Snackbar.make(root, R.string.none_contacts,
//                        Snackbar.LENGTH_SHORT);
//                snackbar.show();
//
//
//                View view = snackbar.getView();
//                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
//                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//            }

        }


    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mUserData.clear();


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mAdapter.notifyDataSetChanged();

            }
        });
        loadContacts();
    }


    private void minimizeCallScreen(JSONObject obj) {
        try {
            Intent intent = new Intent(ContactsSecretChat.this, ChatMessageScreen.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("receiverUid", obj.getString("receiverUid"));
            intent.putExtra("receiverName", obj.getString("receiverName"));
            intent.putExtra("documentId", obj.getString("documentId"));

            intent.putExtra("receiverImage", obj.getString("receiverImage"));
            intent.putExtra("colorCode", obj.getString("colorCode"));

            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Subscribe
    public void getMessage(JSONObject object) {
        try {
            if (object.getString("eventName").equals("callMinimized")) {

                minimizeCallScreen(object);
            } else if (object.getString("eventName").equals(MqttEvents.UserUpdates.value + "/" + AppController.getInstance().getUserId())) {


                switch (object.getInt("type")) {


                    case 2:
/*
 * Profile pic update
 */


 /*
                         * To update in the contacts list
                         */


                        final int pos1 = findContactPositionInList(object.getString("userId"));
                        if (pos1 != -1) {

                            /*
                             * Have to clear the glide cache
                             */

                            SelectUserItem item = mUserData.get(pos1);


                            item.setUserImage(object.getString("profilePic"));
                            mUserData.set(pos1, item);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemChanged(pos1);
                                }
                            });
                        }


                        break;
                    case 3:
/*
 * Any of mine previous phone contact join
 */

                        SelectUserItem item2 = new SelectUserItem();
                        item2.setUserImage("");
                        item2.setUserId(object.getString("userId"));


                        item2.setUserIdentifier(object.getString("number"));


                        item2.setUserName(object.getString("name"));
                        final int position = findContactPositionInList(object.getString("userId"));
                        if (position == -1) {


                            mUserData.add(item2);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemInserted(mUserData.size() - 1);
                                }
                            });


                        } else {
                            mUserData.set(position, item2);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemChanged(position);
                                }
                            });

                        }
                        break;

                    case 4:
/*
 * New contact added request sent,for the response of the PUT contact api
 */


                        switch (object.getInt("subtype")) {

                            case 0:

                                /*
                                 * Follow name or number changed but number still valid
                                 */


                                final int pos3 = findContactPositionInList(object.getString("contactUid"));


                                if (pos3 != -1) {


                                    SelectUserItem item = mUserData.get(pos3);


                                    item.setUserName(object.getString("contactName"));
                                    mUserData.set(pos3, item);

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemChanged(pos3);
                                        }
                                    });


                                }

                                break;
                            case 1:
/*
 * Number of active contact changed and new number not in contact
 */

                                final int pos4 = findContactPositionInList(object.getString("contactUid"));


                                if (pos4 != -1) {


                                    mUserData.remove(pos4);


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemChanged(pos4);
                                        }
                                    });
                                }

                                break;

                            case 2:

                                /*
                                 * New contact added
                                 */


                                try {
                                    SelectUserItem item = new SelectUserItem();

                                    item.setUserImage(object.getString("contactPicUrl"));
                                    item.setUserName(object.getString("contactName"));

                                    item.setUserIdentifier(object.getString("contactIdentifier"));
                                    item.setUserId(object.getString("contactUid"));
                                    mUserData.add(item);


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemInserted(mUserData.size() - 1);
                                        }
                                    });

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                        }


                        break;

                    case 5:
/*
 * Follow deleted request sent,for the response of the DELETE contact api
 */


                        /*
                         * Number was in active contact
                         */
                        if (object.has("status") && object.getInt("status") == 0) {

                            final int pos2 = findContactPositionInList(object.getString("userId"));

                            if (pos2 != -1) {


                                mUserData.remove(pos2);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        mAdapter.notifyItemChanged(pos2);
                                        //  mAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        }
                        break;

                    case 6: {

                        if (object.getBoolean("blocked")) {
                            removeFromContacts(object.getString("initiatorId"));

                        }
                        break;
                    }
                }


            } else if (object.getString("eventName").equals("ContactNameUpdated")) {

                final int pos = findContactPositionInList(object.getString("contactUid"));


                SelectUserItem item = mUserData.get(pos);


                item.setUserName(object.getString("contactName"));
                mUserData.set(pos, item);


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });

            }


        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        bus.unregister(this);
    }


        /*
     * To remove the blocked user from the list to whom the message can be forwarded
     *
     *
     * As of now,only feature to remove a contact from the list is there,not the option to update the list incase you are unblocked
     *
     */


    private void removeFromContacts(String opponentId) {


        /*
         * For removing from the contacts list
         */

        for (int i = 0; i < mUserData.size(); i++) {

            if (mUserData.get(i).getUserId().equals(opponentId)) {


                mUserData.remove(i);

                final int k = i;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (k == 0) {

                            mAdapter.notifyDataSetChanged();
                        } else {

                            mAdapter.notifyItemRemoved(k);
                        }
                    }
                });

                break;
            }
        }


    }

    private int findContactPositionInList(String contactUid) {
        int pos = -1;

        for (int i = 0; i < mUserData.size(); i++) {


            if (mUserData.get(i).getUserId().equals(contactUid)) {

                return i;
            }
        }

        return pos;
    }

}
