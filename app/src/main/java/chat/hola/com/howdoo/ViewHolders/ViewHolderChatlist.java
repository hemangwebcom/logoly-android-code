package chat.hola.com.howdoo.ViewHolders;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.howdoo.dubly.R;

import chat.hola.com.howdoo.AppController;


/**
 * Created by moda on 20/06/17.
 */

public class ViewHolderChatlist extends RecyclerView.ViewHolder {
    public TextView newMessageTime, newMessage, storeName, newMessageDate, newMessageCount;
    public ImageView storeImage, lock, tick, chatSelected;
    public RelativeLayout rl;
    private AppController appController;
    private Typeface fontMedium,fontRegular;
    public ViewHolderChatlist(View view) {
        super(view);
         appController=AppController.getInstance();
         fontMedium=appController.getMediumFont();
         fontRegular=appController.getRegularFont();
        tick = (ImageView) view.findViewById(R.id.tick);
        newMessageTime = (TextView) view.findViewById(R.id.newMessageTime);
        newMessage = (TextView) view.findViewById(R.id.newMessage);
        newMessageDate = (TextView) view.findViewById(R.id.newMessageDate);
        storeName = (TextView) view.findViewById(R.id.storeName);
        storeImage = (ImageView) view.findViewById(R.id.storeImage2);

        chatSelected = (ImageView) view.findViewById(R.id.chatSelected);

        rl = (RelativeLayout) view.findViewById(R.id.rl);


        newMessageCount = (TextView) view.findViewById(R.id.newMessageCount);
        lock = (ImageView) view.findViewById(R.id.secretLockIv);


        Typeface tf = AppController.getInstance().getRegularFont();


        newMessageCount.setTypeface(fontMedium);
        newMessageDate.setTypeface(fontRegular);
        newMessageTime.setTypeface(fontRegular);
        newMessage.setTypeface(fontRegular);
        storeName.setTypeface(fontMedium);
    }
}
