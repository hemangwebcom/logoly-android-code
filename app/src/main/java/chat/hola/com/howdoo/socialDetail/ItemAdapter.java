package chat.hola.com.howdoo.socialDetail;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.RoundedImageView;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.profileScreen.followers.Model.Data;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 9/3/2018.
 */
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    //    implements Filterable
    private List<Data> mItems = new ArrayList<>();
    private ItemListener mListener;
    private Context context;
    private TypefaceManager typefaceManager;

    ItemAdapter(Context context, List<Data> items, ItemListener listener, TypefaceManager typefaceManager) {
        this.context = context;
        if (items != null) {
            mItems = items;
        }
        mListener = listener;
        this.typefaceManager = typefaceManager;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.follow_bottom_sheet_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvUserName.setTypeface(typefaceManager.getSemiboldFont());
        holder.tvName.setTypeface(typefaceManager.getRegularFont());
        holder.tvUserName.setText(mItems.get(position).getUsername());
        String fullName = mItems.get(position).getFirstName();
        if (mItems.get(position).getLastName() != null && !mItems.get(position).getLastName().isEmpty())
            fullName = fullName + " " + mItems.get(position).getLastName();

        holder.tvName.setText(fullName);
        String profilepic = Constants.DEFAULT_PROFILE_PIC_LINK;
        if (mItems.get(position).getProfilePic() != null | mItems.get(position).getProfilePic().isEmpty())
            profilepic = mItems.get(position).getProfilePic();
        Glide.with(context).load(profilepic).asBitmap().centerCrop().into(holder.ivProfilePic);

        holder.tbSend.setOnCheckedChangeListener(null);
        holder.tbSend.setChecked(mItems.get(position).isSent());

        holder.tbSend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mItems.get(position).setSent(b);
                if (b)
                    holder.tbSend.setEnabled(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName;
        TextView tvUserName;
        RoundedImageView ivProfilePic;
        ToggleButton tbSend;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvUserName = (TextView) itemView.findViewById(R.id.tvUserName);
            ivProfilePic = (RoundedImageView) itemView.findViewById(R.id.ivProfilePic);
            tbSend = (ToggleButton) itemView.findViewById(R.id.tbSend);
            tbSend.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(mItems.get(getAdapterPosition()));
            }
        }
    }

    interface ItemListener {
        void onItemClick(Data data);
    }
}
