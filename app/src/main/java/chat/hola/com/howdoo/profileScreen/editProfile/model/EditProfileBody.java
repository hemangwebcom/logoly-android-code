package chat.hola.com.howdoo.profileScreen.editProfile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 19/3/18.
 */

public class EditProfileBody {

    @Expose
    @SerializedName("profilePic")
    String imgUrl;

    @Expose
    @SerializedName("firstName")
    String firstName;

    @Expose
    @SerializedName("lastName")
    String lastName;

    @Expose
    @SerializedName("userName")
    String userName;

    @Expose
    @SerializedName("status")
    String status;

    @Expose
    @SerializedName("email")
    String email;

    @Expose
    @SerializedName("private")
    Integer _private;

//    public void setImgPath(String imgPath) {
//        this.imgPath = imgPath;
//    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEmail(String email) {
        this.email = email;
    }


//    public String getImgPath() {
//        return imgPath;
//    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserName() {
        return userName;
    }

    public String getStatus() {
        return status;
    }

    public String getEmail() {
        return email;
    }

    public Integer get_private() {
        return _private;
    }

    public void set_private(Integer _private) {
        this._private = _private;
    }
}
