package chat.hola.com.howdoo.socialDetail;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 11/27/2018.
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.player)
    public VideoPlayerView mPlayer;
    @BindView(R.id.cover)
    public ImageView mCover;
    @BindView(R.id.tvUserName)
    public TextView tvUserName;
    @BindView(R.id.tvTitle)
    public TextView tvTitle;
    @BindView(R.id.tvLocation)
    public TextView tvLocation;
    @BindView(R.id.tvCategory)
    public TextView tvCategory;
    @BindView(R.id.tvChannel)
    public TextView tvChannel;
    @BindView(R.id.ivProfilePic)
    public SimpleDraweeView ivProfilePic;
    @BindView(R.id.ivLike)
    public CheckBox ivLike;
    @BindView(R.id.ivDisLike)
    public CheckBox ivDisLike;
    @BindView(R.id.ivFavourite)
    public CheckBox ivFavourite;
    @BindView(R.id.ivComment)
    public ImageView ivComment;
    @BindView(R.id.tvLikeCount)
    public TextView tvLikeCount;
    @BindView(R.id.tvDisLikeCount)
    public TextView tvDisLikeCount;
    @BindView(R.id.tvFavouriteCount)
    public TextView tvFavouriteCount;
    @BindView(R.id.tvCommentCount)
    public TextView tvCommentCount;
    @BindView(R.id.tvViewCount)
    public TextView tvViewCount;
    @BindView(R.id.flMediaContainer)
    public RelativeLayout flMediaContainer;
    @BindView(R.id.tvMusic)
    public TextView tvMusic;
    @BindView(R.id.llMusic)
    public LinearLayout llMusic;
    @BindView(R.id.tvDivide)
    public TextView tvDivide;


    public ViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
