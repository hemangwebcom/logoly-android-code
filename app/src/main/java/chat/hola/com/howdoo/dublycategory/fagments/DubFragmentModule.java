package chat.hola.com.howdoo.dublycategory.fagments;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 23/2/18.
 */

@FragmentScoped
@Module
public interface DubFragmentModule {

    @FragmentScoped
    @Binds
    DubFragmentContract.Presenter presenter(DubFragmentPresenter presenter);

   /* @FragmentScoped
    @Binds
    DubFavouriteFragmentContract.View view(StoryFragment fragment);*/
}
