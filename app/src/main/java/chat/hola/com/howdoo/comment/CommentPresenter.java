package chat.hola.com.howdoo.comment;

import android.util.Log;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.comment.model.ClickListner;
import chat.hola.com.howdoo.comment.model.CommentResponse;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;
import chat.hola.com.howdoo.models.NetworkConnector;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h1>BlockUserPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class CommentPresenter implements CommentContract.Presenter, ClickListner {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;
    @Inject
    HowdooService service;
    @Inject
    CommentContract.View view;
    @Inject
    CommentModel model;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    CommentPresenter() {
    }


    @Override
    public void addComment(String postId, String comment) {
        service.addComment(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, model.getParamsAddComment(comment))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<CommentResponse>>() {
                    @Override
                    public void onNext(Response<CommentResponse> response) {
                        if (response.code() == 200) {
                            model.addToList(response.body().getComments());
                            view.commented(true);
                        } else if (response.code() == 401) {
                            view.sessionExpired();
                            view.commented(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        view.commented(false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getComments(String postId, int offset, int limit) {
        isLoading = true;
        service.getComment(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, offset, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<chat.hola.com.howdoo.comment.model.Response>>() {
                    @Override
                    public void onNext(Response<chat.hola.com.howdoo.comment.model.Response> response) {
                        try {
                            if (response.code() == 200) {
                                isLastPage = response.body().getData().size() < PAGE_SIZE;
                                if (offset == 0)
                                    model.clearList();
                                model.setData(response.body().getData());
                            } else if (response.code() == 401) {
                                view.sessionExpired();
                            }
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }
                });
    }

    @Override
    public void searchHashTag(String hashTag) {
        service.checkHashTag(AppController.getInstance().getApiToken(), Constants.LANGUAGE, hashTag)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Hash_tag_people_pojo>>() {
                    @Override
                    public void onNext(retrofit2.Response<Hash_tag_people_pojo> response) {
                        Log.i("RESPONSE", response.toString());
                        if (response.code() == 200)
                            view.setTag(response.body());
                        else if (response.code() == 401)
                            view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void searchUserTag(String userTag) {

        service.checkUserTag(AppController.getInstance().getApiToken(), Constants.LANGUAGE, userTag)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Hash_tag_people_pojo>>() {
                    @Override
                    public void onNext(retrofit2.Response<Hash_tag_people_pojo> response) {
                        if (response.code() == 200)
                            view.setUser(response.body());
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void callApiOnScroll(String postId, int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                getComments(postId, PAGE_SIZE * page, PAGE_SIZE);
            }
        }
    }

    @Override
    public ClickListner getPresenter() {
        return this;
    }

    @Override
    public void onUserClick(int position) {
        view.openProfile(model.getUserId(position));
    }

    @Override
    public void itemSelect(int position, boolean isSelected) {
        model.selectItem(position, isSelected);
    }
}
