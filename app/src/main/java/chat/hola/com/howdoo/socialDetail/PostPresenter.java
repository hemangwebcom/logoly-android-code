package chat.hola.com.howdoo.socialDetail;

import android.content.Context;
import android.support.annotation.Nullable;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>PostPresenter</h1>
 *
 * @author 3Embed
 * @since 4/5/2018.
 */

public class PostPresenter implements PostContract.Presenter {
    public static final String TAG = "ClubPostPresenter";

    @Nullable
    PostContract.View view;
    @Inject
    HowdooService service;
    PostModel model;
    private Context context;

    @Inject
    PostPresenter(Context context) {
        this.context = context;
        model = new PostModel();
    }


    @Override
    public void like(String postId) {
        service.like(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, model.getParams(postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.liked(true);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    @Override
    public void unlike(String postId) {
        service.unlike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, model.getParams(postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.liked(false);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void deletePost(String postId) {
        service.deletePost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            view.deleted();
                            view.showMessage(null, R.string.deleted);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void reportPost(String postId, String reason, String message) {
        service.reportPost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, model.getReasonParams(postId, reason, message))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {

                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.showMessage(null, R.string.reported);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void attachView(PostContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }
}
