package chat.hola.com.howdoo.GroupChat.ViewHolders;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.howdoo.dubly.R;

import chat.hola.com.howdoo.AppController;

/**
 * Created by moda on 26/09/17.
 */

public class ViewHolderAddMember extends RecyclerView.ViewHolder {


    public TextView contactName, contactStatus;

    public ImageView contactImage;


    public ViewHolderAddMember(View view) {
        super(view);

        contactStatus = (TextView) view.findViewById(R.id.contactStatus);


        contactName = (TextView) view.findViewById(R.id.contactName);

        contactImage = (ImageView) view.findViewById(R.id.storeImage2);


        Typeface tf = AppController.getInstance().getRegularFont();

        contactName.setTypeface(tf, Typeface.NORMAL);
        contactStatus.setTypeface(tf, Typeface.ITALIC);

    }
}
