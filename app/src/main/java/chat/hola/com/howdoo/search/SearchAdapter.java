package chat.hola.com.howdoo.search;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.search.model.SearchData;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    private AppController appController;
    private Typeface fontMedium;
    private List<SearchData> searchData = new ArrayList<>();
    private Context context;
    private SearchAdapter.ClickListner clickListner;

    public void setData(Context context, List<SearchData> data) {
        this.context = context;
        this.searchData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_row, parent, false);
        appController = AppController.getInstance();
        fontMedium = appController.getMediumFont();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchAdapter.ViewHolder holder, final int position) {
        if (getItemCount() != -1) {
            SearchData data = searchData.get(position);

            Glide.with(context).load(data.getProfilePic()).asBitmap().centerCrop()
                    .placeholder(R.drawable.profile_one).into(new BitmapImageViewTarget(holder.profileIv));

            String name = "";
            if (data.getFirstName() != null)
                name += data.getFirstName();
            if (data.getLastName() != null)
                name += " " + data.getLastName();
            holder.tvName.setText(name);
            holder.profileNameTv.setText(data.getUserName());
            holder.relativeLayout.setOnClickListener(view -> clickListner.onItemClick(position));


            boolean isFollowingMe = data.isFollowingMe();

            if (data.getId().equals(AppController.getInstance().getUserId())) {
                holder.follow.setVisibility(View.GONE);
                holder.action.setVisibility(View.GONE);
            } else if (isFollowingMe) {
                holder.action.setVisibility(View.VISIBLE);
                holder.follow.setVisibility(View.GONE);
            } else {
                holder.action.setVisibility(View.GONE);
                holder.follow.setVisibility(View.VISIBLE);
            }

            boolean isPrivate = data.getPrivate() == 1;
            boolean isChecked;

            switch (data.getFollowStatus()) {
                case 0:
                    //public - unfollow
                    isPrivate = data.getPrivate().equals("1");
                    isChecked = false;
                    break;
                case 1:
                    //public - follow
                    isPrivate = false;
                    isChecked = true;
                    break;
                case 2:
                    //private - requested
                    isPrivate = true;
                    isChecked = true;
                    break;
                case 3:
                    //private - request
                    isPrivate = true;
                    isChecked = false;
                    break;
                default:
                    isChecked = false;
                    break;

            }
            holder.follow.setTextOn(context.getResources().getString(isPrivate ? R.string.requested : R.string.unfollow));
            holder.follow.setTextOff(context.getResources().getString(isPrivate ? R.string.request : R.string.follow));
            holder.follow.setChecked(isChecked);

            holder.follow.setOnClickListener(view -> clickListner.onFollow(data.getId(), holder.follow.isChecked(), position));
            holder.action.setOnClickListener(view -> clickListner.openActionPopup(position, data.getId()));

        }
    }

    @Override
    public int getItemCount() {
        return searchData != null ? searchData.size() : -1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView profileIv;
        private TextView profileNameTv, tvName;
        private RelativeLayout relativeLayout;
        private ToggleButton follow;
        private ImageButton action;

        public ViewHolder(View itemView) {
            super(itemView);
            profileNameTv = (TextView) itemView.findViewById(R.id.profileNameTv);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            profileIv = (ImageView) itemView.findViewById(R.id.profileIv);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rlItem);
            follow = (ToggleButton) itemView.findViewById(R.id.tbFollow);
            action = (ImageButton) itemView.findViewById(R.id.btnAction);
            profileNameTv.setTypeface(fontMedium);
            tvName.setTypeface(fontMedium);
        }
    }

    public void setListener(SearchAdapter.ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public interface ClickListner {
        void onItemClick(int position);

        void onFollow(String userId, boolean follow, int position);

        void openActionPopup(int position, String id);
    }
}
