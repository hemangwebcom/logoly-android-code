package chat.hola.com.howdoo.profileScreen;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.profileScreen.bottomProfileMenu.ProfileMenuFrag;
import chat.hola.com.howdoo.profileScreen.channel.ChannelFragment;
import chat.hola.com.howdoo.profileScreen.channel.ChannelModule;
import chat.hola.com.howdoo.profileScreen.story.StoryFragment;
import chat.hola.com.howdoo.profileScreen.story.StoryModule;
import chat.hola.com.howdoo.profileScreen.tag.TagFragment;
import chat.hola.com.howdoo.profileScreen.tag.TagModule;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ankit on 22/2/18.
 */

@ActivityScoped
@Module
public interface ProfileModule {

    @FragmentScoped
    @ContributesAndroidInjector()
    ProfileMenuFrag profileMenuFrag();

    @FragmentScoped
    @ContributesAndroidInjector(modules = TagModule.class)
    TagFragment tagFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {StoryModule.class})
    StoryFragment storyFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ChannelModule.class})
    ChannelFragment channelFragment();

    @ActivityScoped
    @Binds
    ProfileContract.Presenter presenter(ProfilePresenter presenter);

    @ActivityScoped
    @Binds
    ProfileContract.View view(ProfileActivity profileActivity);

}
