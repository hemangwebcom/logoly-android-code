package chat.hola.com.howdoo.home.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.activity.blog.BlogFragment;
import chat.hola.com.howdoo.home.activity.followingTab.FollowingFrag;
import chat.hola.com.howdoo.home.activity.youTab.YouFrag;
import dagger.android.support.DaggerFragment;

/**
 * <h>DiscoverActivity.class</h>
 * <p>
 * This fragment has two tabs {@link FollowingFrag} and {@link YouFrag}
 * which is handle by {@link ActivitiesPageAdapter}.
 *
 * @author 3Embed
 * @since 14/2/18.
 */

public class ActivitiesFragment extends DaggerFragment {

    @Inject
    TypefaceManager typefaceManager;

    @Inject
    FollowingFrag followingFrag;

    @Inject
    YouFrag youFrag;

    @Inject
    LandingActivity landingPageActivity;


    private Unbinder unbinder;

    @BindView(R.id.viewPagerActivities)
    ViewPager mViewPager;
    @BindView(R.id.tabLayoutActivities)
    TabLayout mTabLayout;

    @Inject
    public ActivitiesFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activities, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        ActivitiesPageAdapter fragmentPageAdapter = new ActivitiesPageAdapter(getChildFragmentManager());
        fragmentPageAdapter.addFragment(youFrag, "Community");
        fragmentPageAdapter.addFragment(new BlogFragment(), "Wave");
        fragmentPageAdapter.addFragment(new BlogFragment(), "Blog");
        mTabLayout.setupWithViewPager(mViewPager, true);
        mViewPager.setAdapter(fragmentPageAdapter);
        mViewPager.setCurrentItem(1);
        tabSetup();
        landingPageActivity.visibleActionBar();
        return rootView;
    }

    private void tabSetup() {
        //their exist some easier way to setfont to tab
        //need to replace.
        TextView tabOne = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.tab_textview, null);
        tabOne.setTypeface(typefaceManager.getSemiboldFont());
        tabOne.setText("Community");
        mTabLayout.getTabAt(0).setCustomView(tabOne);
        TextView tabTwo = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.tab_textview, null);
        tabTwo.setTypeface(typefaceManager.getSemiboldFont());
        tabTwo.setText("Wave");
        mTabLayout.getTabAt(1).setCustomView(tabTwo);
        TextView tabThree = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.tab_textview, null);
        tabThree.setTypeface(typefaceManager.getSemiboldFont());
        tabThree.setText("Blog");
        mTabLayout.getTabAt(2).setCustomView(tabThree);
    }

    @Override
    public void onResume() {
        super.onResume();
        landingPageActivity.visibleActionBar();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }
}
