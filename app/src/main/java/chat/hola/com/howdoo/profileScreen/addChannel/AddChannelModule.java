package chat.hola.com.howdoo.profileScreen.addChannel;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.profileScreen.ProfileContract;
import chat.hola.com.howdoo.profileScreen.ProfilePresenter;
import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 22/2/18.
 */

@ActivityScoped
@Module
public interface AddChannelModule {

    @ActivityScoped
    @Binds
    AddChannelContract.Presenter presenter(AddChannelPresenter presenter);

    @ActivityScoped
    @Binds
    AddChannelContract.View view(AddChannelActivity addChannelActivity);

}
