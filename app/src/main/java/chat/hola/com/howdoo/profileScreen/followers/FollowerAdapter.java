package chat.hola.com.howdoo.profileScreen.followers;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.profileScreen.discover.facebook.FacebookFrag;
import chat.hola.com.howdoo.profileScreen.followers.Model.Data;

/**
 * <h>youAdapter.class</h>
 * <p>
 * This adapter class is used by {@link FacebookFrag}.
 *
 * @author 3Embed
 * @since 02/03/18.
 */

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.ViewHolder> {

    private static final String TAG = FollowerAdapter.class.getSimpleName();

    //private final int DATA_SIZE = 10;
    private List<Data> followers = new ArrayList<>();
    private Context context;
    private TypefaceManager typefaceManager;
    boolean isFollowers = false;
    // Random random =  new Random();
    OnFollowUnfollowClickCallback clickCallback = null;

    public void setFollowers(boolean isFollowers) {
        this.isFollowers = isFollowers;
    }

    interface OnFollowUnfollowClickCallback {
        void onFollow(String userId);

        void onUnfollow(String userId);

        void openActionPopup(Data data, boolean isBlocked);
    }

    public void setOnFollowUnfollowClickCallback(OnFollowUnfollowClickCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    @Inject
    public FollowerAdapter(Context context, TypefaceManager typefaceManager) {
        this.context = context;
        this.typefaceManager = typefaceManager;
    }

    public void setData(List<Data> followers) {
        this.followers = followers;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return this.isFollowers ? 0 : 1;
    }

    @Override
    public FollowerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.you_row_new, parent, false);
        return new FollowerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FollowerAdapter.ViewHolder holder, int position) {

        Data follower = followers.get(position);

        String profilePic = follower.getProfilePic();
        if (profilePic == null || profilePic.isEmpty()) {
            profilePic = Constants.DEFAULT_PROFILE_PIC_LINK;
        }

        Glide.with(context).load(profilePic)
                .asBitmap()
                .centerCrop()
                .into(holder.ivRow);

        holder.tvRowTitle.setText(follower.getUsername());
        holder.tvRowTime.setText("");
        follower.setFollowing(follower.getType().getStatus().equals(1));
        String userId = isFollowers ? follower.getFollower() : follower.getFollowee();
        if (getItemViewType(position) == 0) {
            holder.tbFollow.setVisibility(View.GONE);
            holder.btnAction.setVisibility(userId.equals(AppController.getInstance().getUserId()) ? View.GONE : View.VISIBLE);
        } else {
            if (follower.isContact() || follower.isBlocked()) {
                holder.btnAction.setVisibility(View.VISIBLE);
                holder.tbFollow.setVisibility(View.GONE);
            } else {
                holder.btnAction.setVisibility(View.GONE);
                holder.tbFollow.setVisibility(userId.equals(AppController.getInstance().getUserId()) ? View.GONE : View.VISIBLE);
                holder.tbFollow.setChecked(follower.isFollowing());
            }
        }

    }

    @Override
    public int getItemCount() {
        return followers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ivRow)
        ImageView ivRow;
        @BindView(R.id.tvRowTitle)
        TextView tvRowTitle;
        @BindView(R.id.tvRowTime)
        TextView tvRowTime;
        @BindView(R.id.tbFollow)
        ToggleButton tbFollow;
        @BindView(R.id.btnAction)
        ImageButton btnAction;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvRowTime.setVisibility(View.GONE);
            tvRowTitle.setTypeface(typefaceManager.getMediumFont());
            tvRowTitle.setTypeface(typefaceManager.getMediumFont());
            tbFollow.setTypeface(typefaceManager.getMediumFont());
            tbFollow.setOnClickListener(this);
            btnAction.setOnClickListener(this);
            itemView.setOnClickListener(v -> {
                Data follower = followers.get(getAdapterPosition());
                Intent intent = new Intent(context, ProfileActivity.class);
                if (isFollowers)
                    intent.putExtra("userId", follower.getFollower());
                else
                    intent.putExtra("userId", follower.getFollowee());
                context.startActivity(intent);
                //  ((ClubActivity) context).finish();
            });
        }

        @Override
        public void onClick(View v) {
            Data follower = followers.get(getAdapterPosition());
            switch (v.getId()) {
                case R.id.tbFollow:
                    if (tbFollow.isChecked())
                        if (isFollowers)
                            clickCallback.onFollow(follower.getFollower());
                        else
                            clickCallback.onFollow(follower.getFollowee());
                    else if (isFollowers)
                        clickCallback.onUnfollow(follower.getFollower());
                    else
                        clickCallback.onUnfollow(follower.getFollowee());
                    break;
                case R.id.btnAction:
                    clickCallback.openActionPopup(follower, false);
                    break;
                default:
            }
        }
    }
}
