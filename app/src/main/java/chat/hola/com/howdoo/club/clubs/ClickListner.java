package chat.hola.com.howdoo.club.clubs;

/**
 * <h1>ClickListner</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public interface ClickListner {
    void onChannelClick(int position);

    void itemSelect(int position, int action);

    void leave(int position);

    void itemSelect(int position);

    void delete(int position);
}
