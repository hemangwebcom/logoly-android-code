package chat.hola.com.howdoo.profileScreen.story;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 23/2/18.
 */

@FragmentScoped
@Module
public interface StoryModule {

    @FragmentScoped
    @Binds
    StoryContract.Presenter presenter(StoryPresenter presenter);

   /* @FragmentScoped
    @Binds
    DubFavouriteFragmentContract.View view(StoryFragment fragment);*/
}
