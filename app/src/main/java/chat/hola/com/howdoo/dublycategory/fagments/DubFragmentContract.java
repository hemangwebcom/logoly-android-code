package chat.hola.com.howdoo.dublycategory.fagments;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.dublycategory.favourite.DubFavFragmentContract;

/**
 * Created by ankit on 23/2/18.
 */

public interface DubFragmentContract {

    interface View extends BaseView {
        void setMax(Integer max);

        void startedDownload();

        void progress(int downloadedSize);

        void finishedDownload(String path, String name, String musicId);

        void play(String audio, boolean isPlaying);

        void isLoading(boolean flag);
    }

    interface Presenter extends BasePresenter<DubFragmentContract.View> {

        void loadData(int skip, int limit);

        void startedDownload();
    }
}
