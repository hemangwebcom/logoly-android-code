package chat.hola.com.howdoo.club.member;

import android.app.Activity;

import chat.hola.com.howdoo.comment.CommentActivity;
import chat.hola.com.howdoo.comment.CommentContract;
import chat.hola.com.howdoo.comment.CommentPresenter;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>BlockUserModule</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

@ActivityScoped
@Module
public interface MemberModule {
    @ActivityScoped
    @Binds
    MemberContract.Presenter presenter(MemberPresenter presenter);

    @ActivityScoped
    @Binds
    MemberContract.View view(MemberActivity activity);

    @ActivityScoped
    @Binds
    Activity activity(MemberActivity activity);

}
