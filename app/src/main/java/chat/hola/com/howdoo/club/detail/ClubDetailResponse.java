package chat.hola.com.howdoo.club.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 10/20/2018.
 */
public class ClubDetailResponse implements Serializable {
    @SerializedName("club")
    @Expose
    private ClubDetail club;

    public ClubDetail getClub() {
        return club;
    }

    public void setClub(ClubDetail club) {
        this.club = club;
    }

    public static class ClubDetail implements Serializable {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("coverPic")
        @Expose
        private String coverPic;
        @SerializedName("access")
        @Expose
        private Integer access;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("about")
        @Expose
        private String about;
        @SerializedName("memberCount")
        @Expose
        private Integer memberCount;
        @SerializedName("postCount")
        @Expose
        private Integer postCount;
        @SerializedName("members")
        @Expose
        private List<String> members;
        @SerializedName("isAdmin")
        @Expose
        private Boolean isAdmin = false;
        @SerializedName("isOwner")
        @Expose
        private Boolean isOwner = false;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCoverPic() {
            return coverPic;
        }

        public void setCoverPic(String coverPic) {
            this.coverPic = coverPic;
        }

        public Integer getAccess() {
            return access;
        }

        public void setAccess(Integer access) {
            this.access = access;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public Integer getMemberCount() {
            return memberCount;
        }

        public void setMemberCount(Integer memberCount) {
            this.memberCount = memberCount;
        }

        public Integer getPostCount() {
            return postCount;
        }

        public void setPostCount(Integer postCount) {
            this.postCount = postCount;
        }

        public Boolean getAdmin() {
            return isAdmin;
        }

        public void setAdmin(Boolean admin) {
            isAdmin = admin;
        }

        public Boolean getOwner() {
            return isOwner;
        }

        public void setOwner(Boolean owner) {
            isOwner = owner;
        }

        public List<String> getMembers() {
            return members;
        }

        public void setMembers(List<String> members) {
            this.members = members;
        }
    }
}
