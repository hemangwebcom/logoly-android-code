package chat.hola.com.howdoo.socialDetail;

import com.howdoo.dubly.R;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.model.SocialDetailModel;
import chat.hola.com.howdoo.post.ReportReason;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.FollowResponse;
import chat.hola.com.howdoo.profileScreen.followers.Model.FollowersResponse;
import chat.hola.com.howdoo.socialDetail.model.PostResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>SocialDetailPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 23/3/18.
 */

public class SocialDetailPresenter implements SocialDetailContract.Presenter {

    private static final String TAG = SocialDetailActivity.class.getSimpleName();

    @Inject
    SocialDetailContract.View mView;
    @Inject
    HowdooService service;
    @Inject
    SocialDetailModel model;

    @Inject
    public SocialDetailPresenter() {

    }

    @Override
    public void getPostById(String postId, boolean isJustForView) {
        service.postDetail(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<PostResponse>>() {
                    @Override
                    public void onNext(Response<PostResponse> response) {
                        if (response.code() == 200 && !isJustForView)
                            mView.setData(response.body().getData());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    @Override
    public void getReportReasons() {
        service.getReportReason(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ReportReason>>() {
                    @Override
                    public void onNext(Response<ReportReason> response) {
                        if (response.code() == 200) {
                            mView.addToReportList(response.body().getData());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    //favourite
    @Override
    public void like(String postId) {
        service.like(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, model.getParams(postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            mView.liked(true);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    //unfavourite
    @Override
    public void unlike(String postId) {
        service.unlike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, model.getParams(postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            mView.liked(false);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void deletePost(String postId) {
        service.deletePost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            mView.deleted();
                            mView.showMessage(null, R.string.deleted);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void reportPost(String postId, String reason, String message) {
        service.reportPost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, model.getReasonParams(postId, reason, message))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {

                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            mView.showMessage(null, R.string.reported);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void follow(String followingId) {
        Map<String,Object> params = new HashMap<>();
        params.put("followingId",followingId);
        service.follow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void unfollow(String followingId) {
        service.unfollow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, followingId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> response) {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void postLike(String postId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", AppController.getInstance().getUserId());
        service.postLike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void postDislike(String postId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", AppController.getInstance().getUserId());
        service.postDislike(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void getFollowUsers() {
        service.getContacts(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowersResponse>>() {
                    @Override
                    public void onNext(Response<FollowersResponse> response) {
                        if (response.code() == 200)
                            mView.followers(response.body().getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}


