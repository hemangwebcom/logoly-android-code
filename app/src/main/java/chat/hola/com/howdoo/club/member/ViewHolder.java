package chat.hola.com.howdoo.club.member;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.comment.model.ClickListner;

/**
 * <h1>ViewHolder</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.ivProfilePic)
    SimpleDraweeView ivProfilePic;
    @BindView(R.id.cbSelect)
    CheckBox cbSelect;
    @BindView(R.id.btnAction)
    ImageButton btnAction;
    @BindView(R.id.llRequestActions)
    LinearLayout llRequestActions;
    @BindView(R.id.ibAccept)
    ImageButton ibAccept;
    @BindView(R.id.ibDecline)
    ImageButton ibDecline;
    private chat.hola.com.howdoo.club.member.ClickListner clickListner;

    public ViewHolder(View itemView, TypefaceManager typefaceManager, chat.hola.com.howdoo.club.member.ClickListner clickListner) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        tvName.setTypeface(typefaceManager.getSemiboldFont());

        this.clickListner = clickListner;
    }


    @OnClick(R.id.cbSelect)
    public void itemSelect() {
        clickListner.itemSelect(getAdapterPosition(), !cbSelect.isSelected());
    }

    @OnClick(R.id.ivProfilePic)
    public void profile() {
        clickListner.onUserClick(getAdapterPosition());
    }


}