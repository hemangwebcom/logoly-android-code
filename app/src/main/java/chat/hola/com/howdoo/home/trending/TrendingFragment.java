package chat.hola.com.howdoo.home.trending;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.SpannedGridLayoutManager;
import chat.hola.com.howdoo.Utilities.TagSpannable;
import chat.hola.com.howdoo.home.model.ContentAdapter;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.home.trending.model.Header;
import chat.hola.com.howdoo.home.trending.model.HeaderAdapter;
import chat.hola.com.howdoo.home.trending.model.TrendingContentAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;
import dagger.android.support.DaggerFragment;

/**
 * <h>TrendingFragment.class</h>
 * <p>
 * This Fragment shows the trendingPosts row using {@link HeaderAdapter } and
 * {@link ContentAdapter} with recyclerecyclerHeader.nd recyclerecyclerContent.
 *
 * @author 3Embed
 * @since 13/2/18.
 */

@SuppressLint("ValidFragment")
public class TrendingFragment extends DaggerFragment implements TrendingContract.View {


    @Inject
    public TrendingFragment() {
    }

    @Inject
    TrendingPresenter presenter;
    @Inject
    TrendingContentAdapter contentAdapter;
    @Inject
    HeaderAdapter headerAdapter;

    @BindView(R.id.rvHeader)
    RecyclerView rvHeader;
    @BindView(R.id.gvContent)
    RecyclerView gvContent;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @BindView(R.id.llHashTags)
    LinearLayout llHashTags;
    @BindView(R.id.tvHashTags)
    TextView tvHashTags;
    @BindView(R.id.appbarLayout)
    AppBarLayout appbarLayout;

    private Unbinder unbinder;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Inject
    SessionManager sessionManager;

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_trending, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        presenter.attachView(this);
        presenter.init();
        isLoading(true);
        swiperefresh.setOnRefreshListener(() -> {
            onResume();
            isLoading(true);
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //    presenter.loadHeader();
    }

    @Override
    public void initContentRecycler() {
        contentAdapter.setPostListner(presenter);
        SpannedGridLayoutManager glm = new SpannedGridLayoutManager(position -> {
            if (position == 1 || position == 18) {
                return new SpannedGridLayoutManager.SpanInfo(2, 2);
            } else {
                return new SpannedGridLayoutManager.SpanInfo(1, 1);
            }
        }, 3 /* Three columns */, 1f /* We want our items to be 1:1 ratio */);
        gvContent.setLayoutManager(glm);
        gvContent.setHasFixedSize(true);
        gvContent.setAdapter(contentAdapter);
        // presenter.loadContent(presenter.getDefaultCategoryId());
    }


    @Override
    public void initHeaderRecycler() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvHeader.setLayoutManager(llm);
        rvHeader.setHasFixedSize(true);
        rvHeader.setAdapter(headerAdapter);
        headerAdapter.setListner(presenter);
        presenter.loadHeader();
    }

    @Override
    public void showHeader(ArrayList<Header> headers) {
        headerAdapter.setData(headers);
    }

    @Override
    public void showContent(ArrayList<Data> trendings) {

    }

    @Override
    public void onPostClick(Data data, View view) {
        Intent intent = new Intent(getContext(), SocialDetailActivity.class);
        intent.putExtra("postId", data.getPostId());
        startActivity(intent);
    }

    @Override
    public void isLoading(boolean flag) {
        swiperefresh.setRefreshing(flag);
    }

    @Override
    public void isContentAvailable(boolean flag) {
        llEmpty.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setHashTags(StringBuilder tags) {
        String tag = tags.toString();
        llHashTags.setVisibility(tag.isEmpty() ? View.GONE : View.VISIBLE);

        SpannableString spanString = new SpannableString(tag);
        Matcher matcher = Pattern.compile("#([A-Za-z0-9_-]+)").matcher(spanString);
        findMatch(spanString, matcher);
        tvHashTags.setText(spanString);
        tvHashTags.setMovementMethod(LinkMovementMethod.getInstance());
        isLoading(false);
    }

    private void findMatch(SpannableString spanString, Matcher matcher) {
        while (matcher.find()) {
            final String tag = matcher.group(0);
            spanString.setSpan(new TagSpannable(getContext(), tag, "trending"), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void reload() {

    }

}
