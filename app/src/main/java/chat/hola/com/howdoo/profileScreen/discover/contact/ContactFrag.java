package chat.hola.com.howdoo.profileScreen.discover.contact;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverActivity;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverPresenter;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.Contact;
import dagger.android.support.DaggerFragment;

/**
 * <h>FollowFrag.class</h>
 * <p>  This fragment shows list of posts , links and following info in
 * a recyclerView which is populated by {@link ContactAdapter}
 * </p>
 *
 * @author 3Embed
 * @since 02/03/18.
 */

public class ContactFrag extends DaggerFragment implements ContactContract.View, SwipeRefreshLayout.OnRefreshListener, ContactAdapter.ClickListner {

    private static final String TAG = "ContactFrag";
    @Inject
    ContactPresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    ContactAdapter contactAdapter;
    @Inject
    DiscoverPresenter discoverPresenter;
    @Inject
    SessionManager sessionManager;

    @BindView(R.id.recyclerContact)
    RecyclerView mRecyclerContact;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvMsg)
    TextView tvMsg;
    @BindView(R.id.btnFollowAll)
    Button btnFollowAll;
    @BindView(R.id.srSwipe)
    SwipeRefreshLayout srSwipe;
    @BindView(R.id.llHeader)
    LinearLayout llHeader;
    @BindView(R.id.tvEmpty)
    TextView tvEmpty;
    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @BindView(R.id.llPermission)
    LinearLayout llPermission;

    private Bus bus = AppController.getBus();
    private Unbinder unbinder;
    private List<String> contactIds = new ArrayList<>();
    private String prefixTitle = "";

    private static final int REQUEST_PERMISSION = 101;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
    }

    @Inject
    public ContactFrag() {
    }

    private DiscoverActivity mActivity;
    private ArrayList<Contact> contactsList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus.register(this);
        prefixTitle = getResources().getString(R.string.FollowFragTitle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_tab, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        presenter.attachView(this);
        return rootView;
    }

    @OnClick(R.id.btnFollowAll)
    public void followAll() {
        srSwipe.setRefreshing(true);
        presenter.followAll(contactIds);
        btnFollowAll.setEnabled(false);
    }

    @Override
    public void initialization() {
        mActivity = (DiscoverActivity) getActivity();
        contactsList = new ArrayList<>();
        srSwipe.setOnRefreshListener(this);
        if (!AppController.getInstance().getContactSynced())
            permission();
        else
            presenter.addContacts();

//        getContact();
    }

    @Override
    public void requestContactsPermission() {
        //  requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_PERMISSION);
    }

    @Override
    public void applyFont() {
        tvTitle.setTypeface(typefaceManager.getSemiboldFont());
        tvMsg.setTypeface(typefaceManager.getMediumFont());
        btnFollowAll.setTypeface(typefaceManager.getSemiboldFont());
        tvEmpty.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void initPostRecycler() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mRecyclerContact.setLayoutManager(llm);
        mRecyclerContact.setHasFixedSize(true);
        mRecyclerContact.setAdapter(contactAdapter);
        contactAdapter.setListener(this);
    }

    @Override
    public void isFollowing(int pos, boolean flag, int status, int isPrivate, boolean isFollowAll) {
//        contactsList.get(pos).setFollowing(status);
//        contactAdapter.notifyDataSetChanged();
        sessionManager.setFollowAll(isFollowAll);
        if (isFollowAll) {
            llHeader.setVisibility(View.GONE);
            btnFollowAll.setEnabled(false);
        } else if (!flag) {
            llHeader.setVisibility(View.VISIBLE);
            btnFollowAll.setEnabled(true);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showContacts(ArrayList<Contact> contacts) {
        llPermission.setVisibility(View.GONE);
        if (contacts == null || contacts.isEmpty()) {
            llEmpty.setVisibility(View.VISIBLE);
            mRecyclerContact.setVisibility(View.GONE);
            llHeader.setVisibility(View.GONE);
        } else {
            llEmpty.setVisibility(View.GONE);
            mRecyclerContact.setVisibility(View.VISIBLE);
            for (Contact contact : contacts)
                contactIds.add(contact.getId());
            tvTitle.setText(contacts.size() + " " + prefixTitle);
            contactsList.clear();
            contactsList.addAll(contacts);
            contactAdapter.setData(contacts);
            contactAdapter.notifyDataSetChanged();
            llHeader.setVisibility(sessionManager.isFollowAll() ? View.GONE : View.VISIBLE);
        }
        srSwipe.setRefreshing(false);
    }

    @Override
    public void loading(boolean flag) {
        srSwipe.setRefreshing(flag);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
        if (unbinder != null)
            unbinder.unbind();
        presenter.detachView();
    }


    @Subscribe
    public void getMessage(JSONObject object) {
        presenter.mqttResponse(object);
    }

    @Override
    public void postToBus() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("eventName", "contactRefreshed");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        bus.post(obj);
    }

    @Override
    public void followedAll(boolean flag) {
        llEmpty.setVisibility(View.GONE);
        sessionManager.setFollowAll(flag);
        if (flag) {
            presenter.fetchContact();
            llHeader.setVisibility(View.GONE);
        } else
            btnFollowAll.setEnabled(true);
        srSwipe.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (AppController.getInstance().getContactSynced())
//            presenter.addContacts();
//        else
        if (AppController.getInstance().getContactSynced() && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED)
            presenter.fetchContact();
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//
//        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
//                //contact
//                // if (AppController.getInstance().canPublish())
////                srSwipe.setRefreshing(true);
//                presenter.fetchContact();
//                // }
//            }
//        } else {
//            // permission was not granted
//            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_CONTACTS)) {
//                showPermissionMessage();
//            } else {
//                gotoSettings();
//            }
//        }
//    }

    private void gotoSettings() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("We need permission to access your contact list, please grant");
        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (getActivity() == null) {
                    return;
                }
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                ContactFrag.this.startActivity(intent);
            }
        });
        builder.show();

        srSwipe.setRefreshing(false);
        llEmpty.setVisibility(View.GONE);
    }

    private void showPermissionMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("We need permission to access your contact list, please grant");
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                presenter.requestContactsPermission();
                Log.i(TAG, "4");
            }
        });
        builder.show();

        srSwipe.setRefreshing(false);
//        llPermission.setVisibility(View.VISIBLE);
        llEmpty.setVisibility(View.GONE);
    }


    @Override
    public void follow(int position, boolean isFollowing) {
        try {
            if (isFollowing)
                presenter.unfollow(position, contactsList.get(position).getId());
            else
                presenter.follow(position, contactsList.get(position).getId());

            discoverPresenter.changeText(getString(R.string.next));

        } catch (Exception e) {

        }
    }

    @Override
    public void onRefresh() {
        permission();
    }

    public void permission() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.READ_CONTACTS)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        srSwipe.setRefreshing(true);
                        presenter.fetchContact();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            gotoSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public void getContact() {
        if (!AppController.getInstance().getContactSynced()) {
            //data not in database
            sessionManager.setFollowAll(false);
            if (AppController.getInstance().canPublish()) {
                if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    // permission already granted
                    srSwipe.setRefreshing(true);
                    presenter.fetchContact();
                } else {
                    // request for permission
                    presenter.requestContactsPermission();
                    Log.i(TAG, "1");
                }
            }
        } else {
            // data is in database, fetch from it
            presenter.addContacts();
        }
    }

    @Override
    public void reload() {

    }
}