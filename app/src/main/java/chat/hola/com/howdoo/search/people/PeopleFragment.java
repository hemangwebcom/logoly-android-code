package chat.hola.com.howdoo.search.people;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.search.SearchAdapter;
import chat.hola.com.howdoo.search.model.SearchData;
import dagger.android.support.DaggerFragment;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class PeopleFragment extends DaggerFragment implements PeopleContract.View, SearchAdapter.ClickListner {

    @Inject
    PeoplePresenter presenter;

    private SearchAdapter madapter;
    private RecyclerView.LayoutManager mlayoutManager;
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;

    @BindView(R.id.howdooSugTv)
    TextView howdooSugTv;
    @BindView(R.id.peopleRv)
    RecyclerView peopleRv;
    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @BindView(R.id.ivEmpty)
    ImageView ivEmpty;
    private Unbinder unbinder;
    private EditText searchInputEt;
    List<SearchData> data = new ArrayList<>();
    private String currentUserId;
    AlertDialog.Builder actionDialog;
    ArrayAdapter<String> actionList;
    private int position = 0;

    public PeopleFragment() {
    }

    @Inject
    SessionManager sessionManager;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_people, container, false);
        unbinder = ButterKnife.bind(this, view);
        searchInputEt = (EditText) getActivity().findViewById(R.id.searchInputEt);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        searchInputEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0)
                    presenter.search(charSequence, 0, PAGE_SIZE);
                else
                    presenter.search(searchInputEt.getText().toString(), 0, PAGE_SIZE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        madapter = new SearchAdapter();
        madapter.setListener(this);
        peopleRv.setHasFixedSize(true);
        mlayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        peopleRv.setLayoutManager(mlayoutManager);
        peopleRv.setItemAnimator(new DefaultItemAnimator());
        peopleRv.setAdapter(madapter);
        peopleRv.addOnScrollListener(recyclerViewOnScrollListener);
        super.onViewCreated(view, savedInstanceState);

        actionDialog = new AlertDialog.Builder(getContext());
        actionDialog.setTitle("Action for this user");
        ArrayList<String> data = new ArrayList<>();
        data.add("Refollow");
        data.add("Block");
        data.add("Add to contact");
        actionList = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, data);
        actionDialog.setAdapter(actionList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        presenter.follow(position, currentUserId);
                        break;
                    case 1:
                        presenter.block(currentUserId);
                        break;
                    case 2:
                        presenter.addToContact(currentUserId);
                        break;
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attachView(this);
        presenter.search(searchInputEt.getText().toString(), 0, PAGE_SIZE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void showData(List<SearchData> data, boolean isFirst) {
        try {
            if (isFirst)
                this.data.clear();
            this.data.addAll(data);
            ivEmpty.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_default_people));
            peopleRv.setVisibility(data.isEmpty() ? View.GONE : View.VISIBLE);
            llEmpty.setVisibility(data.isEmpty() ? View.VISIBLE : View.GONE);
            madapter.setData(getContext(), data);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void noData() {
        peopleRv.setVisibility(View.GONE);
        llEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void blocked() {
        showMessage("User blocked", -1);
    }

    @Override
    public void isFollowing(int pos, int b) {
        data.get(pos).setFollowStatus(b);
        madapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra("userId", data.get(position).getId());
        intent.putExtra("isFollowingMe", data.get(position).isFollowingMe());
        startActivity(intent);
    }

    @Override
    public void onFollow(String userId, boolean follow, int position) {
        if (follow)
            presenter.follow(position, userId);
        else
            presenter.unfollow(position, userId);
    }

    @Override
    public void openActionPopup(int position, String id) {
        currentUserId = id;
        this.position = position;
        actionDialog.show();
    }


    @Override
    public void reload() {

    }

    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = mlayoutManager.getChildCount();
            int totalItemCount = mlayoutManager.getItemCount();
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            presenter.callApiOnScroll(searchInputEt.getText().toString(), firstVisibleItemPosition, visibleItemCount, totalItemCount);
        }
    };
}
