package chat.hola.com.howdoo.club.clubs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 10/16/2018.
 */
public class ClubResponse implements Serializable {
    @SerializedName("clubs")
    @Expose
    private List<Club> clubs = null;

    @SerializedName("data")
    @Expose
    private List<Club> data = null;

    public List<Club> getClubs() {
        return clubs;
    }

    public void setClubs(List<Club> clubs) {
        this.clubs = clubs;
    }

    public List<Club> getData() {
        return data;
    }

    public void setData(List<Club> data) {
        this.data = data;
    }
}
