package chat.hola.com.howdoo.home.connect;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.howdoo.dubly.R;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import chat.hola.com.howdoo.Activities.ChatMessageScreen;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.Activities.ContactsSecretChat;
import chat.hola.com.howdoo.Adapters.ContactsAdapter;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.GroupChat.Activities.CreateGroup;
import chat.hola.com.howdoo.ModelClasses.ContactsItem;
import chat.hola.com.howdoo.Utilities.ApiOnServer;
import chat.hola.com.howdoo.Utilities.MqttEvents;
import chat.hola.com.howdoo.Utilities.RecyclerItemClickListener;
import chat.hola.com.howdoo.Utilities.SortContacts;
import chat.hola.com.howdoo.Utilities.Utilities;

/**
 * Created by moda on 26/07/17.
 */


/*
 *
 * To load the list of the contacts
 */

@SuppressLint("ValidFragment")
public class ContactsFragment extends Fragment implements View.OnClickListener {
    private FloatingActionButton fab;
    private TabLayout tabLayout;


    @SuppressLint("ValidFragment")
    public ContactsFragment(FloatingActionButton fab, TabLayout tabLayout) {
        this.fab = fab;
        this.tabLayout = tabLayout;
    }

    private View view;
    private ContactsAdapter mAdapter;
    private ArrayList<ContactsItem> contactsList = new ArrayList<>();
    private SearchView searchView;
    private TextView messageCall, noMatch;
    private boolean firstTime = true;
    private CoordinatorLayout root;
    private LandingActivity mActivity;
    private ProgressDialog pDialog;
    private ArrayList<Map<String, Object>> localContactsList = new ArrayList<>();
    private ArrayList<String> alreadyAddedContacts = new ArrayList<>();
    private boolean responseCame = false;
    private Typeface fontMedium, fontBold;
    private AppController appController = AppController.getInstance();
    private ImageView backIv;
    private RelativeLayout newSecretChat;
    private RelativeLayout newGroupChat;
    private LinearLayout llEmpty;

    @Override
    @SuppressWarnings("unchecked")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LandingActivity.isConnect = true;
        if (view == null) {
            view = inflater.inflate(R.layout.contact_list, container, false);
        } else {

            if (view.getParent() != null)
                ((ViewGroup) view.getParent()).removeView(view);
        }

        mActivity = (LandingActivity) getActivity();
        mActivity.hideActionBar();
        root = (CoordinatorLayout) view.findViewById(R.id.root);
        fontMedium = appController.getMediumFont();
        fontBold = appController.getSemiboldFont();

        newSecretChat = (RelativeLayout) view.findViewById(R.id.requestSecretChat);

        newGroupChat = (RelativeLayout) view.findViewById(R.id.requestGroupChat);

        pDialog = new ProgressDialog(mActivity);
        pDialog.setMessage(getString(R.string.Sync_Contacts));
        pDialog.setCancelable(false);
        if (!AppController.getInstance().getContactSynced()) {

            if (AppController.getInstance().canPublish()) {
                if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {
                    new syncContacts().execute();
                } else {

                    requestContactsPermission(1);
                }
            } else {
                try {
                    if (root != null) {
                        Snackbar snackbar = Snackbar.make(root, getString(R.string.No_Internet_Connection_Available), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        View view = snackbar.getView();
                        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        final RelativeLayout backButton = (RelativeLayout) view.findViewById(R.id.close_rl);

        //backButton.setVisibility(View.GONE);
        RelativeLayout addContacts = (RelativeLayout) view.findViewById(R.id.delete_rl);

        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        //uncomment below
//        mAdapter = new ContactsAdapter(getActivity(), contactsList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());

//        StickyLayoutManager layoutManager = new TopSnappedStickyLayoutManager(getActivity(), mAdapter);
//        layoutManager.elevateHeaders(true); // Default elevation of 5dp
//        // You can also specify a specific dp for elevation
////        layoutManager.elevateHeaders(10);
//        rv.setLayoutManager(layoutManager);
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(mAdapter);
        messageCall = (TextView) view.findViewById(R.id.userMessagechat);
        llEmpty = (LinearLayout) view.findViewById(R.id.llEmpty);
        noMatch = (TextView) view.findViewById(R.id.noMatch);
        backIv = (ImageView) view.findViewById(R.id.backIv);

        backIv.setOnClickListener(this);


        /*
         * Api calls to fetch the list of the calls from the server
         */


        //makeStringReq();
        final TextView title = (TextView) view.findViewById(R.id.title);
        searchView = (SearchView) view.findViewById(R.id.search);
        searchView.setIconified(true);
        //searchView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_white));
        searchView.setIconifiedByDefault(true);
        searchView.clearFocus();
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    //backButton.setVisibility(View.INVISIBLE);
                    title.setVisibility(View.GONE);
                    backButton.setVisibility(View.GONE);
                    searchView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_white));
                } else {

                    //title.setVisibility(View.VISIBLE);

                }
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);

                return false;
            }
        });


        /*
         * Dont   need it anymore in the present logic
         */

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        noMatch.setVisibility(View.GONE);
                        if (contactsList.size() == 0) {
                            llEmpty.setVisibility(View.VISIBLE);
                        }

                        backButton.setVisibility(View.VISIBLE);
                        title.setVisibility(View.VISIBLE);
                        searchView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorHintOfRed));
                    }
                });


                return false;
            }
        });

        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*
         * Open activity to add the contacts
         */
        addContacts.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    startActivity(intent);


                } else {


                    requestContactsPermission(0);
                }


            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getFragmentManager().getBackStackEntryCount() > 0) {
                    getFragmentManager().popBackStack();
                } else {
                    mActivity.onBackPressed();
                }
                // getActivity().onBackPressed();
            }
        });

        newSecretChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(), ContactsSecretChat.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);


            }
        });


        newGroupChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(), CreateGroup.class);


                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);


            }
        });


        RelativeLayout refresh = (RelativeLayout) view.findViewById(R.id.refresh_rl);


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                AppController.getInstance().setChatSynced(false);


                if (AppController.getInstance().canPublish()) {
                    if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_CONTACTS)
                            == PackageManager.PERMISSION_GRANTED) {
                        new syncContacts().execute();
                    } else {

                        requestContactsPermission(1);
                    }
                } else {

                    try {
                        if (root != null) {
                            Snackbar snackbar = Snackbar.make(root, getString(R.string.No_Internet_Connection_Available), Snackbar.LENGTH_SHORT);


                            snackbar.show();
                            View view = snackbar.getView();
                            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rv, new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {

                if (position >= 0) {
                    final ContactsItem item = (ContactsItem) mAdapter.getList().get(position);


//                    Intent intent = new Intent(view.getContext(), ChatMessagesScreen.class);

                    Intent intent = new Intent(view.getContext(), ChatMessageScreen.class);
                    intent.putExtra("receiverUid", item.getContactUid());
                    intent.putExtra("receiverName", item.getContactName());

                    String docId = AppController.getInstance().findDocumentIdOfReceiver(item.getContactUid(), "");

                    if (docId.isEmpty()) {
                        docId = AppController.findDocumentIdOfReceiver(item.getContactUid(), Utilities.tsInGmt(), item.getContactName(),
                                item.getContactImage(), "", false, item.getContactIdentifier(), "", false);
                    }

                    intent.putExtra("documentId", docId);
                    intent.putExtra("receiverIdentifier", item.getContactIdentifier());
                    intent.putExtra("receiverImage", item.getContactImage());
                    intent.putExtra("colorCode", AppController.getInstance().getColorCode(position % 19));
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                    startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));


        Typeface tf = AppController.getInstance().getRegularFont();
        title.setTypeface(tf, Typeface.BOLD);

//        MQttResponseHandler handler = new MQttResponseHandler(getActivity()) {
//
//            @Override
//            public void execute(String event, JSONObject jsonObject) throws JSONException {
//
//
//                if (event.equals(MqttEvents.ContactSync.value + "/" + AppController.getInstance().getUserId())) {
//                    responseCame = true;
//                    saveContactsFromApiResponse(jsonObject);
//
//                    if (!AppController.getInstance().getChatSynced()) {
//                        JSONObject obj = new JSONObject();
//
//                        obj.put("eventName", "SyncChats");
//                        bus.post(obj);
//                    }
//                    AppController.getInstance().setContactSynced(true);
//
//                    AppController.getInstance().registerContactsObserver();
//
//                }
//
//
//            }
//        };


//        AppController.initMQttContactSyncHandlerInstance(handler);


        TextView tvSecret = (TextView) view.findViewById(R.id.tvSecret);


        tvSecret.setTypeface(tf, Typeface.NORMAL);


        TextView tvGroup = (TextView) view.findViewById(R.id.tvGroup);
        TextView tvContact = (TextView) view.findViewById(R.id.tvContact);

        tvGroup.setTypeface(fontMedium);
        tvContact.setTypeface(fontBold);
        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            if (getActivity() != null) {
                hideKeyboard(getActivity());
            }
        } else if (searchView != null) {

            if (!searchView.isIconified()) {

                searchView.setIconified(true);
                if (getActivity() != null) {
                    addContacts();
                }
            }

        }


    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    public void showNoSearchResults(final CharSequence constraint, boolean flag) {

        try {
            if (flag) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        noMatch.setVisibility(View.GONE);


                        if (contactsList.size() == 0) {


                            llEmpty.setVisibility(View.VISIBLE);
                        } else {

                            llEmpty.setVisibility(View.GONE);
                        }
                    }
                });
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (mAdapter.getList().size() == 0) {
                            if (noMatch != null) {


                                noMatch.setVisibility(View.VISIBLE);

                                noMatch.setText(getString(R.string.noMatch) + " " + constraint);
                                llEmpty.setVisibility(View.GONE);

                            }
                        } else {
                            noMatch.setVisibility(View.GONE);


                        }
                    }
                });
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void requestContactsPermission(int k) {

        if (k == 0) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                    Manifest.permission.WRITE_CONTACTS)) {


                Snackbar snackbar = Snackbar.make(root, R.string.string_66,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS},
                                0);
                    }
                });


                snackbar.show();


                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);

            } else

            {

                requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS},
                        0);
            }


        } else {


            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.READ_CONTACTS)) {

                try {
                    Snackbar snackbar = Snackbar.make(root, R.string.string_756,
                            Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                                    1);
                        }
                    });


                    snackbar.show();


                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }


            } else

            {

                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                        1);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    startActivity(intent);


                } else {


                    Snackbar snackbar = Snackbar.make(root, R.string.string_60,
                            Snackbar.LENGTH_SHORT);


                    snackbar.show();
                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);

                }
            } else {


                Snackbar snackbar = Snackbar.make(root, R.string.string_60,
                        Snackbar.LENGTH_SHORT);


                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);

            }
        } else if (requestCode == 1) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (AppController.getInstance().canPublish()) {
                        new syncContacts().execute();
                    } else {

                        try {
                            if (root != null) {
                                Snackbar snackbar = Snackbar.make(root, getString(R.string.No_Internet_Connection_Available), Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {


                    Snackbar snackbar = Snackbar.make(root, R.string.string_60,
                            Snackbar.LENGTH_SHORT);


                    snackbar.show();
                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);

                    //   requestContactsPermission(1);

                }
            } else {


                Snackbar snackbar = Snackbar.make(root, R.string.string_60,
                        Snackbar.LENGTH_SHORT);


                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);

                //  requestContactsPermission(1);
            }
        }

    }


    private void addContacts() {

        if (mActivity != null) {
            contactsList.clear();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    mAdapter.notifyDataSetChanged();
                }
            });


            ArrayList<Map<String, Object>> contacts = AppController.getInstance().getDbController().loadContacts(AppController.getInstance().getContactsDocId());

            ContactsItem contact;
            Map<String, Object> contactIthPosition;


            if (contactsList != null) {

                for (int i = 0; i < contacts.size(); i++) {


                    contact = new ContactsItem();


                    contactIthPosition = contacts.get(i);
//                    if (!contactIthPosition.containsKey("blocked")) {
//contact.setItemType(0);
                    contact.setContactIdentifier((String) contactIthPosition.get("contactIdentifier"));

                    contact.setContactImage((String) contactIthPosition.get("contactPicUrl"));
                    contact.setContactName((String) contactIthPosition.get("contactName"));
                    contact.setContactStatus((String) contactIthPosition.get("contactStatus"));
                    contact.setContactUid((String) contactIthPosition.get("contactUid"));
                    contactsList.add(contact);

//                    }
                }


                if (mActivity != null) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                        }
                    });

                }

                if (contactsList.size() == 0) {


                    llEmpty.setVisibility(View.VISIBLE);

                } else if (contactsList.size() > 0) {


                    llEmpty.setVisibility(View.GONE);
                    Collections.sort(contactsList, new SortContacts());


                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onResume() {


        super.onResume();
        if (mAdapter != null) {

            if (firstTime) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (getActivity() != null)
                            addContacts();
                    }
                }, 500);

                firstTime = false;
            } else {
                if (getActivity() != null)
                    addContacts();
            }

            if (contactsList != null && contactsList.size() > 0) {

                if (llEmpty != null) {


                    llEmpty.setVisibility(View.GONE);


                } else {


                    TextView messageCall = (TextView) view.findViewById(R.id.userMessagechat);
                    llEmpty.setVisibility(View.GONE);


                }
            }


        }
    }


    @SuppressWarnings("TryWithIdenticalCatches,unchecked")
    private void saveContactsFromApiResponse(JSONObject jsonObject) {

        AppController.getInstance().unsubscribeToTopic(MqttEvents.ContactSync.value + "/" + AppController.getInstance().getUserId());


        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                contactsList.clear();


                mAdapter.notifyDataSetChanged();
            }
        });


        try {
            JSONArray array = jsonObject.getJSONArray("contacts");

            JSONObject obj;
            ContactsItem item;


            ArrayList<Map<String, Object>> contacts = new ArrayList<>();


            Map<String, Object> contact;
            String contactUid, contactName, contactStatus, contactProfilePic, contactIdentifier, localContactNumber;
            int followStatus, _private;

            for (int i = 0; i < array.length(); i++) {

                obj = array.getJSONObject(i);

                localContactNumber = obj.getString("localNumber");
                followStatus = obj.getInt("followStatus");
                _private = obj.getInt("private");

                Map<String, Object> localContactInfo = getLocalContactIdAndName(localContactNumber);


                // contactName = (String) localContactInfo.get("userName");
                contactName = obj.getString("userName");
                contactUid = obj.getString("_id");
                contactProfilePic = "";


                if (obj.has("profilePic")) {
                    contactProfilePic = obj.getString("profilePic");
                }
                contactIdentifier = obj.getString("number");

                if (obj.has("socialStatus")) {

                    contactStatus = obj.getString("socialStatus");

                } else {
                    contactStatus = getString(R.string.default_status);
                }

                item = new ContactsItem();

                item.setContactStatus(contactStatus);
                item.setContactUid(contactUid);

                item.setContactName(contactName);
                item.setContactImage(contactProfilePic);
//                item.setItemType(0);
                item.setContactIdentifier(contactIdentifier);
                item.set_private(_private);
                item.setFollowStatus(followStatus);

                contactsList.add(item);


                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        mAdapter.notifyItemInserted(contactsList.size() - 1);


                    }
                });

                contact = new HashMap<>();

                contact.put("contactLocalNumber", localContactNumber);
                contact.put("contactLocalId", localContactInfo.get("contactId"));
                contact.put("contactUid", contactUid);


                contact.put("contactType", localContactInfo.get("type"));


                contact.put("contactName", contactName);
                contact.put("contactStatus", contactStatus);
                contact.put("contactPicUrl", contactProfilePic);
                contact.put("contactIdentifier", contactIdentifier);
                contact.put("followStatus", followStatus);
                contact.put("private", _private);
                contacts.add(contact);
            }


            if (contactsList != null && contactsList.size() > 0) {

                if (llEmpty != null) {


                    llEmpty.setVisibility(View.GONE);


                } else {


                    TextView messageCall = (TextView) view.findViewById(R.id.userMessagechat);
                    llEmpty.setVisibility(View.GONE);


                }
            }
            Collections.sort(contactsList, new SortContacts());
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    mAdapter.notifyDataSetChanged();
                }
            });
            AppController.getInstance().getDbController().insertContactsInfo(contacts, AppController.getInstance().getContactsDocId());
            AppController.getInstance().getDbController().updateAllContacts(localContactsList, AppController.getInstance().getAllContactsDocId());


        } catch (JSONException e) {
            e.printStackTrace();
        }




        /*
         * To hit the API to send the contacts
         */
        if (pDialog != null && pDialog.isShowing()) {


            Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();
            if (context instanceof Activity) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                        pDialog.dismiss();
                    }
                } else {
                    if (!((Activity) context).isFinishing()) {
                        pDialog.dismiss();
                    }
                }
            } else {
                try {
                    pDialog.dismiss();
                } catch (final IllegalArgumentException e) {
                    e.printStackTrace();

                } catch (final Exception e) {
                    e.printStackTrace();

                }
            }
        }


        JSONObject obj = new JSONObject();
        try {
            obj.put("eventName", "contactRefreshed");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        bus.post(obj);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.backIv) {
            //  fab.setVisibility(View.VISIBLE);
            FragmentManager fm = getFragmentManager();
            // FragmentManager fm = getChildFragmentManager();

            fm.popBackStack();
            //fm.
            //   getActivity().finish();
            // getFragmentManager().popBackStack();

            mActivity.visibleActionBar();
            tabLayout.setVisibility(View.VISIBLE);
//            Log.d("exe", "back of contact fragment ");
        }
    }

//    private void syncContacts() {
//    }


    /*
     * Have to filter out the contacts,to remove the spaces
     */


    /**
     * FIXED_LINE = 0,
     * MOBILE = 1,
     * FIXED_LINE_OR_MOBILE = 2,
     * TOLL_FREE = 3,
     * PREMIUM_RATE = 4,
     * SHARED_COST = 5,
     * VOIP = 6,
     * PERSONAL_NUMBER = 7,
     * PAGER = 8,
     * UAN = 9,
     * VOICEMAIL = 10,
     * UNKNOWN = -1
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    private class syncContacts extends AsyncTask {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            if (pDialog != null) {
                pDialog.show();
                ProgressBar bar = (ProgressBar) pDialog.findViewById(android.R.id.progress);


                bar.getIndeterminateDrawable().setColorFilter(
                        ContextCompat.getColor(mActivity, R.color.color_black),
                        android.graphics.PorterDuff.Mode.SRC_IN);

            }

        }

        @Override

        protected Object doInBackground(Object[] params) {

            /*
             * To fetch the list of the contacts
             */

            alreadyAddedContacts.clear();

            responseCame = false;
            localContactsList.clear();
            JSONArray arr = new JSONArray();


            ContentResolver cr = mActivity.getContentResolver();

            /*
             * To get all the contacts
             */
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

            if (cur != null && cur.getCount() > 0) {


                JSONObject obj;
                HashMap<String, Object> contactsInfo;


                int type;

                while (cur.moveToNext()) {


                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));


                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);

                        if (pCur != null) {


                            while (pCur.moveToNext()) {


                                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                /*
                                 * To replace brackets or the dashes or the spaces
                                 *
                                 * */

                                phoneNo = phoneNo.replaceAll("[\\D\\s\\-()]", "");

                                /*
                                 * To get all the phone numbers for the given contacts
                                 */
                                if (phoneNo != null && !phoneNo.trim().equals("null") && !phoneNo.trim().isEmpty()) {


                                    /*
                                     * By default assuming the number is of unknown type
                                     */

                                    type = -1;
                                    switch (pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))) {
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                            type = 1;
                                            break;
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                            type = 0;
                                            break;
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
                                            type = 8;
                                            //              break;
//                                        case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
//
//                                            break;
//                                        default:
//                                            break;
                                    }
                                    //localNumberAndNames.put(phoneNo, name);

                                    if (!alreadyAddedContacts.contains(phoneNo)) {

                                        alreadyAddedContacts.add(phoneNo);
                                        contactsInfo = new HashMap<>();

                                        contactsInfo.put("phoneNumber", "+" + phoneNo);
                                        contactsInfo.put("userName", name);
                                        contactsInfo.put("type", type);
                                        contactsInfo.put("contactId", id);


                                        localContactsList.add(contactsInfo);
                                        obj = new JSONObject();
                                        try {

                                            obj.put("number", "+" + phoneNo);

                                            obj.put("type", type);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        arr.put(obj);
                                    }
                                }

                            }

                            pCur.close();
                        }
                    }
                }
            }

            if (cur != null) {
                cur.close();
            }


            /*
             * To add the contacts array
             */

            if (arr.length() > 0) {
                try {
                    JSONObject obj = new JSONObject();


                    obj.put("contacts", arr);


                    hitContactSyncApi(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {


                if (pDialog != null && pDialog.isShowing()) {


                    Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();


                    if (context instanceof Activity) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                pDialog.dismiss();
                            }
                        } else {


                            if (!((Activity) context).isFinishing()) {
                                pDialog.dismiss();
                            }
                        }
                    } else {


                        try {
                            pDialog.dismiss();
                        } catch (final IllegalArgumentException e) {
                            e.printStackTrace();

                        } catch (final Exception e) {
                            e.printStackTrace();

                        }
                    }
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Snackbar snackbar = Snackbar.make(root, R.string.No_Sync_Contacts,
                                Snackbar.LENGTH_SHORT);


                        snackbar.show();
                        View view = snackbar.getView();
                        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                });

            }
            return null;
        }
    }


    @SuppressWarnings("TryWithIdenticalCatches")
    private void hitContactSyncApi(JSONObject obj) {


        AppController.getInstance().subscribeToTopic(MqttEvents.ContactSync.value + "/" + AppController.getInstance().getUserId(), 0);

        /*
         * For auto cancel of progress dialog if no response came for 20 seconds
         */
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                if (!responseCame) {
//                    if (pDialog != null && pDialog.isShowing()) {
//
//
//                        Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();
//
//
//                        if (context instanceof Activity) {
//
//
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
//                                    pDialog.dismiss();
//                                }
//                            } else {
//
//
//                                if (!((Activity) context).isFinishing()) {
//                                    pDialog.dismiss();
//                                }
//                            }
//                        } else {
//
//
//                            try {
//                                pDialog.dismiss();
//                            } catch (final IllegalArgumentException e) {
//                                e.printStackTrace();
//
//                            } catch (final Exception e) {
//                                e.printStackTrace();
//
//                            }
//                        }
//                    }
//                }
//
//            }
//        }, 20000);


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiOnServer.SYNC_CONTACTS, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.i("MQTT", "onResponse: ");
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("MQTT", "error: ");

                if (pDialog != null && pDialog.isShowing()) {


                    Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();


                    if (context instanceof Activity) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                                pDialog.dismiss();
                            }
                        } else {


                            if (!((Activity) context).isFinishing()) {
                                pDialog.dismiss();
                            }
                        }
                    } else {


                        try {
                            pDialog.dismiss();
                        } catch (final IllegalArgumentException e) {
                            e.printStackTrace();

                        } catch (final Exception e) {
                            e.printStackTrace();

                        }
                    }
                }
            }
        }


        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");


                headers.put("token", AppController.getInstance().getApiToken());

                return headers;
            }
        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        /* Add the request to the RequestQueue.*/
        AppController.getInstance().addToRequestQueue(jsonObjReq, "syncContactsApiRequest");

    }


    private Map<String, Object> getLocalContactIdAndName(String localContactNumber) {

        Map<String, Object> map = new HashMap<>();
        for (int i = 0; i < localContactsList.size(); i++) {

            map = localContactsList.get(i);


            if (map.get("phoneNumber").equals(localContactNumber)) {


                return map;
            }
        }
        return map;
    }

    private static Bus bus = AppController.getBus();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        bus.register(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        bus.unregister(this);
    }

    @Subscribe
    public void getMessage(JSONObject object) {
        try {


            if (object.getString("eventName")
                    .equals(MqttEvents.UserUpdates.value + "/" + AppController.getInstance().getUserId())) {


                switch (object.getInt("type")) {


                    case 1:


                        /*
                         * Status update by any of the contact
                         */



                        /*
                         * To update in the contacts list
                         */

                        final int pos = findContactPositionInList(object.getString("userId"));
                        if (pos != -1) {


                            ContactsItem item = contactsList.get(pos);


                            item.setContactStatus(object.getString("socialStatus"));

                            contactsList.set(pos, item);


                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemChanged(pos);
                                }
                            });
                        }


                        break;

                    case 2:
                        /*
                         * Profile pic update
                         */


                        /*
                         * To update in the contacts list
                         */


                        final int pos1 = findContactPositionInList(object.getString("userId"));
                        if (pos1 != -1) {

                            /*
                             * Have to clear the glide cache
                             */
//                            Glide.get(getActivity()).clearDiskCache();
//
//                            Glide.get(getActivity()).clearMemory();
//                            Glide.get(getActivity()).getBitmapPool().clearMemory();


                            ContactsItem item = contactsList.get(pos1);

                            item.setContactImage(object.getString("profilePic"));
                            contactsList.set(pos1, item);


                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemChanged(pos1);
                                }
                            });
                        }


                        break;
                    case 3:
                        /*
                         * Any of mine previous phone contact join
                         */

                        ContactsItem item2 = new ContactsItem();
                        item2.setContactImage("");
                        item2.setContactUid(object.getString("userId"));


                        item2.setContactStatus(getString(R.string.default_status));

                        item2.setContactIdentifier(object.getString("number"));


                        item2.setContactName(object.getString("name"));
                        final int position = findContactPositionInList(object.getString("userId"));
                        if (position == -1) {

                            //  item2.setItemType(0);
                            contactsList.add(item2);


                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemInserted(contactsList.size() - 1);


                                }
                            });


                        } else {
                            contactsList.set(position, item2);


                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemChanged(position);
                                }
                            });

                        }
                        break;

                    case 4:
                        /*
                         * New contact added request sent,for the response of the PUT contact api
                         */


                        switch (object.getInt("subtype")) {

                            case 0:

                                /*
                                 * Follow name or number changed but number still valid
                                 */


                                final int pos3 = findContactPositionInList(object.getString("contactUid"));


                                if (pos3 != -1) {


                                    ContactsItem item = contactsList.get(pos3);


                                    item.setContactName(object.getString("contactName"));
                                    contactsList.set(pos3, item);

                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemChanged(pos3);
                                        }
                                    });


                                }

                                break;
                            case 1:
                                /*
                                 * Number of active contact changed and new number not in contact
                                 */

                                final int pos4 = findContactPositionInList(object.getString("contactUid"));


                                if (pos4 != -1) {


                                    contactsList.remove(pos4);


                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemChanged(pos4);
                                        }
                                    });
                                }

                                break;

                            case 2:

                                /*
                                 * New contact added
                                 */


                                try {

                                    String contactUid = object.getString("contactUid");


                                    ContactsItem item = new ContactsItem();

                                    item.setContactImage(object.getString("contactPicUrl"));
                                    item.setContactName(object.getString("contactName"));
                                    item.setContactStatus(object.getString("contactStatus"));
                                    item.setContactIdentifier(object.getString("contactIdentifier"));
                                    item.setContactUid(object.getString("contactUid"));
                                    //  item.setItemType(0);
                                    boolean contactAlreadyInList = false;


                                    for (int i = 0; i < contactsList.size(); i++) {


                                        if (contactsList.get(i).getContactUid().equals(contactUid)) {


                                            contactAlreadyInList = true;

                                            break;
                                        }


                                    }
                                    if (!contactAlreadyInList) {
                                        contactsList.add(item);


                                        mActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mAdapter.notifyItemInserted(contactsList.size() - 1);


                                            }
                                        });
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                        }


                        break;

                    case 5:
                        /*
                         * Follow deleted request sent,for the response of the DELETE contact api
                         */


                        /*
                         * Number was in active contact
                         */
                        if (object.has("status") && object.getInt("status") == 0) {

                            final int pos2 = findContactPositionInList(object.getString("userId"));

                            if (pos2 != -1) {


                                contactsList.remove(pos2);


                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        mAdapter.notifyItemChanged(pos2);
                                        //  mAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        }
                        break;


                }


            } else if (object.getString("eventName").equals("ContactNameUpdated")) {

                final int pos = findContactPositionInList(object.getString("contactUid"));


                ContactsItem item = contactsList.get(pos);


                item.setContactName(object.getString("contactName"));
                contactsList.set(pos, item);


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });

            } else if (object.getString("eventName").equals(MqttEvents.ContactSync.value + "/" + AppController.getInstance().getUserId())) {
                responseCame = true;
                saveContactsFromApiResponse(object);

                AppController.getInstance().setContactSynced(true);

                AppController.getInstance().registerContactsObserver();


                if (!AppController.getInstance().getChatSynced()) {


                    JSONObject obj = new JSONObject();

                    obj.put("eventName", "SyncChats");
                    bus.post(obj);


                    obj = new JSONObject();

                    obj.put("eventName", "SyncCallLogs");
                    bus.post(obj);

                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int findContactPositionInList(String contactUid) {
        int pos = -1;
        for (int i = 0; i < contactsList.size(); i++) {
            if (contactsList.get(i).getContactUid().equals(contactUid)) {
                return i;
            }
        }
        return pos;

    }

    @Override
    public void onDetach() {
        super.onDetach();
//        Log.d("exe", "destroy the present fragment");
        tabLayout.setVisibility(View.VISIBLE);
        //tabLayout.setVisibility(View.GONE);

        newSecretChat.setVisibility(View.GONE);
        fab.setVisibility(View.VISIBLE);
    }
}