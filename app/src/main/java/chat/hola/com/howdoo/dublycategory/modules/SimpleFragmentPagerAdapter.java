package chat.hola.com.howdoo.dublycategory.modules;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.howdoo.dubly.R;

import chat.hola.com.howdoo.dublycategory.fagments.DubFragment;
import chat.hola.com.howdoo.dublycategory.favourite.DubFavFragment;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 7/19/2018.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private int position;

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        DubFragment dubFragment = new DubFragment();
        dubFragment.getPosition(position);

        DubFavFragment dubFavouriteFragment = new DubFavFragment();
        dubFavouriteFragment.getPosition(position);

        switch (position) {
            case 0:
                return dubFragment;
            case 1:
                return dubFavouriteFragment;
            default:
                return dubFragment;
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.tab_hot_songs);
            case 1:
                return mContext.getString(R.string.tab_my_favs);
            default:
                return null;
        }
    }

    public void setPosition(int position) {
        this.position = position;
        getItem(position);
    }
}
