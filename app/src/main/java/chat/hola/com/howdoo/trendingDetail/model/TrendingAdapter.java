package chat.hola.com.howdoo.trendingDetail.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.trendingDetail.TrendingDetail;

/**
 * <h>TrendingDtlAdapter</h>
 * <p> This Adapter is used by {@link TrendingDetail} activity recyclerView.</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 14/2/18.
 */

public class TrendingAdapter extends RecyclerView.Adapter<ViewHolder> {

    private Context context;
    private List<Data> data = new ArrayList<>();
    private ClickListner clickListner;

    @Inject
    public TrendingAdapter(Context context, List<Data> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_detail_item, parent, false), clickListner);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        initHolder(viewHolder);
    }

    private void initHolder(ViewHolder holder) {
        int position = holder.getAdapterPosition();
        Data data = this.data.get(position);
        if (data != null) {
            boolean isVideo = data.getMediaType1() != null && data.getMediaType1() == 1;
            holder.ibPlay.setVisibility(isVideo ? View.VISIBLE : View.GONE);
            Glide.with(context).load(data.getImageUrl1().replace("mp4", "jpg")).asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.ivImage));
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }
}