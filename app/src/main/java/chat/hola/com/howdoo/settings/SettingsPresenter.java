package chat.hola.com.howdoo.settings;

import android.content.Intent;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.howdoo.dubly.R;

import javax.inject.Inject;

/**
 * <h1>SettingsPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 24/2/18.
 */

public class SettingsPresenter implements SettingsContract.Presenter {

    @Inject
    SettingsContract.View mView;

    @Inject
    SettingsPresenter() {

    }

    @Override
    public void init() {
        mView.applyFont();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data, int RESULT_OK) {
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
            } else {
                mView.showMessage(null,R.string.inviteFailedMsg);
            }
        }
    }
}
