package chat.hola.com.howdoo.camera;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.howdoo.dubly.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;
import com.yalantis.ucrop.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.ImageFilePath;
import chat.hola.com.howdoo.Utilities.UriUtil;
import chat.hola.com.howdoo.camera.filter.BitmapUtils;
import chat.hola.com.howdoo.cameraActivities.manager.CameraOutputModel;
import chat.hola.com.howdoo.dublycategory.DubCategoryActivity;
import cn.lankton.flowlayout.FlowLayout;

public class CameraActivity extends AppCompatActivity implements CameraKitEventListener,
        CameraControls.OptionClickListner, CameraControls.VideoListner {
    private final int CAMERA_REQUEST = 222;
    private final int READ_STORAGE_REQ_CODE = 26;
    private final int RESULT_LOAD_IMAGE = 1;
    @BindView(R.id.contentFrame)
    RelativeLayout parent;


    @BindView(R.id.camera)
    CameraView camera;
    @BindView(R.id.cameraControls)
    CameraControls cameraControls;
    @BindView(R.id.selctsound)
    LinearLayout selectSound;
    @BindView(R.id.tvselectsound)
    TextView tvselectsound;
    long callbackTime;
    CountDownTimer mCountDownTimer;
    File videoMerge;
    private FlowLayout flowLayout;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    FFmpeg ffmpeg;
    String[] complexCommand;
    File f;
    File audioTrim;
    File videoFile;
    @BindView(R.id.tick)
    ImageView tick;
    @BindView(R.id.cross)
    ImageView cross;


    @BindView(R.id.iv_gallery)
    ImageView ibGallery;

    public int speed = 3;
    public int flag = 0;
    ArrayList<String> videoPaths = new ArrayList<String>();
    File dest;
    File videoSpeed;
    @BindView(R.id.tvRatio_2X)
    TextView tvRatio_2X;
    @BindView(R.id.tvRatio_1X)
    TextView tvRatio_1X;
    @BindView(R.id.tvRatio_01x)
    TextView tvRatio0_1X;
    @BindView(R.id.tvRatio_05X)
    TextView tvRatio0_5X;
    @BindView(R.id.tvRatio_3X)
    TextView tvRatio_3X;
    float speedf = 1.0f;
    int length = 0;

    private ArrayList<Integer> a = new ArrayList<>();
    private ArrayList<Integer> b = new ArrayList<>();
    private ArrayList<Integer> c = new ArrayList<>();

    int count = 0, countfirst = 0, init = 0;

    private MediaPlayer player;
    private int cameraMethod = CameraKit.Constants.METHOD_STANDARD;
    private boolean cropOutput = true;
    private long captureStartTime;
    private boolean isRecording = false;
    String call;
    private String audioFile = "";
    private String audio_name = "";
    private String musicId = "", type = "";
    private boolean isFrontFace = false;
    private int millSecond = 0;
    private long milli = 0;
    /*    @BindView(R.id.progresbar)
        SegmentedProgressBar mProgressBar;*/
    int prog = 0;
    int height, widthScreen;
    private ProgressDialog dialog;


    private int rawHeight;
    private View tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_main);
        a = new ArrayList<>();
        b = new ArrayList<>();
        ButterKnife.bind(this);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        flowLayout = (FlowLayout) findViewById(R.id.flowlayout);
        dialog = new ProgressDialog(this);
        height = displayMetrics.heightPixels;
        widthScreen = displayMetrics.widthPixels;
        rawHeight = dip2px(this, 10);
        //Mapper mapper = new Mapper.Mapper1();
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        tvselectsound.setTypeface(AppController.getInstance().getSemiboldFont());
        call = getIntent().getStringExtra("call");
        audioFile = getIntent().getStringExtra("audio");
        audio_name = getIntent().getStringExtra("name");
        type = getIntent().getStringExtra("type");
        musicId = getIntent().getStringExtra("musicId");
        if (audioFile != null) {
            Uri uri = Uri.parse(audioFile);
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(getApplicationContext(), uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            milli = Long.parseLong(durationStr);
            millSecond = Integer.parseInt(durationStr) / 1000;

            cameraControls.setDuration(millSecond);

            Log.d("duration", "onCreate: " + millSecond + "" + audioFile);
        }
        isFrontFace = true;
        camera.setMethod(cameraMethod);
        camera.setCropOutput(cropOutput);
        camera.addCameraKitListener(this);
        camera.toggleFlash();
        cameraControls.setOptionClickListner(this, millSecond);
        cameraControls.setListner(this);


        if (audio_name != null) {
            tvselectsound.setText(audio_name.replace(".aac", ""));
            tvselectsound.setSelected(true);
            setMicMuted(true);
        }

        if (call == null)
            call = "post";
        if (call.equals("SaveProfile"))
            selectSound.setVisibility(View.GONE);

        //cameraControls.isDubly(false);

        tvRatio0_1X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speed = 1;
                tvRatio0_1X.setBackgroundColor(getResources().getColor(R.color.message_select));
                tvRatio0_5X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_2X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_3X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_1X.setBackgroundColor(getResources().getColor(R.color.gray));

            }
        });

        tvRatio0_5X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speed = 2;
                tvRatio0_5X.setBackgroundColor(getResources().getColor(R.color.message_select));
                tvRatio_1X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_2X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_3X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio0_1X.setBackgroundColor(getResources().getColor(R.color.gray));
            }
        });
        tvRatio_1X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speed = 3;
                tvRatio_1X.setBackgroundColor(getResources().getColor(R.color.message_select));
                tvRatio0_5X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_2X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_3X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio0_1X.setBackgroundColor(getResources().getColor(R.color.gray));
            }
        });

        tvRatio_2X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speed = 4;
                tvRatio_2X.setBackgroundColor(getResources().getColor(R.color.message_select));
                tvRatio_1X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio0_5X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_3X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio0_1X.setBackgroundColor(getResources().getColor(R.color.gray));
            }
        });
        tvRatio_3X.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speed = 5;
                tvRatio_3X.setBackgroundColor(getResources().getColor(R.color.message_select));
                tvRatio_1X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio_2X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio0_5X.setBackgroundColor(getResources().getColor(R.color.gray));
                tvRatio0_1X.setBackgroundColor(getResources().getColor(R.color.gray));
            }
        });


        loadFFMpegBinary();


    }


    @OnClick(R.id.iv_gallery)
    public void openGallery() {
        if (ActivityCompat.checkSelfPermission(CameraActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CameraActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            launchImagePicker();
        } else {
            requestReadImagePermission();
        }
    }


    @OnClick(R.id.selctsound)
    public void selectSound() {
        startActivity(new Intent(this, DubCategoryActivity.class));
    }

    @OnClick(R.id.back_button)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (flowLayout.getChildCount() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Tips");
            builder.setMessage("Discard current sound and recording progress?");
            builder.setPositiveButton("confirm", (dialogInterface, i) -> onBackPressed());
            builder.setNegativeButton("No", (dialogInterface, i) -> {
            });
            builder.show();
        } else {
            super.onBackPressed();
        }
    }

    //load FFmpeg
    private void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {
                Log.d("ffmpeg", "ffmpeg : null");
                ffmpeg = FFmpeg.getInstance(this);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    //showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    Log.d("Success", "ffmpeg : correct Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            //showUnsupportedExceptionDialog();
        } catch (Exception e) {
            Log.d("Exception", "EXception not supported : " + e);
        }
    }


    ////concat Video

    private void
    execFFmpegBinary(final String[] command, final File file, int timer) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {

                    Log.d("OnFailure", "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    Toast.makeText(CameraActivity.this, "Done+" + "success", Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onProgress(String s) {
                    dialog.setMessage("processing video..");
                    dialog.show();
                    // Toast.makeText(CameraActivity.this,"Deleting the last segment..",Toast.LENGTH_LONG).show();
                    Log.d("OnProgress", "progress : " + s);

                }

                @Override
                public void onStart() {
                    Log.d("OnStart", "Started command : ffmpeg " + command);
                }

                @Override
                public void onFinish() {
                    videoPaths.add(videoSpeed.getAbsolutePath());
                    Toast.makeText(CameraActivity.this, "Done+" + "finish", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                    if (millSecond != 0) {
                        if (timer >= millSecond) {
                            sendVideo();
                        }
                    } else {
                        if (timer >= 60) {
                            sendVideo();
                        }
                    }


                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (audio_name != null) {
            permission();

        } else {
            camera.start();
            TabLayout.Tab tab = cameraControls.tabLayout.getTabAt(0);
            tab.select();
        }

    }


    private void permission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            player = MediaPlayer.create(CameraActivity.this, Uri.parse(audioFile));
                            player.setLooping(false);
                            camera.start();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            Toast.makeText(CameraActivity.this, "Permission required please grant it from app settings", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    @Override
    protected void onPause() {
        camera.stop();
        super.onPause();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    private void videoCaptured(CameraKitVideo video) {
        flag = 1;

        int update = 0;
        for (int k = 0; k < c.size(); k++) {
            update = update + c.get(k);
        }

        Log.d("dsdf", "video: " + update);
        selectSound.setClickable(false);
        selectSound.setEnabled(false);
        videoFile = video.getVideoFile();
        callbackTime = System.currentTimeMillis();
        videoSpeed = new File("/storage/emulated/0/Download", System.currentTimeMillis() + ".mp4");
        try {
            videoSpeed.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (speed == 1) {

            String[] complexCommand = {"-y", "-i", videoFile.getAbsolutePath(), "-preset", "ultrafast", "-filter_complex", "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0 [a]", "-map", "[v]", "-map", "[a]", "-b:v", "2097k", "-r", "60", "-vcodec", "mpeg4", videoSpeed.getAbsolutePath()};
            execFFmpegBinary(complexCommand, videoSpeed, update);
            //videoPaths.add(videoSpeed.getAbsolutePath());


        } else if (speed == 4) {
            String[] complexCommand = {"-y", "-i", videoFile.getAbsolutePath(), "-preset", "ultrafast", "-filter_complex", "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0 [a]", "-map", "[v]", "-map", "[a]", "-b:v", "2097k", "-r", "60", "-vcodec", "mpeg4", videoSpeed.getAbsolutePath()};
            execFFmpegBinary(complexCommand, videoSpeed, update);


            //count= Integer.parseInt(String.valueOf(getDuration(videoSpeed)));
        } else if (speed == 3) {
           /* String[] complexCommand = {"-y", "-i", videoFile.getAbsolutePath(),"-preset", "ultrafast", "-filter_complex", "[0:v]setpts=1.0*PTS[v];[0:a]atempo=1.0 [a]", "-map", "[v]", "-map", "[a]", "-b:v", "2097k", "-r", "60", "-vcodec", "mpeg4", videoSpeed.getAbsolutePath()};
            execFFmpegBinary(complexCommand,videoSpeed);
           */

            try {
                FileUtils.copyFile(videoFile.getAbsolutePath(), videoSpeed.getAbsolutePath());
                videoPaths.add(videoSpeed.getAbsolutePath());

                if (millSecond != 0) {
                    if (update >= millSecond) {
                        sendVideo();
                    }
                } else {
                    if (update >= 60) {
                        sendVideo();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (speed == 2) {
            String[] complexCommand = {"-y", "-i", videoFile.getAbsolutePath(), "-preset", "ultrafast", "-filter_complex", "[0:v]setpts=2.0*PTS[v];[0:a]atempo=0.5 [a]", "-map", "[v]", "-map", "[a]", "-b:v", "2097k", "-r", "60", "-vcodec", "mpeg4", videoSpeed.getAbsolutePath()};
            execFFmpegBinary(complexCommand, videoSpeed, update);
            //videoPaths.add(videoSpeed.getAbsolutePath());


        } else if (speed == 5) {
            String[] complexCommand = {"-y", "-i", videoFile.getAbsolutePath(), "-preset", "ultrafast", "-filter_complex", "[0:v]setpts=0.25*PTS[v];[0:a]atempo=2.0 [a]", "-map", "[v]", "-map", "[a]", "-b:v", "2097k", "-r", "60", "-vcodec", "mpeg4", videoSpeed.getAbsolutePath()};
            execFFmpegBinary(complexCommand, videoSpeed, update);


        }


        tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (videoPaths.size() > 0) {
                    if (videoFile != null || videoSpeed != null) {

                        if (audio_name != null) {

                            File moviesDir = Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_MOVIES
                            );

                            String filePrefix = "full";
                            String fileExtn = ".mp4";
                            videoMerge = new File(moviesDir, filePrefix + SystemClock.currentThreadTimeMillis() + fileExtn);
                            int fileNo = 0;
                            while (videoMerge.exists()) {
                                fileNo++;
                                videoMerge = new File(moviesDir, filePrefix + fileNo + fileExtn);
                            }
                            ResultHolder.setVideo(videoMerge);
                            ResultHolder.setCall(call);
                            ResultHolder.setPath(videoMerge.getAbsolutePath());
                            ResultHolder.setType("video");
                            Intent intent = new Intent(CameraActivity.this, PreviewActivity.class);
                            intent.putExtra("videoArray", videoPaths);
                            intent.putExtra("audio", audioFile);
                            intent.putExtra("time", millSecond);
                            startActivity(intent);

                            //mergeFiles(videoMerge.getAbsolutePath(),audioTrim);


                        } else {
                            File moviesDir = Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_MOVIES
                            );


                            videoMerge = new File("/storage/emulated/0/Download", System.currentTimeMillis() + "vidN" + ".mp4");
                            int fileNo = 0;
                            while (videoMerge.exists()) {
                                fileNo++;
                                videoMerge = new File("/storage/emulated/0/Download", System.currentTimeMillis() + "vid" + ".mp4");
                            }


                            ResultHolder.dispose();
                            ResultHolder.setCall(call);
                            ResultHolder.setVideo(videoMerge);
                            ResultHolder.setPath(videoMerge.getAbsolutePath());
                            ResultHolder.setType("video");
                            ResultHolder.setNativeCaptureSize(camera.getCaptureSize());
                            ResultHolder.setTimeToCallback(callbackTime - captureStartTime);
                            progressBar.setVisibility(View.GONE);
                            Intent intent = new Intent(CameraActivity.this, PreviewActivity.class);
                            intent.putExtra("video", videoMerge.getAbsolutePath());
                            intent.putExtra("videoArray", videoPaths);
                            startActivity(intent);
                            if (call.equals("story"))
                                finish();

                        }
                    }
                } else {

                }

            }
        });


        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (flowLayout.getChildCount() > 0) {
                    c.remove(c.size() - 1);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(CameraActivity.this);
                    alertDialog.setTitle("Message !");
                    alertDialog.setMessage("Are Your sure , you want to delete last segment ?");
                    alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (videoPaths.size() > 0) {
                                videoPaths.remove(videoPaths.size() - 1);
                            }
                            if (flowLayout.getChildCount() > 0) {
                                //flowLayout.removeViewAt(flowLayout.getChildCount()-1);
                                flowLayout.removeViewsInLayout(flowLayout.getChildCount() - (a.get(a.size() - 1)), a.get(a.size() - 1));
                                Log.d("asda", "onClick: " + flowLayout.getChildCount() + "ssd" + a.get(a.size() - 1) + "");
                                a.remove(a.size() - 1);

                            }
                            if (b.size() != 0) {
                                b.remove(b.size() - 1);
                            }

                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).show();
                }
            }
        });

    }

    private void sendVideo() {

        if (videoFile != null || videoSpeed != null) {

            if (audio_name != null) {

                File moviesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MOVIES
                );

                String filePrefix = "full";
                String fileExtn = ".mp4";
                videoMerge = new File(moviesDir, filePrefix + SystemClock.currentThreadTimeMillis() + fileExtn);
                int fileNo = 0;
                while (videoMerge.exists()) {
                    fileNo++;
                    videoMerge = new File(moviesDir, filePrefix + fileNo + fileExtn);
                }
                ResultHolder.setVideo(videoMerge);
                ResultHolder.setCall(call);
                ResultHolder.setPath(videoMerge.getAbsolutePath());
                ResultHolder.setType("video");
                Intent intent = new Intent(CameraActivity.this, PreviewActivity.class);
                intent.putExtra("videoArray", videoPaths);
                intent.putExtra("audio", audioFile);
                intent.putExtra("time", millSecond);
                startActivity(intent);

                //mergeFiles(videoMerge.getAbsolutePath(),audioTrim);


            } else {
                File moviesDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MOVIES
                );


                videoMerge = new File("/storage/emulated/0/Download", System.currentTimeMillis() + "vidN" + ".mp4");
                int fileNo = 0;
                while (videoMerge.exists()) {
                    fileNo++;
                    videoMerge = new File("/storage/emulated/0/Download", System.currentTimeMillis() + "vid" + ".mp4");
                }


                ResultHolder.dispose();
                ResultHolder.setCall(call);
                ResultHolder.setVideo(videoMerge);
                ResultHolder.setPath(videoMerge.getAbsolutePath());
                ResultHolder.setType("video");
                ResultHolder.setNativeCaptureSize(camera.getCaptureSize());
                ResultHolder.setTimeToCallback(callbackTime - captureStartTime);
                progressBar.setVisibility(View.GONE);
                Intent intent = new Intent(CameraActivity.this, PreviewActivity.class);
                intent.putExtra("video", videoMerge.getAbsolutePath());
                intent.putExtra("videoArray", videoPaths);
                startActivity(intent);
                if (call.equals("story"))
                    finish();

            }
        }

    }

    private void setMicMuted(boolean state) {
        AudioManager myAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        // get the working mode and keep it
        int workingAudioMode = myAudioManager.getMode();
        myAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);

        // change mic state only if needed
        if (myAudioManager.isMicrophoneMute() != state) {
            myAudioManager.setMicrophoneMute(state);
        }

        // set back the original working mode
        myAudioManager.setMode(workingAudioMode);
    }

    @Override
    public void onEvent(CameraKitEvent cameraKitEvent) {
        Log.i("EVENT", "" + cameraKitEvent);
    }

    @Override
    public void onError(CameraKitError cameraKitError) {
        Log.e("CAMERA", "" + cameraKitError.getMessage());
    }

    @Override
    public void onImage(CameraKitImage cameraKitImage) {
        imageCaptured(cameraKitImage);
    }

    @Override
    public void onVideo(CameraKitVideo cameraKitVideo) {

        // if (audio_name != null || !call.equals("SaveProfile"))
        videoCaptured(cameraKitVideo);
    }

    private void requestReadImagePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //Sow message
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_STORAGE_REQ_CODE);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_STORAGE_REQ_CODE);
        }
    }

    public void launchImagePicker() {
        String chooseTitle = "";
        Intent intent = new Intent();
//        if (!call.equals("SaveProfile")) {
//            intent.setType("*/*");
//            String[] mimetypes = {"image/*", "video/*"};
//            intent.putExtra("android.intent.extra.MIME_TYPES", mimetypes);
//            chooseTitle = "Select Media";
//        } else {
        intent.setType("image/*");
        chooseTitle = "select Image";
//        }
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, chooseTitle), RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE) {
            sendMedia(data);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            parseCropedImage(resultCode, data);
        }
    }


    public void sendMedia(Intent data) {
        try {
            ResultHolder.dispose();
            Uri uri = data.getData();
            ContentResolver cr = this.getContentResolver();
            String mime = cr.getType(uri);

            ResultHolder.dispose();
            ResultHolder.setNativeCaptureSize(camera.getCaptureSize());
            ResultHolder.setCall(call);

            String picturePath = ImageFilePath.getPath(this, uri);
            ResultHolder.setPath(picturePath);

            if (mime != null && !mime.contains("image")) {
                //video
                ResultHolder.setType("video");

                File file = new File(picturePath);
                long timeInMillisec = getDuration(file);

                ResultHolder.setDuration(timeInMillisec);

                if (timeInMillisec > 30000) {
                    Toast.makeText(this, R.string.video_size_message, Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                //image
                ResultHolder.setType("image");
            }

            //   CropImage.activity(uri).start(this);

            Intent intent = new Intent(this, PreviewActivity.class);
            if (call.equals("SaveProfile")) {
                Bundle bundle = getIntent().getExtras();
                String userName = bundle != null ? bundle.getString("userName") : "";
                String firstName = bundle != null ? bundle.getString("firstName") : "";
                String lastName = bundle != null ? bundle.getString("lastName") : "";
                boolean isPrivate = bundle.getBoolean("private");
                if (!TextUtils.isEmpty(userName))
                    intent.putExtra("userName", userName);
                if (!TextUtils.isEmpty(firstName))
                    intent.putExtra("firstName", firstName);
                if (!TextUtils.isEmpty(lastName))
                    intent.putExtra("lastName", lastName);
                intent.putExtra("private", isPrivate);
                Log.i("CameraActivity", " private " + isPrivate);
            }

            startActivity(intent);
            if (call.equals("story"))
                finish();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    private long getDuration(File file) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(this, Uri.fromFile(file));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);
        retriever.release();
        return timeInMillisec;
    }


    public void parseCropedImage(int resultCode, Intent data) {
        String picturePath;
        try {
            // CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                picturePath = UriUtil.getPath(this, data.getData());
                ResultHolder.setPath(picturePath);
                ResultHolder.setCall(call);
                if (picturePath != null) {
                    Bitmap bitmapToUpload = BitmapFactory.decodeFile(picturePath);
                    Bitmap bitmap = getCircleBitmap(bitmapToUpload);
                    if (bitmap != null && bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
                        launchPostActivity(new CameraOutputModel(0, picturePath));
                    } else {
                        picturePath = null;
                    }
                } else {
                    picturePath = null;
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                picturePath = null;
            }
        } catch (OutOfMemoryError e) {
            picturePath = null;
        }
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        try {
            final Bitmap circuleBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(circuleBitmap);

            final int color = Color.GRAY;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
            final RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawOval(rectF, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return circuleBitmap;
        } catch (Exception e) {
            return null;
        }
    }

    private void launchPostActivity(CameraOutputModel model) {
        Intent intent = new Intent(getApplicationContext(), PreviewActivity.class);
        intent.putExtra(Constants.Post.PATH, model.getPath());
        intent.putExtra(Constants.Post.TYPE, model.getType() == 0 ? Constants.Post.IMAGE : Constants.Post.VIDEO);
        startActivityForResult(intent, CAMERA_REQUEST);
        if (call.equals("story"))
            finish();
    }


    boolean handleViewTouchFeedback(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                touchDownAnimation(view);
                return true;
            }

            case MotionEvent.ACTION_UP: {
                touchUpAnimation(view);
                return true;
            }

            default: {
                return true;
            }
        }
    }

    void touchDownAnimation(View view) {
        view.animate()
                .scaleX(0.88f)
                .scaleY(0.88f)
                .setDuration(300)
                .setInterpolator(new OvershootInterpolator())
                .start();
    }

    void touchUpAnimation(View view) {
        view.animate()
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(300)
                .setInterpolator(new OvershootInterpolator())
                .start();
    }

    @Override
    public void selectOption(int position) {
        switch (position) {
            case 0:
                break;
            case 1:

                break;
            case 2:
                try {
                    startActivity(new Intent(this, DubCategoryActivity.class));
                } catch (NullPointerException e) {
                }
                break;
        }
    }

    @Override
    public void video(boolean flag, int i) {
        Log.d("dsdf", "videosw4: " + i);


        // cameraControls.sta
        flowLayout.setVisibility(View.VISIBLE);
        ibGallery.setVisibility(View.GONE);
        tick.setVisibility(View.VISIBLE);
        cross.setVisibility(View.VISIBLE);


        if (flag) {
            if (audio_name != null) {
                if (speed == 1) {
                    player = MediaPlayer.create(CameraActivity.this, Uri.parse(audioFile));
                    player.setLooping(false);
                    speedf = 3.0f;
                    PlaybackParams playbackParams = new PlaybackParams();
                    playbackParams.setAudioFallbackMode(
                            PlaybackParams.AUDIO_FALLBACK_MODE_DEFAULT);
                    playbackParams.setSpeed(speedf);
                    player.setPlaybackParams(playbackParams);

                    if (b.size() != 0) {
                        player.seekTo(b.get(b.size() - 1));
                        player.start();
                    } else {
                        player.seekTo(0);
                        player.start();
                    }


                } else if (speed == 2) {

                    player = MediaPlayer.create(CameraActivity.this, Uri.parse(audioFile));
                    player.setLooping(false);
                    speedf = 2.0f;
                    PlaybackParams playbackParams = new PlaybackParams();
                    playbackParams.setAudioFallbackMode(
                            PlaybackParams.AUDIO_FALLBACK_MODE_DEFAULT);
                    playbackParams.setSpeed(speedf);
                    player.setPlaybackParams(playbackParams);
                    if (b.size() != 0) {
                        player.seekTo(b.get(b.size() - 1));
                        player.start();
                    } else {
                        player.seekTo(0);
                        player.start();
                    }

                } else if (speed == 3) {
                    player = MediaPlayer.create(CameraActivity.this, Uri.parse(audioFile));
                    player.setLooping(false);
                    speedf = 1.0f;
                    PlaybackParams playbackParams = new PlaybackParams();
                    playbackParams.setAudioFallbackMode(
                            PlaybackParams.AUDIO_FALLBACK_MODE_DEFAULT);
                    playbackParams.setSpeed(speedf);
                    player.setPlaybackParams(playbackParams);
                    if (b.size() != 0) {
                        player.seekTo(b.get(b.size() - 1));
                        player.start();
                    } else {
                        player.seekTo(0);
                        player.start();
                    }

                } else if (speed == 4) {
                    player = MediaPlayer.create(CameraActivity.this, Uri.parse(audioFile));
                    player.setLooping(false);
                    speedf = 0.5f;
                    PlaybackParams playbackParams = new PlaybackParams();
                    playbackParams.setAudioFallbackMode(
                            PlaybackParams.AUDIO_FALLBACK_MODE_DEFAULT);
                    playbackParams.setSpeed(speedf);
                    player.setPlaybackParams(playbackParams);
                    if (b.size() != 0) {
                        player.seekTo(b.get(b.size() - 1));
                        player.start();
                    } else {
                        player.seekTo(0);
                        player.start();
                    }
                } else if (speed == 5) {
                    player = MediaPlayer.create(CameraActivity.this, Uri.parse(audioFile));
                    player.setLooping(false);
                    speedf = 0.33f;
                    PlaybackParams playbackParams = new PlaybackParams();
                    playbackParams.setAudioFallbackMode(
                            PlaybackParams.AUDIO_FALLBACK_MODE_DEFAULT);
                    playbackParams.setSpeed(speedf);
                    player.setPlaybackParams(playbackParams);

                    if (b.size() != 0) {
                        player.seekTo(b.get(b.size() - 1));
                        player.start();
                    } else {
                        player.seekTo(0);
                        player.start();
                    }

                }


            }


        } else {

            if (audioFile != null) {
                player.pause();
                length = player.getCurrentPosition();
            }
            b.add(length);
            if (speed == 1) {
                c.add(i * 10);
            } else if (speed == 2) {
                c.add(i * 2);
            } else if (speed == 3) {
                c.add(i);
            } else if (speed == 4) {
                c.add(i / 2);
            } else if (speed == 5) {
                c.add(i / 3);
            }
            Log.d("dsdf", "video: ");

        }
    }

    public void imageCaptured(CameraKitImage cameraKitImage) {
        byte[] jpeg = cameraKitImage.getJpeg();
        Bitmap bitmap = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);
        String name = System.currentTimeMillis() + ".jpg";
        final String path = BitmapUtils.insertImage(getContentResolver(), bitmap, name, null);

        long callbackTime = System.currentTimeMillis();
        ResultHolder.dispose();
        ResultHolder.setImage(jpeg);
        ResultHolder.setNativeCaptureSize(camera.getCaptureSize());
        ResultHolder.setTimeToCallback(callbackTime - captureStartTime);
        ResultHolder.setCall(call);
        ResultHolder.setType("image");
        Uri uri = Uri.parse(path);
        String picturePath = UriUtil.getPath(this, uri);
        ResultHolder.setPath(picturePath);
        //   CropImage.activity(uri).start(this);

        Intent intent = new Intent(this, PreviewActivity.class);
        if (call.equals("SaveProfile")) {
            selectSound.setVisibility(View.GONE);
            Bundle bundle = getIntent().getExtras();
            String userName = bundle != null ? bundle.getString("userName") : "";
            String firstName = bundle != null ? bundle.getString("firstName") : "";
            String lastName = bundle != null ? bundle.getString("lastName") : "";
            boolean isPrivate = bundle.getBoolean("isPrivate");
            if (!TextUtils.isEmpty(userName))
                intent.putExtra("userName", userName);
            if (!TextUtils.isEmpty(firstName))
                intent.putExtra("firstName", firstName);
            if (!TextUtils.isEmpty(lastName))
                intent.putExtra("lastName", lastName);
            intent.putExtra("isPrivate", isPrivate);
        }
        startActivity(intent);
        if (call.equals("story"))
            finish();
    }

    @Override
    public void face(boolean isFace) {
        this.isFrontFace = isFace;
    }

    @Override
    public void progress() {
        if (speed == 1) {
            if (millSecond != 0) {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (millSecond / 10), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            } else {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (30), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            }

        }
        if (speed == 2) {
            if (millSecond != 0) {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (millSecond / 2), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            } else {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (30), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            }

        } else if (speed == 3) {

            if (millSecond != 0) {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (millSecond), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            } else {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (60), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            }
        } else if (speed == 4) {
            if (millSecond != 0) {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (millSecond * 2), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            } else {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (120), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            }

        } else if (speed == 5) {
            if (millSecond != 0) {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (millSecond * 3), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            } else {
                ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(widthScreen / (180), rawHeight);
                View tv = new View(this);
                tv.setBackgroundColor(getResources().getColor(R.color.blue));
                flowLayout.addView(tv, lp);
            }

        }
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale);
    }

    @Override
    public void stopProgress() {
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(1, rawHeight);
        View tv = new View(this);
        tv.setBackgroundColor(getResources().getColor(R.color.whiteOverlay));
        flowLayout.addView(tv, lp);
        count = flowLayout.getChildCount();
        a.add(count - init);
        Log.d("sfd", "stopProgress: ");
    }

    @Override
    public void getCount() {
        init = flowLayout.getChildCount();
    }

}
