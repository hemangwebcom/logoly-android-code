package chat.hola.com.howdoo.Utilities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import chat.hola.com.howdoo.AppController;


/**
 * Created by moda on 13/07/17.
 */


/*
 * To keep the device awake when it is booting
 */
public class BootCompletedIntentReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // This is the Intent to deliver to our service.

  /*
         * For the case when phone was rebooted with the app being already started
         */




        AppController.getInstance().createMQttConnection(AppController.getInstance().getUserId(),true);

    }
}