package chat.hola.com.howdoo.profileScreen;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Dialog.ImageSourcePicker;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Profile.UpdateStatus;
import chat.hola.com.howdoo.Utilities.ConnectivityReceiver;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.cameraActivities.SandriosCamera;
import chat.hola.com.howdoo.cameraActivities.manager.CameraOutputModel;
import chat.hola.com.howdoo.club.clubs.ClubActivity;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.bottomProfileMenu.ProfileMenuFrag;
import chat.hola.com.howdoo.profileScreen.channel.ChannelFragment;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverActivity;
import chat.hola.com.howdoo.profileScreen.editProfile.EditProfileActivity;
import chat.hola.com.howdoo.profileScreen.followers.FollowersActivity;
import chat.hola.com.howdoo.profileScreen.model.Data;
import chat.hola.com.howdoo.profileScreen.model.Profile;
import chat.hola.com.howdoo.profileScreen.story.StoryFragment;
import chat.hola.com.howdoo.profileScreen.tag.TagFragment;
import chat.hola.com.howdoo.settings.SettingsActivity;
import dagger.android.support.DaggerAppCompatActivity;


public class ProfileActivity extends DaggerAppCompatActivity implements DialogInterface.OnClickListener,
        ProfileContract.View, ConnectivityReceiver.ConnectivityReceiverListener,
        SwipeRefreshLayout.OnRefreshListener, SandriosCamera.CameraCallback {

    private static final String TAG = ProfileActivity.class.getSimpleName();
    private int STATUS_UPDATE_REQ_CODE = 555;
    private int CAMERA_REQ_CODE = 24;
    private int READ_STORAGE_REQ_CODE = 26;
    private int WRITE_STORAGE_REQ_CODE = 27;
    private static final int RESULT_CAPTURE_IMAGE = 0;
    private static final int RESULT_LOAD_IMAGE = 1;

    public static boolean isPrivate = false;
    public static int followStatus = 0;

    @Inject
    SessionManager sessionManager;
    @Inject
    BlockDialog dialog1;
    @Inject
    StoryFragment storyFragment;
    @Inject
    ImageSourcePicker imageSourcePicker;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    ProfilePresenter presenter;
    @Inject
    ProfileMenuFrag profileMenuFrag;
    @Inject
    ChannelFragment channelFragment;
    @Inject
    TagFragment tagFragment;
    @BindView(R.id.ivEditProfile)
    ImageView ibEditProfile;
    @BindView(R.id.ivEditCover)
    ImageView ivEditCover;
    @BindView(R.id.ivProfile)
    ImageView ivProfile;
    @BindView(R.id.ivProfileBg)
    ImageView ivProfileBg;
    @BindView(R.id.tvProfileName)
    TextView tvProfileName;
    @BindView(R.id.tvProfileMob)
    TextView tvProfileMob;
    @BindView(R.id.tvProfileStatus)
    TextView tvProfileStatus;
    @BindView(R.id.tvPostCount)
    TextView tvPostCount;
    @BindView(R.id.tvClubCount)
    TextView tvClubCount;
    @BindView(R.id.tvClubTitle)
    TextView tvClubTitle;
    @BindView(R.id.tvFollowingCount)
    TextView tvFollowingCount;
    @BindView(R.id.tvFollowersCount)
    TextView tvFollowersCount;
    @BindView(R.id.tvPostTitle)
    TextView tvPostTitle;
    @BindView(R.id.tvFollowingTitle)
    TextView tvFollowingTitle;
    @BindView(R.id.tvFollowersTitle)
    TextView tvFollowersTitle;
    @BindView(R.id.tabLayoutProfile)
    TabLayout tabLayoutProfile;
    @BindView(R.id.viewPagerProfile)
    ViewPager viewPagerProfile;
    @BindView(R.id.faButton)
    FloatingActionButton faButton;
    @BindView(R.id.btnProfile)
    Button btnProfile;
    @BindView(R.id.btnAction)
    ImageButton btnAction;
    @BindView(R.id.layoutAppBar)
    AppBarLayout appBarLayout;
    @BindView(R.id.btnFollow)
    public ToggleButton btnFollow;
    @BindView(R.id.collapseToolbarLayout)
    CollapsingToolbarLayout collapseToolbarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.profileRoot)
    CoordinatorLayout root;
    @BindView(R.id.editStatus)
    ImageView editStatus;

    AlertDialog.Builder reportDialog;
    AlertDialog.Builder blockDialog;
    ArrayAdapter<String> arrayAdapter;
    ArrayAdapter<String> blockReasons;

    private Unbinder unbinder;
    private ProgressDialog dialog;
    public String userId;
    private Data profileData;
    private Menu menu;
    private ActionBar actionBar;
    private Drawable backDrawableBlack;
    private Drawable backDrawableWhite;
    private String myid;
    boolean isBlocked = false;
    MenuItem block;
    Drawable menudots;
    Drawable menudotsWhite;
    private int picFor; //1= profile pic, 2= cover pic

    List<chat.hola.com.howdoo.profileScreen.followers.Model.Data> dataList = new ArrayList<>();
    AlertDialog.Builder actionDialog;
    ArrayAdapter<String> actionList;
    private boolean isFollowingMe;

    @Override
    public void userBlocked() {
        dialog1.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_new);
        ButterKnife.bind(this);
        backDrawableBlack = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
        backDrawableWhite = getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp);
        menudots = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_more_black);
        menudotsWhite = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_more_vert);
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            this.actionBar = actionBar;
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(backDrawableWhite);
            if (toolbar != null)
                toolbar.setOverflowIcon(menudotsWhite);
        }

        myid = AppController.getInstance().getUserId();

        //collapseToolbarLayout.setTitle("Profile");
        collapseToolbarLayout.setCollapsedTitleTypeface(typefaceManager.getSemiboldFont());
        collapseToolbarLayout.setExpandedTitleTypeface(typefaceManager.getSemiboldFont());
        collapseToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);

        userId = getIntent().getStringExtra("userId");
        isFollowingMe = getIntent().getBooleanExtra("isFollowingMe", false);
        //preferences = getSharedPreferences("userProfile", MODE_PRIVATE);
        presenter.init();
        reportDialog = new AlertDialog.Builder(this);
        reportDialog.setTitle(R.string.report);
        presenter.getReportReasons();


        blockDialog = new AlertDialog.Builder(this);
        blockDialog.setTitle(R.string.Block);
        presenter.getBlockReasons();


        dialog = new ProgressDialog(this);
        dialog.setMessage((CharSequence) getResources().getString(R.string.please_wait));
        dialog.setCancelable(false);

        //   llNetworkError.setErrorListner(this);

        btnFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!AppController.getInstance().getUserId().equals(userId)) {
                    if (btnFollow.isChecked()) {
                        presenter.follow(userId);
                    } else {
                        presenter.unfollow(userId);
                    }
                }
            }
        });


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    Log.w(TAG, "scrollRange: " + scrollRange);
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    changeColor(false);
                } else if (isShow) {
                    isShow = false;
                    changeColor(true);
                }
            }
        });

        //for followers
        actionDialog = new AlertDialog.Builder(this);
        actionDialog.setTitle("Action for this user");
//        ArrayList<String> data = new ArrayList<>();
//        data.add("Refollow");
//        data.add("Add to contact");
//        actionList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);
//        actionDialog.setAdapter(actionList, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                switch (i) {
//                    case 0:
//                        presenter.follow(userId);
//                        break;
//                    case 1:
//                        presenter.addToContact(userId);
//                        break;
//                }
//            }
//        });
    }

    private void changeColor(boolean white) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (white) {
                    actionBar.setHomeAsUpIndicator(backDrawableWhite);
                    if (toolbar != null)
                        toolbar.setOverflowIcon(menudotsWhite);
                } else {
                    actionBar.setHomeAsUpIndicator(backDrawableBlack);
                    if (toolbar != null)
                        toolbar.setOverflowIcon(menudots);
                }
            }
        });

    }

    @OnClick(R.id.editStatus)
    public void status() {
        Intent intent = new Intent(this, UpdateStatus.class);
        intent.putExtra("status", tvProfileStatus.getText().toString());
        startActivityForResult(intent, STATUS_UPDATE_REQ_CODE);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuItem report = menu.findItem(R.id.actionReport);
        report.setVisible(!AppController.getInstance().getUserId().equals(userId));

        block = menu.findItem(R.id.actionBlock);
        block.setTitle(getString(isBlocked ? R.string.ubBlock : R.string.Block));
        block.setVisible(!AppController.getInstance().getUserId().equals(userId));

        MenuItem subscription = menu.findItem(R.id.actionSubscription);
        subscription.setVisible(AppController.getInstance().getUserId().equals(userId));
        MenuItem consent = menu.findItem(R.id.actionConsent);
        consent.setVisible(AppController.getInstance().getUserId().equals(userId));
        MenuItem settings = menu.findItem(R.id.actionSettings);
        settings.setVisible(AppController.getInstance().getUserId().equals(userId));

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.new_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                back();
                return true;
            case R.id.actionSettings:
                settings();
                return true;
            case R.id.actionReport:
                reportDialog.show();
                return true;
            case R.id.actionBlock:
                if (isBlocked) {
                    presenter.block(userId, "unblock", "");
                } else {
                    blockDialog.show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void back() {
        onBackPressed();
    }

    public void discover() {
        startActivity(new Intent(this, DiscoverActivity.class).putExtra("caller", "ProfileActivity"));
    }

    @OnClick(R.id.tvPostCount)
    public void post() {
        viewPagerProfile.setCurrentItem(0);
        appBarLayout.setExpanded(false, true);
    }

    @OnClick(R.id.tvClubCount)
    public void club() {
        startActivity(new Intent(this, ClubActivity.class).putExtra("userId", userId));
    }

    @OnClick({R.id.tvFollowersCount, R.id.tvFollowersTitle})
    public void followers() {
        Intent intent = new Intent(ProfileActivity.this, FollowersActivity.class);
        intent.putExtra("title", getResources().getString(R.string.followers));
        if (profileData != null && profileData.getFollowing() != null) {
            intent.putExtra("following", profileData.getFollowers());
            intent.putExtra("userId", profileData.getId());
        }
        startActivity(intent);
    }

    @OnClick({R.id.tvFollowingCount, R.id.tvFollowingTitle})
    public void following() {
        String followingCount = tvFollowingCount.getText().toString();
        Intent intent = new Intent(ProfileActivity.this, FollowersActivity.class);
        intent.putExtra("title", getResources().getString(R.string.following));
        if (profileData != null && profileData.getFollowing() != null) {
            intent.putExtra("following", profileData.getFollowing());
            intent.putExtra("userId", profileData.getId());
        }
        startActivity(intent);
    }

    public void settings() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (picFor == 0) {
            if (userId != null && !userId.equals(myid)) {
                presenter.loadMemberData(userId);
            } else {
                presenter.loadProfileData();
            }
        }
    }

    void showButton() {
        if (isFollowingMe) {
            ibEditProfile.setVisibility(View.GONE);
            ivEditCover.setVisibility(View.GONE);
            editStatus.setVisibility(View.GONE);
            btnAction.setVisibility(View.VISIBLE);
            btnFollow.setVisibility(View.GONE);
            faButton.setVisibility(View.GONE);
            btnProfile.setVisibility(View.GONE);
        } else if (userId != null && !userId.equals(myid)) {
            ibEditProfile.setVisibility(View.GONE);
            ivEditCover.setVisibility(View.GONE);
            editStatus.setVisibility(View.GONE);
            btnAction.setVisibility(View.GONE);
            btnFollow.setVisibility(View.VISIBLE);
            faButton.setVisibility(View.GONE);
            btnProfile.setVisibility(View.GONE);
        } else {
            ibEditProfile.setVisibility(View.VISIBLE);
            ivEditCover.setVisibility(View.VISIBLE);
            editStatus.setVisibility(View.VISIBLE);
            btnAction.setVisibility(View.GONE);
            btnFollow.setVisibility(View.GONE);
            faButton.setVisibility(View.VISIBLE);
            btnProfile.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btnAction)
    public void action() {
        ArrayList<String> options = new ArrayList<>();
        actionList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, options);
        if (profileData.isContact()) {
            options.add("Delete");
            actionDialog.setAdapter(actionList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            presenter.deleteContact(profileData.getId());
                            break;
                    }
                }
            });
        } else {
            options.add("Refollow");
            options.add("Add to contact");
            actionDialog.setAdapter(actionList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            presenter.follow(profileData.getId());
                            break;
                        case 1:
                            presenter.addToContact(profileData.getId());
                            break;
                    }
                }
            });

        }
        actionDialog.show();
    }

    @Override
    public void applyFont() {
        tvProfileName.setTypeface(typefaceManager.getMediumFont());
        tvProfileMob.setTypeface(typefaceManager.getRegularFont());
        tvProfileStatus.setTypeface(typefaceManager.getRegularFont());
        tvPostCount.setTypeface(typefaceManager.getMediumFont());
        tvClubCount.setTypeface(typefaceManager.getMediumFont());
        tvFollowingCount.setTypeface(typefaceManager.getMediumFont());
        tvFollowersCount.setTypeface(typefaceManager.getMediumFont());
        tvPostTitle.setTypeface(typefaceManager.getRegularFont());
        tvFollowingTitle.setTypeface(typefaceManager.getRegularFont());
        tvFollowersTitle.setTypeface(typefaceManager.getRegularFont());
        tvClubTitle.setTypeface(typefaceManager.getRegularFont());
        btnFollow.setTypeface(typefaceManager.getMediumFont());
    }


    @Override
    public void setupViewPager() {
        tabLayoutProfile.setupWithViewPager(viewPagerProfile);
        ProfilePageAdapter fragmentPageAdapter = new ProfilePageAdapter(getSupportFragmentManager());
        fragmentPageAdapter.addFragment(storyFragment, "", "grid", userId);
        fragmentPageAdapter.addFragment(new StoryFragment(), "", "nogrid", userId);


        fragmentPageAdapter.addFragment(new DummyFragment(), "", "", userId);
        fragmentPageAdapter.addFragment(new DummyFragment(), "", "", userId);

        //fragmentPageAdapter.addFragment(new TagFragment(), "", "", userId);
        viewPagerProfile.setAdapter(fragmentPageAdapter);
        viewPagerProfile.setCurrentItem(0);
        Drawable drawableOne = getResources().getDrawable(R.drawable.story_on);
        Drawable drawableTwo = getResources().getDrawable(R.drawable.ic_list_selected);
        Drawable drawableThree = getResources().getDrawable(R.drawable.channel_on);
        Drawable drawableFour = getResources().getDrawable(R.drawable.ic_description);

        final Drawable[] tabDrawableOn = {drawableOne, drawableTwo, drawableThree, drawableFour};

        Drawable drawableOneOff = getResources().getDrawable(R.drawable.story_off);
        Drawable drawableTwoOff = getResources().getDrawable(R.drawable.ic_list_unselected);
        Drawable drawableThreeOff = getResources().getDrawable(R.drawable.channel_off);
        Drawable drawableFourOff = getResources().getDrawable(R.drawable.ic_description_off);

        final Drawable[] tabDrawableOff = {drawableOneOff, drawableTwoOff, drawableThreeOff, drawableFourOff};

        tabLayoutProfile.getTabAt(0).setIcon(tabDrawableOn[0]);
        tabLayoutProfile.getTabAt(1).setIcon(tabDrawableOff[1]);
        tabLayoutProfile.getTabAt(2).setIcon(tabDrawableOff[2]);
        tabLayoutProfile.getTabAt(3).setIcon(tabDrawableOff[3]);

        //default selected tab
        tabLayoutProfile.getTabAt(0).select();
        //first = true;
        tabLayoutProfile.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.setIcon(tabDrawableOn[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.setIcon(tabDrawableOff[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void isLoading(boolean flag) {
    }

    @OnClick(R.id.faButton)
    public void fab() {
        if (profileMenuFrag.isAdded())
            return;
        profileMenuFrag.show(getSupportFragmentManager(), "profileMenuFrag");
    }

    @OnClick(R.id.ivEditProfile)
    public void editProfilePic() {
        picFor = 1;
        imageSourcePicker.setOnSelectImageSource(callback);
        imageSourcePicker.setTitle("Add profile picture");
        imageSourcePicker.show();
    }

    @OnClick(R.id.ivEditCover)
    public void editCoverPic() {
        picFor = 2;
        imageSourcePicker.setOnSelectImageSource(callback);
        imageSourcePicker.setTitle("Add cover photo");
        imageSourcePicker.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    public void setFabButtonVisible(boolean show) {
        btnFollow.setVisibility(show ? View.GONE : View.VISIBLE);
        faButton.setVisibility(show ? View.VISIBLE : View.GONE);
        btnProfile.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        Toast.makeText(this, msg != null && !msg.isEmpty() ? msg : getResources().getString(msgId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        //   llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showProfileData(Profile profile) {
        userId = profile.getData().get(0).getId();
        showProfile(profile);
    }

    private void showProfile(Profile profile) {
        try {
            if (!profile.getData().isEmpty()) {
//                isFollowingMe = profile.getData().get(0).isFollowingMe();
//                btnProfile.setVisibility(isFollowingMe || (userId != null && !userId.equals(myid)) ?View.VISIBLE:View.GONE);
//                btnFollow.setVisibility(!(isFollowingMe || (userId != null && !userId.equals(myid)))?View.GONE:View.VISIBLE);
//                btnProfile.setText(isFollowingMe ? "Action" : getString(R.string.Profile));

                invalidateOptionsMenu();
                //  binding.setUser(profile.getData().get(0));
                followStatus = profile.getData().get(0).getFollowStatus();

                Data data = profile.getData().get(0);
                isFollowingMe = data.isFollowingMe();
                userId = data.getId();
                showButton();

                Glide.with(getBaseContext()).load(data.getProfilePic()).asBitmap().centerCrop()
                        .placeholder(R.drawable.profile_one).into(ivProfile);

                Glide.with(getBaseContext()).load(data.getCoverPic()).asBitmap().centerCrop().into(ivProfileBg);

                String profileStatus = data.getStatus();
                if (profileStatus == null || profileStatus.isEmpty()) {
                    profileStatus = getResources().getString(R.string.default_status);
                }
                tvProfileStatus.setText(profileStatus);
                collapseToolbarLayout.setTitle(data.getUserName());
                tvProfileName.setText(data.getUserName());

                tvProfileMob.setText(data.getNumber());

                tvPostCount.setText(String.valueOf(data.getPostsCount()));
                tvFollowingCount.setText(String.valueOf(data.getFollowing()));
                tvFollowersCount.setText(String.valueOf(data.getFollowers()));
                tvClubCount.setText(String.valueOf(data.getClubCount()));

                isBlocked = profile.getData().get(0).getBlock(); // 1=blocked
                this.block.setTitle(getResources().getString(isBlocked ? R.string.Block : R.string.ubBlock));
                isPrivate = false;//profile.getData().get(0).getPrivate().equals("1");

                if (profile.getData().get(0).getId().equals(myid))
                    sessionManager.setUserProfilePic(profile.getData().get(0).getProfilePic());

                this.profileData = profile.getData().get(0);

//                1)type: { status: 0, message: "unfollowed" }
//                2)type: { status: 1, message: ' started following' }
//                3)type: { status: 2, message: 'requested' }
//                4)type: { status: 3, message: 'reject' }
                boolean isChecked = profileData.getFollowStatus() == 1;
                if (btnFollow != null) {

//                    switch (profileData.getFollowStatus()) {
//                        case 0:
//                            //public - unfollow
//                            isPrivate = profileData.getPrivate().equals("1");
//                            isChecked = false;
//                            break;
//                        case 1:
//                            //public - follow
//                            isPrivate = false;
//                            isChecked = true;
//                            break;
//                        case 2:
//                            //private - requested
//                            isPrivate = true;
//                            isChecked = true;
//                            break;
//                        case 3:
//                            //private - request
//                            isPrivate = true;
//                            isChecked = false;
//                            break;
//                        default:
//                            isChecked = false;
//                            break;
//
//                    }
                    btnFollow.setTextOn(getResources().getString(isPrivate ? R.string.requested : R.string.following));
                    btnFollow.setTextOff(getResources().getString(isPrivate ? R.string.request : R.string.follow));
                    btnFollow.setTextOn(getResources().getString(R.string.unfollow));
                    btnFollow.setTextOff(getResources().getString(R.string.follow));
                    btnFollow.setChecked(isChecked);
                }

            }
        } catch (Exception ignored) {
            Log.i(TAG, "showProfile: ");
        }
    }

    @Override
    public void isFollowing(boolean flag) {
        if (btnFollow != null) {
            if (userId != null && !userId.equals(myid))
                presenter.loadMemberData(userId);
            else
                presenter.loadProfileData();

            btnFollow.setEnabled(true);
            btnFollow.setChecked(flag);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Toast.makeText(this, isConnected ? "Internet connected" : "No internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void addToReportList(ArrayList<String> data) {
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);
        reportDialog.setAdapter(arrayAdapter, this);
    }

    @Override
    public void addToBlockList(ArrayList<String> data) {
        blockReasons = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);
        blockDialog.setAdapter(blockReasons, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                presenter.block(userId, "block", blockReasons.getItem(i));
            }
        });
    }

    @Override
    public void block(boolean block) {
        isBlocked = block;
        String message = getResources().getString(block ? R.string.ubBlock : R.string.Block);
        this.block.setTitle(message);
        Toast.makeText(this, getResources().getString(!block ? R.string.ubBlock : R.string.Block), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSnackMsg(int msgId) {
        String msg = getResources().getString(msgId);
        Snackbar snackbar = Snackbar.make(root, "" + msg, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        ((TextView) view.findViewById(android.support.design.R.id.snackbar_text)).setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void reload() {
        presenter.init();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        presenter.reportUser(userId, arrayAdapter.getItem(i), arrayAdapter.getItem(i));
    }

    @Override
    public void onComplete(CameraOutputModel cameraOutputModel) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == STATUS_UPDATE_REQ_CODE) {
                String newStatus = data.getStringExtra("updatedValue");
                if (newStatus != null && !newStatus.isEmpty())
                    tvProfileStatus.setText(newStatus);
                else
                    tvProfileStatus.setText(getResources().getString(R.string.default_status));
                presenter.update(tvProfileStatus.getText().toString(), 0);
            } else if (requestCode == RESULT_LOAD_IMAGE) {

                presenter.parseSelectedImage(requestCode, resultCode, data);

            } else if (requestCode == RESULT_CAPTURE_IMAGE) {
                presenter.parseCapturedImage(requestCode, resultCode, data);

            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                presenter.parseCropedImage(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_REQ_CODE) {

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        presenter.launchCamera(getPackageManager());
                    } else {
                        requestReadImagePermission(0);
                    }
                } else {
                    //camera permission denied msg
                    showSnackMsg(R.string.string_62);
                }
            } else {
                //camera permission denied msg
                showSnackMsg(R.string.string_62);
            }

        } else if (requestCode == READ_STORAGE_REQ_CODE) {

            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    presenter.launchImagePicker();

                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            } else {
                //Access storage permission denied
                showSnackMsg(R.string.string_1006);
            }
        } else if (requestCode == WRITE_STORAGE_REQ_CODE) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {

                    presenter.launchCamera(getPackageManager());
                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            }

        }
    }

    ImageSourcePicker.OnSelectImageSource callback = new ImageSourcePicker.OnSelectImageSource() {
        @Override
        public void onCamera() {
            checkCameraPermissionImage();
        }

        @Override
        public void onGallary() {
            checkReadImage();
        }

        @Override
        public void onCancel() {
            //nothing to do.
        }
    };

    private void checkCameraPermissionImage() {
        if (ActivityCompat.checkSelfPermission(ProfileActivity
                .this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                presenter.launchCamera(getPackageManager());
            } else {
                /*
                 *permission required to save the image captured
                 */
                requestReadImagePermission(0);
            }
        } else {
            requestCameraPermissionImage();
        }
    }

    private void requestCameraPermissionImage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this,
                Manifest.permission.CAMERA)) {

            Snackbar snackbar = Snackbar.make(root, R.string.string_221,
                    Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA},
                            CAMERA_REQ_CODE);
                }
            });
            snackbar.show();
            View view = snackbar.getView();
            ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                    .setGravity(Gravity.CENTER_HORIZONTAL);
        } else {
            ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA},
                    CAMERA_REQ_CODE);
        }
    }

    private void checkReadImage() {
        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            presenter.launchImagePicker();
        } else {
            requestReadImagePermission(1);
        }
    }


    private void requestReadImagePermission(int k) {
        if (k == 1) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar snackbar = Snackbar.make(root, R.string.string_222,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                READ_STORAGE_REQ_CODE);
                    }
                });
                snackbar.show();
                View view = snackbar.getView();
                ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                        .setGravity(Gravity.CENTER_HORIZONTAL);
            } else {
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        READ_STORAGE_REQ_CODE);
            }
        } else if (k == 0) {
            /*
             * For capturing the image permission
             */
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar snackbar = Snackbar.make(root, R.string.string_1218,
                        Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                WRITE_STORAGE_REQ_CODE);
                    }
                });

                snackbar.show();
                View view = snackbar.getView();
                ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                        .setGravity(Gravity.CENTER_HORIZONTAL);
            } else {
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_STORAGE_REQ_CODE);
            }
        }
    }

    public void launchCamera(Intent intent) {
        startActivityForResult(intent, RESULT_CAPTURE_IMAGE);
    }

    public void launchImagePicker(Intent intent) {
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    public void launchCropImage(Uri data) {
        CropImage.activity(data)
                .start(this);
    }

    @Override
    public void setImage(Bitmap bitmap) {
        if (picFor == 1)
            ivProfile.setImageBitmap(bitmap);
        else if (picFor == 2)
            ivProfileBg.setImageBitmap(bitmap);
        presenter.uploadPhoto(picFor);
    }
}
