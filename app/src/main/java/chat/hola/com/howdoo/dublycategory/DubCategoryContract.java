package chat.hola.com.howdoo.dublycategory;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.dublycategory.modules.CategoryClickListner;
import chat.hola.com.howdoo.dublycategory.modules.ClickListner;

/**
 * <h1>DubCategoryContract</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 7/16/2018.
 */

public interface DubCategoryContract {

    interface View extends BaseView {
        void getList(String categoryId, String name);
    }

    interface Presenter {

        CategoryClickListner getCategoryPresenter();

        void getCategories(boolean b);
    }
}
