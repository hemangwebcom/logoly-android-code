package chat.hola.com.howdoo.home;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.Dialog.ImageSourcePicker;
import chat.hola.com.howdoo.Utilities.SocialShare;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.home.social.model.SocialAdapter;
import chat.hola.com.howdoo.home.stories.model.StoriesAdapter;
import chat.hola.com.howdoo.home.stories.model.StoryData;
import chat.hola.com.howdoo.home.stories.model.StoryPost;
import chat.hola.com.howdoo.home.trending.model.Header;
import chat.hola.com.howdoo.home.trending.model.HeaderAdapter;
import chat.hola.com.howdoo.home.trending.model.TrendingContentAdapter;
import chat.hola.com.howdoo.preview.PreviewActivity;
import dagger.Module;
import dagger.Provides;

/**
 * <h1>LandingUtilModule</h1>
 *
 * @author 3Embed
 * @since 22/3/18.
 */

@ActivityScoped
@Module
public class LandingUtilModule {

    @ActivityScoped
    @Provides
    SocialShare instaShare(LandingActivity landingPage) {
        return new SocialShare(landingPage);
    }

    @ActivityScoped
    @Provides
    List<Data> socialLists() {
        return new ArrayList<Data>();
    }

    @ActivityScoped
    @Provides
    SocialAdapter getSocialAdapter(List<Data> arrayList, LandingActivity mContext, TypefaceManager typefaceManager) {
        return new SocialAdapter(arrayList, mContext, typefaceManager);
    }



    @ActivityScoped
    @Provides
    List<StoryPost> storyPost() {
        return new ArrayList<StoryPost>();
    }

    @ActivityScoped
    @Provides
    List<StoryData> storyData() {
        return new ArrayList<StoryData>();
    }

    @ActivityScoped
    @Provides
    StoriesAdapter getStoriesAdapter(List<StoryData> data, TypefaceManager typefaceManager) {
        return new StoriesAdapter(data, typefaceManager);
    }

    @ActivityScoped
    @Provides
    ImageSourcePicker imageSourcePicker(LandingActivity activity) {
        return new ImageSourcePicker(activity, true);
    }

    @ActivityScoped
    @Provides
    ArrayList<Header> headers() {
        return new ArrayList<Header>();
    }

    @ActivityScoped
    @Provides
    HeaderAdapter headerAdapter(LandingActivity context, TypefaceManager typefaceManager, ArrayList<Header> headers) {
        return new HeaderAdapter(context, typefaceManager, headers);
    }


    @ActivityScoped
    @Provides
    ArrayList<Data> content() {
        return new ArrayList<Data>();
    }

    @ActivityScoped
    @Provides
    TrendingContentAdapter contentAdapter(LandingActivity context, TypefaceManager typefaceManager, ArrayList<Data> data) {
        return new TrendingContentAdapter(context, typefaceManager, data);
    }

    @ActivityScoped
    @Provides
    PreviewActivity previewActivity() {
        return new PreviewActivity();
    }

    /*@ActivityScoped
    @Provides
    StaggeredGridLayoutManager getStaggeredGridLayoutManager()
    {
        return new StaggeredGridLayoutManager(Constants.ClubPostFragment.COLUMNS, LinearLayoutManager.VERTICAL);
    }*/
}
