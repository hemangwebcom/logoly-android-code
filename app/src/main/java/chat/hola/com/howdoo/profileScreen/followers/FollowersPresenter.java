package chat.hola.com.howdoo.profileScreen.followers;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.models.ConnectionObserver;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.profileScreen.discover.contact.pojo.FollowResponse;
import chat.hola.com.howdoo.profileScreen.followers.Model.FollowersResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 19/3/18.
 */

public class FollowersPresenter implements FollowersContract.Presenter {

    static final String TAG = FollowersPresenter.class.getSimpleName();
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;

    @Inject
    FollowersContract.View view;
    @Inject
    HowdooService service;
    @Inject
    NetworkConnector networkConnector;

    @Inject
    public FollowersPresenter() {
    }

    @Override
    public void init() {
        view.applyFont();
        view.recyclerViewSetup();
    }


    @Override
    public void loadFollowersData(int skip, int limit, String userId) {
        isLoading = true;
        view.isDataLoading(true);
        service.getFollowers(AppController.getInstance().getApiToken(), Constants.LANGUAGE,
                userId, skip, limit)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowersResponse>>() {
                    @Override
                    public void onNext(Response<FollowersResponse> followersResponse) {
                        if (followersResponse.code() == 200) {
                            isLastPage = followersResponse.body().getData().size() < PAGE_SIZE;
                            Log.w(TAG, "followers fetched successfully");
                            view.showFollowers(followersResponse.body().getData());
                        } else if (followersResponse.code() == 401)
                            view.sessionExpired();
                        view.isDataLoading(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        Log.e(TAG, "failed to get Followers list!!");
                        view.isDataLoading(false);
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }

                });
    }

    @Override
    public void loadFolloweesData(int skip, int limit, String userId) {
        isLoading = true;
        view.isDataLoading(true);
        service.getFollowees(AppController.getInstance().getApiToken(), Constants.LANGUAGE,
                userId, skip, limit)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowersResponse>>() {
                    @Override
                    public void onNext(Response<FollowersResponse> followersResponse) {
                        if (followersResponse.code() == 200) {
                            isLastPage = followersResponse.body().getData().size() < PAGE_SIZE;
                            Log.w(TAG, "followees fetched successfully");
                            view.showFollowees(followersResponse.body().getData());
                        } else if (followersResponse.code() == 401)
                            view.sessionExpired();
                        view.isDataLoading(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        Log.e(TAG, "failed to get Followees list!!");
                        view.isDataLoading(false);
                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                    }
                });
    }

    @Override
    public void follow(String followingId, boolean refollow) {
        Map<String, Object> params = new HashMap<>();
        params.put("followingId", followingId);
        params.put("refollow", refollow);
        service.follow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> body) {
                        Log.w(TAG, body.toString());
                        //view.invalidateBtn(index,true);
                        Log.w(TAG, "followed successfully");
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        Log.e(TAG, "failed to follow: " + e.getMessage());
                        //view.invalidateBtn(index,false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void unFollow(String followingId) {
        service.unfollow(AppController.getInstance().getApiToken(), "en",
                followingId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> body) {
                        Log.w(TAG, body.toString());
                        Log.w(TAG, "unfollowed successfully");
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                        Log.e(TAG, "failed to unFollow: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void callApiOnScroll(boolean isFollowerPage, String userId, int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                if (isFollowerPage) {
                    loadFollowersData(PAGE_SIZE * page, PAGE_SIZE, userId);
                } else {
                    loadFolloweesData(PAGE_SIZE * page, PAGE_SIZE, userId);
                }
            }
        }
    }

    public void block(String currentUserId, String type) {
        Map<String, String> map = new HashMap<>();
        map.put("reason", type);
        service.block(AppController.getInstance().getApiToken(), Constants.LANGUAGE, currentUserId, type, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            view.blocked();
                            view.reload();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void addToContact(String currentUserId) {
        Map<String, Object> params = new HashMap<>();
        params.put("contactId", currentUserId);
        service.addToContact(AppController.getInstance().getApiToken(), "en", params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.reload();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.isInternetAvailable(networkConnector.isConnected());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteContact(String currentUserId) {
        service.deleteContact(AppController.getInstance().getApiToken(), Constants.LANGUAGE, currentUserId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            view.showMessage("Contact delete", -1);
                            view.reload();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
