package chat.hola.com.howdoo.profileScreen.discover.contact.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author 3Embed.
 * @since 02/03/18.
 */

public class Contact implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("localNumber")
    @Expose
    private String localNumber;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("follow")
    @Expose
    private Follow follow;
    @SerializedName("isAllFollow")
    @Expose
    private boolean isAllFollow;
    @SerializedName("private")
    @Expose
    private Integer _private;
    @SerializedName("followStatus")
    @Expose
    private Integer followStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocalNumber() {
        return localNumber;
    }

    public void setLocalNumber(String localNumber) {
        this.localNumber = localNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Follow getFollow() {
        return follow;
    }

    public void setFollow(Follow follow) {
        this.follow = follow;
    }

    private int profileImg;
    private String profileName;
    private int isFollowing;

    public Contact() {
    }

    public Contact(int profileImg, String profileName, int isFollowing) {
        this.profileImg = profileImg;
        this.profileName = profileName;
        this.isFollowing = isFollowing;
    }

    public int getProfileImg() {
        return profileImg;
    }

    public String getProfileName() {
        return profileName;
    }

    @SerializedName("type")
    @Expose
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public int isFollowing() {
        return isFollowing;
    }

    public void setFollowing(int following) {
        isFollowing = following;
    }

    public boolean isAllFollow() {
        return isAllFollow;
    }

    public void setAllFollow(boolean allFollow) {
        isAllFollow = allFollow;
    }

    public Integer get_private() {
        return _private;
    }

    public void set_private(Integer _private) {
        this._private = _private;
    }

    public Integer getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(Integer followStatus) {
        this.followStatus = followStatus;
    }
}
