package chat.hola.com.howdoo.socialDetail.video_manager;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 12/1/2018.
 */
public interface ClickListner {
    void like(boolean isChecked, String postId);

    void disLike(boolean isChecked, String postId);

    void favorite(boolean isChecked, String postId);

    void comment(String postId);

    void send();

    void profile(String userId);

    void category();

    void club();

    void music(String musicId, String musicName);

}
