package chat.hola.com.howdoo.home.trending.model;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.howdoo.dubly.R;

import java.util.ArrayList;

import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 6/18/2018.
 */

public class TrendingContentAdapter extends RecyclerView.Adapter<TrendingContentAdapter.ViewHolder> {
    private TypefaceManager typefaceManager;
    private Context context;
    private ArrayList<Data> content = new ArrayList<>();
    private ClickListner clickListner;

    public TrendingContentAdapter(LandingActivity context, TypefaceManager typefaceManager, ArrayList<Data> data) {
        this.content = data;
        this.context = context;
        this.typefaceManager = typefaceManager;
    }

    public void setPostListner(ClickListner listner) {
        this.clickListner = listner;
    }

    void refresh(ArrayList<Data> data) {
        this.content = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item_content, parent, false);
        return new TrendingContentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Data data = content.get(position);
        if (data.getMediaType1() == 1) {
            //video
            DrawableRequestBuilder<String> thumbnail = Glide.with(context).load(data.getImageUrl1().replace("upload/", "upload/vs_20,e_loop/").replace("mp4", "jpg").replace(".mov", ".jpg")).diskCacheStrategy(DiskCacheStrategy.ALL);
            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(holder.imageView);
            Glide.with(context).load(data.getImageUrl1().replace("upload/", "upload/vs_20,e_loop/")
                    .replace("mp4", "gif").replace(".mov", ".gif"))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_default))
                    .thumbnail(thumbnail)
                    .into(imageViewTarget);
        } else {
            //image
            Glide.with(context).load(data.getImageUrl1())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_default))
                    .into(holder.imageView);
        }
        holder.imageButton.setVisibility(content.get(position).getMediaType1() == 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        ImageButton imageButton;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.ivImage);
            imageButton = (ImageButton) itemView.findViewById(R.id.ibPlay);
            imageView.setOnClickListener(this);
            imageButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.ivImage || view.getId() == R.id.ibPlay) {
                //   clickListner.onPostClick(getAdapterPosition(), imageView);
                Intent intent = new Intent(context, SocialDetailActivity.class);
                intent.putExtra("postId", content.get(getAdapterPosition()).getPostId());
                context.startActivity(intent);
            }
        }
    }

    public interface ClickListner {
        void onPostClick(int position, View view);
    }
}
