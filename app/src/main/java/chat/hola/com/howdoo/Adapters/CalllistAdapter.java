package chat.hola.com.howdoo.Adapters;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.howdoo.dubly.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Calls.CallingApis;
import chat.hola.com.howdoo.Calls.Common;
import chat.hola.com.howdoo.Database.CouchDbController;
import chat.hola.com.howdoo.ModelClasses.CallItem;
import chat.hola.com.howdoo.Utilities.TextDrawable;
import chat.hola.com.howdoo.Utilities.Utilities;
import chat.hola.com.howdoo.ViewHolders.ViewHolderCall;
import chat.hola.com.howdoo.home.connect.CallsFragment;

/**
 * Created by moda on 24/08/17.
 */

public class CalllistAdapter extends RecyclerView.Adapter<ViewHolderCall> implements Filterable {

    private Context mcontext;
    private int position = 0;

    private ArrayList<CallItem> mOriginalListData = new ArrayList<>();
    private ArrayList<CallItem> mFilteredListData;


    private CouchDbController db = AppController.getInstance().getDbController();


    private CallsFragment fragment;


    //   private Typeface tf;
    private String str = "";


    private int density;

    private Drawable drawable1, drawable2;


    public CalllistAdapter(Context activity, ArrayList<CallItem> mListData, CallsFragment fragment) {

        this.mcontext = activity;
        this.mOriginalListData = mListData;
        this.mFilteredListData = mListData;
        this.fragment = fragment;
        // tf = AppController.getInstance().getMediumFont();
        density = (int) mcontext.getResources().getDisplayMetrics().density;
        drawable1 = ContextCompat.getDrawable(mcontext, R.drawable.call_incoming_call_icon);
        drawable2 = ContextCompat.getDrawable(mcontext, R.drawable.call_outgoing_call_icon);
        str = mcontext.getString(R.string.VideoCall);
    }

    @Override
    public ViewHolderCall onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.call_item, parent, false);

        return new ViewHolderCall(itemView);
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    @Override
    public void onBindViewHolder(final ViewHolderCall vh, int position) {


        final CallItem callItem = mFilteredListData.get(position);


        if (callItem != null) {
            this.position = vh.getAdapterPosition();

//            vh.callTypeTv.setTypeface(tf, Typeface.NORMAL);
//            vh.name.setTypeface(tf, Typeface.NORMAL);
//
//            vh.callTime.setTypeface(tf, Typeface.NORMAL);

            vh.name.setText(callItem.getReceiverName());
            vh.callTypeTv.setText("- " + callItem.getCallType());


            if (callItem.isCallNotAllowed()) {

                vh.callTypeIv.setVisibility(View.GONE);
            } else {
                vh.callTypeIv.setVisibility(View.VISIBLE);
                if (callItem.getCallType().equals(str)) {
                    vh.callTypeIv.setImageDrawable(ContextCompat.getDrawable(mcontext, R.drawable.ic_video));
                } else {
                    vh.callTypeIv.setImageDrawable(ContextCompat.getDrawable(mcontext,R.drawable.ic_old_handphone));
                }
            }
            vh.name.setTextColor(Color.parseColor("#252525"));

            if (callItem.getReceiverImage() != null && !callItem.getReceiverImage().isEmpty()) {

                try {
                    Glide.with(mcontext).load(callItem.getReceiverImage()).asBitmap()


                            .centerCrop().placeholder(R.drawable.chat_attachment_profile_default_image_frame).
                            into(new BitmapImageViewTarget(vh.receiverImage) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(mcontext.getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    vh.receiverImage.setImageDrawable(circularBitmapDrawable);
                                }
                            });


                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            } else {

                try {
                    vh.receiverImage.setImageDrawable(TextDrawable.builder()


                            .beginConfig()
                            .textColor(Color.WHITE)
                            .useFont(Typeface.DEFAULT)
                            .fontSize(24 * density) /* size in px */
                            .bold()
                            .toUpperCase()
                            .endConfig()


                            .buildRound((callItem.getReceiverName().trim()).charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(vh.getAdapterPosition() % 19))));

                } catch (IndexOutOfBoundsException e) {
//                    vh.receiverImage.setImageDrawable(TextDrawable.builder()
//
//
//                            .beginConfig()
//                            .textColor(Color.WHITE)
//                            .useFont(Typeface.DEFAULT)
//                            .fontSize(24 * density) /* size in px */
//                            .bold()
//                            .toUpperCase()
//                            .endConfig()
//
//
//                            .buildRound("C", Color.parseColor(AppController.getInstance().getColorCode(vh.getAdapterPosition() % 19))));
                } catch (NullPointerException e) {
//                    vh.receiverImage.setImageDrawable(TextDrawable.builder()
//
//
//                            .beginConfig()
//                            .textColor(Color.WHITE)
//                            .useFont(Typeface.DEFAULT)
//                            .fontSize(24 * density) /* size in px */
//                            .bold()
//                            .toUpperCase()
//                            .endConfig()
//
//
//                            .buildRound("C", Color.parseColor(AppController.getInstance().getColorCode(vh.getAdapterPosition() % 19))));
                }
            }

            if (callItem.isCallInitiated()) {


                vh.callDialledIv.setImageDrawable(drawable1);

            } else {


                vh.callDialledIv.setImageDrawable(drawable2);
            }


            String[] myDate = Utilities.tsFromGmtToLocalTimeZone(callItem.getCallInitiateTime()).split(" ");


            String last = convert24to12hourformat(myDate[1]);


            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            try {
                Date date = format.parse(myDate[0]);
                boolean isSameWeek = isDateInCurrentWeek(date);

                Calendar calendar = Calendar.getInstance();

                calendar.setTime(date);


                String[] days = mcontext.getResources().getStringArray(R.array.days);
                String day = "";
                if (calendar.get(Calendar.DAY_OF_WEEK) <= 6) {


                    day = days[calendar.get(Calendar.DAY_OF_WEEK) - 1];
                } else {
                    day = days[0];
                }

                if (isSameWeek) {
                    vh.callTime.setText(day + ",  " + last);
                } else {
                    vh.callTime.setText(myDate[0] + " " + last);
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }


            vh.callTypeIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(mcontext) || !Settings.canDrawOverlays(mcontext)) {
                            if (!Settings.System.canWrite(mcontext)) {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                                intent.setData(Uri.parse("package:" + mcontext.getPackageName()));
                                mcontext.startActivity(intent);
                            }

                            //If the draw over permission is not available open the settings screen
                            //to grant the permission.

                            if (!Settings.canDrawOverlays(mcontext)) {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                        Uri.parse("package:" + mcontext.getPackageName()));
                                mcontext.startActivity(intent);
                            }
                        } else {

                            showCallTypeChooserPopup(callItem);

                        }
                    } else {
                        showCallTypeChooserPopup(callItem);

                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mFilteredListData.size();
    }


    public String convert24to12hourformat(String d) {

        String datein12hour = null;

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm", Locale.US);
            final Date dateObj = sdf.parse(d);


            datein12hour = new SimpleDateFormat("h:mm a", Locale.US).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }


        return datein12hour;

    }


    private static boolean isDateInCurrentWeek(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);
        return week == targetWeek && year == targetYear;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredListData = (ArrayList<CallItem>) results.values;
                CalllistAdapter.this.notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<CallItem> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = mOriginalListData;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());


                }


                mFilteredListData = filteredResults;

                fragment.showNoSearchResults(constraint, mFilteredListData.size() == mOriginalListData.size());

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }
    private ArrayList<CallItem> getFilteredResults(String constraint) {
        ArrayList<CallItem> results = new ArrayList<>();

        for (CallItem item : mOriginalListData) {
            if (item.getReceiverName().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

    public List<CallItem> getList() {


        return mFilteredListData;
    }


    public void requestAudioCall(CallItem contactsCallItem) {


        if (contactsCallItem == null) {


            contactsCallItem = mFilteredListData.get(position);
        }


        Map<String, Object> callItem = new HashMap<>();


        String callId = AppController.getInstance().randomString();
        callItem.put("receiverName", contactsCallItem.getReceiverName());
        callItem.put("receiverImage", contactsCallItem.getReceiverImage());
        callItem.put("receiverUid", contactsCallItem.getReceiverUid());
        callItem.put("receiverIdentifier", contactsCallItem.getReceiverIdentifier());
        callItem.put("callTime", Utilities.tsInGmt());
        callItem.put("callInitiated", true);
        callItem.put("callId", callId);
        callItem.put("callType", mcontext.getResources().getString(R.string.AudioCall));


        db.addNewCall(AppController.getInstance().getCallsDocId(), callItem);
        Common.callerName = contactsCallItem.getReceiverName();

        CallingApis.initiateCall(mcontext, contactsCallItem.getReceiverUid(), contactsCallItem.getReceiverName(), contactsCallItem.getReceiverImage(),
                "0", contactsCallItem.getReceiverIdentifier(), callId);


    }


    public void requestVideoCall(CallItem contactsCallItem) {

        if (contactsCallItem == null) {


            contactsCallItem = mFilteredListData.get(position);
        }


        Map<String, Object> callItem = new HashMap<>();

        String callId = AppController.getInstance().randomString();


        callItem.put("receiverName", contactsCallItem.getReceiverName());
        callItem.put("receiverImage", contactsCallItem.getReceiverImage());
        callItem.put("receiverUid", contactsCallItem.getReceiverUid());
        callItem.put("receiverIdentifier", contactsCallItem.getReceiverIdentifier());
        callItem.put("callTime", Utilities.tsInGmt());
        callItem.put("callInitiated", true);
        callItem.put("callId", callId);
        callItem.put("callType", mcontext.getResources().getString(R.string.VideoCall));


        db.addNewCall(AppController.getInstance().getCallsDocId(), callItem);
        Common.callerName = contactsCallItem.getReceiverName();
        CallingApis.initiateCall(mcontext, contactsCallItem.getReceiverUid(), contactsCallItem.getReceiverName(), contactsCallItem.getReceiverImage(),
                "1", contactsCallItem.getReceiverIdentifier(), callId);
    }

    private void showCallTypeChooserPopup(final CallItem callItem) {


        if (!AppController.getInstance().isActiveOnACall()) {
            final AlertDialog.Builder builder;


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(mcontext, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(mcontext);
            }

            builder.setTitle(mcontext.getResources().getString(R.string.Start_Call));


            builder.setMessage(mcontext.getResources().getString(R.string.Start_Audio_Video_call));
            builder.setPositiveButton(mcontext.getResources().getString(R.string.AudioCall), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {


                    if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.RECORD_AUDIO)
                            != PackageManager.PERMISSION_GRANTED) {


                        fragment.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                                71);

                    } else {


                        requestAudioCall(callItem);

                    }


                }
            });
            builder.setNegativeButton(mcontext.getResources().getString(R.string.VideoCall), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {


                    ArrayList<String> arr1 = new ArrayList<>();


                    if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {

                        arr1.add(Manifest.permission.CAMERA);
                    }

                    if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.RECORD_AUDIO)
                            != PackageManager.PERMISSION_GRANTED) {


                        arr1.add(Manifest.permission.RECORD_AUDIO);

                    }
                    if (arr1.size() > 0) {

                        fragment.requestPermissions(arr1.toArray(new String[arr1.size()]),
                                72);
                    } else {
                        requestVideoCall(callItem);


                    }


                }
            });


            if (AppController.getInstance().canPublish()) {
                ((Activity) mcontext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        builder.show();
                    }
                });
            } else {
                ((Activity) mcontext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mcontext, mcontext.getResources().getString(R.string.No_Internet_Connection_Available),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        } else {

            ((Activity) mcontext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mcontext, mcontext.getResources().getString(R.string.call_initiate),
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

}
