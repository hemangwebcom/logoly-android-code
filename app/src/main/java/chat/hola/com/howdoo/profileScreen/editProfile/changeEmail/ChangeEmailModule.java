package chat.hola.com.howdoo.profileScreen.editProfile.changeEmail;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h>ChangeEmailModule</h>
 * @author 3Embed.
 * @since 19/3/18.
 */

@ActivityScoped
@Module
public interface ChangeEmailModule {

    @ActivityScoped
    @Binds
    ChangeEmailContract.Presenter changeEmailPresenter(ChangeEmailPresenter presenter);

    @ActivityScoped
    @Binds
    ChangeEmailContract.View changeEmailView(ChangeEmail changeEmail);

}
