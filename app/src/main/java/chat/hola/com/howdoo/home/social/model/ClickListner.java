package chat.hola.com.howdoo.home.social.model;

import android.view.View;

/**
 * <h1>ClickListner</h1>
 *
 * @author 3Embed
 * @since 24/2/2018.
 */

public interface ClickListner {
    void onItemClick(int position, View view);

    void onUserClick(int position);

    void onChannelClick(int position);
}
