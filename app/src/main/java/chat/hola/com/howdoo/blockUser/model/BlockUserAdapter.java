package chat.hola.com.howdoo.blockUser.model;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.howdoo.dubly.R;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.TagSpannable;
import chat.hola.com.howdoo.Utilities.TimeAgo;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.Utilities.UserSpannable;

/**
 * <h1>BlockUserAdapter</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class BlockUserAdapter extends RecyclerView.Adapter<ViewHolder> {
    private TypefaceManager typefaceManager;
    private ClickListner clickListner;
    private List<User> comments;
    private Context context;

    @Inject
    public BlockUserAdapter(List<User> comments, Activity mContext, TypefaceManager typefaceManager) {
        this.typefaceManager = typefaceManager;
        this.comments = comments;
        this.context = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.block_item, parent, false);
        return new ViewHolder(itemView, typefaceManager, clickListner);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (getItemCount() > 0) {
            final User comment = comments.get(position);
            holder.tvUserName.setText(comment.getUserName());
            holder.ivProfilePic.setImageURI(comment.getProfilePic());
        }
    }


    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }
}