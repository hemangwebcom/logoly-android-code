package chat.hola.com.howdoo.dublycategory.favourite;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.dublycategory.fagments.DubFragmentContract;
import chat.hola.com.howdoo.dublycategory.fagments.DubFragmentPresenter;
import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 23/2/18.
 */

@FragmentScoped
@Module
public interface DubFavFragmentModule {

    @FragmentScoped
    @Binds
    DubFragmentContract.Presenter presenter(DubFragmentPresenter presenter);

   /* @FragmentScoped
    @Binds
    DubFavouriteFragmentContract.View view(StoryFragment fragment);*/
}
