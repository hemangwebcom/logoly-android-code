package chat.hola.com.howdoo.profileScreen.discover.contact;

import android.app.Activity;

import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverActivity;
import chat.hola.com.howdoo.profileScreen.story.StoryContract;
import chat.hola.com.howdoo.profileScreen.story.StoryPresenter;
import dagger.Binds;
import dagger.Module;

/**
 * <h>ContactModule</h>
 * @author 3Embed.
 * @since 23/2/18.
 */

@FragmentScoped
@Module
public interface ContactModule {

    @FragmentScoped
    @Binds
    ContactContract.Presenter presenter(ContactPresenter presenter);

}
