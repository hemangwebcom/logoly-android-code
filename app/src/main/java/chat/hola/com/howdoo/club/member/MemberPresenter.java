package chat.hola.com.howdoo.club.member;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.comment.model.ClickListner;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.profileScreen.followers.Model.FollowersResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>BlockUserPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class MemberPresenter implements MemberContract.Presenter, chat.hola.com.howdoo.club.member.ClickListner {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;
    @Inject
    HowdooService service;
    @Inject
    MemberContract.View view;
    @Inject
    MemberModel model;
    @Inject
    NetworkConnector networkConnector;
    List<String> members;

    @Inject
    MemberPresenter() {
    }

    @Override
    public void setMembers(List<String> members) {
        this.members = members;
    }

    @Override
    public void callApiOnScroll(String postId, int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                getMembers(PAGE_SIZE * page, PAGE_SIZE);
            }
        }
    }

    @Override
    public void createClub(Map<String, Object> params) {
        ArrayList<String> members = model.getSelectedUsers();
        if (members == null)
            members = new ArrayList<>();
//        if (members.isEmpty() || members.size() < 2) {
//            view.showMessage("Please add at least two user", -1);
//        } else {
        params.put("members", members);

        service.createClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                view.clubCreated();
                                break;
                            case 401:
                                view.sessionExpired();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
//        }
    }

    @Override
    public void getClubMembers(String clubId) {
        service.getClubMembers(AppController.getInstance().getApiToken(), Constants.LANGUAGE, clubId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowersResponse>>() {
                    @Override
                    public void onNext(Response<FollowersResponse> response) {
                        if (response.code() == 200)

                            model.setData(response.body().getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("", "onError: ");
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void removeFromClub(String clubId, String userId) {
        service.removeFromClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, clubId, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.reloadData();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("", "onError: " + e.toString());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void blockFromClub(String clubId, String userId) {
        service.blockFromClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, clubId, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.reloadData();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("", "onError: " + e.toString());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void unblockFromClub(String clubId, String userId) {
        service.unblockFromClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, clubId, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.reloadData();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("", "onError: " + e.toString());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void getClubPendingRequest(String clubId) {
        service.getClubPendingRequest(AppController.getInstance().getApiToken(), Constants.LANGUAGE, clubId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowersResponse>>() {
                    @Override
                    public void onNext(Response<FollowersResponse> response) {
                        if (response.code() == 200)
                            model.setData(response.body().getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("", "onError: " + e.toString());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void addMembers(String clubId) {
        Map<String, Object> params = new HashMap<>();
        ArrayList<String> members = model.getSelectedUsers();
        params.put("clubId", clubId);

        if (members.isEmpty()) {
            view.showMessage("Please add at least one user", -1);
        } else {

            params.put("members", members);
            service.addMembers(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                        @Override
                        public void onNext(Response<ResponseBody> response) {
                            if (response.code() == 200)
                                view.memberAdded();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i("", "onError: " + e.toString());
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
    }

    @Override
    public void getMembers(int offset, int limit) {
        service.getContacts(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowersResponse>>() {
                    @Override
                    public void onNext(Response<FollowersResponse> response) {
                        if (response.code() == 200) {
                            if (members != null)
                                model.setData1(members, response.body().getData());
                            else
                                model.setData(response.body().getData());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public chat.hola.com.howdoo.club.member.ClickListner getPresenter() {
        return this;
    }

    @Override
    public void onUserClick(int position) {
        view.openProfile(model.getUserId(position));
    }

    @Override
    public void itemSelect(int position, boolean isSelected) {
        model.selectItem(position, isSelected);
    }

    @Override
    public void openPopup(String userId, boolean isBlocked) {
        view.showPopup(userId, isBlocked);
    }

    @Override
    public void requestAction(String userId, boolean accept) {
        Map<String, Object> map = new HashMap<>();
        map.put("clubId", MemberActivity.clubId);
        map.put("memberId", userId);
        map.put("accept", accept);
        service.joinAction(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (response.code() == 200)
                            view.reloadData();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
