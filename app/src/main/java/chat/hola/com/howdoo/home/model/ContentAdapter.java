package chat.hola.com.howdoo.home.model;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.trending.TrendingFragment;
import chat.hola.com.howdoo.profileScreen.addChannel.AddChannelActivity;
import chat.hola.com.howdoo.profileScreen.channel.Model.ChannelData;

/**
 * <h>ContentAdapter.class</h>
 * <p>
 * This Adapter is used by {@link TrendingFragment} contentRecyclerView.
 *
 * @author 3Embed
 * @since 13/2/18.
 */

public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ViewHolder> {

    private TypefaceManager typefaceManager;
    private Context context;
    ArrayList<ChannelData> channelDataList = new ArrayList<>();
    private AdapterClickCallback clickCallback;
    private Integer POS_TAG = 0;

    public interface AdapterClickCallback {

        void onChannelDetail(int pos);

        void onPostDetail(Data data, int type, View view);
    }

    public void setCallback(AdapterClickCallback callback) {
        this.clickCallback = callback;
    }

    @Inject
    public ContentAdapter(Context context, TypefaceManager typefaceManager) {
        this.context = context;
        this.typefaceManager = typefaceManager;
    }

    public void setData(ArrayList<ChannelData> channelDataList) {
        this.channelDataList = channelDataList;
        notifyDataSetChanged();
    }

    @Override
    public ContentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trending_content_item, parent, false);
        return new ContentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContentAdapter.ViewHolder holder, int position) {
        try {
            ChannelData channelData = channelDataList.get(position);


            String channelPic = channelData.getChannelImageUrl();
            if (channelPic != null) {
                Glide.with(context)
                        .load(channelPic)
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.ivItemTopImage);
            }
            String channelName = channelData.getChannelName();
            holder.tvItemTopTitle.setText((channelName == null) ? "Title" : channelName);
            holder.tvSubscribe.setText("");

            holder.ivArrow.setImageDrawable(channelData.getUserId().equals(AppController.getInstance().getUserId()) ? context.getResources().getDrawable(R.drawable.ic_edit) : context.getResources().getDrawable(R.drawable.ic_next_arrow_icon));

            if (channelData.getPrivate() && !channelData.getUserId().equals(AppController.getInstance().getUserId())) {
                holder.rlData.setVisibility(View.GONE);
                holder.ivPrivacy.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_lock));
            } else {
                holder.rlData.setVisibility(View.VISIBLE);
                holder.ivPrivacy.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_public));
                List<Data> dataList = channelData.getData();
                if (!dataList.isEmpty()) {
                    if (dataList.size() >= 1) {
                        loadImage(holder, dataList.get(0).getMediaType1(), holder.ibPlayOne, dataList.get(0).getImageUrl1().replace("mp4", "jpg"), holder.ivImageOne);
                        //holder.ibPlay.setTag(0);
                    }

                    if (dataList.size() >= 2) {
                        loadImage(holder, dataList.get(1).getMediaType1(), holder.ibPlayTwo, dataList.get(1).getImageUrl1().replace("mp4", "jpg"), holder.ivImageTwo);
                        //holder.ibPlay.setTag(1);
                    }

                    if (dataList.size() >= 3) {
                        loadImage(holder, dataList.get(2).getMediaType1(), holder.ibPlayThree, dataList.get(2).getImageUrl1().replace("mp4", "jpg"), holder.ivImageThree);
                        //holder.ibPlay.setTag(2);
                    }

                    if (dataList.size() >= 4) {
                        loadImage(holder, dataList.get(3).getMediaType1(), holder.ibPlayFour, dataList.get(3).getImageUrl1().replace("mp4", "jpg"), holder.ivImageFour);
                        // holder.ibPlay.setTag(3);
                    }

                    if (dataList.size() > 4) {
                        holder.viewImageFilter.setVisibility(View.VISIBLE);
                        holder.tvMoreImage.setVisibility(View.VISIBLE);
                        holder.tvMoreImage.setText("+" + (dataList.size() - 4));
                    } else {
                        holder.viewImageFilter.setVisibility(View.GONE);
                        holder.tvMoreImage.setVisibility(View.GONE);
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
    }

    private void loadImage(ContentAdapter.ViewHolder holder, int type, ImageButton ibPlay, String mediaUrl, ImageView ivImageView) {

        if (type == 0) {
            //image media
            ibPlay.setVisibility(View.GONE);
        } else {
            //video media
            ibPlay.setVisibility(View.VISIBLE);
        }


        Glide.with(context)
                .load(mediaUrl)
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivImageView);
    }

    @Override
    public int getItemCount() {
        return channelDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.ivArrow)
        ImageView ivArrow;
        @BindView(R.id.ivPrivacy)
        ImageView ivPrivacy;
        @BindView(R.id.ivItemTopImage)
        ImageView ivItemTopImage;
        @BindView(R.id.ibPlayOne)
        ImageButton ibPlayOne;
        @BindView(R.id.ibPlayTwo)
        ImageButton ibPlayTwo;
        @BindView(R.id.ibPlayThree)
        ImageButton ibPlayThree;
        @BindView(R.id.ibPlayFour)
        ImageButton ibPlayFour;
        @BindView(R.id.rlData)
        RelativeLayout rlData;
        @BindView(R.id.ivImageOne)
        ImageView ivImageOne;
        @BindView(R.id.ivImageTwo)
        ImageView ivImageTwo;
        @BindView(R.id.ivImageThree)
        ImageView ivImageThree;
        @BindView(R.id.ivImageFour)
        ImageView ivImageFour;
        @BindView(R.id.viewImgFilter)
        View viewImageFilter;
        @BindView(R.id.tvItemTitle)
        TextView tvItemTopTitle;
        @BindView(R.id.llRowHeader)
        RelativeLayout llRowHeader;
        @BindView(R.id.llImageContainer)
        LinearLayout llImageContainer;
        @BindView(R.id.tvMoreImage)
        TextView tvMoreImage;
        @BindView(R.id.tvItemSubscribe)
        TextView tvSubscribe;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvItemTopTitle.setTypeface(typefaceManager.getMediumFont());
            tvSubscribe.setTypeface(typefaceManager.getMediumFont());
            tvMoreImage.setTypeface(typefaceManager.getMediumFont());

            ivImageOne.setOnClickListener(this);
            ivImageTwo.setOnClickListener(this);
            ivImageThree.setOnClickListener(this);
            ivImageFour.setOnClickListener(this);
            ivArrow.setOnClickListener(this);

            tvMoreImage.setOnClickListener(this);
            llRowHeader.setOnClickListener(this);
            ibPlayOne.setOnClickListener(this);
            ibPlayTwo.setOnClickListener(this);
            ibPlayThree.setOnClickListener(this);
            ibPlayFour.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                if (channelDataList.size() > 0) {
                    List<Data> dataList = channelDataList.get(getAdapterPosition()).getData();
                    Data data;
                    String channelId;
                    switch (v.getId()) {
                        case R.id.ivArrow:
                            boolean isMine = (channelDataList.get(getAdapterPosition()).getUserId()).equals(AppController.getInstance().getUserId());
                            if (isMine) {
                                Intent intent = new Intent(context, AddChannelActivity.class);
                                intent.putExtra("isEdit", true);
                                intent.putExtra("data", channelDataList.get(getAdapterPosition()));
                                context.startActivity(intent);
                            } else {
                                channelId = channelDataList.get(getAdapterPosition()).getId();
                                if (channelId != null)
                                    if (clickCallback != null)
                                        clickCallback.onChannelDetail(getAdapterPosition());
                            }
                            break;
                        case R.id.tvMoreImage:
                            //detail page.
                            channelId = channelDataList.get(getAdapterPosition()).getId();
                            if (channelId != null)
                                if (clickCallback != null)
                                    clickCallback.onChannelDetail(getAdapterPosition());
                            break;
                        case R.id.llRowHeader:
                            //detail page.
                            channelId = channelDataList.get(getAdapterPosition()).getId();
                            if (channelId != null)
                                if (clickCallback != null)
                                    clickCallback.onChannelDetail(getAdapterPosition());
                            break;
                        case R.id.ivImageOne:
                            if (dataList.size() < 1)
                                break;
                            data = dataList.get(0);
                            if (clickCallback != null)
                                clickCallback.onPostDetail(data, data.getMediaType1(), v);
                            break;
                        case R.id.ivImageTwo:
                            if (dataList.size() < 2)
                                break;
                            data = dataList.get(1);
                            if (clickCallback != null)
                                clickCallback.onPostDetail(data, data.getMediaType1(), v);
                            break;
                        case R.id.ivImageThree:
                            if (dataList.size() < 3)
                                break;
                            data = dataList.get(2);
                            if (clickCallback != null)
                                clickCallback.onPostDetail(data, data.getMediaType1(), v);
                            break;
                        case R.id.ivImageFour:
                            if (dataList.size() < 4)
                                break;
                            data = dataList.get(3);
                            if (clickCallback != null)
                                clickCallback.onPostDetail(data, data.getMediaType1(), v);
                            break;
                        case R.id.ibPlayOne:
                            if (dataList.size() < 1)
                                break;
                            data = dataList.get(0);
                            if (clickCallback != null)
                                clickCallback.onPostDetail(data, data.getMediaType1(), v);
                            break;
                        case R.id.ibPlayTwo:
                            if (dataList.size() < 2)
                                break;
                            data = dataList.get(1);
                            if (clickCallback != null)
                                clickCallback.onPostDetail(data, data.getMediaType1(), v);
                            break;
                        case R.id.ibPlayThree:
                            if (dataList.size() < 3)
                                break;
                            data = dataList.get(2);
                            if (clickCallback != null)
                                clickCallback.onPostDetail(data, data.getMediaType1(), v);
                            break;
                        case R.id.ibPlayFour:
                            if (dataList.size() < 4)
                                break;
                            data = dataList.get(3);
                            if (clickCallback != null)
                                clickCallback.onPostDetail(data, data.getMediaType1(), v);
                            break;
                        default:
                    }
                }
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
    }
}