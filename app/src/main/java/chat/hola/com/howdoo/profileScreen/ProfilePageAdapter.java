package chat.hola.com.howdoo.profileScreen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by ankit on 22/2/18.
 */


class ProfilePageAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> fragList = new ArrayList<>();
    private ArrayList<String> fragName = new ArrayList<>();

    public ProfilePageAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String name, String fragmentName, String userId) {
        Bundle bundle = new Bundle();
        bundle.putString("name", fragmentName);
        bundle.putString("userId", userId);
        fragment.setArguments(bundle);

        fragList.add(fragment);
        fragName.add(name);
    }


    //
//    public void addFragment(Fragment fragment, String name, String fragmentName) {
//        Bundle bundle = new Bundle();
//        bundle.putString("name", fragmentName);
//        fragment.setArguments(bundle);
//
//        fragList.add(fragment);
//        fragName.add(name);
//    }

    @Override
    public Fragment getItem(int position) {
        return fragList.get(position);
    }

    @Override
    public int getCount() {
        return fragList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (fragName != null && !fragName.isEmpty())
            return fragName.get(position);
        return super.getPageTitle(position);
    }
}