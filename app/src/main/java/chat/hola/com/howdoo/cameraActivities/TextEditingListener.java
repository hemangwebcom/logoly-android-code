package chat.hola.com.howdoo.cameraActivities;

import chat.hola.com.howdoo.motionView.motionviews.widget.entity.TextEntity;

/*
 * Created by embed on 7/2/18.
 */

public interface TextEditingListener {

    void startEditingTextEntity(TextEntity textEntity);
}
