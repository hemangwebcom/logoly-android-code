package chat.hola.com.howdoo.CustomGallery;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.howdoo.dubly.R;


/**
 * Handler for the Gallery image view .
 *
 * @since 09/05/17.
 */
public class GalleryItemViewholder extends RecyclerView.ViewHolder {
    GrideSquareImageView thumb_nail;

    GalleryItemViewholder(View itemView) {
        super(itemView);
        thumb_nail = (GrideSquareImageView) itemView.findViewById(R.id.thumb_nail_image);
    }
}