package chat.hola.com.howdoo.club.detail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.howdoo.dubly.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Utilities.CountFormat;
import chat.hola.com.howdoo.club.create.CreateClubActivity;
import chat.hola.com.howdoo.club.member.MemberActivity;
import chat.hola.com.howdoo.comment.CommentActivity;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;
import dagger.android.support.DaggerAppCompatActivity;


public class ClubDetailActivity extends DaggerAppCompatActivity implements ClubPostAdapter.ClickListner, ClubDetailContract.View {

    private static final String TAG = ClubDetailActivity.class.getSimpleName();
    private String clubId;
    @Inject
    ClubDetailContract.Presenter presenter;
    @BindView(R.id.ivProfileBg)
    ImageView ivCover;
    @BindView(R.id.tvProfileName)
    TextView tvClubName;
    @BindView(R.id.tvProfileStatus)
    TextView tvClubStatus;
    @BindView(R.id.ivProfile)
    ImageView ivClubPhoto;
    @BindView(R.id.tvMemberCount)
    TextView tvMemberCount;
    @BindView(R.id.tvPostCount)
    TextView tvPostCount;
    @BindView(R.id.rvPosts)
    RecyclerView rvPosts;
    @BindView(R.id.ibGrid)
    ImageButton ibGrid;
    @BindView(R.id.ibList)
    ImageButton ibList;
    @BindView(R.id.tvAbout)
    TextView tvAbout;
    @BindView(R.id.btnEdit)
    Button btnEdit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private int itemType = 0;
    private ClubPostAdapter clubPostAdapter;
    private List<Data> posts = new ArrayList<>();
    private ClubDetailResponse.ClubDetail clubDetail;

    Menu menu;
    MenuItem pendingRequest;
    MenuItem report;
    MenuItem delete;
    MenuItem addMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_detail);
        ButterKnife.bind(this);
        clubId = getIntent().getStringExtra("clubId");
        if (!TextUtils.isEmpty(clubId)) {
            presenter.clubDetail(clubId);
            presenter.clubPosts(clubId);
        }

        ibGrid.setSelected(true);
        btnEdit.setVisibility(View.INVISIBLE);
        rvPosts.setLayoutManager(new GridLayoutManager(this, 3));
        clubPostAdapter = new ClubPostAdapter(this, posts, itemType);
        clubPostAdapter.setListener(this);
        rvPosts.setAdapter(clubPostAdapter);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        report = menu.findItem(R.id.actionReport);
        pendingRequest = menu.findItem(R.id.actionRequest);
        delete = menu.findItem(R.id.actionDelete);
        addMember = menu.findItem(R.id.actionAdd);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.club_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.actionAdd:
                Intent intent1 = new Intent(this, MemberActivity.class);
                intent1.putExtra("call", 4);
                intent1.putExtra("clubId", clubId);
                intent1.putExtra("members", (Serializable) clubDetail.getMembers());
                startActivity(intent1);
                return true;
            case R.id.actionRequest:
                Intent intent = new Intent(this, MemberActivity.class);
                intent.putExtra("call", 3);
                intent.putExtra("clubId", clubId);
                startActivity(intent);
                return true;
            case R.id.actionAbout:
                tvAbout.setVisibility(tvAbout.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                return true;
            case R.id.actionReport:
                return true;
            case R.id.actionDelete:
                presenter.deleteClub(clubDetail.getId());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.llMember)
    public void llMember() {
        Intent intent = new Intent(this, MemberActivity.class);
        intent.putExtra("call", clubDetail.getAdmin() || clubDetail.getOwner() ? 1 : 2);
        intent.putExtra("clubId", clubDetail.getId());
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    //to edit the club details pass current data
    @OnClick(R.id.btnEdit)
    public void edit() {
        Intent intent = new Intent(this, CreateClubActivity.class);
        intent.putExtra("clubDetail", clubDetail);
        startActivity(intent);
    }

    @OnClick(R.id.ibGrid)
    public void grid() {
        ibGrid.setSelected(true);
        ibList.setSelected(false);
        rvPosts.setLayoutManager(new GridLayoutManager(this, 3));
        itemType = 0;
        clubPostAdapter.setData(posts, itemType);
    }

    @OnClick(R.id.ibList)
    public void list() {
        ibGrid.setSelected(false);
        ibList.setSelected(true);
        rvPosts.setLayoutManager(new LinearLayoutManager(this));
        itemType = 1;
        clubPostAdapter.setData(posts, itemType);
    }

    @Override
    public void displayDetail(ClubDetailResponse.ClubDetail club) {
        this.clubDetail = club;
        report.setVisible(!clubDetail.getOwner() && !clubDetail.getAdmin());
        pendingRequest.setVisible(clubDetail.getAccess() == 3 && (clubDetail.getOwner() || clubDetail.getAdmin()));
        delete.setVisible(clubDetail.getOwner());
        addMember.setVisible(clubDetail.getOwner() && clubDetail.getAccess() == 2);

        btnEdit.setVisibility(club.getAdmin() || club.getOwner() ? View.VISIBLE : View.INVISIBLE);
        tvPostCount.setText("" + club.getPostCount());
        tvMemberCount.setText("" + club.getMemberCount());
        tvClubName.setText(club.getSubject());
        tvClubStatus.setText(club.getStatus());
        tvAbout.setText(club.getAbout());

        Glide.with(getBaseContext()).load(club.getImage()).asBitmap().centerCrop()
                .placeholder(R.drawable.profile_one).into(ivClubPhoto);

        Glide.with(this)
                .load(club.getCoverPic())
                .into(ivCover);
    }

    @Override
    public void setPosts(List<Data> data) {
        posts.clear();
        posts.addAll(data);
        clubPostAdapter.setData(posts, itemType);
    }

    @Override
    public void deleted() {
        finish();
    }

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {

    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    public void userBlocked() {

    }

    @Override
    public void reload() {

    }

    @Override
    public void onItemClick(int position, int type, View view) {
        Intent intent = new Intent(this, SocialDetailActivity.class);
        intent.putExtra("postId", posts.get(position).getPostId());
        intent.putExtra("call", "profile");
        startActivity(intent);
    }

    @Override
    public void onLikeClick(String postId, boolean like) {
        presenter.like(postId, like);
    }

    @Override
    public void onDisLikeClick(String postId, boolean like) {
        presenter.dislike(postId, like);
    }

    @Override
    public void onFavoriteClick(String postId, boolean like) {
        presenter.favorite(postId, like);

    }

    @Override
    public void onCommentClick(String postId) {
        startActivity(new Intent(this, CommentActivity.class).putExtra("postId", postId));
    }

}
