package chat.hola.com.howdoo.profileScreen.story;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.profileScreen.story.model.ProfilePostRequest;
import chat.hola.com.howdoo.profileScreen.story.model.StoryData;

/**
 * Created by ankit on 23/2/18.
 */

public interface StoryContract {

    interface View extends BaseView {
        void setupRecyclerView();

        void showData(List<StoryData> profilePostRequests, boolean isFirst);

        void isLoading(boolean flag);

        void showEmptyUi(boolean show);

        void noData();
    }

    interface Presenter extends BasePresenter<StoryContract.View> {
        void init();

        void loadData(int skip, int limit);

        void loadMemberData(String userId, int skip, int limit);

        void favorite(String postId);

        void unfavorite(String postId);
    }
}
