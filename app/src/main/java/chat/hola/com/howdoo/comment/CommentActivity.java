package chat.hola.com.howdoo.comment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.comment.model.CommentAdapter;
import chat.hola.com.howdoo.hastag.AutoCompleteTextView;
import chat.hola.com.howdoo.hastag.Hash_tag_people_pojo;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.InternetErrorView;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>BlockUserActivity</h1>
 * <p>All the comments appears on this screen.
 * User can add new comment also</p>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class CommentActivity extends DaggerAppCompatActivity implements AdapterView.OnItemClickListener,
        CommentContract.View, TextWatcher, AutoCompleteTextView.AutoTxtCallback {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;

    private Unbinder unbinder;
    private String postId;

    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    CommentContract.Presenter commentPresenter;
    @Inject
    CommentAdapter adapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvCommentList)
    RecyclerView rvCommentList;

    @BindView(R.id.ivUserProfilePic)
    SimpleDraweeView ivUserProfilePic;
    @BindView(R.id.etComment)
    AutoCompleteTextView etComment;
    @BindView(R.id.ibSend)
    ImageButton ibSend;
    @BindView(R.id.tvTbTitle)
    TextView tvTbTitle;
    @BindView(R.id.llNetworkError)
    InternetErrorView llNetworkError;

    private LinearLayoutManager layoutManager;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_activity);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));

        postId = getIntent().getStringExtra("postId");
        layoutManager = new LinearLayoutManager(this);
        ibSend.setEnabled(false);
        tvTbTitle.setTypeface(AppController.getInstance().getSemiboldFont());
        etComment.setTypeface(AppController.getInstance().getRegularFont());
        etComment.addTextChangedListener(this);
        ivUserProfilePic.setImageURI(sessionManager.getUserProfilePic());
        rvCommentList.setLayoutManager(layoutManager);
        rvCommentList.addOnScrollListener(recyclerViewOnScrollListener);
        adapter.setListener(commentPresenter.getPresenter());
        rvCommentList.setAdapter(adapter);
        commentPresenter.getComments(postId, 0, PAGE_SIZE);

        etComment.setOnItemClickListener(this);
        etComment.setListener(this);
        toolbarSetup();
        llNetworkError.setErrorListner(this);
    }

    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.ibSend)
    public void send() {
        if (postId != null) {
            ibSend.setEnabled(false);
            commentPresenter.addComment(postId, etComment.getText().toString());
        }
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        ibSend.setEnabled(charSequence.length() > 0);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {
        if (sessionManager != null)
            sessionManager.sessionExpired(this);
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void reload() {
        commentPresenter.getComments(postId, 0, PAGE_SIZE);
    }

    @Override
    public void commented(boolean flag) {
        if (flag) {
            if (etComment != null)
                etComment.setText("");
            int comment = Integer.parseInt(sessionManager.getCommentCount());
            sessionManager.setCommentCount(String.valueOf(++comment));
            adapter.notifyDataSetChanged();
            rvCommentList.smoothScrollToPosition(adapter.getItemCount());
            ibSend.setEnabled(true);
        }
    }

    @Override
    public void openProfile(String userId) {
        startActivity(new Intent(this, ProfileActivity.class).putExtra("userId", userId));
    }

    @Override
    public void setTag(Hash_tag_people_pojo tag) {
        etComment.updateHashTagDetails(tag);
    }

    @Override
    public void setUser(Hash_tag_people_pojo tag) {
        etComment.updateUserSearch(tag);
    }

    @Override
    public void onHashTag(String tag) {
        commentPresenter.searchHashTag(tag);
    }

    @Override
    public void onUserSearch(String tag) {
        commentPresenter.searchUserTag(tag);
    }

    @Override
    public void onClear() {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        etComment.setText(etComment.getText().toString().replace("##", "#"));
        etComment.setSelection(etComment.getText().length());
    }

    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int[] firstVisibleItemPositions = new int[PAGE_SIZE];
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            commentPresenter.callApiOnScroll(postId, firstVisibleItemPosition, visibleItemCount, totalItemCount);
        }
    };

}
