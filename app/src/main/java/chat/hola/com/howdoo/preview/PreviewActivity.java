package chat.hola.com.howdoo.preview;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.stories.StoriesFrag;
import chat.hola.com.howdoo.home.stories.model.StoryPost;
import chat.hola.com.howdoo.home.stories.model.ViewerAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.NetworkConnector;
import dagger.android.support.DaggerAppCompatActivity;

import static chat.hola.com.howdoo.home.stories.StoriesFrag.TAG;

/**
 * <h1>PreviewActivity</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 4/24/2018.
 */

public class PreviewActivity extends DaggerAppCompatActivity implements PreviewContract.View, Player.EventListener {
    private Unbinder unbinder;
    private static int PROGRESS_COUNT;

    @Inject
    SessionManager sessionManager;
    @Inject
    PreviewPresenter presenter;


    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.rlMediaContainer)
    RelativeLayout rlMediaContainer;
    @BindView(R.id.video)
    SimpleExoPlayerView video;

    @BindView(R.id.ivView)
    ImageView ivView;
    @BindView(R.id.listViewer)
    RecyclerView listViewer;
    @BindView(R.id.bottom_sheet)
    LinearLayout bottom_sheet;
    @BindView(R.id.totalViews)
    TextView totalViews;
    @BindView(R.id.progresbar)
    ProgressBar progresbar;
    @BindView(R.id.llProgressbarContainer)
    LinearLayout llProgressbarContainer;

    @Inject
    NetworkConnector networkConnector;
    private SimpleExoPlayer player;
    private List<StoryPost> storyData;
    private List<ProgressBar> progressBarList;

    private int counter = 0;
    private BottomSheetBehavior bottomSheetBehavior;
    private ViewerAdapter adapter;
    private boolean isMine;
    private boolean isImage = false;
    private boolean isPlaying = false;
    private Handler handler = new Handler();
    private int imageCounter = 0;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_activity);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        progressBarList = new ArrayList<>();
        storyData = (List<StoryPost>) getIntent().getSerializableExtra("data");


        if (!storyData.isEmpty()) {
            PROGRESS_COUNT = storyData.size();
            counter = 0;
            createProgress();
            bottomsheet();
        }

        rlMediaContainer.setOnLongClickListener(view -> {
            if (isPlaying) {
                pausePlayer();
            } else {
                restartPlayer();
            }
            return false;
        });

    }

    @OnClick(R.id.next)
    public void nextPost() {
        next();
    }

    @OnClick(R.id.prev)
    public void prevPost() {
        prev();
    }

    private void createProgress() {
        llProgressbarContainer.setWeightSum(storyData.size());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        params.setMargins(2, 0, 2, 0);
        int i = 0;
        while (i < PROGRESS_COUNT) {
            ProgressBar pb = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
            pb.setLayoutParams(params);
            pb.setPadding(0, -15, 0, -15);
            pb.setMax(100);
            progressBarList.add(pb);
            llProgressbarContainer.addView(pb);
            i++;
        }

        //media is image or video
        isImage = storyData.get(counter).getType().equals("1");

        //load media
        presenter.viewStory(storyData.get(counter).getStoryId());
        if (isImage)
            imageLoader();
        else
            videoPlayer();
    }

    private void imageLoader() {
        handler.removeCallbacks(runnable);
        progresbar.setVisibility(View.GONE);
        image.setVisibility(View.VISIBLE);
        video.setVisibility(View.GONE);
        Glide.with(getBaseContext()).load(storyData.get(counter).getUrlPath()).into(image);
        imageCounter = 1;
        handler.postDelayed(runnable1, 1000);
    }

    private final Runnable runnable1 = new Runnable() {
        @Override
        public void run() {
            if (counter >= 0 && counter < PROGRESS_COUNT) {
                imageCounter++;
                setProgressAnimate(progressBarList.get(counter), imageCounter * 10);
                //progressBarList.get(count).setProgress((int) ((player.getCurrentPosition() * 100) / player.getDuration()));
                if (imageCounter >= 8)
                    next();
                else
                    handler.postDelayed(runnable1, 1000);
            }
        }
    };

    private void videoPlayer() {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        video.setUseController(false);
        video.requestFocus();
        video.setPlayer(player);

        player.addListener(this);
        player.setPlayWhenReady(true); //run file/link when ready to play.

        startPlayer();
    }

    private void startPlayer() {
        if (counter >= 0 && counter < PROGRESS_COUNT) {

            handler.removeCallbacks(runnable1);
            image.setVisibility(View.GONE);
            video.setVisibility(View.VISIBLE);
            progresbar.setVisibility(View.VISIBLE);

            DefaultBandwidthMeter bandwidthMeterA = new DefaultBandwidthMeter();
            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "howdoo"), bandwidthMeterA);
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource videoSource = new ExtractorMediaSource(Uri.parse(storyData.get(counter).getUrlPath()), dataSourceFactory, extractorsFactory, null, null);//mp4VideoUri, dataSourceFactory, 1, null, null);
            player.prepare(videoSource);
        }
    }

    @OnClick(R.id.llView)
    public void clickView() {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void bottomsheet() {
        isMine = getIntent().getBooleanExtra("isMine", false);
        bottom_sheet.setVisibility(isMine ? View.VISIBLE : View.GONE);
        if (isMine) {
            totalViews.setText(storyData.get(counter).getUniqueViewCount());

            // init the bottom sheet behavior
            bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

            bottomSheetBehavior.setPeekHeight(340);
            bottomSheetBehavior.setHideable(false);
            // set callback for changes
            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    //  if (newState == BottomSheetBehavior.STATE_EXPANDED)
                    // storiesProgressView.pause();
                    //else if (newState == BottomSheetBehavior.STATE_COLLAPSED)
                    //storiesProgressView.resume();
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });


            adapter = new ViewerAdapter(storyData.get(counter).getViewerList());
            listViewer.setLayoutManager(new LinearLayoutManager(this));
            if (storyData.get(counter).getViewerList().size() > 0)
                listViewer.setAdapter(adapter);
        }
    }

    private void pausePlayer() {
        try {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
//            storiesProgressView.pause();
        } catch (Exception ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void restartPlayer() {
        player.setPlayWhenReady(true);
        player.getPlaybackState();
//        storiesProgressView.resume();
    }


    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(this);
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        if (!flag) onBackPressed();
    }

    @Override
    public void reload() {

    }

    private void setProgressAnimate(ProgressBar pb, int progressTo) {
        ObjectAnimator animation = ObjectAnimator.ofInt(pb, "progress", progressTo);
        animation.setDuration(500); // 1 second
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    public StoriesFrag getMyFragment() {
        final android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
        return (StoriesFrag) manager.findFragmentByTag(TAG);
    }

    private void next() {
        try {
            if (counter < PROGRESS_COUNT) {
                setProgressAnimate(progressBarList.get(counter), 100);
                counter++;
                isImage = storyData.get(counter).getType().equals("1");
                presenter.viewStory(storyData.get(counter).getStoryId());
                if (!isImage)
                    startPlayer();
                else
                    imageLoader();
            } else {
                finish();
            }
        } catch (Exception ignored) {
            finish();
        }
    }

    private void prev() {
        if (counter > 0) {
            setProgressAnimate(progressBarList.get(counter), 0);
            counter--;
            isImage = storyData.get(counter).getType().equals("1");
            if (!isImage)
                startPlayer();
            else
                imageLoader();
        }
    }

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (counter >= 0 && counter < PROGRESS_COUNT) {

                setProgressAnimate(progressBarList.get(counter), (int) ((player.getCurrentPosition() * 100) / player.getDuration()));
                //progressBarList.get(count).setProgress((int) ((player.getCurrentPosition() * 100) / player.getDuration()));
                handler.postDelayed(runnable, 1000);
            }
        }
    };

    //player
    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_ENDED) {
            next();
        } else if (playbackState == Player.STATE_READY) {
            progresbar.setVisibility(View.GONE);
            handler.postDelayed(runnable, 1000);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }


}
