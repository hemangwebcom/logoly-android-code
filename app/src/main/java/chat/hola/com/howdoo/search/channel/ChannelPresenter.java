package chat.hola.com.howdoo.search.channel;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.club.clubs.ClubResponse;
import chat.hola.com.howdoo.search.channel.module.ChannelResponse;
import chat.hola.com.howdoo.search.tags.module.TagsResponse;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 24/2/18.
 */

public class ChannelPresenter implements ChannelContract.Presenter {

    @Nullable
    private ChannelContract.View view;
    @Inject
    HowdooService service;


    @Inject
    ChannelPresenter() {
    }

    @Override
    public void attachView(ChannelContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
    }

    @Override
    public void search(CharSequence charSequence) {
        service.searchClub(AppController.getInstance().getApiToken(), "en", charSequence.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ClubResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ClubResponse> response) {
                        Log.i("RESPONSE", response.toString());
                        if (response.code() == 200 && response.body().getData() != null && response.body().getData().size() > 0)
                            view.showData(response.body().getData());
                        else if (response.code() == 204)
                            view.noData();
                        else if (response.code() == 401)
                            view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("search", "onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void subscribeChannel(String channelId) {
        service.subscribeChannel(AppController.getInstance().getApiToken(), Constants.LANGUAGE, channelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });

    }

    @Override
    public void unSubscribeChannel(String channelId) {
        service.unSubscribeChannel(AppController.getInstance().getApiToken(), Constants.LANGUAGE, channelId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }

    public void join(String id) {
        Map<String, Object> map = new HashMap<>();
        map.put("clubId", id);
        service.joinClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        search("");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }

    public void leave(String id) {
        Map<String, Object> map = new HashMap<>();
        map.put("clubId", id);
        service.leaveClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        search("");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        this.dispose();
                    }
                });
    }
}
