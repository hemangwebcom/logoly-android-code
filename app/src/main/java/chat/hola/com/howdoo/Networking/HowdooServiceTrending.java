package chat.hola.com.howdoo.Networking;

import java.util.Map;

import chat.hola.com.howdoo.home.model.Posts;
import chat.hola.com.howdoo.home.trending.model.TrendingContentResponse;
import chat.hola.com.howdoo.home.trending.model.TrendingResponse;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * <h1>HowdooServiceTrending</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 18/6/18.
 */


public interface HowdooServiceTrending {
    @GET("/getCategories")
    Observable<Response<TrendingResponse>> getCategories(@Header("authorization") String auth,
                                                         @Header("lang") String lang);

    @POST("/trendingPosts/")
    Observable<Response<TrendingContentResponse>> getTrendingData(@Header("authorization") String auth,
                                                                  @Header("lang") String lang,
                                                                  @Body Map<String, Object> params);

    @GET("/filterPosts")
    Observable<Response<Posts>> filter(@Header("authorization") String auth,
                                       @Header("lang") String lang,
                                       @Header("filterType") String filter,
                                       @Header("page") String page);
}
