package chat.hola.com.howdoo.search;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.search.model.SearchData;

/**
 * Created by ankit on 24/2/18.
 */

public interface SearchContract {

    interface View extends BaseView {
        void stop();
    }

    interface Presenter {
        void stop();


    }

}
