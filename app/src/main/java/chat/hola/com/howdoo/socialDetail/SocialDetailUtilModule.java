package chat.hola.com.howdoo.socialDetail;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import com.howdoo.dubly.R;

import java.util.List;

import chat.hola.com.howdoo.Utilities.CustomVideoSurfaceView;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.home.model.Data;
import dagger.Module;
import dagger.Provides;

/**
 * <h1>SocialDetailUtilModule</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 23/3/18.
 */

@ActivityScoped
@Module
public class SocialDetailUtilModule {

    @ActivityScoped
    @Provides
    CustomVideoSurfaceView customVideoSurfaceView(SocialDetailActivity activity) {
        return new CustomVideoSurfaceView(activity);
    }

    @ActivityScoped
    @Provides
    AlertDialog.Builder builder(SocialDetailActivity activity) {
        return new AlertDialog.Builder(activity);
    }

    @ActivityScoped
    @Provides
    ArrayAdapter<String> reportReasons(SocialDetailActivity activity) {
        return new ArrayAdapter<String>(activity, R.layout.custom_select_dialog_singlechoice);
    }


}
