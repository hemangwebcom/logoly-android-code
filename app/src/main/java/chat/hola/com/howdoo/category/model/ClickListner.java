package chat.hola.com.howdoo.category.model;

/**
 * <h1>ClickListner</h1>
 *
 * @author 3Embed
 * @since 5/2/2018.
 */

public interface ClickListner {

    void onItemSelected(int position);
}
