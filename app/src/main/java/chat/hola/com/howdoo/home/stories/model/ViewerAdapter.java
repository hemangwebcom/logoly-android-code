package chat.hola.com.howdoo.home.stories.model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.howdoo.Utilities.TimeAgo;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 5/29/2018.
 */

public class ViewerAdapter extends RecyclerView.Adapter<ViewerAdapter.ViewHolder> {

    private List<Viewer> mItems;

    public ViewerAdapter(List<Viewer> mItems) {
        this.mItems = mItems;
    }

    public void setData(List<Viewer> mItems) {
        this.mItems = mItems;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bottom_sheet_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        Viewer item;
        @BindView(R.id.ivProfilePic)
        SimpleDraweeView profilePic;
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvTime)
        TextView tvTime;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setData(Viewer viewer) {
            this.item = viewer;
            profilePic.setImageURI(item.getProfilePic().replace("upload/", "upload/q_10/").replace("mp4", "jpg"));
            tvUserName.setText(item.getUserName());
            tvTime.setText(TimeAgo.getTimeAgo(Long.parseLong(item.getTimestamp())));
        }

    }

}
