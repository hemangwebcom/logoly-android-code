package chat.hola.com.howdoo.trendingDetail.model;

import android.view.View;

/**
 * <h1>ClickListner</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 5/4/2018.
 */

public interface ClickListner {
    void onItemClick(int position, View view);
}
