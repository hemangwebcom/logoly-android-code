package chat.hola.com.howdoo.preview;

import chat.hola.com.howdoo.Utilities.BaseView;

/**
 * <h1>PreviewContract</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 4/24/2018.
 */

public interface PreviewContract {

    interface View extends BaseView {

    }

    interface Presenter {

        void viewStory(String storyId);
    }
}
