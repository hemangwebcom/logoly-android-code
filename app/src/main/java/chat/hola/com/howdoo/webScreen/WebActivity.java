package chat.hola.com.howdoo.webScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by ankit on 15/3/18.
 */

public class WebActivity extends DaggerAppCompatActivity {

    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.pbProgress)
    ProgressBar pbLoadProgress;

    @BindView(R.id.tvError)
    TextView tvError;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private Unbinder unbinder;
    final Activity activity = this;
    private String url = null;
    private String title = null;

    @Inject
    TypefaceManager typefaceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        Bundle bundle = getIntent().getBundleExtra("url_data");
        url = bundle.getString("url");
        title = bundle.getString("title");
        tvTitle.setTypeface(typefaceManager.getSemiboldFont());
        tvTitle.setText(title);
        initWebview();
    }


    /**
     * <p>
     * Enable javaScript and set WebChromeClient and webViewClient
     * to webView.
     * </P>
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void initWebview() {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(false);
        webView.loadUrl(url);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    pbLoadProgress.setVisibility(View.GONE);

                } else {
                    pbLoadProgress.setVisibility(View.VISIBLE);

                }
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        webView.reload();
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack())
            webView.goBack();
        else
            super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @OnClick(R.id.ivBack)
    public void back() {
        onBackPressed();
    }

}