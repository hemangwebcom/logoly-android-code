package chat.hola.com.howdoo.profileScreen.story;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.comment.CommentActivity;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.profileScreen.story.model.StoryData;
import chat.hola.com.howdoo.profileScreen.story.model.TrendingDtlAdapter;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;
import dagger.android.support.DaggerFragment;

/**
 * Created by ankit on 17/2/18.
 */

public class StoryFragment extends DaggerFragment implements StoryContract.View, TrendingDtlAdapter.ClickListner {

    @Inject
    StoryPresenter presenter;
    @Inject
    SessionManager sessionManager;
    TrendingDtlAdapter adapter;
    @Inject
    List<StoryData> data;
    @Inject
    BlockDialog dialog;

    @BindView(R.id.recyclerProfileTab)
    RecyclerView recyclerViewProfileTab;
    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @BindView(R.id.tvEmpty)
    TextView tvEmpty;
    @BindView(R.id.ivEmpty)
    ImageView ivEmpty;


    private Unbinder unbinder;
    private String type = "";
    private String userId = "";
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager linearLayoutManager;


    @Override
    public void userBlocked() {
        dialog.show();
    }

    @OnClick(R.id.btnCreatePost)
    public void createpost() {
    }

    public StoryFragment() {
    }

    public static StoryFragment newInstance() {
        return new StoryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_story, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        type = getArguments().getString("name");
        userId = getArguments().getString("userId");
        presenter.init();
        if (userId != null && !userId.equals("")) {
            if (userId.equals(AppController.getInstance().getUserId()))
                presenter.loadData(StoryPresenter.page * StoryPresenter.PAGE_SIZE, StoryPresenter.PAGE_SIZE);
            else
                presenter.loadMemberData(userId, StoryPresenter.page * StoryPresenter.PAGE_SIZE, StoryPresenter.PAGE_SIZE);
        } else {
            presenter.loadData(StoryPresenter.page * StoryPresenter.PAGE_SIZE, StoryPresenter.PAGE_SIZE);
        }
        return rootView;
    }

    @Override
    public void setupRecyclerView() {
        StoryPresenter.page = 0;
        if (type.equals("grid")) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
            //recyclerViewProfileTab.setHasFixedSize(true);
            recyclerViewProfileTab.setLayoutManager(gridLayoutManager);
        } else {
            linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerViewProfileTab.setLayoutManager(linearLayoutManager);
        }
        recyclerViewProfileTab.addOnScrollListener(recyclerViewOnScrollListener);
        recyclerViewProfileTab.setNestedScrollingEnabled(true);
        adapter = new TrendingDtlAdapter(getContext(), data);
        adapter.setListener(this);
        adapter.setData(type);
        recyclerViewProfileTab.setAdapter(adapter);
//        recyclerViewProfileTab.addOnScrollListener(recyclerOnScrollListener);
    }

    @Override
    public void showData(List<StoryData> data, boolean isFirst) {
        try {
            if (isFirst)
                this.data.clear();
            boolean isEmpty = data.isEmpty();
            boolean isMine = ((ProfileActivity) Objects.requireNonNull(getActivity())).userId.equals(AppController.getInstance().getUserId());
            boolean isPrivate = ProfileActivity.isPrivate;
            int followStatus = ProfileActivity.followStatus;
            if (isMine) {
                // its my profile
                if (isEmpty) {
                    llEmpty.setVisibility(View.VISIBLE);
                    ivEmpty.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_default_media));
                    tvEmpty.setText(getResources().getString(R.string.create_post_message));
                }

            } else {
                // others profile
                switch (followStatus) {
                    case 0:
                        //not follow
                        if (isPrivate) {
                            llEmpty.setVisibility(View.VISIBLE);
                            ivEmpty.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_lock));
                            tvEmpty.setText(getResources().getString(R.string.private_no_post));
                        } else if (isEmpty) {
                            llEmpty.setVisibility(View.VISIBLE);
                            ivEmpty.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_default_media));
                            tvEmpty.setText(getResources().getString(R.string.no_post));
                        }
                        break;
                    case 1:
                        //following
                        if (isEmpty) {
                            llEmpty.setVisibility(View.VISIBLE);
                            ivEmpty.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_default_media));
                            tvEmpty.setText(getResources().getString(R.string.no_post));
                        }
                        break;
                    case 2:
                        //requested
                        llEmpty.setVisibility(View.VISIBLE);
                        ivEmpty.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_lock));
                        tvEmpty.setText(getResources().getString(R.string.private_no_post));
                        break;
                    default:
                        llEmpty.setVisibility(View.GONE);
                }

            }

            recyclerViewProfileTab.setVisibility(data.isEmpty() ? View.GONE : View.VISIBLE);
            this.data.addAll(data);
            adapter.notifyDataSetChanged();
        } catch (NullPointerException e) {

        }
    }

    @Override
    public void isLoading(boolean flag) {
    }


    @Override
    public void showEmptyUi(boolean show) {
        //llEmpty.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void noData() {
        llEmpty.setVisibility(View.VISIBLE);
        recyclerViewProfileTab.setVisibility(View.GONE);
        if (AppController.getInstance().getUserId().equals(((ProfileActivity) getActivity()).userId)) {
            //my profile
            ivEmpty.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_default_media));
            tvEmpty.setText(getResources().getString(R.string.create_post_message));
        } else if (ProfileActivity.isPrivate) {
            //other user's profile
            if (((ProfileActivity) getActivity()).btnFollow.isChecked()) {
                ivEmpty.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_lock));
                tvEmpty.setText(getResources().getString(R.string.private_no_post));
            } else {
                ivEmpty.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_default_media));
                tvEmpty.setText(getResources().getString(R.string.create_post_message));
                return;
            }
        }


    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        //   llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();

        presenter.detachView();
    }

    @Override
    public void onItemClick(int position, int type, View view) {
        Intent intent = new Intent(getContext(), SocialDetailActivity.class);
        intent.putExtra("postId", data.get(position).getPostId());
        intent.putExtra("call", "profile");
        startActivity(intent);
    }

    @Override
    public void onCommentClick(String postId) {
        startActivity(new Intent(getContext(), CommentActivity.class).putExtra("postId", postId));
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onLikeClick(String postId, boolean like) {
        presenter.like(like, postId);
    }

    @Override
    public void onDisLikeClick(String postId, boolean like) {
        presenter.dislike(like, postId);
    }

    @Override
    public void onFavoriteClick(String postId, boolean like) {
        if (like)
            presenter.favorite(postId);
        else
            presenter.unfavorite(postId);
    }

    @Override
    public void reload() {
        onResume();
    }

    public RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount;
            int totalItemCount;
            int firstVisibleItemPosition;
            if (type.equals("grid")) {
                visibleItemCount = gridLayoutManager.getChildCount();
                totalItemCount = gridLayoutManager.getItemCount();
                firstVisibleItemPosition = ((GridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            } else {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            }
            presenter.callApiOnScroll(firstVisibleItemPosition, visibleItemCount, totalItemCount, userId);
        }
    };
}
