package chat.hola.com.howdoo.profileScreen.followers.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 20/3/18.
 */

public class Data {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("followee")
    @Expose
    private String followee;
    @SerializedName("follower")
    @Expose
    private String follower;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private Boolean end;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("followsBack")
    @Expose
    private Boolean followsBack;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("userName")
    @Expose
    private String username;
    @SerializedName("businessProfile")
    @Expose
    private Boolean businessProfile;


    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("isBlocked")
    @Expose
    private boolean blocked;
    @SerializedName("isContact")
    @Expose
    private boolean isContact;
    @SerializedName("isFollowing")
    @Expose
    private boolean isFollowing;
    @SerializedName("isAdmin")
    @Expose
    private boolean admin;
    @SerializedName("isOwner")
    @Expose
    private boolean owner;

    private boolean isSent = false;
    private boolean isSelected = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFollowee() {
        return followee;
    }

    public void setFollowee(String followee) {
        this.followee = followee;
    }

    public String getFollower() {
        return follower;
    }

    public void setFollower(String follower) {
        this.follower = follower;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public Boolean getEnd() {
        return end;
    }

    public void setEnd(Boolean end) {
        this.end = end;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Boolean getFollowsBack() {
        return followsBack;
    }

    public void setFollowsBack(Boolean followsBack) {
        this.followsBack = followsBack;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getBusinessProfile() {
        return businessProfile;
    }

    public void setBusinessProfile(Boolean businessProfile) {
        this.businessProfile = businessProfile;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setFollowing(boolean following) {
        isFollowing = following;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    public boolean isContact() {
        return isContact;
    }

    public void setContact(boolean contact) {
        isContact = contact;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
