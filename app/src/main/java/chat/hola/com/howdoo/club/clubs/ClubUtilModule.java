package chat.hola.com.howdoo.club.clubs;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import dagger.Module;
import dagger.Provides;

/**
 * <h1>BlockUserUtilModule</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */
@ActivityScoped
@Module
public class ClubUtilModule {

    @ActivityScoped
    @Provides
    List<Club> getMembers() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    ClubAdapter memberAdapter(List<Club> data, Activity mContext, TypefaceManager typefaceManager) {
        return new ClubAdapter(data, mContext, typefaceManager);
    }

}
