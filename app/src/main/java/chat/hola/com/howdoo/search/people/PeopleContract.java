package chat.hola.com.howdoo.search.people;

import java.util.List;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.search.model.SearchData;

/**
 * Created by ankit on 24/2/18.
 */

public interface PeopleContract {

    interface View extends BaseView {
        void showData(List<SearchData> data, boolean isFirst);

        void isFollowing(int pos, int status);

        void noData();

        void blocked();
    }

    interface Presenter extends BasePresenter<PeopleContract.View> {
        void search(CharSequence charSequence, int offset, int limit);

        void follow(int pos, String followingId);

        void unfollow(int pos, String followingId);
    }
}
