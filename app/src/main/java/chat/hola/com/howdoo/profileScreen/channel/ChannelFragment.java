package chat.hola.com.howdoo.profileScreen.channel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.howdoo.dubly.R;

import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.home.model.ContentAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.profileScreen.channel.Model.ChannelData;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;
import chat.hola.com.howdoo.trendingDetail.TrendingDetail;
import dagger.android.support.DaggerFragment;

/**
 * <h>ChannelFragment</h>
 * <p>
 * This fragment show the All channels exist for the particular user
 * along with all post in that channel. in order to do so it uses two Adapter
 * {@link ContentAdapter }
 *
 * @author 3Embed
 * @since 17/2/18.
 */


public class ChannelFragment extends DaggerFragment implements ChannelContract.View,
        ContentAdapter.AdapterClickCallback {

    @Inject
    ChannelPresenter presenter;
    @Inject
    SessionManager sessionManager;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    ContentAdapter contentAdapter;
    @BindView(R.id.ivEmpty)
    ImageView ivEmpty;
    @BindView(R.id.recyclerProfileTab)
    RecyclerView recyclerViewProfileTab;
    @BindView(R.id.tvEmpty)
    TextView tvEmpty;
    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    private Unbinder unbinder;
    private int PAGE_SIZE = 5;
    private int page = 0;
    private String userId;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    LinearLayoutManager linearLayoutManager;
    private ArrayList<ChannelData> channelDataList = new ArrayList<>();

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    page++;
                    presenter.getChannelData(PAGE_SIZE * page, PAGE_SIZE, userId);
                }
            }
        }
    };

    @Inject
    public ChannelFragment() {
    }

    public static ChannelFragment newInstance() {
        return new ChannelFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
        page = 0;
        channelDataList.clear();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_story, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        userId = getArguments().getString("userId");
        presenter.init();
        applyFont();
        return rootView;
    }

    private void applyFont() {
        tvEmpty.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void onResume() {
        super.onResume();
        channelDataList.clear();
        presenter.getChannelData(page * PAGE_SIZE, PAGE_SIZE, userId);
    }

    @Override
    public void setupRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewProfileTab.setLayoutManager(linearLayoutManager);
        recyclerViewProfileTab.setHasFixedSize(true);
        recyclerViewProfileTab.setNestedScrollingEnabled(true);
        recyclerViewProfileTab.setAdapter(contentAdapter);
        channelDataList.clear();

        recyclerViewProfileTab.addOnScrollListener(recyclerViewOnScrollListener);
        contentAdapter.setCallback(this);
    }

    @Override
    public void showChannelData(ArrayList<ChannelData> channelData) {
        llEmpty.setVisibility(channelData.isEmpty() ? View.VISIBLE : View.GONE);
        recyclerViewProfileTab.setVisibility(channelData.isEmpty() ? View.GONE : View.VISIBLE);
        ivEmpty.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_channel_gray));
        if (channelData.size() <= PAGE_SIZE)
            isLastPage = true;
        channelDataList.addAll(channelData);
        contentAdapter.setData(channelDataList);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void noData() {
        ivEmpty.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_channel_gray));
        llEmpty.setVisibility(View.VISIBLE);
        recyclerViewProfileTab.setVisibility(View.GONE);
    }

    @Override
    public void isLoading(boolean flag) {
        if (flag) {
            isLoading = true;
        } else {
            isLoading = false;
        }
    }

    @Override
    public void showEmptyUi(boolean show) {
        tvEmpty.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onChannelDetail(int position) {
        Intent intent = new Intent(getContext(), TrendingDetail.class);
        intent.putExtra("data", (Serializable) channelDataList.get(position));
        intent.putExtra("call", "profilechannel");
        startActivity(intent);
    }

    @Override
    public void onPostDetail(Data data, int type, View view) {
        Intent intent = new Intent(getContext(), SocialDetailActivity.class);
        intent.putExtra("postId", data.getPostId());
        startActivity(intent);
    }

    @Override
    public void reload() {

    }
}
