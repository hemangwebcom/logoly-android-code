package chat.hola.com.howdoo.search;

import android.util.Log;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.profileScreen.model.Profile;
import chat.hola.com.howdoo.search.model.SearchResponse;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class SearchPresenter implements SearchContract.Presenter {

    @Inject
    SearchContract.View view;

    @Inject
    HowdooService service;

    @Inject
    public SearchPresenter() {
    }

    @Override
    public void stop() {
        view.stop();
    }



}
