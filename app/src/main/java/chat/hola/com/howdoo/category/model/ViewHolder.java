package chat.hola.com.howdoo.category.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.howdoo.dubly.R;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.post.model.CategoryData;

/**
 * <h1>ViewHolder</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 5/2/2018.
 */

class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    @BindView(R.id.ivRow)
    ImageView ivRow;
    @BindView(R.id.tvRow)
    TextView tvRow;
    @BindView(R.id.cbRow)
    CheckBox cbRow;
    @Inject
    List<CategoryData> categories;
    private ClickListner clickListner;

    public ViewHolder(View itemView, TypefaceManager typefaceManager, ClickListner clickListner) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.clickListner = clickListner;
        cbRow.setOnClickListener(this);
        tvRow.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rlItem || v.getId() == R.id.cbRow) {
            if (((CheckBox) v).isChecked()) {
                clickListner.onItemSelected(getAdapterPosition());
            }
        }
    }
}
