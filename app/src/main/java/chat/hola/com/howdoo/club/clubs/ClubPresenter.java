package chat.hola.com.howdoo.club.clubs;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.profileScreen.followers.Model.FollowersResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>BlockUserPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class ClubPresenter implements ClubContract.Presenter, ClickListner {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;
    @Inject
    HowdooService service;
    @Inject
    ClubContract.View view;
    @Inject
    ClubModel model;
    @Inject
    NetworkConnector networkConnector;
    private boolean isForSelect;

    @Inject
    ClubPresenter() {
    }

    @Override
    public void setForSelect(boolean isForSelect) {
        this.isForSelect = isForSelect;
    }

    @Override
    public void deleteClub(int position) {
        service.deleteClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, model.getClubId(position))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if(response.code()==200)
                            view.showMessage("Club delete",-1);
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void callApiOnScroll(int access, String postId, int firstVisibleItemPosition, int visibleItemCount, int totalItemCount, String userId) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                getClubs(access, PAGE_SIZE * page, PAGE_SIZE, userId);
            }
        }
    }

    @Override
    public void actionOnClub(String clubId) {

    }


    @Override
    public void getClubs(int access, int offset, int limit, String userId) {
        service.getClubs(AppController.getInstance().getApiToken(), Constants.LANGUAGE, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ClubResponse>>() {
                    @Override
                    public void onNext(Response<ClubResponse> response) {
                        switch (response.code()) {
                            case 200:
                                isLastPage = response.body().getClubs().size() < PAGE_SIZE;
                                model.setForSelect(isForSelect);
                                model.setData(offset == 0, AppController.getInstance().getUserId().equals(userId), access, response.body().getClubs());
                                break;
                            case 401:
                                view.sessionExpired();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("clubs", "onError: ");
                    }

                    @Override
                    public void onComplete() {
                        Log.i("clubs", "onError: ");
                    }
                });
    }

    @Override
    public ClickListner getPresenter() {
        return this;
    }


    @Override
    public void onChannelClick(int position) {
        view.openChannel(model.getChannelId(position));
    }

    @Override
    public void itemSelect(int position, int action) {

    }

    @Override
    public void leave(int position) {
        Map<String, Object> params = new HashMap<>();
        params.put("clubId", model.getClubId(position));
        service.leaveClub(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                model.leave(position);
                                break;
                            case 401:
                                view.sessionExpired();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void itemSelect(int position) {
        view.getClubId(model.getClubName(position), model.getClubId(position));
    }

    @Override
    public void delete(int position) {
        view.confirmDelete(position);
    }
}
