package chat.hola.com.howdoo.search.otherSearch;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;

/**
 * Created by ankit on 24/2/18.
 */

public interface OtherSearchContract {

    interface View extends BaseView{

    }

    interface Presenter extends BasePresenter<OtherSearchContract.View>{


    }
}
