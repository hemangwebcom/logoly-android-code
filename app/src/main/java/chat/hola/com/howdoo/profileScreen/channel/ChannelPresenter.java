package chat.hola.com.howdoo.profileScreen.channel;

import android.support.annotation.Nullable;
import android.util.Log;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.profileScreen.channel.Model.ChannelResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h>ChannelPresenter</h>
 *
 * @author 3Embed.
 * @since 23/2/18.
 */

public class ChannelPresenter implements ChannelContract.Presenter {

    private static final String TAG = ChannelPresenter.class.getSimpleName();

    @Nullable
    ChannelContract.View view;

    @Inject
    HowdooService service;

    @Inject
    ChannelPresenter() {
    }

    @Override
    public void init() {
        view.setupRecyclerView();
    }

    @Override
    public void getChannelData(int skip, int limit, String userId) {
        if (view != null)
            view.isLoading(true);
        service.getChannelPost(AppController.getInstance().getApiToken(), "en", userId == null ? AppController.getInstance().getUserId() : userId, skip, limit)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ChannelResponse>>() {
                    @Override
                    public void onNext(Response<ChannelResponse> channelResponse) {
                        if (channelResponse.code() == 200) {
                            if (view != null)
                                view.showChannelData(channelResponse.body().getData());
                        } else if (channelResponse.code() == 401) {
                            if (view != null)
                                view.sessionExpired();
                        } else if (channelResponse.code() == 204) {
                            if (view != null)
                                view.noData();
                        } else {
                            if (view != null)
                                view.showEmptyUi(true);
                        }
                        Log.w(TAG, "channelPost fetched successfully!!");
                        if (view != null)
                            view.isLoading(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "channelPost fetched failed!! " + e.getMessage());
                        if (view != null) {
                            view.isLoading(false);
                            view.showEmptyUi(true);
                        }
                    }

                    @Override
                    public void onComplete() {

                    }

                });
    }

    @Override
    public void attachView(ChannelContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
