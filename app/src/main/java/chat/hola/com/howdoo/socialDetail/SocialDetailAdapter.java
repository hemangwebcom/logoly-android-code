package chat.hola.com.howdoo.socialDetail;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;

import java.util.List;

import chat.hola.com.howdoo.home.model.Data;
import chat.hola.com.howdoo.socialDetail.video_manager.BaseVideoItem;
import chat.hola.com.howdoo.socialDetail.video_manager.ClickListner;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 11/27/2018.
 */
public class SocialDetailAdapter extends RecyclerView.Adapter<ViewHolder> {
    private final VideoPlayerManager mVideoPlayerManager;
    private List<BaseVideoItem> videoItems;
    private List<Data> dataList;
    private Context context;
    private ClickListner clickListner;

    public SocialDetailAdapter(VideoPlayerManager mVideoPlayerManager, Context context, List<BaseVideoItem> videoItems, List<Data> dataList) {
        this.mVideoPlayerManager = mVideoPlayerManager;
        this.context = context;
        this.videoItems = videoItems;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseVideoItem videoItem = videoItems.get(viewType);
        View itemView = videoItem.createView(parent, context.getResources().getDisplayMetrics().heightPixels, context.getResources().getDisplayMetrics().widthPixels);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BaseVideoItem videoItem = videoItems.get(position);
        videoItem.update(position, holder, mVideoPlayerManager);
        Data data = dataList.get(position);

        //comment
        holder.ivComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListner.comment(data.getPostId());
            }
        });

        //profile
        holder.ivProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListner.profile(data.getUserId());
            }
        });

        holder.tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListner.profile(data.getUserId());
            }
        });


        //like
        holder.ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean flag = !data.isLiked();
                clickListner.like(flag, data.getPostId());
                data.setLiked(flag);
                int count = Integer.parseInt(data.getLikesCount());
                if (flag) {
                    count++;
                } else {
                    if (count > 0)
                        count--;
                }
                data.setLikesCount("" + count);
                holder.tvLikeCount.setText("" + count);
                holder.ivDisLike.setChecked(!flag);
            }
        });

        //dislike
        holder.ivDisLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean flag = !data.isLiked();
                clickListner.disLike(flag, data.getPostId());
                data.setDisliked(flag);
                int count = Integer.parseInt(data.getDislikesCount());
                if (flag) {
                    count++;
                } else {
                    if (count > 0)
                        count--;
                }
                data.setLikesCount("" + count);
                holder.tvDisLikeCount.setText("" + count);
                holder.ivLike.setChecked(!flag);
            }
        });

        //dislike
        holder.ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean flag = !data.isLiked();
                clickListner.favorite(flag, data.getPostId());
                data.setFavourite(flag);
                int count = Integer.parseInt(data.getFavouritesCount());
                if (flag) {
                    count++;
                } else {
                    if (count > 0)
                        count--;
                }
                data.setFavouritesCount("" + count);
                holder.tvFavouriteCount.setText("" + count);
            }
        });
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public void setClickListner(ClickListner clickListner) {
        this.clickListner = clickListner;
    }
}
