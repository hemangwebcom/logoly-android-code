package chat.hola.com.howdoo.dagger;

import chat.hola.com.howdoo.Activities.Terms;
import chat.hola.com.howdoo.NumberVerification.VerifyPhoneModule;
import chat.hola.com.howdoo.NumberVerification.VerifyPhoneNumber;
import chat.hola.com.howdoo.Profile.SaveProfile;
import chat.hola.com.howdoo.Profile.SaveProfileModule;
import chat.hola.com.howdoo.blockUser.BlockUserActivity;
import chat.hola.com.howdoo.blockUser.BlockUserModule;
import chat.hola.com.howdoo.blockUser.BlockUserUtilModule;
import chat.hola.com.howdoo.category.CategoryActivity;
import chat.hola.com.howdoo.category.CategoryModule;
import chat.hola.com.howdoo.category.CategoryUtilModule;
import chat.hola.com.howdoo.club.clubs.ClubActivity;
import chat.hola.com.howdoo.club.clubs.ClubModule;
import chat.hola.com.howdoo.club.clubs.ClubUtilModule;
import chat.hola.com.howdoo.club.create.CreateClubActivity;
import chat.hola.com.howdoo.club.create.CreateClubModule;
import chat.hola.com.howdoo.club.create.CreateClubUtilModule;
import chat.hola.com.howdoo.club.detail.ClubDetailActivity;
import chat.hola.com.howdoo.club.detail.ClubDetailModule;
import chat.hola.com.howdoo.club.detail.ClubDetailUtilModule;
import chat.hola.com.howdoo.club.member.MemberActivity;
import chat.hola.com.howdoo.club.member.MemberModule;
import chat.hola.com.howdoo.club.member.MemberUtilModule;
import chat.hola.com.howdoo.comment.CommentActivity;
import chat.hola.com.howdoo.comment.CommentModule;
import chat.hola.com.howdoo.comment.CommentUtilModule;
import chat.hola.com.howdoo.dubly.DubsActivity;
import chat.hola.com.howdoo.dubly.DubsModule;
import chat.hola.com.howdoo.dubly.DubsUtilModule;
import chat.hola.com.howdoo.dublycategory.DubCategoryActivity;
import chat.hola.com.howdoo.dublycategory.DubCategoryModule;
import chat.hola.com.howdoo.dublycategory.DubCategoryUtilModule;
import chat.hola.com.howdoo.home.LandingActivity;
import chat.hola.com.howdoo.home.LandingModule;
import chat.hola.com.howdoo.home.LandingUtilModule;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.ChannelRequestersActivity;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.ChannelRequestersModule;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.ChannelRequestersUtilModule;
import chat.hola.com.howdoo.home.activity.youTab.followrequest.FollowRequestActivity;
import chat.hola.com.howdoo.home.activity.youTab.followrequest.FollowRequestModule;
import chat.hola.com.howdoo.home.activity.youTab.followrequest.FollowRequestUtilModule;
import chat.hola.com.howdoo.location.LocSearchModule;
import chat.hola.com.howdoo.location.LocSearchUtilModule;
import chat.hola.com.howdoo.location.Location_Search_Activity;
import chat.hola.com.howdoo.post.PostActivity;
import chat.hola.com.howdoo.post.PostModule;
import chat.hola.com.howdoo.post.PostUtilModule;
import chat.hola.com.howdoo.preview.PreviewActivity;
import chat.hola.com.howdoo.preview.PreviewModule;
import chat.hola.com.howdoo.preview.PreviewUtilModule;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.profileScreen.ProfileModule;
import chat.hola.com.howdoo.profileScreen.ProfileUtilModule;
import chat.hola.com.howdoo.profileScreen.addChannel.AddChannelActivity;
import chat.hola.com.howdoo.profileScreen.addChannel.AddChannelModule;
import chat.hola.com.howdoo.profileScreen.addChannel.AddChannelUtilModule;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverActivity;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverModule;
import chat.hola.com.howdoo.profileScreen.discover.DiscoverUtilModule;
import chat.hola.com.howdoo.profileScreen.editProfile.EditProfileActivity;
import chat.hola.com.howdoo.profileScreen.editProfile.EditProfileModule;
import chat.hola.com.howdoo.profileScreen.editProfile.EditProfileUtilModule;
import chat.hola.com.howdoo.profileScreen.editProfile.changeEmail.ChangeEmail;
import chat.hola.com.howdoo.profileScreen.editProfile.changeEmail.ChangeEmailModule;
import chat.hola.com.howdoo.profileScreen.followers.FollowersActivity;
import chat.hola.com.howdoo.profileScreen.followers.FollowersModule;
import chat.hola.com.howdoo.profileScreen.followers.FollowersUtilModule;
import chat.hola.com.howdoo.search.SearchActivity;
import chat.hola.com.howdoo.search.SearchModule;
import chat.hola.com.howdoo.settings.SettingsActivity;
import chat.hola.com.howdoo.settings.SettingsModule;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;
import chat.hola.com.howdoo.socialDetail.SocialDetailModule;
import chat.hola.com.howdoo.socialDetail.SocialDetailUtilModule;
import chat.hola.com.howdoo.trendingDetail.TrendingDetail;
import chat.hola.com.howdoo.trendingDetail.TrendingDtlModule;
import chat.hola.com.howdoo.trendingDetail.TrendingDtlUtilModule;
import chat.hola.com.howdoo.webScreen.WebActivity;
import chat.hola.com.howdoo.welcomeScreen.WelcomeActivity;
import chat.hola.com.howdoo.welcomeScreen.WelcomeModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ankit on 20/2/18.
 */

@Module
public interface ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LocSearchModule.class, LocSearchUtilModule.class})
    Location_Search_Activity locationSearchActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CategoryModule.class, CategoryUtilModule.class})
    CategoryActivity categoryActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {FollowersModule.class, FollowersUtilModule.class})
    FollowersActivity followerActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ChangeEmailModule.class)
    ChangeEmail changeEmail();

    @ActivityScoped
    @ContributesAndroidInjector()
    WebActivity webActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {DiscoverModule.class, DiscoverUtilModule.class})
    DiscoverActivity discoverActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SearchModule.class)
    SearchActivity searchActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ClubDetailModule.class, ClubDetailUtilModule.class})
    ClubDetailActivity clubDetailActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {PostModule.class, PostUtilModule.class})
    PostActivity postActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ClubModule.class, ClubUtilModule.class})
    ClubActivity clubActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {MemberModule.class, MemberUtilModule.class})
    MemberActivity memberActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditProfileModule.class, EditProfileUtilModule.class})
    EditProfileActivity editProfileActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {AddChannelModule.class, AddChannelUtilModule.class})
    AddChannelActivity addChannelActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CreateClubModule.class, CreateClubUtilModule.class})
    CreateClubActivity createClubActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ProfileModule.class, ProfileUtilModule.class})
    ProfileActivity profileActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {WelcomeModule.class})
    WelcomeActivity welcomeActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    Terms terms();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LandingModule.class, LandingUtilModule.class})
    LandingActivity contactSyncLandingPage();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {TrendingDtlModule.class, TrendingDtlUtilModule.class})
    TrendingDetail trendingDetail();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SocialDetailModule.class, SocialDetailUtilModule.class})
    SocialDetailActivity socialDetailPresenterImpl();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {VerifyPhoneModule.class})
    VerifyPhoneNumber verifyNumber();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SettingsModule.class})
    SettingsActivity settingsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SaveProfileModule.class})
    SaveProfile saveProfile();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CommentModule.class, CommentUtilModule.class})
    CommentActivity commentActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BlockUserModule.class, BlockUserUtilModule.class})
    BlockUserActivity blockUserActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {DubsModule.class, DubsUtilModule.class})
    DubsActivity dubsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {DubCategoryModule.class, DubCategoryUtilModule.class})
    DubCategoryActivity dubCategoryActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {PreviewModule.class, PreviewUtilModule.class})
    PreviewActivity previewActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ChannelRequestersModule.class, ChannelRequestersUtilModule.class})
    ChannelRequestersActivity channelRequestersActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {FollowRequestModule.class, FollowRequestUtilModule.class})
    FollowRequestActivity followRequestActivity();
}
