package chat.hola.com.howdoo.profileScreen.followers;

import java.util.List;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.profileScreen.followers.Model.Data;

/**
 * Created by ankit on 19/3/18.
 */

public class FollowersContract {
    interface View extends BaseView{

        void applyFont();

        void recyclerViewSetup();

        void showFollowers(List<Data> followers);

        void showFollowees(List<Data> followees);

        void invalidateBtn(int index, boolean isFollowing);

        void isDataLoading(boolean show);

        void blocked();
    }

    interface Presenter {

        void init();

        void loadFollowersData(int skip, int limit, String userId);

        void loadFolloweesData(int skip, int limit, String userId);

        void follow(String followingId, boolean refollow);

        void unFollow(String followingId);
    }
}
