package chat.hola.com.howdoo.home;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.home.activity.ActivitiesFragment;
import chat.hola.com.howdoo.home.activity.ActivitiesModule;
import chat.hola.com.howdoo.home.connect.ConnectFragment;
import chat.hola.com.howdoo.home.social.SocialFragment;
import chat.hola.com.howdoo.home.social.SocialModule;
import chat.hola.com.howdoo.home.stories.StoriesFrag;
import chat.hola.com.howdoo.home.stories.StoriesModule;
import chat.hola.com.howdoo.home.trending.ContentFragment;
import chat.hola.com.howdoo.home.trending.TrendingFragment;
import chat.hola.com.howdoo.home.trending.TrendingModule;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h1>LandingModule</h1>
 *
 * @author 3Embed
 * @since 21/2/18.
 */

@ActivityScoped
@Module
public interface LandingModule {

    @FragmentScoped
    @ContributesAndroidInjector(modules = StoriesModule.class)
    StoriesFrag storiesFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {SocialModule.class})
    SocialFragment socialFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = ActivitiesModule.class)
    ActivitiesFragment activitiesFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = TrendingModule.class)
    TrendingFragment trendingFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = TrendingModule.class)
    ContentFragment contentFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    ConnectFragment connectFragment();

    @ActivityScoped
    @Binds
    LandingContract.Presenter presenter(LandingPresenter presenter);

    @ActivityScoped
    @Binds
    LandingContract.View view(LandingActivity landingPage);

}
