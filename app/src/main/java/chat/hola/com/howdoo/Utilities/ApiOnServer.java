package chat.hola.com.howdoo.Utilities;

import com.howdoo.dubly.BuildConfig;

/**
 * Created by moda on 20/06/17.
 */

public class ApiOnServer {


    public static final String CHAT_RECEIVED_THUMBNAILS_FOLDER = "/howdoo/receivedThumbnails";
    public static final String CHAT_UPLOAD_THUMBNAILS_FOLDER = "/howdoo/upload";
    public static final String MQTT_SERVICE_PATH = "chat.hola.com.howdoo.MQtt.MqttService";


    public static final String CHAT_DOODLES_FOLDER = "/howdoo/doodles";

    public static final String CHAT_DOWNLOADS_FOLDER = "/howdoo/";


    public static final String IMAGE_CAPTURE_URI = "/howdoo";

    //    public static final String CHAT_MULTER_UPLOAD_URL = "http://104.236.32.23:8009/";
    public static final String CHAT_MULTER_UPLOAD_URL = "http://" + ApiOnServer.HOST_API + ":8008/";

    private static final String CHAT_UPLOAD_SERVER_URL = "http://" + ApiOnServer.HOST_API + ":8083/";
    public static final String VIDEO_THUMBNAILS = "/howdoo/thumbnails";

    public static final String CHAT_UPLOAD_PATH = " http://109.169.86.130:8083/logoly/uploads/";
    public static final String PROFILEPIC_UPLOAD_PATH = "http://109.169.86.130:8083/logoly/profilePics/";

//    public static final String CHAT_UPLOAD_PATH = CHAT_UPLOAD_SERVER_URL + "howdoo/uploads/";
//    public static final String PROFILEPIC_UPLOAD_PATH = CHAT_UPLOAD_SERVER_URL + "howdoo/profilePics/";

    public static final String DELETE_DOWNLOAD = CHAT_MULTER_UPLOAD_URL + "deleteImage";

    //    public static final String HOST = "104.236.32.23";
//    public static final String HOST = "45.76.87.108";
    public static final String HOST = "45.32.156.167";
    public static final String DUBLY = "109.169.86.130";

    public static final String HOST_API = DUBLY;//"209.250.235.186";

    //    public static final String HOST = "45.77.1.74";
    public static String PORT_API_LIVE = "5005";
    public static String PORT_API_DEV = "5008";
    public static String POST_API_TRENDING = "8003";

    public static final String MQTT_PORT = "1883";
    public static final String PORT_API = PORT_API_LIVE;//Change here


    public static final String TRENDING_STICKERS = "http://api.giphy.com/v1/stickers/trending?api_key=dc6zaTOxFJmzC";
    public static final String TRENDING_GIFS = "http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC";
    public static final String GIPHY_APIKEY = "&api_key=dc6zaTOxFJmzC";
    public static final String SEARCH_STICKERS = "http://api.giphy.com/v1/stickers/search?q=";
    public static final String SEARCH_GIFS = "http://api.giphy.com/v1/gifs/search?q=";

    /*
     *AppType determines whether to search for Name/receiverImage in local contacts for receiving the calls or contacts
     *
     *Type -0
     *
     * Search Locally
     *
     *Type -1
     *
     * Use the name and Profile pic values received
     *
     */
    public static final int APP_TYPE = 0;


    /*
     * Sup specific apis
     */
    private static final String SUP_API_MAIN_LINK = "http://" + HOST_API + ":" + PORT_API + "/"; //"http://45.32.156.167:5007/";
//    private static final String SUP_API_MAIN_LINK = "http://104.236.32.23:5007/";

    public static final String REQUEST_OTP = SUP_API_MAIN_LINK + "RequestOTP";

    public static final String VERIFY_OTP = SUP_API_MAIN_LINK + "verifyOtp";

    public static final String USER_PROFILE = SUP_API_MAIN_LINK + "profile"/*"User/Profile"*/;

    /**
     * To get the opponent Profile details
     */

    public static final String OPPONENT_PROFILE = SUP_API_MAIN_LINK + "Participant/Profile/";


    public static final String USER_STATUS = SUP_API_MAIN_LINK + "User/SocialStatus";

    public static final String SYNC_CONTACTS = SUP_API_MAIN_LINK + "User/Contacts";

    public static final String FETCH_CHATS = SUP_API_MAIN_LINK + "Chats";


    /**
     * GET api to fetch the messages into the list
     */
    public static final String FETCH_MESSAGES = SUP_API_MAIN_LINK + "Messages";

    public static final String TERMS = "http://159.203.135.99/sup/Terms.html";
    //public static final String DELETE_CHAT = SUP_API_MAIN_LINK + "Messages";


    /*
     * For the call logs
     */


    public static final String CALLS_LOGS = SUP_API_MAIN_LINK + "CallLogs";

    /*
     * Group chat api
     */
    public static final String CREATE_GROUP = SUP_API_MAIN_LINK + "GroupChat";


    public static final String GROUP_MEMBER = SUP_API_MAIN_LINK + "GroupChat/Member";

    public static final String DELETE_GROUP = SUP_API_MAIN_LINK + "GroupChat";

    public static final String FETCH_GROUP_MESSAGES = SUP_API_MAIN_LINK + "GroupChat/Messages";

    /*
     *Pending api
     */
    public static final String FETCH_GROUP_ACKS = SUP_API_MAIN_LINK + "GroupChat";

    /*
     *Block user functionality
     */

    public static final String BLOCK_USER = SUP_API_MAIN_LINK + "User/Block";


}
