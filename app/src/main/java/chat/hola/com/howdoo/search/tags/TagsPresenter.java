package chat.hola.com.howdoo.search.tags;

import android.support.annotation.Nullable;
import android.util.Log;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.search.model.SearchResponse;
import chat.hola.com.howdoo.search.tags.module.TagsResponse;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by ankit on 24/2/18.
 */

public class TagsPresenter implements TagsContract.Presenter {

    @Nullable
    private TagsContract.View view;
    @Inject
    HowdooService service;


    @Inject
    TagsPresenter() {
    }

    @Override
    public void attachView(TagsContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {

    }

    @Override
    public void search(CharSequence charSequence) {
        service.searchTags(AppController.getInstance().getApiToken(), Constants.LANGUAGE, charSequence.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<TagsResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<TagsResponse> response) {
                        Log.i("RESPONSE", response.toString());
                        if (response.code() == 200 && response.body().getData() != null && response.body().getData().size() > 0)
                            view.showData(response.body().getData());
                        else if (response.code() == 204)
                            view.noData();
                        else if (response.code() == 401)
                            view.sessionExpired();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("search", "onError: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
