package chat.hola.com.howdoo.profileScreen.editProfile;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.profileScreen.addChannel.AddChannelActivity;
import chat.hola.com.howdoo.profileScreen.addChannel.AddChannelContract;
import chat.hola.com.howdoo.profileScreen.addChannel.AddChannelPresenter;
import chat.hola.com.howdoo.profileScreen.editProfile.changeEmail.ChangeEmail;
import chat.hola.com.howdoo.profileScreen.editProfile.changeEmail.ChangeEmailModule;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h>EditProfileModule</h>
 * @author 3Embed.
 * @since 22/2/18.
 */

@ActivityScoped
@Module
public interface EditProfileModule {

    @ActivityScoped
    @Binds
    EditProfileContract.Presenter presenter(EditProfilePresenter presenter);

    @ActivityScoped
    @Binds
    EditProfileContract.View view(EditProfileActivity editProfileActivity);

}
