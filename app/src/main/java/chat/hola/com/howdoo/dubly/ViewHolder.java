package chat.hola.com.howdoo.dubly;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.howdoo.dubly.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.Utilities.TypefaceManager;

/**
 * <h1>ViewHolder</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.ivThumbnail)
    ImageView ivThumbnail;
    @BindView(R.id.ibPlay)
    CheckBox ibPlay;
    @BindView(R.id.llDubWithIt)
    LinearLayout llDubWithIt;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.tvDuration)
    TextView tvDuration;
    @BindView(R.id.ivLike)
    CheckBox ivLike;
    private ClickListner clickListner;

    public ViewHolder(View itemView, TypefaceManager typefaceManager, ClickListner clickListner) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        tvTitle.setTypeface(typefaceManager.getSemiboldFont());
        tvDescription.setTypeface(typefaceManager.getMediumFont());
        tvDuration.setTypeface(typefaceManager.getRegularFont());
        this.clickListner = clickListner;
    }

    @OnClick(R.id.llDubWithIt)
    public void dubWithIt() {
        clickListner.dubWithIt(getAdapterPosition());
    }

    @OnClick({R.id.item, R.id.ibPlay})
    public void item() {
        clickListner.play(getAdapterPosition(), !ibPlay.isChecked());
    }

//    @OnClick(R.id.llContainer)
//    public void item() {
//        clickListner.play(getAdapterPosition(), ibPlay.isChecked());
//    }

    @OnClick(R.id.ivLike)
    public void like() {
        clickListner.like(getAdapterPosition(), !ibPlay.isChecked());
    }
}