package chat.hola.com.howdoo.club.member;

/**
 * <h1>ClickListner</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public interface ClickListner {
    void onUserClick(int position);

    void itemSelect(int position, boolean isSelected);

    void openPopup(String userId, boolean isBlocked);

    void requestAction(String userId, boolean accept);
}
