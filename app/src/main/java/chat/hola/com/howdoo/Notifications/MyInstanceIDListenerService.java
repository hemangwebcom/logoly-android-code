package chat.hola.com.howdoo.Notifications;

/*
 * Created by moda on 24/2/16.
 */

import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

/*
 *To automatically change and generate new token when system feels the need for it
 */


public class MyInstanceIDListenerService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        Log.i("NOTIFICATION-TOKEN", "" + FirebaseInstanceId.getInstance().getToken());
    }

}