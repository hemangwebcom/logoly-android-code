package chat.hola.com.howdoo.Utilities;

/**
 * @author 3Embed
 * @since 2/26/2018.
 */

public interface Constants {

    public static int FOREGROUND_SERVICE = 109;
    public String ACTION_START_FOURGROUND = "com.dubly.foreground";
    public String NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE = "com.dubly.service_channel_id";
    public String NOTIFICATION_CHANNEL_ID_CHAT = "com.dubly.chat_channel_id";
    public String NOTIFICATION_CHANNEL_ID_DUBLY_NOTIFICATION = "com.dubly.datum_channel_id";
    public String NOTIFICATION_CHANNEL_NAME_CHAT_SERVICE = "DUBLY CHAT";
    public String NOTIFICATION_CHANNEL_NAME_CHAT_NOTIFICATION = "DUBLY CHAT NOTIFICATION";
    public String NOTIFICATION_CHANNEL_NAME_DUBLY_NOTIFICATION = "DUBLY NOTIFICATION";


    String EMPTY = "";
    String AUTH = "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj";
    String LANGUAGE = "en";
    String TYPE = "type";
    String PATH = "path";
    String DEFAULT_PROFILE_PIC_LINK = "https://res.cloudinary.com/dqodmo1yc/image/upload/c_thumb,w_200,g_face/v1536126847/default/profile_one.png";
    String IMAGE_QUALITY = "upload/q_80/";
    String CHANNEL_IMAGE_QUALITY = "upload/w_80,h_80,c_thumb/";//,g_face,r_max/";
    String PROFILE_PIC_SHAPE = "upload/w_90,h_90,c_thumb,g_face,r_max/";
    int PAGE_SIZE = 20;

    interface SocialFragment {
        String USERID = "userId";
        String DATA = "data";
        int COLUMNS = 2;
    }

    interface Camera {
        int FILE_SIZE = 40;
    }

    interface Landing {
        String IMAGE = "image";
    }

    interface Mqtt {
        String EVENT_NAME = "eventName";
    }

    interface FollowUnfollow {
        String FOLLOWER_ID = "followerId";
    }

    interface DiscoverContact {
        String CONTACT_IDENTIFIRE = "contactIdentifier";
        String CONTACT_PICTURE = "contactPicUrl";
        String CONTACT_NAME = "contactName";
        String CONTACTS = "contacts";
        String PRIVATE = "private";
        String LOCAL_NUMBER = "localNumber";
        String USER_NAME = "userName";
        String ID = "_id";
        String NUMBER = "number";
        String PROFILE_PICTURE = "profilePic";
        String SOCIAL_STATUS = "socialStatus";
        String FOLLOWING = "followStatus";
        String PROFILE_PICTURE_URL = "contactPicUrl";
        String CONTACT_ID = "contactId";
        String TYPE = "type";
        String CONTACT_LOCAL_NUMBER = "contactLocalNumber";
        String CONTACT_LOCAL_ID = "contactLocalId";
        String CONTACT_UID = "contactUid";
        String CONTACT_TYPE = "contactType";
        String CONTACT_STATUS = "contactStatus";
        String PHONE_NUMBER = "phoneNumber";
    }

    interface Google {
        String GOOGLE_API_KEY = "AIzaSyCrwFRUNc1n86Rj2I63TZ7xpiM3boNTe_E";
    }

    interface Post {
        String PATH_FILE = "file://";
        String PATH = "path";
        String TYPE = "type";
        String IMAGE = "image";
        String VIDEO = "video";

        String RESOURCE_TYPE = "resource_type";
        String URL = "url";
        String PUBLIC_ID = "public_id";

//        String ="userId";
//        String ="title";
//        String ="categoryId";
//        String ="channelId";
//        String ="story";
//        String ="hashTags";
//        String ="latitude";
//        String ="longitude";
//        String ="location";
//        String ="countrySname";
//        String ="city";
//        String ="imageUrl1";
//        String ="imageUrl2";
//        String ="imageUrl3";
//        String ="imageUrl4";
//        String ="imageUrl5";
//        String ="thumbnailUrl1";
//        String ="thumbnailUrl2";
//        String ="thumbnailUrl3";
//        String ="thumbnailUrl4";
//        String ="thumbnailUrl5";
//        String ="hasAudio1";
//        String ="hasAudio2";
//        String ="hasAudio3";
//        String ="hasAudio4";
//        String ="hasAudio5";
//        String ="mediaType1";
//        String ="mediaType2";
//        String ="mediaType3";
//        String ="mediaType4";
//        String ="mediaType5";
//        String ="cloudinaryPublicId1";
//        String ="cloudinaryPublicId2";
//        String ="cloudinaryPublicId3";
//        String ="cloudinaryPublicId4";
//        String ="cloudinaryPublicId5";
    }
}
