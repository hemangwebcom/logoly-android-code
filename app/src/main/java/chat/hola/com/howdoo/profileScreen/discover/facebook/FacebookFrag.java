package chat.hola.com.howdoo.profileScreen.discover.facebook;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.howdoo.dubly.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.profileScreen.discover.facebook.apiModel.Data;
import chat.hola.com.howdoo.profileScreen.discover.follow.Follow;
import chat.hola.com.howdoo.profileScreen.discover.follow.FollowAdapter;

/**
 * <h>FacebookFrag.class</h>
 * <p>
 * This fragment show activities of other on your post and use the
 *
 * @author 3Embed
 * @since 02/03/18.
 */

public class FacebookFrag extends Fragment implements FacebookContract.View,
        FollowAdapter.OnFollowUnfollowClickCallback, SwipeRefreshLayout.OnRefreshListener {

    private CallbackManager callbackManager;

    @Inject
    FacebookPresenter presenter;

    @Inject
    FollowAdapter followAdapter;

    @Inject
    TypefaceManager typefaceManager;

    @BindView(R.id.tvTitleFb)
    TextView tvTitleFb;

    @BindView(R.id.tvMsgFb)
    TextView tvMsgFb;

    @BindView(R.id.btnFollowFbFend)
    Button btnFollowFbFend;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.btnFollowAll)
    Button btnFollowAll;

    @BindView(R.id.llFacebook)
    LinearLayout llFacebook;

    @BindView(R.id.rlContact)
    RelativeLayout rlContact;

    @BindView(R.id.recyclerContact)
    RecyclerView mRecyclerContact;

    @BindView(R.id.srSwipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.llHeader)
    LinearLayout llHeader;

    @BindView(R.id.llEmpty)
    LinearLayout llEmpty;

    @BindView(R.id.tvEmpty)
    TextView tvEmpty;

    @Inject
    SessionManager sessionManager;
    private AccessToken fbToken;

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    private Unbinder unbinder;
    private String prefixTitle = "";
    private List<String> contactIds = new ArrayList<>();
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Inject
    public FacebookFrag() {
    }

    public static FacebookFrag newInstance() {
        return new FacebookFrag();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefixTitle = getResources().getString(R.string.FollowFragTitle);
        //init the facebook callback manager
        callbackManager = CallbackManager.Factory.create();
    }

    private void getFBFriendsList() {
        //fbToken return from login with facebook
        GraphRequestAsyncTask r = GraphRequest.newGraphPathRequest(fbToken,
                "/me/taggable_friends", new GraphRequest.Callback() {

                    @Override
                    public void onCompleted(GraphResponse response) {
                        parseResponse(response.getJSONObject());
                    }
                }

        ).executeAsync();

    }

    private void parseResponse(JSONObject friends) {
        try {
            JSONArray friendsArray = (JSONArray) friends.get("data");
            Log.i("fiend_list", "" + friendsArray.toString());
//            if (friendsArray != null) {
//                for (int i = 0; i < friendsArray.length(); i++) {
//                    FriendItem item = new FriendItem();
//                    try {
//                        item.setUserId(friendsArray.getJSONObject(i).get(
//                                "id")
//                                + "");
//
//                        item.setUserName(friendsArray.getJSONObject(i).get(
//                                "name")
//                                + "");
//                        JSONObject picObject = new JSONObject(friendsArray
//                                .getJSONObject(i).get("picture") + "");
//                        String picURL = (String) (new JSONObject(picObject
//                                .get("data").toString())).get("url");
//                        item.setPictureURL(picURL);
//                        friendsList.add(item);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                // facebook use paging if have "next" this mean you still have friends if not start load fbFriends list
//                String next = friends.getJSONObject("paging")
//                        .getString("next");
//                if (next != null) {
//                    getFBFriendsList(next);
//                } else {
//                    loadFriendsList();
//                }
//            }
        } catch (JSONException e1) {
            loadFriendsList();
            e1.printStackTrace();
        }
    }

    private void loadFriendsList() {
//        swipeLayout.setRefreshing(false);
//        if ((friendsList != null) && (friendsList.size() > 0)) {
//            lvFriendsList.setVisibility(View.VISIBLE);
//            friendsAdapter.notifyDataSetChanged();
//        } else {
//            lvFriendsList.setVisibility(View.GONE);
//        }
    }

    private void login() {
        LoginManager.getInstance().registerCallback(callbackManager, loginResultFacebookCallback);
        LoginManager.getInstance().logInWithReadPermissions(this,
                Collections.singletonList("user_friends"));
    }

    //login result callback
    FacebookCallback<LoginResult> loginResultFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            //facebook login success
            Log.i("Facebook-Login", "onSuccess: ");
            fbToken = AccessToken.getCurrentAccessToken();
            sessionManager.setFacebookAccessToken(loginResult.getAccessToken());
            getFBFriendsList();
        }

        @Override
        public void onCancel() {
            if (llFacebook.getVisibility() == View.GONE) {
                presenter.setFbVisibility(true);
                presenter.setContactVisibility(true);
            }
            Log.i("Facebook-Login", "onCancel: ");
        }

        @Override
        public void onError(FacebookException error) {
            if (llFacebook.getVisibility() == View.GONE) {
                presenter.setFbVisibility(true);
                presenter.setContactVisibility(true);
            }
            Log.e("Facebook-Login", "onError: ", error);
            Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_facebook_tab, container, false);
        presenter.attachView(this);
        unbinder = ButterKnife.bind(this, rootView);
        presenter.init();
        swipeRefreshLayout.setOnRefreshListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.checkForFbLogin();
    }

    @OnClick(R.id.btnFollowAll)
    public void followAll() {
        presenter.followAll(contactIds);
        btnFollowAll.setEnabled(false);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(getContext(), getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void applyFont() {
        tvTitle.setTypeface(typefaceManager.getSemiboldFont());
        tvMsg.setTypeface(typefaceManager.getMediumFont());
        btnFollowFbFend.setTypeface(typefaceManager.getSemiboldFont());

        tvTitleFb.setTypeface(typefaceManager.getSemiboldFont());
        tvMsgFb.setTypeface(typefaceManager.getMediumFont());
        btnFollowAll.setTypeface(typefaceManager.getSemiboldFont());
        tvEmpty.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void initPostRecycler() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mRecyclerContact.setLayoutManager(llm);
        mRecyclerContact.setHasFixedSize(true);
        mRecyclerContact.setAdapter(followAdapter);
        followAdapter.setOnFollowUnfollowClickCallback(this);
        presenter.loadFollows();
    }

    @Override
    public void showFollows(Follow follow) {
        // tvTitle.setText(follow.getFollowersCount()+" "+tvTitle.getText());
        // followAdapter.setData(follow.getContacts());
    }

    @OnClick(R.id.btnFollowFbFend)
    public void followFbFriend() {
        if (llFacebook.getVisibility() == View.VISIBLE) {
            presenter.setFbVisibility(false);
            presenter.setContactVisibility(true);
        }
        if (!sessionManager.isFacebookLogin() || !sessionManager.isFacebookPublishPermission()) {
            //login with facebook
            login();

        } else {
            getFBFriendsList();
        }

    }


    public void setFbVisibility(boolean show) {
        if (show)
            llFacebook.setVisibility(View.VISIBLE);
        else
            llFacebook.setVisibility(View.GONE);
    }

    public void setContactVisibility(boolean show) {
        if (show)
            rlContact.setVisibility(View.VISIBLE);
        else
            rlContact.setVisibility(View.GONE);
    }

    @Override
    public void showFbContacts(ArrayList<Data> dataList) {
        if (dataList.size() > 0) {
            for (Data data : dataList)
                contactIds.add(data.getId());
            llHeader.setVisibility(View.VISIBLE);
            tvTitle.setText(dataList.size() + " " + prefixTitle);
            followAdapter.setData(dataList);
            showEmptyUi(false);
        } else {
            showEmptyUi(true);
        }

        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }

    public void showEmptyUi(boolean show) {
        if (show) {
            llEmpty.setVisibility(View.VISIBLE);
            setFbVisibility(false);
        } else {
            llEmpty.setVisibility(View.GONE);
            //setFbVisibility(true);
        }
    }

    @Override
    public void followedAll(boolean flag) {
        if (flag) {
            llHeader.setVisibility(View.GONE);
            presenter.fetchFriendList();
        } else
            btnFollowAll.setEnabled(true);
    }

    @Override
    public void showFbContactUi(boolean show) {
        if (show) {
            presenter.setFbVisibility(true);
            presenter.setContactVisibility(false);
        } else {
            presenter.setFbVisibility(false);
            presenter.setContactVisibility(true);
        }
    }

    public void isDataLoading(boolean isLoading) {
        if (swipeRefreshLayout != null) {
            if (isLoading)
                swipeRefreshLayout.setRefreshing(true);
            else
                swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();

        presenter.detachView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFollow(String followerId) {
        presenter.follow(followerId);
    }

    @Override
    public void onUnfollow(String followerId) {
        presenter.unFollow(followerId);
    }

    @Override
    public void onRefresh() {
        presenter.fbLogin();
    }

    @Override
    public void reload() {

    }
}