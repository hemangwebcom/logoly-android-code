package chat.hola.com.howdoo.dagger;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by ankit on 20/2/18.
 */

@Singleton
@Module
public abstract class AppModule {

    @Binds
    abstract Context bindContext(Application application);

}
