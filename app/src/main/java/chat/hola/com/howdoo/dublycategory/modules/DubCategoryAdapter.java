package chat.hola.com.howdoo.dublycategory.modules;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.howdoo.dubly.R;

import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.TypefaceManager;

/**
 * <h1>BlockUserAdapter</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class DubCategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {
    private List<DubCategory> dubs;
    private Context context;
    private CategoryClickListner clickListner;
    private TypefaceManager typefaceManager;

    @Inject
    public DubCategoryAdapter(List<DubCategory> dubs, Activity mContext, TypefaceManager typefaceManager) {
        this.dubs = dubs;
        this.context = mContext;
        this.typefaceManager = typefaceManager;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dub_category_item, parent, false);
        return new CategoryViewHolder(itemView, typefaceManager, clickListner);
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final DubCategory dub = dubs.get(position);
        Glide.with(context).load(dub.getImageUrl()).placeholder(R.drawable.ic_default).into(holder.ivThumbnail);
        holder.tvTitle.setText(dub.getName());
    }

    @Override
    public int getItemCount() {
        return dubs.size();
    }

    public void setListener(CategoryClickListner clickListner) {
        this.clickListner = clickListner;
    }
}