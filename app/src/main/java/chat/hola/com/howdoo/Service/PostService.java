package chat.hola.com.howdoo.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.policy.TimeWindow;
import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AACTrackImpl;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import com.howdoo.dubly.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Database.PostDb;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.models.PostObserver;
import chat.hola.com.howdoo.post.model.Post;
import chat.hola.com.howdoo.post.model.PostData;
import chat.hola.com.howdoo.post.model.UploadObserver;
import dagger.android.DaggerService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by DELL on 3/8/2018.
 */

public class PostService extends DaggerService implements UploadCallback {
    private static final String TAG = "PostService";
    private String requestId;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder1, builder2;
    private RemoteViews contentView;

    @Inject
    HowdooService service;
    @Inject
    UploadObserver uploadObserver;
    @Inject
    PostDb postDb;

    @Inject
    PostObserver postObserver;
    Post post;

    private boolean fbShare = false;
    private boolean twitterShare = false;
    private boolean instaShare = false;
    private boolean isFbShareCompleted = false;
    private boolean isTwitterShareCompleted = false;
    public static final String CHANNEL_ONE_ID = "com.howdoo.chat.TWO";
    public static final String CHANNEL_ONE_NAME = "Channel Two";
    Map<String, Object> map = new HashMap<>();

    File f;
    FFmpeg ffmpeg;
    ArrayList<String> files;
    String audioFile;
    File fileMain, fileMp4;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind: ");
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
//        Intent intent = new Intent("chat.hola.com.howdoo.Service");
//        sendBroadcast(rootIntent);
        if (notificationManager != null) {
            notificationManager.cancel(Integer.parseInt(post.getId()));//re(Integer.parseInt(post.getId()), builder2.build());
        }
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Bundle bundle = intent.getBundleExtra("data");
        loadFFMpegBinary();
        postIt();

        return START_NOT_STICKY;
    }

    private void postIt() {
        int size = postDb.getAllData().size();
        if (size > 0) {
            PostData postData = postDb.getAllData().get(0);
            String data = postData.getData(); //bundle.getString("postData");
            post = new Gson().fromJson(data, Post.class);

            files = post.getFiles();
            audioFile = post.getAudioFile();

            fbShare = postData.isFbShare();
            twitterShare = postData.isTwitterShare();
            instaShare = postData.isInstaShare();

            contentView = new RemoteViews(getPackageName(), R.layout.custom_push);
            if (post.isDub()) {
                //dub video
                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(files.get(0), MediaStore.Video.Thumbnails.MICRO_KIND);
                contentView.setImageViewBitmap(R.id.image, bMap);
                contentView.setImageViewResource(R.id.play, R.drawable.ic_play_circle_outline_black_24dp);
                contentView.setTextViewText(R.id.title, "Processing video...");
            } else if (post.getTypeForCloudinary().equals(Constants.Post.IMAGE)) {
                //image
                Bitmap bitmap = BitmapFactory.decodeFile(post.getPathForCloudinary());
                contentView.setImageViewBitmap(R.id.image, bitmap);
                contentView.setTextViewText(R.id.title, "Posting...");
            } else {
                // normal video
                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(post.getPathForCloudinary(), MediaStore.Video.Thumbnails.MICRO_KIND);
                contentView.setImageViewBitmap(R.id.image, bMap);
                contentView.setImageViewResource(R.id.play, R.drawable.ic_play_circle_outline_black_24dp);
                contentView.setTextViewText(R.id.title, "Posting...");
            }

            contentView.setProgressBar(R.id.progress, 100, 0, true);

            builder1 = new NotificationCompat.Builder(this, CHANNEL_ONE_ID)
                    .setContent(contentView)
                    .setSmallIcon(R.mipmap.ic_notification)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setAutoCancel(false).setOngoing(false);

            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder2 = new NotificationCompat.Builder(this, CHANNEL_ONE_ID)
                    .setContent(contentView)
                    .setSmallIcon(R.mipmap.ic_notification)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setAutoCancel(true).setOngoing(false)
                    .setSound(soundUri);

            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel notificationChannel;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel = new NotificationChannel(CHANNEL_ONE_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_HIGH);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setShowBadge(true);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            if (post.isDub())
                concatVideoCommand();
            else
                normalPost();

            //start notification
            if (notificationManager != null) {
                notificationManager.notify(Integer.parseInt(post.getId()), builder1.build());
            }


        } else {
            stopSelf();
        }
    }

    private void normalPost() {
//        if (data != null && !data.trim().equals("")) {
        map.put("musicId", post.getMusicId());
        map.put("hasAudio1", "1");
        map.put("story", post.isStory());
        if (post.getHashTags() != null && !TextUtils.isEmpty(post.getHashTags())) {
            map.put("hashTags", post.getHashTags());
        }

        if (post.getLatitude() != null && !TextUtils.isEmpty(post.getLatitude())) {
            map.put("latitude", Double.parseDouble(post.getLatitude()));
        }

        if (post.getLongitude() != null && !TextUtils.isEmpty(post.getLongitude())) {
            map.put("longitude", Double.parseDouble(post.getLongitude()));
        }

        if (post.getLocation() != null && !TextUtils.isEmpty(post.getLocation())) {
            map.put("location", post.getLocation());
        }

        if (post.getCountrySname() != null && !TextUtils.isEmpty(post.getCountrySname())) {
            map.put("countrySname", post.getCountrySname());
        }

        if (post.getCity() != null && !TextUtils.isEmpty(post.getCity())) {
            map.put("city", post.getCity());
        }

        if (post.getChannelId() != null && !TextUtils.isEmpty(post.getChannelId()))
            map.put("channelId", post.getChannelId());

        if (post.getCategoryId() != null && !TextUtils.isEmpty(post.getCategoryId()))
            map.put("categoryId", post.getCategoryId());

        map.put("title", post.getTitle());
        map.put("access", post.getAccess());
        map.put("publishAsHome", post.isPublishAsHome());
        map.put("clubId", post.getClubId());
        map.put("clubName", post.getClubName());
        map.put("deviceOS", "a");
        map.put("deviceName", Build.MODEL);

        //userTag
        String regexPattern1 = "(@\\w+)";
        Pattern p1 = Pattern.compile(regexPattern1);
        Matcher m1 = p1.matcher(post.getTitle());
        int i = 0;
        ArrayList<String> strings = new ArrayList<>();
        String userTag[] = new String[i + 1];
        while (m1.find()) {
            strings.add(m1.group(1).replace("@", ""));
            // userTag[i++] = m1.group(1).replace("@", "");
        }

        map.put("userTags", strings);
        try {

            String s = "[{\"streaming_profile\":\"full_hd\"},{\"format\":\"m3u8\"}]";

            requestId = MediaManager.get().upload(post.getPathForCloudinary())
                    .option("folder", "posts")
                    .option("eager_async", true)
                    .option(Constants.Post.RESOURCE_TYPE, post.getTypeForCloudinary())
                    .callback(this).constrain(TimeWindow.immediate()).dispatch();
        } catch (Exception ignored) {

        }
//        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart(String requestId) {
        if (notificationManager != null) {
            notificationManager.notify(Integer.parseInt(post.getId()), builder1.build());
        }
        Log.i("Cloudinary", "onStart: ");
    }

    @Override
    public void onProgress(String requestId, long bytes, long totalBytes) {
        Double progress = (double) bytes / totalBytes;
        Log.i("Cloudinary", "onProgress: " + progress);
    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        String type = resultData.get("resource_type").equals("video") ? "1" : "0";
        String url = String.valueOf(resultData.get("url"));
        map.put("thumbnailUrl1", url);
        map.put("imageUrl1", url);
        map.put("mediaType1", type);
        map.put("cloudinaryPublicId1", resultData.get("public_id"));
        map.put("imageUrl1Width", String.valueOf(resultData.get("width")));
        map.put("imageUrl1Height", String.valueOf(resultData.get("height")));
        map.put("fileSize", resultData.get("bytes"));
        startPost(type, url);
    }

    private void startPost(String type, String mediaUrl) {
        Log.i(TAG, "startPost: ");
        service.createPost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        Log.i(TAG, "onSuccess: 2");
                        if (responseBodyResponse.code() == 200) {
                            Log.i(TAG, "onSuccess: 3");
                            postObserver.postData(true);
                            if (notificationManager != null) {
                                contentView.setTextViewText(R.id.title, "Posted successfully");
                                contentView.setProgressBar(R.id.progress, 100, 100, false);
                                notificationManager.notify(Integer.parseInt(post.getId()), builder2.build());
                            }

                            if (postDb.delete(post.getId()))
                                onStartCommand(null, 0, 0);
                        } else {
                            stopSelf();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onSuccess: 4");
                        if (notificationManager != null) {
                            contentView.setTextViewText(R.id.title, "Failed to post..");
                            contentView.setProgressBar(R.id.progress, 100, 100, false);
                            notificationManager.notify(Integer.parseInt(post.getId()), builder2.build());
                        }

                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onSuccess: 5");
                    }
                });
    }

    @Override
    public void onError(String requestId, ErrorInfo error) {
        Log.i(TAG, "onError: ");
        if (notificationManager != null) {
            contentView.setTextViewText(R.id.title, "Failed to post..");
            contentView.setProgressBar(R.id.progress, 100, 100, false);
            notificationManager.notify(Integer.parseInt(post.getId()), builder2.build());
        }
    }

    @Override
    public void onReschedule(String requestId, ErrorInfo error) {

    }


    //video
    private void concatVideoCommand() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder filterComplex = new StringBuilder();
        filterComplex.append("-filter_complex,");

        Log.d(TAG, "concatVideoCommand: " + files.size());
        for (int i = 0; i < files.size(); i++) {
            stringBuilder.append("-i" + "," + files.get(i) + ",");
            filterComplex.append("[").append(i).append(":v").append(i).append("] [").append(i).append(":a").append(i).append("] ");

        }
        filterComplex.append("concat=n=").append(files.size()).append(":v=1:a=1 [v] [a]");
        String[] inputCommand = stringBuilder.toString().split(",");
        String[] filterCommand = filterComplex.toString().split(",");

        String filePrefix = "reverse_video";
        String fileExtn = ".mp4";
        f = new File("/storage/emulated/0/Download", System.currentTimeMillis() + ".mp4");
        int fileNo = 0;
        while (f.exists()) {
            fileNo++;
            f = new File("/storage/emulated/0/Download", System.currentTimeMillis() + "V" + ".mp4");
        }
        String filePath = f.getAbsolutePath();
        String[] destinationCommand = {"-map", "[v]", "-map", "[a]", f.getAbsolutePath()};
        execFFmpegBinary(combine(inputCommand, filterCommand, destinationCommand));
    }


    private void loadFFMpegBinary() {

        try {
            if (ffmpeg == null) {
                Log.d("ffmpeg", "ffmpeg : null");
                ffmpeg = FFmpeg.getInstance(this);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    //showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    Log.d("Success", "ffmpeg : correct Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            //showUnsupportedExceptionDialog();
        } catch (Exception e) {
            Log.d("Exception", "Exception not supported : " + e);
        }
    }


    private void
    execFFmpegBinary(final String[] command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {

                    Log.d("OnFailure", "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.d("OnProgress", "progress : " + s);

                }

                @Override
                public void onProgress(String s) {

                    //  dialog.show();
                    // Toast.makeText(CameraActivity.this,"Deleting the last segment..",Toast.LENGTH_LONG).show();
                    Log.d("OnProgress", "progress : " + s);

                }

                @Override
                public void onStart() {
                    Log.d("OnStart", "Started command : ffmpeg " + command);
                }

                @Override
                public void onFinish() {
                    //  Toast.makeText(CameraActivity.this, "Done", Toast.LENGTH_SHORT).show();
                    //  dialog.dismiss();
                    Log.d("OnProgress", "progress success : ");

                    mergeFiles(f.getAbsolutePath());

                    //Log.d("OnFinish", "Finished command : ffmpeg " + audioTrim.getAbsolutePath());


                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {

        }
    }

    public static String[] combine(String[] arg1, String[] arg2, String[] arg3) {
        String[] result = new String[arg1.length + arg2.length + arg3.length];
        System.arraycopy(arg1, 0, result, 0, arg1.length);
        System.arraycopy(arg2, 0, result, arg1.length, arg2.length);
        System.arraycopy(arg3, 0, result, arg1.length + arg2.length, arg3.length);
        return result;
    }

    private static double correctTimeToSyncSample(Track track, double cutHere, boolean next) {
        double[] timeOfSyncSamples = new double[track.getSyncSamples().length];
        long currentSample = 0;
        double currentTime = 0;
        int i = 0;
        for (double timeOfSyncSample : timeOfSyncSamples)

        {
            timeOfSyncSamples[Arrays.binarySearch(track.getSyncSamples(), currentSample + 1)] = currentTime;

        }
        currentTime += (double) timeOfSyncSamples[i] / (double) track.getTrackMetaData().getTimescale();
        currentSample++;

        double previous = 0;
        for (double timeOfSyncSample : timeOfSyncSamples)

        {
            if (timeOfSyncSample > cutHere) {
                if (next) {
                    return timeOfSyncSample;
                } else {
                    return previous;
                }
            }
            previous = timeOfSyncSample;
        }
        return timeOfSyncSamples[timeOfSyncSamples.length - 1];

    }

    private void mergeFiles(String video) {
        try {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {


                    try {
                        Movie orig_movie = MovieCreator.build(video);

                        AACTrackImpl aacTrack = new AACTrackImpl(new FileDataSourceImpl(audioFile));

                        IsoFile isoFile = new IsoFile(video);
                        double lengthInSeconds = (double)
                                isoFile.getMovieBox().getMovieHeaderBox().getDuration() /
                                isoFile.getMovieBox().getMovieHeaderBox().getTimescale();


                        Track track = (Track) orig_movie.getTracks().get(0);

                        Track audioTrack = (Track) aacTrack;


                        double startTime1 = 0;
                        double endTime1 = lengthInSeconds;

                        boolean timeCorrected = false;

                        if (audioTrack.getSyncSamples() != null && audioTrack.getSyncSamples().length > 0) {
                            if (timeCorrected) {

                                throw new RuntimeException("The startTime has already been corrected by another track with SyncSample. Not Supported.");
                            }
                            startTime1 = correctTimeToSyncSample(audioTrack, startTime1, false);
                            endTime1 = correctTimeToSyncSample(audioTrack, endTime1, true);
                            timeCorrected = true;
                        }

                        long currentSample = 0;
                        double currentTime = 0;
                        double lastTime = -1;
                        long startSample1 = -1;
                        long endSample1 = -1;


                        for (int i = 0; i < audioTrack.getSampleDurations().length; i++) {
                            long delta = audioTrack.getSampleDurations()[i];

                            if (currentTime > lastTime && currentTime <= startTime1) {
                                // current sample is still before the new starttime
                                startSample1 = currentSample;
                            }
                            if (currentTime > lastTime && currentTime <= endTime1) {
                                // current sample is after the new start time and still before the new endtime
                                endSample1 = currentSample;
                            }

                            lastTime = currentTime;
                            currentTime += (double) delta / (double) audioTrack.getTrackMetaData().getTimescale();
                            currentSample++;
                        }


                        CroppedTrack cropperAacTrack = new CroppedTrack(aacTrack, startSample1, endSample1);

                        Movie movie = new Movie();
                        movie.addTrack(track);
                        movie.addTrack(cropperAacTrack);

                        Container mp4file = new DefaultMp4Builder().build(movie);

                        fileMain = new File("/storage/emulated/0/Download", System.currentTimeMillis() + ".mp4");
                        if (!fileMain.exists())
                            fileMain.createNewFile();
                        mp4file.writeContainer(new FileOutputStream(fileMain).getChannel());
                        fileMp4 = new File("/storage/emulated/0/Download", System.currentTimeMillis() + ".mp4");


                        String[] comand = {"-y", "-i", fileMain.getAbsolutePath(), "-c", "copy", "-movflags", "+faststart", fileMp4.getAbsolutePath()};
//                    String[] comand={"ffmpeg -i",fileMain.getAbsolutePath()," -c copy -movflags +faststart "," -pix_fmt yuv420p",fileMp4.getAbsolutePath()};

                        execFFmpegBinaryFinal(comand);


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception ignored) {

                    }


                }
            }, 15000);

        } catch (Exception e) {
        }
    }


    private void
    execFFmpegBinaryFinal(final String[] command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {

                    Log.d("OnFailure", "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.d("OnProgress", "progress : " + s);

                }

                @Override
                public void onProgress(String s) {

                    //  dialog.show();
                    // Toast.makeText(CameraActivity.this,"Deleting the last segment..",Toast.LENGTH_LONG).show();
                    Log.d("OnProgress", "progress : " + s);

                }

                @Override
                public void onStart() {
                    Log.d("OnStart", "Started command : ffmpeg " + command);
                }

                @Override
                public void onFinish() {
                    //  Toast.makeText(CameraActivity.this, "Done", Toast.LENGTH_SHORT).show();
                    //  dialog.dismiss();
                    Log.d("OnProgress", "progress success : ");
                    post.setPathForCloudinary(fileMp4.getAbsolutePath());
                    post.setTypeForCloudinary(Constants.Post.VIDEO);
                    contentView.setTextViewText(R.id.title, "Posting...");
                    contentView.setProgressBar(R.id.progress, 100, 0, true);
                    notificationManager.notify(Integer.parseInt(post.getId()), builder2.build());
                    Log.i(TAG, "PATH: " + post.getPathForCloudinary());
                    normalPost();
                    //Log.d("OnFinish", "Finished command : ffmpeg " + audioTrim.getAbsolutePath());


                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {

        }
    }
}
