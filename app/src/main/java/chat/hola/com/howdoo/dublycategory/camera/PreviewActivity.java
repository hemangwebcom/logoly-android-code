package chat.hola.com.howdoo.dublycategory.camera;

import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.Surface;
import android.view.TextureView;

import com.howdoo.dubly.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.post.PostActivity;

public class PreviewActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener {

    private final int CAMERA_REQUEST = 222;
    @BindView(R.id.video)
    TextureView textureView;
    @BindView(R.id.ivNext)
    AppCompatImageView ivNext;

    private String path;
    private boolean isFrontFace = false;
    private MediaPlayer mediaPlayer;
    private String musicId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_dubly);
        ButterKnife.bind(this);
        path = getIntent().getStringExtra("path");
        musicId = getIntent().getStringExtra("musicId");
        isFrontFace = getIntent().getBooleanExtra("isFrontFace", false);
        textureView.setSurfaceTextureListener(this);
        textureView.setScaleX(isFrontFace ? -1 : 1);
        ivNext.bringToFront();
    }

    @OnClick(R.id.ivNext)
    public void next() {
        Intent intent = new Intent(this, PostActivity.class);
        intent.putExtra(Constants.Post.PATH, path);
        intent.putExtra(Constants.Post.TYPE, Constants.Post.VIDEO);
        intent.putExtra("musicId", musicId);
        startActivityForResult(intent, CAMERA_REQUEST);
        finish();
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int i, int i1) {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setSurface(new Surface(surface));
        try {
            mediaPlayer.setDataSource(path);
            mediaPlayer.setLooping(true);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }
}
