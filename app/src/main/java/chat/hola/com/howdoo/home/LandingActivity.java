package chat.hola.com.howdoo.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.security.MessageDigest;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Dialog.ImageSourcePicker;
import chat.hola.com.howdoo.ImageCropper.CropImage;
import chat.hola.com.howdoo.Service.PostService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.Utilities.CustomBottomNavigationView;
import chat.hola.com.howdoo.Utilities.ServiceTools;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.camera.CameraActivity;
import chat.hola.com.howdoo.cameraActivities.SandriosCamera;
import chat.hola.com.howdoo.cameraActivities.configuration.CameraConfiguration;
import chat.hola.com.howdoo.cameraActivities.manager.CameraOutputModel;
import chat.hola.com.howdoo.home.activity.ActivitiesFragment;
import chat.hola.com.howdoo.home.connect.ConnectFragment;
import chat.hola.com.howdoo.home.social.SocialFragment;
import chat.hola.com.howdoo.home.stories.StoriesFrag;
import chat.hola.com.howdoo.home.trending.TrendingFragment;
import chat.hola.com.howdoo.manager.session.SessionManager;
import chat.hola.com.howdoo.models.ConnectionObserver;
import chat.hola.com.howdoo.models.NetworkConnector;
import chat.hola.com.howdoo.post.PostActivity;
import chat.hola.com.howdoo.post.model.UploadObserver;
import chat.hola.com.howdoo.profileScreen.ProfileActivity;
import chat.hola.com.howdoo.search.SearchActivity;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * <h1>LandingActivity</h1>
 *
 * @author 3Embed
 * @since 21/2/18.
 */

public class LandingActivity extends DaggerAppCompatActivity implements LandingContract.View,
        SandriosCamera.CameraCallback, BottomNavigationView.OnNavigationItemSelectedListener {

    private final int CAMERA_REQUEST = 222;
    private final int READ_STORAGE_REQ_CODE = 26;
    private final int RESULT_LOAD_IMAGE = 1;
    private static final int REQUEST_CODE = 111;
    public static boolean isConnect = false;
    private Unbinder unbinder;
    private FragmentTransaction ft;

    @Inject
    LandingPresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    UploadObserver uploadObserver;
    @Inject
    SocialFragment socialFragment;
//    @Inject
//    ClubPostFragment clubPostFragment;

    @Inject
    TrendingFragment trendingFragment;
    @Inject
    StoriesFrag storiesFrag;
    @Inject
    ActivitiesFragment activitiesFragment;
    @Inject
    ConnectFragment connectFragment;
    @Inject
    SessionManager sessionManager;
    @Inject
    ImageSourcePicker imageSourcePicker;
    @Inject
    NetworkConnector networkConnector;

    @BindView(R.id.bottomNavigationView)
    CustomBottomNavigationView bottomNavigationView;
    @BindView(R.id.actionBarRl)
    RelativeLayout actionBarRl;
    @BindView(R.id.profilePicIv)
    SimpleDraweeView ivProfilePic;
    @BindView(R.id.searchIv)
    ImageView searchIv;
    @BindView(R.id.ivCamera)
    ImageView ivCamera;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    ConnectionObserver connectionObserver;
    private boolean doubleBackToExitPressedOnce = false;

    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_page);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        if (getIntent().getStringExtra("to") != null && getIntent().getStringExtra("to").equals("youFrag"))
            removeShift(bottomNavigationView, 4);
        else
            removeShift(bottomNavigationView, 0);
        presenter.profile();
        setSupportActionBar(toolbar);

        Log.d("TOKEN", AppController.getInstance().getApiToken());
        Log.d("USERID", AppController.getInstance().getUserId());
        printHashKey(this);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_feed_menu, menu);
        if (menu instanceof MenuBuilder) {
            MenuBuilder m = (MenuBuilder) menu;
            //noinspection RestrictedApi
            m.setOptionalIconsVisible(true);
        }

        return true;
    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("HASH KEY", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (Exception e) {
            Log.e("HASH KEY", "printHashKey()", e);
        }
    }

    @Override
    public void removeShift(CustomBottomNavigationView bottomNavigationView, int position) {
        bottomNavigationView.enableItemShiftingMode(false);
        bottomNavigationView.enableItemShiftingMode(false);
        bottomNavigationView.setTextVisibility(true);
        bottomNavigationView.enableShiftingMode(false);
        bottomNavigationView.enableAnimation(false);
        bottomNavigationView.setTextSize(10);
        bottomNavigationView.setTypeface(typefaceManager.getRegularFont());

        Menu menu = bottomNavigationView.getMenu();
        selectFragment(menu.getItem(position));
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @OnClick(R.id.cam)
    public void cam() {
        openCamera();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return selectFragment(item);
    }

    @Override
    public boolean selectFragment(MenuItem item) {
        item.setChecked(true);
        FragmentManager fragmentManager = getSupportFragmentManager();
        ft = fragmentManager.beginTransaction();

        return fragment(item.getItemId());
    }

    private boolean fragment(int itemId) {
        isConnect = false;
        switch (itemId) {
            case R.id.social:
                if (trendingFragment.isAdded()) {
                    ft.hide(trendingFragment);
                }
                if (storiesFrag.isAdded()) {
                    ft.hide(storiesFrag);
                }
                if (activitiesFragment.isAdded()) {
                    ft.hide(activitiesFragment);
                }
                if (connectFragment.isAdded()) {
                    ft.hide(connectFragment);
                }
//                if (clubPostFragment.isAdded()) {
//                    ft.hide(clubPostFragment);
//                }
                if (!socialFragment.isAdded()) {
                    socialFragment.setType("1");
                    pushFragment(socialFragment);
                } else {
                    socialFragment.setType("1");
                    ft.show(socialFragment);
                    ft.commit();
                }
                return true;
            case R.id.trending:
                if (socialFragment.isAdded()) {
                    ft.hide(socialFragment);
                }
                if (storiesFrag.isAdded()) {
                    ft.hide(storiesFrag);
                }
                if (activitiesFragment.isAdded()) {
                    ft.hide(activitiesFragment);
                }
                if (connectFragment.isAdded()) {
                    ft.hide(connectFragment);
                }
//                if (clubPostFragment.isAdded()) {
//                    ft.hide(clubPostFragment);
//                }
                if (!trendingFragment.isAdded()) {
                    pushFragment(trendingFragment);
                } else {
                    ft.show(trendingFragment);
                    ft.commit();
                }
                return true;
            case R.id.club:
                if (trendingFragment.isAdded()) {
                    ft.hide(trendingFragment);
                }
                if (storiesFrag.isAdded()) {
                    ft.hide(storiesFrag);
                }
                if (activitiesFragment.isAdded()) {
                    ft.hide(activitiesFragment);
                }
                if (connectFragment.isAdded()) {
                    ft.hide(connectFragment);
                }
//                if (socialFragment.isAdded()) {
//                    ft.hide(socialFragment);
//                }
                if (!socialFragment.isAdded()) {
                    pushFragment(socialFragment);
                    socialFragment.setType("2");
                } else {
                    socialFragment.setType("2");
                    ft.show(socialFragment);
                    ft.commit();
                }
                return true;

            case R.id.connect:
                if (socialFragment.isAdded()) {
                    ft.hide(socialFragment);
                }
                if (trendingFragment.isAdded()) {
                    ft.hide(trendingFragment);
                }
                if (storiesFrag.isAdded()) {
                    ft.hide(storiesFrag);
                }
                if (activitiesFragment.isAdded()) {
                    ft.hide(activitiesFragment);
                }
//                if (clubPostFragment.isAdded()) {
//                    ft.hide(clubPostFragment);
//                }
                if (!connectFragment.isAdded()) {
                    pushFragment(connectFragment);
                } else {
                    ft.show(connectFragment);
                    ft.commit();
                }
                return true;
            default:
                return false;
        }

    }

    @OnClick(R.id.ivNotification)
    public void notifications() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ft = fragmentManager.beginTransaction();
        if (socialFragment.isAdded()) {
            ft.hide(socialFragment);
        }
        if (trendingFragment.isAdded()) {
            ft.hide(trendingFragment);
        }
        if (storiesFrag.isAdded()) {
            ft.hide(storiesFrag);
        }
        if (connectFragment.isAdded()) {
            ft.hide(connectFragment);
        }
//        if (clubPostFragment.isAdded()) {
//            ft.hide(clubPostFragment);
//        }
        if (!activitiesFragment.isAdded()) {
            pushFragment(activitiesFragment);
        } else {
            ft.show(activitiesFragment);
            ft.commit();
        }
    }

    @Override
    public void pushFragment(Fragment fragment) {
        ft.replace(R.id.frameLayout, fragment);
        ft.commit();
    }

    @Override
    public void hideActionBar() {
        actionBarRl.setVisibility(View.GONE);
    }

    @Override
    public void visibleActionBar() {
        actionBarRl.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(presenter);

        if (!ServiceTools.isServiceRunning(this.getApplicationContext(), PostService.class)) {
            startService(new Intent(this, PostService.class));
        }

        connectionObserver.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<NetworkConnector>() {
                    @Override
                    public void onNext(NetworkConnector networkConnector) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

//        //network checker
//        Toast.makeText(this, networkConnector.isConnected() ? "Connected" : "Not connected", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @OnClick(R.id.ivCamera)
    public void camera() {
        openCamera();
    }

    private void openCamera() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            startActivity(new Intent(LandingActivity.this, CameraActivity.class).putExtra("call", "post"));
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }


    ImageSourcePicker.OnSelectImageSource callback = new ImageSourcePicker.OnSelectImageSource() {
        @Override
        public void onCamera() {
            launchCamera();
        }

        @Override
        public void onGallary() {
            checkReadImage();
        }

        @Override
        public void onCancel() {
        }
    };

    @Override
    public void launchImagePicker(Intent intent) {
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    private void launchCamera() {

        SandriosCamera.with(this).setShowPicker(true)
                .setVideoFileSize(Constants.Camera.FILE_SIZE)
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                .enableImageCropping(true)
                .launchCamera(this);
    }

    private void checkReadImage() {
        if (ActivityCompat.checkSelfPermission(LandingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LandingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            presenter.launchImagePicker();
        } else {
            requestReadImagePermission();
        }
    }

    private void requestReadImagePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(LandingActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(LandingActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            Snackbar snackbar = Snackbar.make(root, R.string.string_222,
                    Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityCompat.requestPermissions(LandingActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            READ_STORAGE_REQ_CODE);
                }
            });
            snackbar.show();
            View view = snackbar.getView();
            ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                    .setGravity(Gravity.CENTER_HORIZONTAL);
        } else {
            ActivityCompat.requestPermissions(LandingActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    READ_STORAGE_REQ_CODE);
        }
    }

    @Override
    public void showSnackMsg(int msgId) {
        String msg = getResources().getString(msgId);
        Snackbar snackbar = Snackbar.make(root, "" + msg,
                Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        ((TextView) view.findViewById(android.support.design.R.id.snackbar_text))
                .setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void launchCropImage(Uri data) {
        CropImage.activity(data).start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            presenter.instagramShare(data.getStringExtra(Constants.TYPE), data.getStringExtra(Constants.PATH));
        } else if (requestCode == RESULT_LOAD_IMAGE) {
            presenter.parseMedia(requestCode, resultCode, data);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            presenter.parseCropedImage(requestCode, resultCode, data);
        } else if (requestCode == 123) {
            connectFragment.selectTab(1);
            removeShift(bottomNavigationView, 4);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_STORAGE_REQ_CODE) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(LandingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    presenter.launchImagePicker();
                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            } else {
                //Access storage permission denied
                showSnackMsg(R.string.string_1006);
            }
        } else if (requestCode == REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(this, CameraActivity.class));
            }
        }
    }

    @OnClick(R.id.searchIv)
    public void search() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.profilePicIv)
    public void profilePic() {
        startActivity(new Intent(LandingActivity.this, ProfileActivity.class));

    }

    @Override
    public void onBackPressed() {
//        if (isConnect) {
//            removeShift(bottomNavigationView, 4);
//        } else
        if (socialFragment.isVisible()) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                actionBarRl.setVisibility(View.VISIBLE);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            removeShift(bottomNavigationView, 0);
        }
    }

    @Override
    public void showMessage(String msg, int msgId) {
        Toast.makeText(this, msg != null && !msg.isEmpty() ? msg : getResources().getString(msgId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete(CameraOutputModel model) {
        Intent intent = new Intent(getApplicationContext(), PostActivity.class);
        intent.putExtra(Constants.Post.PATH, model.getPath());
        intent.putExtra(Constants.Post.TYPE, model.getType() == 0 ? Constants.Post.IMAGE : Constants.Post.VIDEO);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @Override
    public void launchPostActivity(CameraOutputModel model) {
        onComplete(model);
    }

    @Override
    public void intenetStatusChanged(boolean isConnected) {
        //showSnack(isConnected);
    }

    @Override
    public void profilepic(String profilePic) {
        sessionManager.setUserProfilePic(profilePic.replace("upload/", Constants.PROFILE_PIC_SHAPE));
        ivProfilePic.setImageURI(sessionManager.getUserProfilePic());
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        Snackbar snackbar = Snackbar.make(findViewById(R.id.root), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }


    @Override
    public void reload() {

    }
}
