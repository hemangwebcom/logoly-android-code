package chat.hola.com.howdoo.dublycategory;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.howdoo.dubly.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import chat.hola.com.howdoo.Dialog.BlockDialog;
import chat.hola.com.howdoo.Utilities.MyExceptionHandler;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.dubly.DubsActivity;
import chat.hola.com.howdoo.dublycategory.modules.SimpleFragmentPagerAdapter;
import chat.hola.com.howdoo.dublycategory.modules.DubCategoryAdapter;
import chat.hola.com.howdoo.manager.session.SessionManager;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>DubCategoryActivity</h1>
 * <p>All the Dubs appears on this screen.
 * User can add new Dubs also</p>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */

public class DubCategoryActivity extends DaggerAppCompatActivity implements DubCategoryContract.View {
    public static int page = 0;
    private Unbinder unbinder;

    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    DubCategoryContract.Presenter presenter;
    @Inject
    DubCategoryAdapter categoryAdapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvCategories)
    RecyclerView rvCategories;
//    @BindView(R.id.tvTbTitle)
//    TextView tvTbTitle;

    @BindView(R.id.sliding_tabs)
    TabLayout sliding_tabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @Inject
    BlockDialog dialog;

    @Override
    public void userBlocked() {
        dialog.show();
    }

    private LinearLayoutManager layoutManager;
    private GridLayoutManager gridLayoutManager;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dub_activity);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        layoutManager = new LinearLayoutManager(this);
        gridLayoutManager = new GridLayoutManager(this, 4);

       // tvTbTitle.setTypeface(typefaceManager.getSemiboldFont());

        rvCategories.setLayoutManager(gridLayoutManager);
        categoryAdapter.setListener(presenter.getCategoryPresenter());
        rvCategories.setAdapter(categoryAdapter);
        presenter.getCategories(false);

        toolbarSetup();


        // Create an adapter that knows which fragment should be shown on each page
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        sliding_tabs.setupWithViewPager(viewpager);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                adapter.setPosition(position);
                viewpager.getParent().requestDisallowInterceptTouchEvent(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewpager.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });
    }


    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {
        if (sessionManager != null)
            sessionManager.sessionExpired(this);
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    public void reload() {
    }


    @Override
    public void getList(String categoryId, String name) {
        startActivity(new Intent(this, DubsActivity.class).putExtra("categoryId", categoryId).putExtra("categoryName", name));
    }
}
