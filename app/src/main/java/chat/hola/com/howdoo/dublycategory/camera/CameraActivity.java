package chat.hola.com.howdoo.dublycategory.camera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AACTrackImpl;
import com.howdoo.dubly.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CameraActivity extends AppCompatActivity implements CameraKitEventListener, CameraControls.VideoListner {

    @BindView(R.id.camera)
    CameraView cameraView;
    @BindView(R.id.controls)
    CameraControls controls;
    @BindView(R.id.audioName)
    TextView audioName;
    private MediaPlayer player;
    private String audioFile = "";
    private String audio_name = "";
    private String musicId = "";
    private boolean isFrontFace = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_dubly);
        ButterKnife.bind(this);
        audioFile = getIntent().getStringExtra("audio");
        audio_name = getIntent().getStringExtra("name");
        musicId = getIntent().getStringExtra("musicId");
        cameraView.setMethod(CameraKit.Constants.METHOD_STILL);
        cameraView.setCropOutput(true);
        cameraView.addCameraKitListener(this);
        setMicMuted(true);
        controls.setListner(this);
        audioName.setText(audio_name.replace(".aac", ""));
        audioName.setSelected(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        permission();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        player.stop();
        super.onPause();
    }

    private void setMicMuted(boolean state) {
        AudioManager myAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        // get the working mode and keep it
        int workingAudioMode = myAudioManager.getMode();
        myAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);

        // change mic state only if needed
        if (myAudioManager.isMicrophoneMute() != state) {
            myAudioManager.setMicrophoneMute(state);
        }

        // set back the original working mode
        myAudioManager.setMode(workingAudioMode);
    }

    private void videoCaptured(CameraKitVideo video) {
        File videoFile = video.getVideoFile();
        mergeFiles(videoFile.getAbsolutePath());
    }

    private void permission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            player = MediaPlayer.create(CameraActivity.this, Uri.parse(audioFile));
                            player.setLooping(false);
                            cameraView.start();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            Toast.makeText(CameraActivity.this, "Permission required please grant it from app settings", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void mergeFiles(String video) {
        try {
            //video - mp4 format
            Movie m = MovieCreator.build(video);
            List nuTracks = new ArrayList<>();
            for (Track t : m.getTracks()) {
                if (!"sound".equals(t.getHandler()))
                    nuTracks.add(t);
            }

            //audio - AAC format
            Track nuAudio = new AACTrackImpl(new FileDataSourceImpl(audioFile));
            nuTracks.add(nuAudio); // add the new audio track
            m.setTracks(nuTracks);

            //result video - mp4 format
            Container c = new DefaultMp4Builder().build(m);
            File f = new File("/storage/emulated/0/Download", System.currentTimeMillis() + ".mp4");
            if (!f.exists())
                f.createNewFile();
            c.writeContainer(new FileOutputStream(f).getChannel());
            player.stop();
            startActivity(new Intent(this, PreviewActivity.class).putExtra("musicId",musicId).putExtra("path", f.getAbsolutePath()).putExtra("isFrontFace", isFrontFace));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //camera listner methods
    @Override
    public void onEvent(CameraKitEvent cameraKitEvent) {

    }

    @Override
    public void onError(CameraKitError cameraKitError) {

    }

    @Override
    public void onImage(CameraKitImage cameraKitImage) {

    }

    @Override
    public void onVideo(CameraKitVideo cameraKitVideo) {
        videoCaptured(cameraKitVideo);
    }

    @Override
    public void video(boolean flag) {
        if (flag)
            player.start();
        else
            player.stop();
    }

    @Override
    public void face(boolean isFace) {
        this.isFrontFace = isFace;
    }
}
