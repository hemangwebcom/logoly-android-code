package chat.hola.com.howdoo.home.social;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.home.model.Data;

/**
 * <h1>ClubPostContract</h1>
 *
 * @author 3Embed
 * @since 24/2/2018.
 */

public interface SocialContract {

    interface View extends BaseView {

        void isLoading(boolean flag);

        void onItemClick(android.view.View view, Data data, int position);

        void onUserClick(String userId);

        void showEmptyUi(boolean show);

        void onChannelClick(String channelId);

        void clearRecyclerView();
    }

    interface Presenter extends BasePresenter<View> {
        void callSocialApi(int offset, int limit, boolean load);

        void callApiOnScroll(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount);

        void prefetchImage(int position);

        SocialPresenter getPresenter();

        void postObserver();
    }
}
