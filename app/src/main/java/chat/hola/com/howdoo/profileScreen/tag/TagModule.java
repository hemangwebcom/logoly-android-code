package chat.hola.com.howdoo.profileScreen.tag;

import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.profileScreen.story.StoryContract;
import chat.hola.com.howdoo.profileScreen.story.StoryFragment;
import chat.hola.com.howdoo.profileScreen.story.StoryPresenter;
import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 23/2/18.
 */

@FragmentScoped
@Module
public interface TagModule {

    @FragmentScoped
    @Binds
    TagContract.Presenter presenter(TagPresenter presenter);

   /* @FragmentScoped
    @Binds
    TagContract.View view(TagFragment fragment);*/
}
