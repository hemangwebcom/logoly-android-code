package chat.hola.com.howdoo.club.clubs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.howdoo.dubly.R;

import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.TypefaceManager;

/**
 * <h1>ClubAdapter</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class ClubAdapter extends RecyclerView.Adapter<ViewHolder> {
    private TypefaceManager typefaceManager;
    private ClickListner clickListner;
    private List<Club> dataList;
    private Context context;
    private int lastCheckedPosition = -1;
    private boolean isForSelection = false;

    @Inject
    public ClubAdapter(List<Club> dataList, Activity mContext, TypefaceManager typefaceManager) {
        this.typefaceManager = typefaceManager;
        this.dataList = dataList;
        this.context = mContext;
    }

    public void isForSelection(boolean b) {
        isForSelection = b;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.club_item, parent, false);
        return new ViewHolder(itemView, typefaceManager, clickListner);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (getItemCount() > 0) {
            final Club data = dataList.get(position);
            holder.tvName.setText(data.getSubject());
            if (data.getImage().isEmpty())
                holder.ivProfilePic.setImageResource(R.drawable.ic_club);
            else
                holder.ivProfilePic.setImageURI(data.getImage());

            if (isForSelection) {
                holder.rbSelect.setVisibility(View.VISIBLE);
                holder.btnLeave.setVisibility(View.GONE);
            } else {
                holder.rbSelect.setVisibility(View.GONE);
                holder.btnLeave.setVisibility(View.VISIBLE);
                if (data.getOwner()) holder.btnLeave.setText("Delete");
                else holder.btnLeave.setText("Leave");
            }
            holder.rbSelect.setChecked(position == lastCheckedPosition);

            holder.rbSelect.setOnClickListener(v -> {
                lastCheckedPosition = position;
                clickListner.itemSelect(position);
                notifyDataSetChanged();
            });

            holder.btnLeave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (data.getOwner())
                        clickListner.delete(position);
                    else
                        clickListner.leave(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }
}