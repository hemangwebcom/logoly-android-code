package chat.hola.com.howdoo.home.activity.youTab.followrequest;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.model.ChannelRequesterAdapter;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.model.DataList;
import dagger.Module;
import dagger.Provides;

/**
 * <h1>CommentUtilModule</h1>
 *
 * @author 3Embed
 * @version 1.0.
 * @since 4/9/2018.
 */
@ActivityScoped
@Module
public class FollowRequestUtilModule {

    @ActivityScoped
    @Provides
    List<ReuestData> getFollowingRequested() {
        return new ArrayList<ReuestData>();
    }

    @ActivityScoped
    @Provides
    FollowRequestAdapter channelRequesterAdapter(Activity activity, List<ReuestData> list, TypefaceManager typefaceManager) {
        return new FollowRequestAdapter(activity, list, typefaceManager);
    }

}
