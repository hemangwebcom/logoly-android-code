package chat.hola.com.howdoo.preview;

import chat.hola.com.howdoo.Utilities.CustomVideoSurfaceView;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.socialDetail.SocialDetailActivity;
import dagger.Module;
import dagger.Provides;

/**
 * <h1>PreviewUtilModule</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 23/3/18.
 */

@ActivityScoped
@Module
public class PreviewUtilModule {

    @ActivityScoped
    @Provides
    CustomVideoSurfaceView customVideoSurfaceView(PreviewActivity activity) {
        return new CustomVideoSurfaceView(activity);
    }
}
