package chat.hola.com.howdoo.search.people;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.search.otherSearch.OtherSearchContract;
import chat.hola.com.howdoo.search.otherSearch.OtherSearchFrag;
import chat.hola.com.howdoo.search.otherSearch.OtherSearchPresenter;
import dagger.Binds;
import dagger.Module;
import github.ankushsachdeva.emojicon.emoji.People;

/**
 * Created by ankit on 24/2/18.
 */

@FragmentScoped
@Module
public interface PeopleModule {

    @FragmentScoped
    @Binds
    PeopleContract.Presenter presenter(PeoplePresenter presenter);

   /* @FragmentScoped
    @Binds
    ChannelContract.View view(ChannelFragment frag);
*/
}
