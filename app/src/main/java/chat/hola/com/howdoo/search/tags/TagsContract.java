package chat.hola.com.howdoo.search.tags;

import java.util.List;

import chat.hola.com.howdoo.Utilities.BasePresenter;
import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.search.tags.module.Tags;

/**
 * Created by ankit on 24/2/18.
 */

public interface TagsContract {

    interface View extends BaseView {

        void showData(List<Tags> data);

        void noData();
    }

    interface Presenter extends BasePresenter<TagsContract.View> {
        void search(CharSequence charSequence);

    }
}
