package chat.hola.com.howdoo.club.clubs;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import chat.hola.com.howdoo.manager.session.SessionManager;

/**
 * <h1>CommentModel</h1>
 *
 * @author 3Embed
 * @since 4/10/2018.
 */

class ClubModel {

    @Inject
    List<Club> dataList;
    @Inject
    ClubAdapter adapter;
    @Inject
    SessionManager sessionManager;
    private boolean forSelect = false;

    @Inject
    ClubModel() {
    }

    public void setData(boolean isFirst, boolean isMine, int access, List<Club> data) {
        if (data != null) {

            if (isFirst)
                this.dataList.clear();

            if (forSelect) {
                List<Club> filteredClub = new ArrayList<>();
                switch (access) {
                    case 1:
                        //public
                        for (Club club : data) {
                            if (club.getAccess() == 1 || club.getAccess() == 3)
                                filteredClub.add(club);
                        }
                        break;
                    case 2:
                        //private
                        for (Club club : data) {
                            if (club.getAccess() == 2)
                                filteredClub.add(club);
                        }
                        break;
                    case 3:
                        //exclusive
                        for (Club club : data) {
                            if (club.getAccess() == 3)
                                filteredClub.add(club);
                        }
                        break;
                }

                this.dataList.addAll(filteredClub);
            } else if (isMine) {
                this.dataList.addAll(data);
            }
            adapter.notifyDataSetChanged();
        }
    }

    public void clearList() {
        this.dataList.clear();
    }

    public String getUserId(int position) {
        return dataList.get(position).getId();
    }

    public void selectItem(int position, boolean isSelected) {
        dataList.get(position).setSelected(isSelected);
    }

    public ArrayList<String> getSelectedUsers() {
        ArrayList<String> selectedUsers = new ArrayList<>();
        for (Club data : dataList) {
            if (data.isSelected())
                selectedUsers.add(data.getId());
        }
        return selectedUsers;
    }

    public String getChannelId(int position) {
        return dataList.get(position).getId();
    }

    public void leave(int position) {
        dataList.remove(position);
        adapter.notifyDataSetChanged();
    }

    public String getClubId(int position) {
        return dataList.get(position).getId();
    }


    public String getClubName(int position) {
        return dataList.get(position).getSubject();
    }

    public void setForSelect(boolean forSelect) {
        this.forSelect = forSelect;
    }
}
