package chat.hola.com.howdoo.profileScreen.editProfile;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.profileScreen.editProfile.model.EditProfileBody;
import chat.hola.com.howdoo.profileScreen.model.Data;

/**
 * <h>EditProfileContract</h>
 * <p>
 * @author 3Embed.
 * @since 22/2/18.
 */

public interface EditProfileContract {

    interface View extends BaseView{
        void applyFont();

        void finishActivity(boolean success);

        void showProgress(boolean b);

        void setEmail(String email);

        void launchCamera(Intent intent);

        void showSnackMsg(int msgId);

        void launchImagePicker(Intent intent);

        void launchCropImage(Uri data);

        void setProfileImage(Bitmap bitmap);
    }

    interface Presenter{

        void init();

        void initUpdateProfile(EditProfileBody editProfileBody,Data profileData);

        void launchCamera(PackageManager packageManager);

        void launchImagePicker();

        void parseSelectedImage(int requestCode, int resultCode, Intent data);

        void parseCapturedImage(int requestCode, int resultCode, Intent data);

        void parseCropedImage(int requestCode, int resultCode, Intent data);
    }
}
