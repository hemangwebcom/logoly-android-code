package chat.hola.com.howdoo.dagger;

import com.howdoo.dubly.BuildConfig;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import chat.hola.com.howdoo.Networking.DublyService;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Networking.HowdooServiceTrending;
import chat.hola.com.howdoo.Utilities.ApiOnServer;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ankit on 21/2/18.
 */

@Singleton
@Module
public class NetworkModule {

    private File file = new File("cache_file");
    private Cache cache = new Cache(file, 50);


    @Provides
    @Singleton
    @Named("howdoo")
    Retrofit retrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .pingInterval(2000, TimeUnit.MILLISECONDS)
                .readTimeout(15000, TimeUnit.MILLISECONDS)
                .connectTimeout(15000, TimeUnit.MILLISECONDS)
                .cache(cache)
                .build();

        return new Retrofit.Builder()
                .baseUrl("http://" + ApiOnServer.DUBLY + ":" + ApiOnServer.PORT_API)
//                .baseUrl("http://" + ApiOnServer.HOST_API + ":" + ApiOnServer.PORT_API)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    @Provides
    @Singleton
    @Named("trending")
    Retrofit retrofit1() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .pingInterval(2000, TimeUnit.MILLISECONDS)
                .readTimeout(15000, TimeUnit.MILLISECONDS)
                .connectTimeout(15000, TimeUnit.MILLISECONDS)
                .cache(cache)
                .build();

        return new Retrofit.Builder()
                .baseUrl("http://" + ApiOnServer.HOST_API + ":" + ApiOnServer.POST_API_TRENDING)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    @Named("dubly")
    Retrofit retrofit2() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .pingInterval(2000, TimeUnit.MILLISECONDS)
                .readTimeout(15000, TimeUnit.MILLISECONDS)
                .connectTimeout(15000, TimeUnit.MILLISECONDS)
                .cache(cache)
                .build();

        return new Retrofit.Builder()
                .baseUrl("http://" + ApiOnServer.DUBLY + ":" + ApiOnServer.PORT_API)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    HowdooService howdooService(@Named("howdoo") Retrofit retrofit) {
        return retrofit.create(HowdooService.class);
    }

    @Provides
    @Singleton
    HowdooServiceTrending howdooServiceTrending(@Named("trending") Retrofit retrofit) {
        return retrofit.create(HowdooServiceTrending.class);
    }

    @Provides
    @Singleton
    DublyService dublyService(@Named("dubly") Retrofit retrofit) {
        return retrofit.create(DublyService.class);
    }
}
