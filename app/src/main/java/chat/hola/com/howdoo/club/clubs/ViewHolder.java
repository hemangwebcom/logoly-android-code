package chat.hola.com.howdoo.club.clubs;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.howdoo.dubly.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.howdoo.Utilities.TypefaceManager;

/**
 * <h1>ViewHolder</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.ivProfilePic)
    SimpleDraweeView ivProfilePic;
    @BindView(R.id.rbSelect)
    RadioButton rbSelect;
    @BindView(R.id.btnLeave)
    Button btnLeave;
    @BindView(R.id.rlItem)
    RelativeLayout rlItem;
    private ClickListner clickListner;

    public ViewHolder(View itemView, TypefaceManager typefaceManager, ClickListner clickListner) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        tvName.setTypeface(typefaceManager.getSemiboldFont());

        this.clickListner = clickListner;
    }

    @OnClick(R.id.rlItem)
    public void profile() {
        clickListner.onChannelClick(getAdapterPosition());
    }

//    @OnClick(R.id.btnLeave)
//    public void leave() {
//        clickListner.leave(getAdapterPosition());
//    }

}