package chat.hola.com.howdoo.settings;

import chat.hola.com.howdoo.Utilities.BaseView;

/**
 * <h1>SettingsContract</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 24/2/18.
 */

public interface SettingsContract {

    interface View extends BaseView {

        /**
         * <p>to apply fonts</p>
         */
        void applyFont();
    }

    interface Presenter {
        /**
         * <p>to initialize</p>
         */
        void init();
    }

}
