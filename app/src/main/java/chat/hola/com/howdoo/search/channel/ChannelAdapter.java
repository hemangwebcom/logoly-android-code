package chat.hola.com.howdoo.search.channel;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.howdoo.dubly.R;

import java.util.List;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.club.clubs.Club;
import chat.hola.com.howdoo.search.channel.module.Channels;

import static com.howdoo.dubly.R.*;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.ViewHolder> {
    private AppController appController;
    private Typeface fontMedium;
    private List<Club> searchData;
    private Context context;
    private ChannelAdapter.ClickListner clickListner;

    public void setData(Context context, List<Club> data) {
        this.context = context;
        this.searchData = data;
        notifyDataSetChanged();
    }

    @Override
    public ChannelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(layout.search_channel_row, parent, false);
        appController = AppController.getInstance();
        fontMedium = appController.getMediumFont();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChannelAdapter.ViewHolder holder, final int position) {
        if (getItemCount() != -1) {
            Club data = searchData.get(position);

            Glide.with(context).load(data.getImage()).asBitmap().centerCrop().into(holder.profileIv);
            holder.profileNameTv.setText(data.getSubject());
            holder.tvSubscriber.setText(data.getMemberCount() + " Members");

            int buttonTextColor;
            int buttonDrawable;
            int buttonText;

            int access = data.getAccess();

            if (data.getAdmin() || data.getOwner()) {
                buttonTextColor = color.colorPrimary;
                buttonDrawable = drawable.btn_selector;
                buttonText = string.edit;
                holder.btnAction.setVisibility(View.GONE);
            } else if (data.getMember()) {
                buttonTextColor = color.pink;
                buttonDrawable = drawable.btn_selector_pink;
                buttonText = string.leave;
                holder.btnAction.setVisibility(View.VISIBLE);
            } else {
                holder.btnAction.setVisibility(View.VISIBLE);
                if (access == 1) {
                    buttonTextColor = color.green1;
                    buttonDrawable = drawable.btn_selector_green;
                    buttonText = string.join;
                } else {
                    buttonTextColor = color.colorPrimaryDark;
                    buttonDrawable = drawable.btn_selector_blue;
                    buttonText = data.isRequested() ? string.asked : string.ask;
                }
            }
            holder.btnAction.setText(buttonText);
            holder.btnAction.setTextColor(context.getResources().getColor(buttonTextColor));
            holder.btnAction.setBackground(context.getResources().getDrawable(buttonDrawable));

            holder.btnAction.setOnClickListener(view -> {
                if (data.getAdmin() || data.getOwner()) {
                    clickListner.edit(position);
                } else if (data.getMember()) {
                    clickListner.leave(position);
                    if (access == 1) {
                        holder.btnAction.setText(string.join);
                        holder.btnAction.setTextColor(context.getResources().getColor(color.green1));
                        holder.btnAction.setBackground(context.getResources().getDrawable(drawable.btn_selector_green));
                    } else {
                        holder.btnAction.setText(!data.isRequested() ? string.asked : string.ask);
                        holder.btnAction.setTextColor(context.getResources().getColor(color.colorPrimaryDark));
                        holder.btnAction.setBackground(context.getResources().getDrawable(drawable.btn_selector_blue));
                    }
                } else {
                    clickListner.join(position);
                    if (access != 1) {
                        holder.btnAction.setText(!data.isRequested() ? string.asked : string.ask);
                    }
                }
            });
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (data.getAccess() == 1 || data.getAdmin() || data.getOwner() || data.getMember()) {
                        clickListner.onItemClick(position);
                    }
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return searchData != null ? searchData.size() : -1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView profileIv;
        private TextView profileNameTv, tvSubscriber;
        private RelativeLayout relativeLayout;
        private Button btnAction;

        public ViewHolder(View itemView) {
            super(itemView);
            profileNameTv = (TextView) itemView.findViewById(id.profileNameTv);
            profileIv = (ImageView) itemView.findViewById(id.profileIv);
            relativeLayout = (RelativeLayout) itemView.findViewById(id.rlItem);
            tvSubscriber = (TextView) itemView.findViewById(id.tvName);
            btnAction = (Button) itemView.findViewById(id.btnAction);
            profileNameTv.setTypeface(fontMedium);
            btnAction.setTypeface(fontMedium);

        }
    }

    public void setListener(ChannelAdapter.ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public interface ClickListner {
        void onItemClick(int position);

        void join(int position);

        void edit(int position);

        void leave(int position);
    }
}
