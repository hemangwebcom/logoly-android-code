package chat.hola.com.howdoo.home.stories.model;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.howdoo.dubly.R;

import java.util.List;

import javax.inject.Inject;

import chat.hola.com.howdoo.Utilities.TimeAgo;
import chat.hola.com.howdoo.Utilities.TypefaceManager;

/**
 * <h1>StoriesAdapter</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/26/2018
 */

public class StoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private TypefaceManager typefaceManager;
    private List<StoryData> arrayList;
    private ClickListner clickListner;

    @Inject
    public StoriesAdapter(List<StoryData> arrayList, TypefaceManager typefaceManager) {
        this.arrayList = arrayList;
        this.typefaceManager = typefaceManager;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.stories_header_row, parent, false);
            return new HeaderViewHolder(itemView, typefaceManager);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.stories_row, parent, false);
            return new ViewHolder(itemView, typefaceManager, clickListner);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mainHolder, @SuppressLint("RecyclerView") int position) {
        if (getItemViewType(position) == 0) {
            HeaderViewHolder holder = (HeaderViewHolder) mainHolder;
            holder.tvHeader.setText(arrayList.get(position).getHeaderTitle());
        } else {
            try {
                ViewHolder holder = (ViewHolder) mainHolder;
                final StoryData storiesList = arrayList.get(position);
                holder.profileNameTv.setText(storiesList.getUserName());
                holder.timeTv.setText(storiesList.getPosts().get(position - 1).getTimestamp());
                holder.profilePicIv.setImageURI(storiesList.getPosts().get(storiesList.getPosts().size() - 1).getThumbnail().replace(".mp4", ".jpg"));
                holder.timeTv.setText(TimeAgo.getTimeAgo(Long.parseLong(storiesList.getPosts().get(storiesList.getPosts().size() - 1).getTimestamp())));
            } catch (Exception ignored) {

            }
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return arrayList.get(position).isHeader() ? 0 : 1;
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

}
