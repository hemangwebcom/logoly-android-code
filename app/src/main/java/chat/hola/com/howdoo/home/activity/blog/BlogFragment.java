package chat.hola.com.howdoo.home.activity.blog;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.howdoo.dubly.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;

/**
 * <h1>BlogFragment</h1>
 * <p>Logo.ly requirement</p>
 *
 * @author DELL
 * @version 1.0
 * @since 10/11/2018.
 */
public class BlogFragment extends Fragment {
    EditText emailBox, passwordBox;
    Button loginButton,registerButon;
    TextView registerLink;
    String URL = "";
    String resu="";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_blog, container, false);
        emailBox = (EditText)rootView.findViewById(R.id.w_username);
        passwordBox = (EditText)rootView.findViewById(R.id.w_password);
        loginButton = (Button)rootView.findViewById(R.id.w_login);
        registerButon = (Button) rootView.findViewById(R.id.w_signup);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (emailBox.getText().toString().equals("")) {
                    Toast.makeText(getContext(), "Enter Username", Toast.LENGTH_LONG).show();
                } else if (passwordBox.getText().toString().equals("")) {
                    Toast.makeText(getContext(), "Enter Password", Toast.LENGTH_LONG).show();
                } else {
                    URL = "https://www.selfun.co.uk/api.aspx?user_id=" + emailBox.getText().toString() + "&password=" + passwordBox.getText().toString() + "&flag=login";
//                Toast.makeText(MainActivity.this, URL, Toast.LENGTH_LONG).show();
                    StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
//                        JSONObject myResponse;
                            try {
//                            Toast.makeText(Login.this, s.length(), Toast.LENGTH_LONG).show();
//                             myResponse = new JSONObject(s);
                                if (s.equals("[]")) {
                                    Toast.makeText(getContext(), "Incorrect Details", Toast.LENGTH_LONG).show();
                                } else {
                                    JSONArray mJsonArray = new JSONArray(s);
                                    JSONObject mJsonObject = mJsonArray.getJSONObject(0);
                                    resu = mJsonObject.getString("User_ID");
//                            Toast.makeText(Login.this,mJsonObject.length(), Toast.LENGTH_LONG).show();

//                            if(resu.equals(emailBox.getText().toString())){
//                                Toast.makeText(Login.this, "Login Successful", Toast.LENGTH_LONG).show();
                                    startActivity(new Intent(getContext(), TestActivity.class));
//                            }
//                            else{
//                                Toast.makeText(Login.this, "Incorrect Details", Toast.LENGTH_LONG).show();
//                            }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getContext(), "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> parameters = new HashMap<String, String>();
//                        parameters.put("flag","login");
                            parameters.put("user_id", emailBox.getText().toString());
                            parameters.put("password", passwordBox.getText().toString());
                            return parameters;
                        }
                    };

                    RequestQueue rQueue = Volley.newRequestQueue(getContext());
                    rQueue.add(request);
                }
            }
        });

        registerButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(Register.this, MainActivity.class));
            }
        });
        return rootView;
    }
}
