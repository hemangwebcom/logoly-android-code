package chat.hola.com.howdoo.search;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.search.channel.ChannelFragment;
import chat.hola.com.howdoo.search.channel.ChannelModule;
import chat.hola.com.howdoo.search.otherSearch.OtherSearchFrag;
import chat.hola.com.howdoo.search.otherSearch.OtherSearchModule;
import chat.hola.com.howdoo.search.people.PeopleFragment;
import chat.hola.com.howdoo.search.people.PeopleModule;
import chat.hola.com.howdoo.search.tags.TagsFragment;
import chat.hola.com.howdoo.search.tags.TagsModule;
import chat.hola.com.howdoo.search.tags.TagsUtilModule;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ankit on 24/2/18.
 */

@ActivityScoped
@Module
public interface SearchModule {


    @FragmentScoped
    @ContributesAndroidInjector(modules = PeopleModule.class)
    PeopleFragment peopleFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {TagsModule.class, TagsUtilModule.class})
    TagsFragment tagsFragment();


    @FragmentScoped
    @ContributesAndroidInjector(modules = {ChannelModule.class})
    ChannelFragment channelFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = OtherSearchModule.class)
    OtherSearchFrag otherSearchFrag();

    @ActivityScoped
    @Binds
    SearchContract.Presenter searchPresenter(SearchPresenter presenter);

    @ActivityScoped
    @Binds
    SearchContract.View view(SearchActivity activity);

}
