package chat.hola.com.howdoo.models;

import javax.inject.Singleton;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 6/4/2018.
 */
public class NetworkConnector {

    private boolean isConnected;

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }
}
