package chat.hola.com.howdoo.profileScreen.tag;

import android.support.annotation.Nullable;

import javax.inject.Inject;

/**
 * Created by ankit on 23/2/18.
 */

public class TagPresenter implements TagContract.Presenter {

    @Nullable
    TagContract.View view;

    @Inject
    TagPresenter(){
    }

    @Override
    public void attachView(TagContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void init() {
        if(view != null)
        view.setupRecyclerView();
    }

    @Override
    public void loadTagImages() {
//        ArrayList<Integer> trendingImages = new ArrayList<>();
//        trendingImages.add(R.drawable.laye672);
//        trendingImages.add(R.drawable.layer23);
//        trendingImages.add(R.drawable.layer650);
//        trendingImages.add(R.drawable.trending_image_sample);
//        trendingImages.add(R.drawable.profile_one);
//        trendingImages.add(R.drawable.profile_two);
//        trendingImages.add(R.drawable.profile_three);
//        trendingImages.add(R.drawable.profile_four);
//        trendingImages.add(R.drawable.profile_five);
//        trendingImages.add(R.drawable.profile_six);
//        Log.w("TAG", "view"+view);
//        if(view != null)
//            view.showTagImages(trendingImages);
    }
}
