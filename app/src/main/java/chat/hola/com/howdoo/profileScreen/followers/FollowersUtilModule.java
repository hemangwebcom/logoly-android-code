package chat.hola.com.howdoo.profileScreen.followers;

import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.profileScreen.discover.contact.ContactAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ankit on 20/3/18.
 */

@ActivityScoped
@Module
public class FollowersUtilModule {

    @ActivityScoped
    @Provides
    FollowerAdapter followerAdapter(FollowersActivity activity, TypefaceManager typefaceManager){
        return new FollowerAdapter(activity,typefaceManager);
    }
}
