package chat.hola.com.howdoo.dubly;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 7/16/2018.
 */

public class Dub implements Serializable {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("musicCategory_id")
    @Expose
    private String musicCategoryId;
    @SerializedName("musicCategoryName")
    @Expose
    private String musicCategoryName;
    @SerializedName("musicCategoryImageUrl")
    @Expose
    private String musicCategoryImageUrl;
    @SerializedName("isFavourite")
    @Expose
    private Integer myFavourite = 0;
    @SerializedName("totalVideos")
    @Expose
    private Integer totalVideos = 0;
    private boolean isPlaying;

    public Dub(String id, String name, String duration, String path, String imageUrl) {
        this.name = name;
        this.id = id;
        this.duration = duration;
        this.path = path;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public Integer isMyFavourite() {
        return myFavourite;
    }

    public void setMyFavourite(Integer myFavourite) {
        this.myFavourite = myFavourite;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public String getMusicCategoryId() {
        return musicCategoryId;
    }

    public void setMusicCategoryId(String musicCategoryId) {
        this.musicCategoryId = musicCategoryId;
    }

    public String getMusicCategoryName() {
        return musicCategoryName;
    }

    public void setMusicCategoryName(String musicCategoryName) {
        this.musicCategoryName = musicCategoryName;
    }

    public String getMusicCategoryImageUrl() {
        return musicCategoryImageUrl;
    }

    public void setMusicCategoryImageUrl(String musicCategoryImageUrl) {
        this.musicCategoryImageUrl = musicCategoryImageUrl;
    }

    public Integer getTotalVideos() {
        return totalVideos;
    }

    public void setTotalVideos(Integer totalVideos) {
        this.totalVideos = totalVideos;
    }
}
