package chat.hola.com.howdoo.home.trending.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>TrendingResponse</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 6/18/2018.
 */

public class TrendingResponse implements Serializable {
    @SerializedName("mesage")
    @Expose
    private String mesage;
    @SerializedName("data")
    @Expose
    private ArrayList<Header> data = null;


    public String getMesage() {
        return mesage;
    }

    public void setMesage(String mesage) {
        this.mesage = mesage;
    }

    public ArrayList<Header> getData() {
        return data;
    }

    public void setData(ArrayList<Header> data) {
        this.data = data;
    }

}
