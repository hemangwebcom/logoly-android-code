package chat.hola.com.howdoo.profileScreen;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import java.util.ArrayList;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.cameraActivities.manager.CameraOutputModel;
import chat.hola.com.howdoo.profileScreen.model.Profile;

/**
 * Created by ankit on 22/2/18.
 */

public interface ProfileContract {

    interface View extends BaseView {
        void applyFont();

        void setupViewPager();

        void isLoading(boolean flag);

        void showProfileData(Profile profile);

        void isFollowing(boolean flag);


        void showSnackMsg(int msgId);

        void addToReportList(ArrayList<String> data);

        void addToBlockList(ArrayList<String> data);

        void block(boolean block);

        void launchCamera(Intent intent);

        void launchImagePicker(Intent intent);

        void launchCropImage(Uri data);

        void setImage(Bitmap bitmap);
    }

    interface Presenter {
        void init();

        void loadProfileData();

        void loadMemberData(String userId);

        void follow(String followingId);

        void unfollow(String followingId);
    }
}
