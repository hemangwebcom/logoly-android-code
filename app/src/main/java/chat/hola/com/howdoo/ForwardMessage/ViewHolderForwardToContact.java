package chat.hola.com.howdoo.ForwardMessage;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.howdoo.dubly.R;

import chat.hola.com.howdoo.AppController;

/**
 * Created by moda on 30/08/17.
 */

public class ViewHolderForwardToContact extends RecyclerView.ViewHolder {


    public TextView contactName, contactStatus;

    public ImageView contactImage, contactSelected;


    public ViewHolderForwardToContact(View view) {
        super(view);

        contactStatus = (TextView) view.findViewById(R.id.contactStatus);


        contactName = (TextView) view.findViewById(R.id.contactName);

        contactImage = (ImageView) view.findViewById(R.id.storeImage2);
        contactSelected = (ImageView) view.findViewById(R.id.contactSelected);

        Typeface tf = AppController.getInstance().getRegularFont();

        contactName.setTypeface(tf, Typeface.NORMAL);
        contactStatus.setTypeface(tf, Typeface.NORMAL);

    }
}
