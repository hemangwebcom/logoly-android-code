package chat.hola.com.howdoo.dublycategory.favourite;

import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.howdoo.AppController;
import chat.hola.com.howdoo.Networking.DublyService;
import chat.hola.com.howdoo.Networking.HowdooService;
import chat.hola.com.howdoo.Utilities.Constants;
import chat.hola.com.howdoo.dubly.DubResponse;
import chat.hola.com.howdoo.dublycategory.modules.ClickListner;
import chat.hola.com.howdoo.post.ReportReason;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by ankit on 23/2/18.
 */

public class DubFavFragmentPresenter implements DubFavFragmentContract.Presenter, ClickListner {
    static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public static int page = 0;
    @Nullable
    DubFavFragmentContract.View view;
    @Inject
    DublyService service;
    @Inject
    HowdooService howdooService;
    @Inject
    DubFavFragmentModel model;
    private int downloadedSize = 0;

    @Inject
    DubFavFragmentPresenter() {
    }

    public void setView(DubFavFragmentContract.View view) {
        this.view = view;
    }

    public void callApiOnScroll(int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                page++;
                loadData(PAGE_SIZE * page, PAGE_SIZE);
            }
        }
    }

    @Override
    public void loadData(int offset, int limit) {
        isLoading = true;
        service.getFavouriteDubs(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<DubResponse>>() {
                    @Override
                    public void onNext(Response<DubResponse> response) {
                        Log.i("MUSIC-FAVLIST", "onNext: " + response.toString());

                        try {
                            if (response.code() == 200) {
                                isLastPage = response.body().getDubs().size() < PAGE_SIZE;
                                if (offset == 0)
                                    model.clearList();
                                model.setData(response.body().getDubs());
                            } else if (response.code() == 401) {
                                view.sessionExpired();
                            }
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("MUSIC-FAVLIST", "onNext: " + e.toString());

                    }

                    @Override
                    public void onComplete() {
                        isLoading = false;
                        view.isLoading(false);
                    }
                });
    }

    @Override
    public void startedDownload() {

    }

    @Override
    public void attachView(DubFavFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    public void getData(int position) {
//        model.getData(position);
    }

    @Override
    public void play(int position, boolean isPlaying) {
        model.setPlaying(position, isPlaying);
        view.play(model.getAudio(position), isPlaying);
    }

    @Override
    public void dubWithIt(int position) {
        download(position);
    }

    @Override
    public void download(int position) {
        view.startedDownload();
        new Thread(() -> downloadFile(position)).start();
    }

    @Override
    public void like(int position, boolean flag) {
        Map<String, Object> map = new HashMap<>();
        map.put("musicId", model.getMusicId(position));
        map.put("isFavourite", flag);
        howdooService.favourite(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ReportReason>>() {
                    @Override
                    public void onNext(Response<ReportReason> reportReasonResponse) {
                        if (reportReasonResponse.code() == 200)
                            model.setFavourite(position, flag);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public ClickListner getPresenter() {
        return this;
    }

    private void downloadFile(int position) {

        try {
            String filePath = model.getPath(position);
            URL url = new URL(model.getPath(position));
            InputStream is = url.openStream();

            DataInputStream dis = new DataInputStream(is);

            byte[] buffer = new byte[1024];
            int length;

            String filename = filePath.substring(filePath.lastIndexOf('/') + 1);
            FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/" + filename));

            while ((length = dis.read(buffer)) > 0) {
                fos.write(buffer, 0, length);
                downloadedSize += length;
                // update the progressbar //
                view.progress(downloadedSize);
            }
            //close the output stream when complete //
            fos.close();
            view.finishedDownload(Environment.getExternalStorageDirectory() + "/" + filename, filename, model.getMusicId(position));
        } catch (final MalformedURLException e) {
            showError("Error : MalformedURLException " + e);
            e.printStackTrace();
        } catch (final IOException e) {
            showError("Error : IOException " + e);
            e.printStackTrace();
        } catch (final Exception e) {
            showError("Error : Please check your internet connection " + e);
        }
    }

    String showError(final String err) {
        return err;
    }
}
