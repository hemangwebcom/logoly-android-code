package chat.hola.com.howdoo.home;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import chat.hola.com.howdoo.Utilities.BaseView;
import chat.hola.com.howdoo.Utilities.CustomBottomNavigationView;
import chat.hola.com.howdoo.cameraActivities.manager.CameraOutputModel;

/**
 * <h1>LandingContract</h1>
 *
 * @author 3Embed
 * @since 21/2/18.
 */

public interface LandingContract {

    interface View extends BaseView {
        void removeShift(CustomBottomNavigationView view, int position);

        boolean selectFragment(MenuItem item);

        void pushFragment(Fragment fragment);

        void hideActionBar();

        void visibleActionBar();

        void launchImagePicker(Intent data);

        void launchCropImage(Uri data);

        void showSnackMsg(int msgId);

        void launchPostActivity(CameraOutputModel model);

        void intenetStatusChanged(boolean isConnected);

        void profilepic(String profilePic);
    }

    interface Presenter {
        void instagramShare(String type, String path);

        LandingActivity getActivity();

        void parseMedia(int requestCode, int resultCode, Intent data);

        void parseSelectedImage(Uri uri, String picturePath);

        void parseCropedImage(int requestCode, int resultCode, Intent data);

        void launchImagePicker();
    }
}
