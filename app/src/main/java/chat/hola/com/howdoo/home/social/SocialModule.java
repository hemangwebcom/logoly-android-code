package chat.hola.com.howdoo.home.social;

import chat.hola.com.howdoo.dagger.FragmentScoped;
import chat.hola.com.howdoo.home.social.model.SocialModel;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>ClubPostModule</h1>
 *
 * @author 3Embed
 * @since 4/5/2018.
 */

@FragmentScoped
@Module
public interface SocialModule {

    @FragmentScoped
    @Binds
    SocialContract.Presenter socialPresenter(SocialPresenter presenter);

    @FragmentScoped
    @Binds
    SocialModel socialModel(SocialModel model);
}
