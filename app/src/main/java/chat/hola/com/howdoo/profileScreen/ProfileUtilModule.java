package chat.hola.com.howdoo.profileScreen;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import com.howdoo.dubly.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import chat.hola.com.howdoo.Dialog.ImageSourcePicker;
import chat.hola.com.howdoo.Utilities.TypefaceManager;
import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.home.model.ContentAdapter;
import chat.hola.com.howdoo.profileScreen.channel.ChannelAdapter;
import chat.hola.com.howdoo.profileScreen.story.StoryFragment;
import chat.hola.com.howdoo.profileScreen.story.model.StoryData;
import chat.hola.com.howdoo.profileScreen.story.model.TrendingDtlAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ankit on 31/3/18.
 */

@ActivityScoped
@Module
public class ProfileUtilModule {

    @ActivityScoped
    @Provides
    List<StoryData> storyData() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    TrendingDtlAdapter trendingDtlAdapter(Context activity, List<StoryData> storyData) {
        return new TrendingDtlAdapter(activity, storyData);
    }


    @ActivityScoped
    @Provides
    ChannelAdapter provideChannelAdapter(ProfileActivity activity, TypefaceManager typefaceManager) {
        return new ChannelAdapter(activity, typefaceManager);
    }

    @ActivityScoped
    @Provides
    ContentAdapter provideContentAdapter(ProfileActivity activity, TypefaceManager typefaceManager) {
        return new ContentAdapter(activity, typefaceManager);
    }

    @ActivityScoped
    @Provides
    ImageSourcePicker imageSourcePicker(ProfileActivity activity) {
        return new ImageSourcePicker(activity, true);
    }

    @ActivityScoped
    @Provides
    AlertDialog.Builder builder(ProfileActivity activity) {
        return new AlertDialog.Builder(activity);
    }

    @ActivityScoped
    @Provides
    ArrayAdapter<String> reportReasons(ProfileActivity activity) {
        return new ArrayAdapter<String>(activity, R.layout.custom_select_dialog_singlechoice);
    }

    @ActivityScoped
    @Provides
    StoryFragment storyFragment() {
        return new StoryFragment();
    }
}
