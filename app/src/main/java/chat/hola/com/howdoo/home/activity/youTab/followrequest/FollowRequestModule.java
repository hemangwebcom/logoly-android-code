package chat.hola.com.howdoo.home.activity.youTab.followrequest;

import android.app.Activity;

import chat.hola.com.howdoo.dagger.ActivityScoped;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.ChannelRequestersActivity;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.ChannelRequestersContract;
import chat.hola.com.howdoo.home.activity.youTab.channelrequesters.ChannelRequestersPresenter;
import dagger.Binds;
import dagger.Module;

/**
 * Created by DELL on 4/9/2018.
 */

@ActivityScoped
@Module
public interface FollowRequestModule {
    @ActivityScoped
    @Binds
    FollowRequestContract.Presenter presenter(FollowRequestPresenter presenter);

    @ActivityScoped
    @Binds
    FollowRequestContract.View view(FollowRequestActivity activity);

    @ActivityScoped
    @Binds
    Activity activity(FollowRequestActivity activity);

}
