package com.facebookmanager.com;
/**
 * <h2>FacebookUserDetails</h2>
 * <P>
 *     Getting the user details to collect the user details.
 * </P>
 * @since  12/14/2017.
 * @author 3Embed.
 * @version 1.0.
 */
public class FacebookUserDetails
{
    /*
   * For error handling for the app.*/
    private String message;
    private boolean isError;
    /*
    * Collecting the user details*/
    private String id;
    private String name;
    private String firstName;
    private String lastName;
    private String userPic;
    private String birthDate;
    private String bio;
    private String email;
    private String otherPic;
    private String education;

    String getMessage()
    {
        return message;
    }

    void setMessage(String message)
    {
        this.message = message;
    }

    boolean isError() {
        return isError;
    }

    void setError(boolean error) {
        isError = error;
    }

    public String getId() {
        return id;
    }

    void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }
    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getUserPic() {
        return userPic;
    }
    void setUserPic(String userPic) {
        this.userPic = userPic;
    }
    public String getBirthDate() {
        return birthDate;
    }
    void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
    public String getBio() {
        return bio;
    }
    void setBio(String bio) {
        this.bio = bio;
    }
    public String getEmail() {
        return email;
    }
    void setEmail(String email) {
        this.email = email;
    }
    public String getOtherPic() {
        return otherPic;
    }
    void setOtherPic(String otherPic) {
        this.otherPic = otherPic;
    }

    @Override
    public String toString() {
        return "FacebookUserDetails{" +
                "message='" + message + '\'' +
                ", isError=" + isError +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userPic='" + userPic + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", bio='" + bio + '\'' +
                ", email='" + email + '\'' +
                ", otherPic='" + otherPic + '\'' +
                '}';
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }
}
