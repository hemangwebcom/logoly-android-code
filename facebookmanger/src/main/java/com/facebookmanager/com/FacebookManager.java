package com.facebookmanager.com;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * <h>FacebookManager</h>
 * <p>
 * Class contain a async task to get the data from facebook .
 * Contains a method to do facebook login .
 * Here doing facebook login and taking data as user_friends .
 * </P>
 *
 * @author 3Embed
 * @since 4/02/2016
 */
public class FacebookManager
{
    private static final String TAG = FacebookManager.class.getSimpleName();
    private boolean isReady = false;
    private FacebookSessionManager facebook_session_manager;
    private Facebook_callback callback = null;
    private FacebookQueryManager fbQueryManager;
    private CallbackManager callbackManager;

    public FacebookManager(Context context, String AppID)
    {
        FacebookSdk.sdkInitialize(context, new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                isReady = true;
            }
        });
        FacebookSdk.setApplicationId(AppID);
        callbackManager = CallbackManager.Factory.create();
        facebook_session_manager = new FacebookSessionManager(context);
        fbQueryManager = FacebookFactory.geInstance();
    }
    /**
     * <h2>faceBook_Login</h2>
     * <p>
     * Facebook login data from user.
     * </P>
     */
    public void faceBook_Login(Activity activity, final Facebook_callback facebook_callback) {
        callback = facebook_callback;
        Refresh_Token();
       // LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("email", "user_birthday", "user_status"));
        LoginManager.getInstance().logInWithPublishPermissions(activity,Arrays.asList("publish_actions"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                /*
                 * Storing all the data in session manger .*/
                AccessToken accessToken = loginResult.getAccessToken();
                //Log.w(TAG, "fb_token: "+accessToken.getToken());
                store_Acess_token_details(accessToken);
//                List<String> permissions = new ArrayList<>();
//                permissions.add("publish_actions");
//                ArrayList<String> grantedPermission =  verify_Permission(permissions);
//                Log.w(TAG, "grantedPermission size: " +grantedPermission.size());
//                for(int i= 0; i< grantedPermission.size() ;i++ )
//                    Log.w(TAG, "grantedPermission: "+grantedPermission.get(0) );

                /*
                 * Giving the call back for success logged in.*/
                if (callback != null)
                    callback.success(facebook_session_manager.getUserId());
            }

            @Override
            public void onCancel() {
                if (callback != null)
                    callback.cancel("User canceled !");
            }

            @Override
            public void onError(FacebookException error) {
                if (callback != null)
                    callback.error("Got some error!");
            }
        });
    }



    /**
     * <h2>get_FB_Friends</h2>
     * <p>
     * Fb login status.
     * </P>
     *
     //* @param callbackmanager   call back managers.
     * @param facebook_callback user interface manager.
     */
    public void get_FB_Friends(Activity mactivity, Facebook_callback facebook_callback) {
        callback = facebook_callback;
        if (isLoggedIn()) {
            fb_ReadPermission( mactivity, new String[]{"user_friends"});
        } else {
            first_d_login(mactivity, new String[]{"user_friends"}, true);
        }
    }

    /**
     * <h2>fbLogin_status_for_permission</h2>
     * <p>
     * Fb login status.
     * </P>
     *
     * @param callbackmanager   call back managers.
     * @param facebook_callback user interface manager.
     * @param permission_array  checking for the  permission result.
     */
    public void ask_PublishPermission(CallbackManager callbackmanager, Activity mactivity, String permission_array[], final Facebook_callback facebook_callback) {
        callback = facebook_callback;
        if (isLoggedIn()) {
            fb_PublishPermission(callbackmanager, mactivity, permission_array);
        } else {
            first_d_login(mactivity, permission_array, false);
        }
    }

    /**
     * <h2>first_d_login</h2>
     * <p>
     * Fb login status.
     * </P>
     *
     //* @param callbackmanager  call back managers.
     * @param permission_array checking for the  permission result.
     */
    private void first_d_login( final Activity mactivity, final String permission_array[], final boolean isRedPermission) {
        Refresh_Token();
        LoginManager.getInstance().logInWithReadPermissions(mactivity, Collections.singletonList("public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                store_Acess_token_details(accessToken);
                if (isRedPermission) {
                    fb_ReadPermission( mactivity, permission_array);
                } else {
                    fb_PublishPermission(callbackManager, mactivity, permission_array);
                }
            }

            @Override
            public void onCancel() {
                if (callback != null)
                    callback.error("User canceled!");
            }

            @Override
            public void onError(FacebookException error) {
//                Log.d("user2", "Canceled data details");
                if (callback != null)
                    callback.error("Got some error!");
            }
        });
    }

    /**
     * <h2>fb_PublishPermission</h2>
     * <p>
     * <p>
     * </P>
     */
    public void fb_ReadPermission( Activity mactivity, String permission_array[]) {
        if (verify_Permission(Arrays.asList(permission_array)).size() == 0) {
            if (callback != null)
                callback.success(facebook_session_manager.getUserId());
            return;
        }
        LoginManager.getInstance().logInWithReadPermissions(mactivity, Arrays.asList(permission_array));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                /*
                 * Giving the call back for sucess logged in.*/
                if (callback != null)
                    callback.success(facebook_session_manager.getUserId());
            }

            @Override
            public void onCancel() {
                if (callback != null)
                    callback.cancel("User canceled !");
            }

            @Override
            public void onError(FacebookException error) {
                if (callback != null)
                    callback.error(""+error.getMessage());
            }
        });
    }

    /**
     * <h2>fb_PublishPermission</h2>
     * <p>
     * <p>
     * </P>
     */
    private void fb_PublishPermission(CallbackManager callbackmanager, Activity mactivity, String permission_array[]) {
        if (verify_Permission(Arrays.asList(permission_array)).size() == 0) {
            if (callback != null)
                callback.success(facebook_session_manager.getUserId());
            return;
        }
        LoginManager.getInstance().logInWithPublishPermissions(mactivity, Arrays.asList(permission_array));
        LoginManager.getInstance().registerCallback(callbackmanager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                /*
                 * Giving the call back for sucess logged in.*/
                store_Acess_token_details(loginResult.getAccessToken());
                if (callback != null)
                    callback.success(facebook_session_manager.getUserId());
            }

            @Override
            public void onCancel() {
                if (callback != null)
                    callback.cancel("User canceled !");
            }

            @Override
            public void onError(FacebookException error) {
                if (callback != null)
                    callback.error("Got some error!");
            }
        });
    }

    /**
     * <h2>verify_Permission</h2>
     * <p>
     * Checking the permission of the data.
     * </P>
     *
     * @param permissions contains the token error.
     */
    private ArrayList<String> verify_Permission(List<String> permissions) {
        ArrayList<String> temp = new ArrayList<>();
        Set<String> granted_list = AccessToken.getCurrentAccessToken().getPermissions();
        for (String check_test : permissions) {
            boolean isFound = false;
            for (String permissioon : granted_list) {
                if (check_test.equals(permissioon)) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                temp.add(check_test);
            }
        }
        return temp;
    }

    public boolean isPublishPermissionGranted(){
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isFound = false;
        if(accessToken != null){
            Set<String> grantedPermission = accessToken.getPermissions();
            for(String permission: grantedPermission){
                if(permission.equals("publish_actions"))
                    isFound = true;
            }
        }
        return isFound;
    }

    public boolean isUserFriendPermissionGranted(){
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isFound = false;
        if(accessToken != null){
            Set<String> grantedPermission = accessToken.getPermissions();
            for(String permission: grantedPermission){
                if(permission.equals("user_friends"))
                    isFound = true;
            }
        }
        return isFound;
    }

    /*
    * Storing the facebook token and other details.*/
    private void store_Acess_token_details(AccessToken accessToken) {
        facebook_session_manager.setFacebookToken(accessToken.getToken());
        facebook_session_manager.setAcessToken(accessToken.getToken());
        facebook_session_manager.setApplicationId(accessToken.getApplicationId());
        facebook_session_manager.setUserId(accessToken.getUserId());
        facebook_session_manager.setPermission(accessToken.getPermissions());
        facebook_session_manager.setDeclinePermission(accessToken.getDeclinedPermissions());

        switch (accessToken.getSource()) {
            case NONE: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.None);
                break;
            }
            case FACEBOOK_APPLICATION_WEB: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.Facebook_app_web);
                break;
            }
            case FACEBOOK_APPLICATION_NATIVE: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.Facebook_app_native);
                break;
            }
            case FACEBOOK_APPLICATION_SERVICE: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.Facebook_app_server);
                break;
            }
            case WEB_VIEW: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.web_view);
                break;
            }
            case TEST_USER: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.test_user);
                break;
            }
            case CLIENT_TOKEN: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.client_token);
                break;
            }
        }
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        facebook_session_manager.setExpTime(formatter.format(accessToken.getExpires()));
        facebook_session_manager.setLastRefTime(formatter.format(accessToken.getLastRefresh()));
    }

    /**
     * <h>Refresh_Token</h>
     * <p>
     * Calback interface of facebook.
     * </P>
     */
    public void Refresh_Token() {
        if (isReady) {
            LoginManager.getInstance().logOut();
            /*
             * Refreshing the acess token of the Facebook*/
            AccessToken.refreshCurrentAccessTokenAsync();
        }
    }

    /**
     * Handling the facebook onActivity result for getting the
     * result.
     */
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager == null) {
            throw new IllegalStateException("Error : Initialize the facebook manager first");
        }
        return callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /*
    * Checking the user is already logged in or not.*/
    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @SuppressLint("StaticFieldLeak")
    public RxJava2FBObservable collectUserDetails() {
        RxJava2FBObservable observable = RxJava2FBObservable.getInstance();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ArrayList<String> permission_list = new ArrayList<>();
                permission_list.add("email");
                permission_list.add("user_birthday");
                permission_list.add("user_status");
                permission_list.add("education");
                permission_list.add("work");
                permission_list.add("interested_in");
                permission_list.add("religion");
                fbQueryManager.getUserDetails(permission_list);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return observable;
    }


    /**
     * <h>Facebook_callback</h>
     * <p>
     * Calback interface of facebook.
     * </P>
     */
    public interface Facebook_callback {
        void success(String id);

        void error(String error);

        void cancel(String cancel);
    }

}
