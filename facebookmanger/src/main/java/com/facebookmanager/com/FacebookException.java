package com.facebookmanager.com;

/**
 * @since 12/14/2017.
 */
class FacebookException extends Exception {
    private String message;

    FacebookException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
